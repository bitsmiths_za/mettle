#******************************************************************************#
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
#******************************************************************************#
#  Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
#******************************************************************************#

function mlib_raise
{
  ###
  # 'Raises' an error calling exit.
  #
  # $1 string : the file the command was run from.
  # $2 int    : the line number the command was run from.
  # $3 string : the error message
  # $4 int    : the error code, default - 1
  # $5 string : pipe to logfile instead of stdout/stderr, default no
  ###
  local rc="$4"

  if [ "$rc" = "" ] || [ "$rc" = "0" ]
  then
    rc=1
  fi

  if [ "$5" != "" ] && [ -f "$5" ]
  then
    echo "" >> $5
    echo " ----------------------------------------" >> $5
    echo " EXCEPTION RAISED!" >> $5
    echo "    - message     : $3" >> $5
    echo "    - return code : $rc" >> $5
    echo "    - source file : $1" >> $5
    echo "    - line number : $2" >> $5
    echo " ----------------------------------------" >> $5
    echo "" >> $5
  fi

  echo "" >&2
  echo " ----------------------------------------" >&2
  echo " EXCEPTION RAISED!" >&2
  echo "    - message     : $3" >&2
  echo "    - return code : $rc" >&2
  echo "    - source file : $1" >&2
  echo "    - line number : $2" >&2
  echo " ----------------------------------------" >&2
  echo "" >&2

  exit $rc
}

function mlib_exec
{
  ###
  # Executes a command and raises a 'mlib_raise' error if ot fails
  #
  # $1 string : the file the command was run from.
  # $2 int    : the line number the command was run from.
  # $3 string : the command
  # $4 string : pipe to logfile instead of stdout/stderr, default no
  ###
  if [ "$4" != "" ] && [ -f "$4" ]; then
    echo "[cmd: ${3}]" >> $4
    eval "$3" >&2 >>$4
  else
    eval "$3"
  fi

  local rc=$?

  if [ $rc -eq 0 ]
  then
    return
  fi

  if [ "$4" != "" ] && [ -f "$4" ]
  then
    echo "" >> $4
    echo " ----------------------------------------" >> $4
    echo " ERROR EXECUTING COMMAND!" >> $4
    echo "    - command     : $3" >> $4
    echo "    - return code : $rc" >> $4
    echo "    - source file : $1" >> $4
    echo "    - line number : $2" >> $4
    echo " ----------------------------------------" >> $4
    echo "" >> $4
  fi

  echo "" >&2
  echo " ----------------------------------------" >&2
  echo " ERROR EXECUTING COMMAND!" >&2
  echo "    - command     : $3" >&2
  echo "    - return code : $rc" >&2
  echo "    - source file : $1" >&2
  echo "    - line number : $2" >&2
  echo " ----------------------------------------" >&2
  echo "" >&2

  exit 1
}

function mlib_log()
{
  ###
  # Log a message to the bs logger
  #
  # $1 string : message
  ###
  if [ "$MLIB_LOGNAME" = "" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Logger not initialized."
  fi

  echo `date '+%Y%m%d%H%M%S'` " $1" >> $MLIB_LOGNAME
}

function mlib_log_debug()
{
  ###
  # Log a debug message
  #
  # $1 string : message
  ###
  if [ "$MLIB_LOGNAME" = "" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Logger not initialized."
  fi

  if [ $MLIB_LOGLEVEL -ge 10 ]; then
    return
  fi

  echo `date '+%Y%m%d%H%M%S'` "DEBUG $1" >> $MLIB_LOGNAME
}

function mlib_log_info()
{
  ###
  # Log an info message
  #
  # $1 string : message
  ###
  if [ "$MLIB_LOGNAME" = "" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Logger not initialized."
  fi

  if [ $MLIB_LOGLEVEL -ge 20 ]; then
    return
  fi

  echo `date '+%Y%m%d%H%M%S'` "INFO $1" >> $MLIB_LOGNAME
}

function mlib_eclo()
{
  ###
  # Echo's and logs a all args the screen and MLIB_LOGNAME logger.
  #
  # $* strings : All args to be printed using log info
  ##

  echo "$*"

  if [ "$MLIB_LOGNAME" != "" ]; then
    mlib_log_info "$*"
  fi
}

function mlib_log_warning()
{
  # Log a warning message
  #
  # $1 string : message
  if [ "$MLIB_LOGNAME" = "" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Logger not initialized."
  fi

  if [ $MLIB_LOGLEVEL -ge 30 ]; then
    return
  fi

  echo `date '+%Y%m%d%H%M%S'` "WARNING $1" >> $MLIB_LOGNAME
}

function mlib_log_error()
{
  ###
  # Log an error message
  #
  # $1 string : message
  ###
  if [ "$MLIB_LOGNAME" = "" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Logger not initialized."
  fi

  if [ $MLIB_LOGLEVEL -ge 40 ]; then
    return
  fi

  echo `date '+%Y%m%d%H%M%S'` "ERROR $1" >> $MLIB_LOGNAME
}

function mlib_logger_init()
{
  ###
  # Initialize the mlib logger
  #
  # $1 string : logname
  # $2 string : loglevel, default = info
  # $3 string : logpath, default = log
  # #4 string : Include date in log file name, default = no
  ###
  if [ "$1" = "" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Logfile name not specified."
  fi

  local logDir="$3"

  if [ "$logDir" = "" ]; then
    logDir=.
  fi

  if [ ! -d "$logDir" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Log directory [${logDir}] not found."
  fi

  export MLIB_LOGDATE=`date '+%Y%m%d'`

  if [ "$4" = "yes" ]; then
    export MLIB_LOGNAME=$logDir/$1.$MLIB_LOGDATE.log
  else
    export MLIB_LOGNAME=$logDir/$1.log
  fi

  if [ "$2" = "info" ]; then
    export MLIB_LOGLEVEL=10
  elif [ "$2" = "warning" ]; then
    export MLIB_LOGLEVEL=20
  elif [ "$3" = "error" ]; then
    export MLIB_LOGLEVEL=30
  else
    export MLIB_LOGLEVEL=0
  fi

  mlib_exec "$BASH_SOURCE" "$LINENO" "touch $MLIB_LOGNAME"

  mlib_log "Logger initialized"
}

function mlib_logger_delete_old()
{
  ###
  # Deletes log files older than x days
  #
  # $1 string : logname
  # $2 int    : number of old days to delete from, default = 10
  # $3 string : logpath, default = log
  ###
  if [ "$1" = "" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Logfile name not specified for delete."
  fi

  local logDir="$3"

  if [ "$logDir" = "" ]; then
    logDir=log
  fi

  if [ ! -d "$HOME/$logDir" ]; then
    return
  fi

  local days=$2

  if [ "$days" = "" ]; then
    days=10
  fi

  mlib_exec "$BASH_SOURCE" "$LINENO" "find $HOME/$logDir/$1*.log -maxdepth 0 -type f -mtime +${days} -exec rm -f {} \;"
}

function mlib_parse_yaml
{
  ###
  # Does light weight yaml parse on a file setting all values as
  # under score seperated varibles.
  #
  # param $1 string: filename
  # param #2 string: prefix for var names.
  ###
  local prefix=$2
  local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')

  sed -ne "s|^\($s\):|\1|" \
       -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
       -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
  awk -F$fs '{
     indent = length($1)/2;
     vname[indent] = $2;
     for (i in vname) {if (i > indent) {delete vname[i]}}
     if (length($3) > 0) {
        vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
        printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
     }
  }'
}

function mlib_yaml_load
{
  ###
  # Reads a yaml config file, default all variables with the cfg_ tag.
  #
  # $1 string: the config file to load.
  # $2 string: optional 'prexix' tag.
  ###
  if [ ! -f "$1" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "File not found - [$1]"
  fi

  local cfgLine=
  local tmpLine=

  for cfgLine in `mlib_parse_yaml "$1" "$2"`
  do
    if [ "${cfgLine: -1}" = '"' ]; then
      eval "$tmpLine $cfgLine"
      tmpLine=""
    else
      tmpLine=`echo "$tmpLine $cfgLine"`
    fi
  done
}

function mlib_environ_assert
{
  ###
  # Asserts that an environment variable is not blank.
  #
  # $1 string: the variable to assert was read.
  ###
  if [[ ! ${!1} && ${!1-_} ]]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Environment variable [$1] is not set"
  fi

  if [ "${!1}" = "" ]; then
    mlib_raise "$BASH_SOURCE" "$LINENO" "Environment variable [$1] is empty"
  fi
}

function mlib_proc_count()
{
  ###
  # Counts the number of process
  #
  # $1     string : user
  # $2     string : process name
  # return int    : the process count.
  ###
  local rc=`ps -fu $1 | grep -w $2 | grep -v grep | wc | cut -c 1-10`
  echo "$rc"
  return $rc
}

function mlib_proc_pid()
{
  ###
  # Gets the process id for a pid
  #
  # $1     string : user
  # $2     string : process name
  # return string :
  ###
  local ppid=`ps -fu $1 | grep -w $2 | grep -v grep | awk '{print $2}'`
  echo "$ppid"

  if [ "$ppid" = "" ]
  then
    return 0
  fi

  return $ppid
}

function mlib_proc_kill()
{
  ###
  # Kills a process by name.
  #
  # $1 string : user
  # $2 string : process name
  # $3 int    : optional, max retries before kill -9 is issued.  Default is 3
  ###
  local pcnt=`mlib_proc_count $1 $2`
  local ppid=0

  if [ "$pcnt" = "" ] || [ $pcnt -eq 0 ]
  then
    return
  fi

  local maxloops=$3
  local loopidx=1

  if [ "$maxloops" = "" ] || [ $maxloops -lt 1 ]
  then
    maxloops=3
  fi

  while [ $loopidx -le $maxloops ]
  do
    pcnt=`mlib_proc_count $1 $2`
    ppid=`mlib_proc_pid $1 $2`

    if [ "$ppid" = "" ] || [ $pcnt = 0 ]
    then
      break
    fi

    echo " - terminating $2 [pid:${ppid}] (${loopidx}/${maxloops})"
    mlib_exec "$BASH_SOURCE" "$LINENO" "kill $ppid"
    sleep 1

   ((loopidx++))
  done

  pcnt=`mlib_proc_count $1 $2`
  ppid=`mlib_proc_pid $1 $2`

  if [ "$ppid" != "" ]
  then
    echo " - killing $2 [pid:${ppid}]"
    mlib_exec "$BASH_SOURCE" "$LINENO" "kill -9 $ppid"
  fi
}

function mlib_element_in()
{
  ###
  # Checks if an element is in an string list. Echo's out `in` if it does.
  #
  # $1      string : element to check
  # $2      string : the string array to look in
  ###
  local ele

  for ele in ${2}; do
    if [[ "$ele" = "$1" ]]; then
      echo "in"
    fi
  done

  echo ""
}

function mlib_local_ip()
{
  ###
  # Prints the local ip address.
  #
  # $1     string : Optionally give the network interface to search on.
  # return string : The local ip address
  ###
  if [ "$(uname)" = "Darwin" ]; then
      ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}'
  else
    if [ "$1" = "" ]; then
      ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1 -d'/'
    else
      ip addr | grep 'inet' | grep "$1" | awk '{print $2}' | cut -f1 -d'/'
    fi
  fi
}
