# JSON schemas for mettle files

These schemas apply to different .yml file types that mettle ingests.

They are based on a human scan of the code and are more permissive than they should be.

They can be used to supply tab-completion and validation in text editors.

## Known limitations

YAML parses `null` as a language-appropriate null type (see [YAML spec](https://yaml.org/type/null.html)).
Mettle will identify use of null in some keys and translate to the string `"null"`.
JSON-schema forbids the use of `null` in property keys.
For these schemas to work correctly, always quote `"null"` in your YAML files.

## Using in editors

### Visual studio code

- Install the [YAML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml) package for VS Code.
- Edit your `settings.json` to include a reference to the schema file and a pattern for matching in the `yaml.schemas` section.

### PyCharm

- In the target repo, open settings.
- Navigate to "Languages & Frameworks | Schemas an DTDs | JSON Schema Mappings".
- Click + on the left hand side
- Type a name for the Schema (example: "Mettle DB Table")
- Select the appropriate schema file in this repo (example `mettle/code/isonschema/mettle.db.table.schema.ison`)
- Click + in the file selector
- Select "File path with pattern"
- Type a pattern which will match the files

## Bulk validation

The following one-liner can be used to bulk-validate files (note this uses [ripgrep](https://github.com/BurntSushi/ripgrep)):

```commandline
rg "file-type.*Mettle.DB.Table$" -tyaml -l | xargs check-jsonschema --schemafile mettle/code/jsonschema/mettle.db.table.schema.json
```
