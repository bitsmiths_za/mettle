#!/usr/bin/env node

const { dtsPlugin } = require("esbuild-plugin-d.ts");
const esbuild = require('esbuild');

esbuild.build({
  entryPoints: ['projects/mettle-db/src/index.ts'],
  bundle: true,
  minify: true,
  platform: 'node',
  logLevel: 'info',
  outdir: 'dist/bitsmiths-mettle-db',
  external: [
    './node_modules/*'
  ],
  format: 'esm',
  splitting: true,
  target: ['node10.4'],
  sourcemap: 'external',
  plugins: [dtsPlugin()],
}).catch(() => process.exit(1))
