#!/usr/bin/env node

const { dtsPlugin } = require("esbuild-plugin-d.ts");
const esbuild = require('esbuild');

esbuild.build({
  entryPoints: ['projects/mettle-braze/src/index.ts'],
  bundle: true,
  minify: true,
  platform: 'node',
  logLevel: 'info',
  outdir: 'dist/bitsmiths-mettle-braze',
  external: [
    './node_modules/*',
    '@bitsmiths/mettle-io'
  ],
  format: 'esm',
  splitting: true,
  target: ['node10.4'],
  sourcemap: 'external',
  plugins: [dtsPlugin()],
}).catch(() => process.exit(1))
