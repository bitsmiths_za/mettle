/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Writer }       from '@bitsmiths/mettle-io';
import { Reader }       from '@bitsmiths/mettle-io';
import { ISerializable} from '@bitsmiths/mettle-io';

export class BoolList extends Array<boolean>
{

  /**
   * Constructor
   */
  constructor() {
    super();
    Object.setPrototypeOf(this, BoolList.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string {
    return 'BoolList';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

    let _idx = 0;

    for (; _idx < this.length; ++_idx) {
      _w.writeBool('Bool', this[_idx]);

      if (_w._prom.broken())
        return;
    }

    _w.writeEnd(_oname); if (_w._prom.broken()) return;
  }

  /**
   * Deserialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
      this[_idx] = _r.readBool('Bool');

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname); if (_r._prom.broken()) return;
  }
}

export class CharList extends Array<string> {

  /**
   * Constructor
   */
  constructor() {
    super();
    Object.setPrototypeOf(this, CharList.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string {
    return 'CharList';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

    let _idx = 0;

    for (; _idx < this.length; ++_idx)
    {
      _w.writeChar('Char', this[_idx]);

      if (_w._prom.broken())
        return;
    }

    _w.writeEnd(_oname); if (_w._prom.broken()) return;
  }

  /**
   * Deserialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
      this[_idx] = _r.readChar('Char');

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname); if (_r._prom.broken()) return;
  }
}

export class StringList extends Array<string> {

  /**
   * Constructor
   */
  constructor() {
    super();
    Object.setPrototypeOf(this, StringList.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string {
    return 'StringList';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

    let _idx = 0;

    for (; _idx < this.length; ++_idx)
    {
      _w.writeString('String', this[_idx]);

      if (_w._prom.broken())
        return;
    }

    _w.writeEnd(_oname); if (_w._prom.broken()) return;
  }

  /**
   * Deserialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
      this[_idx] = _r.readString('String');

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname); if (_r._prom.broken()) return;
  }
}

export class NumberList extends Array<number>
{
  _typeName = 'Number';

  /**
   * Constructor
   */
  constructor() {
    super();
    Object.setPrototypeOf(this, NumberList.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string {
    return this._typeName + 'List';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

    let _idx = 0;

    for (; _idx < this.length; ++_idx)
    {
      _w.writeNumber(this._typeName, this[_idx]);

      if (_w._prom.broken())
        return;
    }

    _w.writeEnd(_oname); if (_w._prom.broken()) return;
  }

  /**
   * Deserialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname)
      _oname = this._name();

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
        this[_idx] = _r.readNumber(this._typeName);

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname); if (_r._prom.broken()) return;
  }
}

export class Int8List extends NumberList
{
  constructor() {
    super();
    Object.setPrototypeOf(this, Int8List.prototype);
    this._typeName = 'Int8';
  }
}

export class Int16List extends NumberList
{
  constructor() {
    super();
    Object.setPrototypeOf(this, Int16List.prototype);
    this._typeName = 'Int16';
  }
}

export class Int32List extends NumberList
{
  constructor() {
    super();
    Object.setPrototypeOf(this, Int32List.prototype);
    this._typeName = 'Int32';
  }
}

export class Int64List extends NumberList
{
  constructor() {
    super();
    Object.setPrototypeOf(this, Int64List.prototype);
    this._typeName = 'Int64';
  }
}

export class Uint8List extends NumberList
{
  constructor() {
    super();
    Object.setPrototypeOf(this, Uint8List.prototype);
    this._typeName = 'Uint8';
  }
}

export class Uint16List extends NumberList
{
  constructor()
  {
    super();
    Object.setPrototypeOf(this, Uint16List.prototype);
    this._typeName = 'Uint16';
  }
}

export class Uint32List extends NumberList
{
  constructor() {
    super();
    Object.setPrototypeOf(this, Uint32List.prototype);
    this._typeName = 'Uint32';
  }
}

export class Uint64List extends NumberList
{
  constructor() {
    super();
    Object.setPrototypeOf(this, Uint64List.prototype);
    this._typeName = 'Uint64';
  }
}

export class DoubleList extends NumberList
{
  constructor() {
    super();
    Object.setPrototypeOf(this, DoubleList.prototype);
    this._typeName = 'Double';
  }
}

export class DateTimeList extends Array<Date> {

  _typeName = 'DateTime';

  /**
   * Constructor
   */
  constructor()
  {
    super();
    Object.setPrototypeOf(this, DateTimeList.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string {
    return this._typeName + 'List';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

    let _idx = 0;

    for (; _idx < this.length; ++_idx)
    {
      _w.writeDateTime(this._typeName, this[_idx]);

      if (_w._prom.broken())
        return;
    }

    _w.writeEnd(_oname); if (_w._prom.broken()) return;
  }

  /**
   * Deserialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
      this[_idx] = _r.readDateTime(this._typeName);

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname); if (_r._prom.broken()) return;
  }
}

export class DateList extends Array<Date> {

  _typeName = 'Date';

  /**
   * Constructor
   */
  constructor() {
    super();
    Object.setPrototypeOf(this, DateList.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string
  {
    return this._typeName + 'List';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

    let _idx = 0;

    for (; _idx < this.length; ++_idx)
    {
      _w.writeDate(this._typeName, this[_idx]);

      if (_w._prom.broken())
        return;
    }

    _w.writeEnd(_oname); if (_w._prom.broken()) return;
  }

  /**
   * Deserialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
      this[_idx] = _r.readDate(this._typeName);

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname); if (_r._prom.broken()) return;
  }
}

export class TimeList extends Array<Date> {

  _typeName = 'Time';

  /**
   * Constructor
   */
  constructor() {
    super();
    Object.setPrototypeOf(this, TimeList.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string {
    return this._typeName + 'List';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

    let _idx = 0;

    for (; _idx < this.length; ++_idx)
    {
      _w.writeTime(this._typeName, this[_idx]);

      if (_w._prom.broken())
        return;
    }

    _w.writeEnd(_oname); if (_w._prom.broken()) return;
  }

  /**
   * Deserialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
      this[_idx] = _r.readTime(this._typeName);

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname); if (_r._prom.broken()) return;
  }
}

export class MemblockList extends Array<string>
{
  /**
   * Constructor
   */
  constructor() {
    super();
    Object.setPrototypeOf(this, MemblockList.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string {
    return 'MemblockList';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

    let _idx = 0;

    for (; _idx < this.length; ++_idx)
    {
      _w.writeByteArray('Memblock', this[_idx]);

      if (_w._prom.broken())
        return;
    }

    _w.writeEnd(_oname); if (_w._prom.broken()) return;
  }

  /**
   * Deserialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
      this[_idx] = _r.readByteArray('Memblock');

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname); if (_r._prom.broken()) return;
  }
}

export class GuidList extends Array<string> {

  /**
   * Constructor
   */
  constructor() {
    super();
    Object.setPrototypeOf(this, GuidList.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string {
    return 'GuidList';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);  if (_w._prom.broken()) return;

    let _idx = 0;

    for (; _idx < this.length; ++_idx)
    {
      _w.writeGuid('Guid', this[_idx]);

      if (_w._prom.broken())
        return;
    }

    _w.writeEnd(_oname); if (_w._prom.broken()) return;
  }

  /**
   * Deserialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);  if (_r._prom.broken()) return;

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
      this[_idx] = _r.readGuid('Guid');

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname); if (_r._prom.broken()) return;
  }
}


/**
 * Standard braze list / array class.
 */
export class List<T extends ISerializable> extends Array<T>
{
  /**
   * Typed List Class
   */
  constructor(private _clsType: new () => T) {
    super();

    Object.setPrototypeOf(this, List.prototype);
  }

  /**
   * Clear the list.
   */
  _clear(): void {
    this.length = 0;
  }

  /**
   * Get the default list name.
   * @returns  The defualt list name.
   */
  _name(): string {
    return 'List';
  }

  /**
   * Serialize the list with a writer.
   * @param _w      The writer object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _serialize(_w: Writer, _oname?: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    _w.writeStartList(_oname, this.length);

    if (_w._prom.broken()) {
      return;
    }

    let _idx = 0;

    for (; _idx < this.length; ++_idx)
    {
      this[_idx]._serialize(_w, _oname);

      if (_w._prom.broken()){
        return;
      }
    }

    _w.writeEnd(_oname);

    if (_w._prom.broken()) {
      return;
    }
  }

  /**
   * Derialize the list with a reader.
   * @param _r      The reader object to use.
   * @param _oname  Optionally pass in a diff name for the object.
   */
  _deserialize(_r: Reader, _oname: string): void {
    if (!_oname) {
      _oname = this._name();
    }

    let   _idx     = 0;
    const _listLen = _r.readStartList(_oname);

    if (_r._prom.broken()) {
      return;
    }

    this.length = _listLen;

    for (; _idx < this.length; ++_idx)
    {
      const obj = new this._clsType();

      obj._deserialize(_r, _oname);

      this[_idx] = obj;

      if (_r._prom.broken())
        return;
    }

    _r.readEnd(_oname);

    if (_r._prom.broken()) {
      return;
    }
  }
}
