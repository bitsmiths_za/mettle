/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { MettlePromise } from '@bitsmiths/mettle-lib'

/**
 * List Stream class, for using an array as a stream of data.
 */
export class ListStream {
  _prom : MettlePromise;
  _list : any[];
  _pos  : number;

  /**
   * List Stream class, for using an array as a stream of data.
   * @param prom The promise to break if anything goes wrong.
   * @param list An optional param to initialize the object with.
   */
  constructor(prom: MettlePromise, list?: any[]) {
    this._prom = prom;
    this._list = list === undefined ? [] : list;
    this._pos  = 0;
  }

  /**
    * Clear the internal list.
    */
  clear() {
    this._list.length = 0;
  }

  /**
   * Reads the next object from the list stream.
   * @ereturns The next object from the list..
   */
  read(): any {
    if (this._pos >= this._list.length) {
      this._prom.raise('mettle.ListStream: Attempted to read stream beyond the boundry!');
      return;
    }

    return this._list[this._pos++];
  }

  /**
   * Writes an object to end of the list stream.
   * @param obj  The object to be written.
   * @returns    The object that was written
   */
  write(obj: any): any {
    this._list.push(obj);

    return obj;
  }

  /**
   * Gets the length/size of the internal list.
   * @returns The length of the list.
   */
  size(): number {
    return this._list.length;
  }

  /**
   * Moves the internal stream position to the start of the stream.
   */
  positionStart() {
    this._pos = 0;
  }

  /**
   * Moves the internal stream position to the end of the stream.
   */
  positionEnd() {
    this._pos = this._list.length;
  }

  /**
   * Moves the internal stream position forward in indexes.
   * @param offset The number of indexes to move forward from the current position.
   */
  positionMove(offset: number) {
    this._pos += offset;
  }

  /**
   * Gets the current position of the stream.
   * @returns The current position of the stream.
   */
  position(): number {
    return this._pos;
  }
}
