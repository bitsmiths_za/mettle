/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/db/sqlparams.h"

#include "mettle/lib/common.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

using namespace Mettle::Lib;

namespace Mettle { namespace DB {

SqlParams::SqlParams() noexcept
{
   bindings      = 0;
   count         = 0;
   _allocated    = 0;
}


SqlParams::~SqlParams() noexcept
{
   Destroy();
}


void SqlParams::Clean() noexcept
{
   for (unsigned index = 0; index < count; index++)
      memset(bindings[index].ptr, 0, bindings[index].ptrSize);
}


void SqlParams::Destroy() noexcept
{
   for (unsigned index = 0; index < count; index++)
      FreeBinding(&bindings[index]);

   _FREE(bindings)

   count      = 0;
   _allocated = 0;
}


void SqlParams::FreeBinding(Binding* bind) noexcept
{
   if (bind->_freePtr)
   {
      _FREE(bind->ptr)
   }

   if (!bind->_freeOrigPtr)
      return;

   switch (bind->type)
   {
      case STRING:
      case JSON:
         _DELETE(bind->orig.origStringPtr)
         break;

      case DATE:
         _DELETE(bind->orig.origDatePtr)
         break;

      case TIME:
         _DELETE(bind->orig.origTimePtr)
         break;

      case DATETIME:
         _DELETE(bind->orig.origDateTimePtr)
         break;

      case UUID:
         _DELETE(bind->orig.origGuidPtr)
         break;

      case MEMBLOCK:
         _DELETE(bind->orig.origMemBlockPtr)
         break;

     default:
        _FREE(bind->orig.origPtr)
  }
}


#define AS_METHOD(name, type, ptr) type* SqlParams::Binding::name() throw () { return (type*) ptr; }

AS_METHOD(asString,      String,      orig.origStringPtr)
AS_METHOD(as_c_str,      const char,  ptr)
AS_METHOD(asBool,        bool,        ptr)
AS_METHOD(asChar,        char,        ptr)
AS_METHOD(asInt8,        int8_t,      ptr)
AS_METHOD(asInt16,       int16_t,     ptr)
AS_METHOD(asInt32,       int32_t,     ptr)
AS_METHOD(asInt64,       int64_t,     ptr)
AS_METHOD(asFloat,       float,       ptr)
AS_METHOD(asDouble,      double,      ptr)
AS_METHOD(asMemoryBlock, MemoryBlock, orig.origMemBlockPtr)
AS_METHOD(asDate,        Date,        orig.origDatePtr)
AS_METHOD(asTime,        Time,        orig.origTimePtr)
AS_METHOD(asDateTime,    DateTime,    orig.origDateTimePtr)
AS_METHOD(asGuid,        Guid,        orig.origGuidPtr)

void SqlParams::Initialize() noexcept
{
   Destroy();
}


SqlParams::Binding* SqlParams::AllocateBinding(const char* name)
{
   Binding* rec;

   if (count >= _allocated)
   {
      void *tmp;

      _allocated += 32;

      tmp = realloc(bindings, sizeof(Binding) * _allocated);

      if (!tmp)
         throw xMettle(__FILE__, __LINE__, "SqlParams", "Error allocating memory (%u/%u)", sizeof(Binding) * _allocated, _allocated);

      bindings = (Binding*) tmp;
   }

   rec = &bindings[count++];

   memset(rec, 0, sizeof(Binding));

   strncpy(rec->name, name, sizeof(rec->name) - 1);
   rec->name[sizeof(rec->name) - 1] = 0;

   return rec;
}


SqlParams::Binding* SqlParams::Add(const char*          name,
                                         void*          ptr,
                                   const unsigned int   ptrSize,
                                   const DataType       type,
                                   const bool           isNull,
                                         bool*          nullPtr)
{
   Binding* rec = AllocateBinding(name);

   rec->type         = type;
   rec->orig.origPtr =
   rec->ptr          = ptr;
   rec->ptrSize      = ptrSize;
   rec->_freePtr     = false;
   rec->_freeOrigPtr = false;
   rec->isNull       = isNull;
   rec->_nullPtr     = nullPtr;

   return rec;
}


SqlParams::Binding* SqlParams::AddString(
   const char*          name,
         String*        ptr,
   const unsigned int   length)
{
   Binding* rec = AllocateBinding(name);

   rec->type               = SqlParams::STRING;
   rec->orig.origStringPtr = ptr;
   rec->_freePtr           = false;
   rec->_freeOrigPtr       = false;
   rec->_nullPtr           = 0;

   if (ptr == 0)
   {
      rec->_freeOrigPtr       = true;
      rec->orig.origStringPtr = new String();
   }

   if (length)
   {
      rec->orig.origStringPtr->setSize(length);
      rec->ptrSize = length + 1;
   }
   else
   {
      rec->ptrSize = rec->orig.origStringPtr->bytesUsed();
   }

   rec->ptr = (char*) rec->orig.origStringPtr->c_str();

   return rec;
}


SqlParams::Binding* SqlParams::Add_c_str(
   const char*         name,
   char*               ptr,
   const unsigned int  length)
{
   if (ptr == 0)
      return AddString(name, 0, length);

   Binding* rec = AllocateBinding(name);

   rec->type      = SqlParams::STRING;
   rec->_freePtr  = false;
   rec->_nullPtr  = 0;

   rec->_freeOrigPtr       = true;
   rec->orig.origStringPtr = new String(ptr);

   if (length)
   {
      rec->orig.origStringPtr->setSize(length);
      rec->ptrSize = length + 1;
   }
   else
   {
      rec->ptrSize = rec->orig.origStringPtr->bytesUsed();
   }

   rec->ptr = (char*) rec->orig.origStringPtr->c_str();

   return rec;
}


SqlParams::Binding* SqlParams::AddDate(
   const char*      name,
         void*      ptr,
   const DataType   type)
{
   Binding* rec = AllocateBinding(name);

   rec->type               = type;
   rec->orig.origPtr       =
   rec->ptr                = ptr;
   rec->ptrSize            = 0;
   rec->_freePtr           = false;
   rec->_freeOrigPtr       = false;
   rec->_nullPtr           = 0;

   if (!ptr)
   {
      rec->_freeOrigPtr = true;

      if (type == DATE)
         rec->orig.origDatePtr = new Date();
      else if (type == DATETIME)
         rec->orig.origDateTimePtr = new DateTime();
      else if (type == TIME)
         rec->orig.origTimePtr = new Time();
   }

   return rec;
}


SqlParams::Binding* SqlParams::AddGuid(
   const char* name,
         Guid* ptr)
{
   Binding* rec = AllocateBinding(name);

   rec->type               = SqlParams::UUID;
   rec->orig.origPtr       =
   rec->ptr                = ptr;
   rec->ptrSize            = 0;
   rec->_freePtr           = false;
   rec->_freeOrigPtr       = false;
   rec->_nullPtr           = 0;

   if (!ptr)
   {
      rec->_freeOrigPtr     = true;
      rec->orig.origGuidPtr = new Guid();
   }

   return rec;
}


SqlParams::Binding* SqlParams::AddMemoryBlock(
   const char*          name,
         MemoryBlock*   ptr,
   const unsigned int   size)
{
   Binding* rec = AllocateBinding(name);

   rec->type                 = SqlParams::MEMBLOCK;
   rec->orig.origMemBlockPtr = ptr;
   rec->_freePtr             = false;
   rec->_freeOrigPtr         = false;
   rec->_nullPtr             = 0;

   if (!ptr)
   {
      rec->_freeOrigPtr         = true;
      rec->orig.origMemBlockPtr = new MemoryBlock();
   }

   if (size)
   {
      rec->orig.origMemBlockPtr->allocate(size);
      rec->ptrSize = size;
   }
   else
   {
      rec->ptrSize = rec->orig.origMemBlockPtr->size();
   }

   rec->ptr = rec->orig.origMemBlockPtr->block();

   return rec;
}


void SqlParams::Bind(
   const char*          name,
         String*        p,
   const unsigned int   length)
{
   AddString(name, p, length);
}


void SqlParams::BindJson(
   const char*          name,
         String*        p,
   const unsigned int   length)
{
   Binding* rec = AddString(name, p, length);

   rec->type = SqlParams::JSON;
}


void SqlParams::Bind_c_str(
   const char*          name,
         char*          p,
   const unsigned int   length)
{
   Add_c_str(name, p, length);
}


void SqlParams::BindJson_c_str(
   const char*          name,
         char*          p,
   const unsigned int   length)
{
   Binding* rec = Add_c_str(name, p, length);

   rec->type = SqlParams::JSON;
}


void SqlParams::Bind(const char* name, char* p)
{
   SqlParams::Binding* b = Add(name, p, sizeof(char), SqlParams::CHAR);

   b->ptr              = 0;
   b->_freePtr         = true;
   b->ptr              = (char*) Common::memoryAlloc(2);
   b->ptrSize          = 2;
   ((char*)b->ptr)[0] = *p;
   ((char*)b->ptr)[1] = 0;
}


void SqlParams::Bind(const char* name, int8_t* p, const bool isNull, bool* nullPtr)
{
   Add(name, p, sizeof(int8_t), SqlParams::INT8, isNull, nullPtr);
}


void SqlParams::Bind(const char* name, int16_t* p, const bool isNull, bool* nullPtr)
{
   Add(name, p, sizeof(int16_t), SqlParams::INT16, isNull, nullPtr);
}


void SqlParams::Bind(const char* name, int32_t* p, const bool isNull, bool* nullPtr)
{
   Add(name, p, sizeof(int32_t), SqlParams::INT32, isNull, nullPtr);
}


void SqlParams::Bind(const char* name, int64_t* p, const bool isNull, bool* nullPtr)
{
   Add(name, p, sizeof(int64_t), SqlParams::INT64, isNull, nullPtr);
}


void SqlParams::Bind(const char* name, float* p)
{
   Add(name, p, sizeof(float), SqlParams::FLOAT);
}


void SqlParams::Bind(const char* name, double* p)
{
   Add(name, p, sizeof(double), SqlParams::DOUBLE);
}


void SqlParams::Bind(const char* name, MemoryBlock* p)
{
   AddMemoryBlock(name, p, 0);
}


void SqlParams::Bind(const char* name, Date* p)
{
   AddDate(name, p, SqlParams::DATE);
}


void SqlParams::Bind(const char* name, Time* p)
{
   AddDate(name, p, SqlParams::TIME);
}


void SqlParams::Bind(const char* name, DateTime* p)
{
   AddDate(name, p, SqlParams::DATETIME);
}


void SqlParams::Bind(const char* name, bool* p)
{
   Add(name, p, sizeof(bool), SqlParams::BOOL);
}


void SqlParams::Bind(const char* name, Guid* p)
{
   AddGuid(name, p);
}


/*------------------------------------------------------------------------------
   Allocate - allocates a new binding with it's own memory
   This function is designed for output only, don't use this function to bind
   input params, rather use the bind functions.
------------------------------------------------------------------------------*/
SqlParams::Binding* SqlParams::Allocate(const DataType type,
                                        const char*    name,
                                        const int      len)
{
   Binding* rec;

   switch (type)
   {
      case STRING:
      case STRING_C_STR:
      case JSON:
         rec = AddString(name, 0, len);
         break;

      case DATE:
         rec = AddDate(name, 0, type);
         break;

      case TIME:
         rec = AddDate(name, 0, type);
         break;

      case DATETIME:
         rec = AddDate(name, 0, type);
         break;

      case UUID:
         rec = AddGuid(name, 0);
         break;

      case MEMBLOCK:
         rec = AddMemoryBlock(name, 0, len);
         break;

      default:
         rec = Add(name, 0, 0, type);

         rec->_freeOrigPtr = true;
         rec->orig.origPtr = (int8_t*) Common::memoryAlloc(len);
         rec->ptr          = rec->orig.origPtr;
         rec->ptrSize      = len;
         break;
  }

   return rec;
}


}}
