/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/db/dbschema.h"

#include "mettle/lib/macros.h"

using namespace Mettle::Lib;

namespace Mettle { namespace DB {

#define MOUDLE  "DBSchema"


DBSchema::Column::Column(const int32_t     colNo,
                         const char       *name,
                         const ColumnType  type,
                         const int32_t    size,
                         const bool       nullable)
{
   _colNo    = colNo;
   _name     = name;
   _type     = type;
   _size     = size;
   _nullable = nullable;
}


DBSchema::Column::~Column()
{
}


bool DBSchema::Column::isDigit() const
{
   return _type == DBSchema::eInt8   ||
          _type == DBSchema::eInt16  ||
          _type == DBSchema::eInt32  ||
          _type == DBSchema::eInt64  ||
          _type == DBSchema::eSeq32  ||
          _type == DBSchema::eSeq64  ||
          _type == DBSchema::eDouble;
}


bool DBSchema::Column::isInteger() const
{
   return _type == DBSchema::eInt8   ||
          _type == DBSchema::eInt16  ||
          _type == DBSchema::eInt32  ||
          _type == DBSchema::eInt64  ||
          _type == DBSchema::eSeq32  ||
          _type == DBSchema::eSeq64;
}


bool DBSchema::Column::isSequence() const
{
   return _type == DBSchema::eSeq32  ||
          _type == DBSchema::eSeq64;
}


bool DBSchema::Column::isDouble() const
{
   return _type == DBSchema::eDouble;
}


bool DBSchema::Column::isChar() const
{
   return _type == DBSchema::eChar;
}


bool DBSchema::Column::isString() const
{
   return _type == DBSchema::eString;
}


bool DBSchema::Column::isDate() const
{
   return _type == DBSchema::eDate;
}


bool DBSchema::Column::isTime() const
{
   return _type == DBSchema::eTime;
}


bool DBSchema::Column::isDateTime() const
{
   return _type == DBSchema::eDateTime ||
          _type == DBSchema::eTimeStamp;
}


bool DBSchema::Column::isTimeStamp() const
{
   return _type == DBSchema::eTimeStamp;
}


bool DBSchema::Column::isDateOrDateTime() const
{
   return _type == DBSchema::eDateTime  ||
          _type == DBSchema::eTimeStamp ||
          _type == DBSchema::eDate;
}

bool DBSchema::Column::isMemoryBlock() const
{
   return _type == DBSchema::eMemoryBlock;
}

bool DBSchema::Column::isBool() const
{
   return _type == DBSchema::eBool;
}

bool DBSchema::Column::isUUID() const
{
   return _type == DBSchema::eUUID;
}

const String &DBSchema::Column::name() const
{
   return _name;
}


DBSchema::PrimaryKey::PrimaryKey(Table *table)
{
   _table = table;
}


DBSchema::PrimaryKey::~PrimaryKey()
{
}


bool DBSchema::PrimaryKey::isSequence() const
{
   if (_columns.count() != 1)
      return false;

   return _table->getColumn(_columns[0])->isSequence();
}


DBSchema::ForeignKey::ForeignKey(Table *table, const char *name, const char *fkTable)
{
   _table   = table;
   _name    = name;
   _fkTable = fkTable;
}


DBSchema::ForeignKey::~ForeignKey()
{
}


const String &DBSchema::ForeignKey::name() const
{
   return _name;
}


DBSchema::UniqueKey::UniqueKey(Table *table, const char *name)
{
   _table = table;
   _name  = name;
}


DBSchema::UniqueKey::~UniqueKey()
{
}


const String &DBSchema::UniqueKey::name() const
{
   return _name;
}


DBSchema::Index::Index(Table *table, const char *name)
{
   _table = table;
   _name  = name;
}


DBSchema::Index::~Index()
{
}


const String &DBSchema::Index::name() const
{
   return _name;
}


DBSchema::Table::Table(DBSchema *schema, const char *name)
                :_primaryKey(this)
{
   _schema = schema;
   _name   = name;
}


DBSchema::Table::~Table()
{
}


const DBSchema::Column* DBSchema::Table::getColumn(const char *name,
                                                   const bool  throwIfMissing) const
{
   _FOREACH(Column, c, _columns)
      if (c.obj->_name == name)
         return c.obj;

   if (throwIfMissing)
      throw xMettle(__FILE__, __LINE__, MOUDLE, "Column (%s) could not be found in Table/Schema (%s/%s)", name, _name.c_str(), _schema->_name.c_str());

   return 0;
}


const DBSchema::ForeignKey* DBSchema::Table::getForeignKey(const char *name,
                                                           const bool  throwIfMissing) const
{
   _FOREACH(ForeignKey, c, _foreignKeys)
      if (c.obj->_name == name)
         return c.obj;

   if (throwIfMissing)
      throw xMettle(__FILE__, __LINE__, MOUDLE, "ForeignKey (%s) could not be found in Table (%s)", name, _name.c_str());

   return 0;
}


const DBSchema::UniqueKey* DBSchema::Table::getUniqueKey(const char *name,
                                                         const bool  throwIfMissing) const
{
   _FOREACH(UniqueKey, c, _uniqueKeys)
      if (c.obj->_name == name)
         return c.obj;

   if (throwIfMissing)
      throw xMettle(__FILE__, __LINE__, MOUDLE, "UniqueKey (%s) could not be found in Table (%s)", name, _name.c_str());

   return 0;
}


const DBSchema::Index* DBSchema::Table::getIndex(const char *name,
                                                 const bool  throwIfMissing) const
{
   _FOREACH(Index, c, _indexes)
      if (c.obj->_name == name)
         return c.obj;

   if (throwIfMissing)
      throw xMettle(__FILE__, __LINE__, MOUDLE, "Indexes (%s) could not be found in Table (%s)", name, _name.c_str());

   return 0;
}


bool DBSchema::Table::hasPrimaryKey() const
{
   return _primaryKey._columns.count() > 0;
}


bool DBSchema::Table::hasForeignKeys() const
{
   return _foreignKeys.count() > 0;
}


bool DBSchema::Table::hasUniqueKeys() const
{
   return _uniqueKeys.count() > 0;
}


bool DBSchema::Table::hasIndexes() const
{
   return _indexes.count() > 0;
}


DBSchema::DBSchema()
{
}


DBSchema::~DBSchema()
{
}


const DBSchema::Table *DBSchema::getTable(const char *name,
                                          const bool  throwIfMissing) const
{
   _FOREACH(Table, t, _tables)
      if (t.obj->_name == name)
         return t.obj;

   if (throwIfMissing)
      throw xMettle(__FILE__, __LINE__, MOUDLE, "Table (%s) could not be found in DBSchema (%s)", name, _name.c_str());

   return 0;
}

#undef MOUDLE

}}
