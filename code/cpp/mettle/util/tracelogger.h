/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_UTIL_TRACELOGGER_H_
#define __METTLE_UTIL_TRACELOGGER_H_

#include <stdarg.h>
#include <stdio.h>

#include "mettle/lib/loggerstdfile.h"

namespace Mettle { namespace Util {

class TraceFileLogger : public Mettle::Lib::LoggerStdFile
{
public :

   class TraceItem
   {
   public:
      char*        file;
      unsigned int line;
      char         datetime[15];
      char         lvl[4];
      char         msg[1024];

      void Clear();
   };


   TraceFileLogger(const char*          logFile,
                   const Level          level,
                   const bool           display   = true,
                   const unsigned short maxTraces = 128);

   virtual ~TraceFileLogger() noexcept;

   void error(const char* fmt, ...) noexcept;

   void warning(const char* fmt, ...) noexcept;

   void info(const char* fmt, ...)  noexcept;

   void debug(const char* fmt, ...) noexcept;

   Mettle::Lib::Logger* Trace(const char* file, const unsigned int line) noexcept;

   void TraceReset() noexcept;

   void TraceUnwind() const noexcept;

protected :

   unsigned short _maxTraces;
   unsigned short _itemIdx;
   TraceItem*     _items;
   TraceItem*     _currItem;

   TraceItem* NextItem()    noexcept;
   TraceItem* NextLogItem() noexcept;

   void GetDate(char* dest) noexcept;

   void TraceLog(const char* fmt,
                 va_list     ap,
                 const char* lvl) noexcept;

   void FileLog(const char* fmt,
                va_list     ap,
                FILE*       fp) noexcept;

   void UnwindMsg(const char* fmt, ...) const noexcept;
   void UnwindMsg(FILE* fp, const char* fmt, va_list ap) const noexcept;
};


}}

#endif
