/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/memoryblock.h"

#include <stdlib.h>
#include <string.h>

namespace Mettle { namespace Lib {

#define MODULE "Mettle.MemoryBlock"

MemoryBlock::MemoryBlock() noexcept
{
   _block = 0;
   _size  = 0;
}

MemoryBlock::MemoryBlock(const unsigned int size)
{
   _block = 0;
   _size  = 0;

   uint8_t *tmp;

   if ((tmp = (uint8_t *) realloc(_block, size)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", size);

   _block = tmp;
   _size  = size;
}

MemoryBlock::MemoryBlock(const MemoryBlock &cp)
{
   _block = 0;
   _size  = 0;

   if (cp._size < 1)
      return;

   uint8_t *tmp;

   if ((tmp = (uint8_t *) realloc(_block, cp._size)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", cp._size);

   _block = tmp;
   _size  = cp._size;

   memcpy(_block, cp._block, _size);
}

MemoryBlock::~MemoryBlock() noexcept
{
   if (_block)
      free(_block);
}

bool MemoryBlock::isEmpty() const noexcept
{
   return _size == 0;
}

void MemoryBlock::clear() noexcept
{
   if (_block)
   {
      free(_block);
      _block = 0;
      _size = 0;
   }
}

void MemoryBlock::allocate(const unsigned int size)
{
   if (size == 0)
   {
      clear();
      return;
   }

   if (size == _size)
      return;

   uint8_t *tmp;

   if ((tmp = (uint8_t *) realloc(_block, size)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", size);

   _block = tmp;
   _size  = size;
}

uint8_t *MemoryBlock::blockU8() const noexcept
{
   return _block;
}

int8_t *MemoryBlock::blockS8() const noexcept
{
   return (int8_t *) _block;
}

void *MemoryBlock::block() const noexcept
{
   return (void *) _block;
}

unsigned int MemoryBlock::size() const noexcept
{
   return _size;
}

MemoryBlock & MemoryBlock::operator = (const MemoryBlock &cp)
{
   allocate(cp._size);

   if (_size < 1)
      return *this;

   memcpy(_block, cp._block, _size);

   return *this;
}

void MemoryBlock::eat(MemoryBlock *me) noexcept
{
   clear();

   _block = me->_block;
   _size  = me->_size;

   me->_block = 0;
   me->_size  = 0;
}

void MemoryBlock::poop(MemoryBlock *dest) noexcept
{
   dest->clear();

   dest->_block = _block;
   dest->_size  = _size;

   _block = 0;
   _size  = 0;
}


#undef MODULE

}}
