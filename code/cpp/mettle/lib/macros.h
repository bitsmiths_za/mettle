/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_MACROS_H_
#define __METTLE_LIB_MACROS_H_

#include "mettle/lib/platform.h"

#define _DELETE(ptr)     if (ptr) { delete ptr; ptr = 0; }
#define _DELETE_ARR(ptr) if (ptr) { delete [] ptr; ptr = 0; }
#define _FREE(ptr) if (ptr) { free(ptr); ptr = 0; }

#define _SWOP_VAL(a, b) (a) ^= (b); (b) ^= (a); (a) ^= (b)
#define _FAST_INT_DIV_BY_2(val) (((val) + (val)) >> 2)
#define _SWOP_SIGN(val) (val) = 0 - (val)
#define _STR_IS_NULL(s) ((s) == 0 || *(s) == 0)

#define _BITON(aa,bb)     (aa |= (bb))
#define _BITOFF(aa,bb)    (aa = ((aa&(bb)) == (bb) ? (aa^(bb)) : aa))
#define _BITTOGGLE(aa,bb) (aa = aa^(bb))
#define _ALLBITSON(aa)    (aa = 0, aa = ~aa)
#define _ALLBITSOFF(aa)   (aa = 0)
#define _ISBITON(aa,bb)   (aa&(bb))

#define _EMPTY_STRING  ""

#define _EXTERN_C_BEGIN  extern "C" {
#define _EXTERN_C_END    }

#define _SPOT_STD_EXCEPTIONS(log, cmd)   catch(Mettle::Lib::xMettle &x)\
{\
   if (log)\
      log->spot(__FILE__, __LINE__, x);\
   else\
      fprintf(stderr, "%s\n", x.what());\
   cmd;\
}\
catch(std::exception &x)\
{\
   if (log)\
      log->spot(__FILE__, __LINE__, x);\
   else\
      fprintf(stderr, "%s\n", x.what());\
   cmd;\
}\
catch(...)\
{\
   if (log)\
      log->spot(__FILE__, __LINE__);\
   else\
      fprintf(stderr, "Unknown Exeption Caught (%s:%d)\n", __FILE__, __LINE__);\
   cmd;\
}

#ifndef _FOREACH

   #ifdef _OLD_FOREACH_PTR_HACK

      #define _FOREACH(type, rec, col) for(type *_##rec##_idx = 0,\
      *rec = (col).count() > 0 ? (col).get(0) : 0;\
      (size_t)_##rec##_idx < (col).count();\
      *((size_t *)&_##rec##_idx) += 1,\
      rec = ((size_t)_##rec##_idx) <= (col).count() ? (col).get((size_t)_##rec##_idx) : 0)

      #define _FOREACH_IDX(rec) ((size_t)_##rec##_idx)

   #else

      #if defined(OS_MS_WINDOWS) && COMPILER_MSC >= 6

         #define _FOREACH(type, rec, col) struct { type *obj; unsigned int idx; } rec = { (col).count() > 0 ? (col).get(0) : 0, 0 }; for(;\
         rec.idx < (col).count();\
         rec.obj = ++rec.idx < (col).count() ? (col).get(rec.idx) : 0)

      #else

         #define _FOREACH(type, rec, col) for(struct { type *obj; unsigned int idx; } rec = { (col).count() > 0 ? (col).get(0) : 0, 0 };\
         rec.idx < (col).count();\
         rec.obj = ++rec.idx < (col).count() ? (col).get(rec.idx) : 0)

      #endif

   #endif

#endif


#define _BIT1  0x01
#define _BIT2  0x02
#define _BIT3  0x04
#define _BIT4  0x08
#define _BIT5  0x10
#define _BIT6  0x20
#define _BIT7  0x40
#define _BIT8  0x80

#endif
