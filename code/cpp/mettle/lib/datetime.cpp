/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "mettle/lib/datetime.h"
#include "mettle/lib/common.h"

namespace Mettle { namespace Lib {

static int8_t  gltzhour    = 0;
static int8_t  gltzmin     = 0;
static int32_t gltzoffset  = 0;
static char    gltzabr[10] = "UTC";

struct _bsDateTimeTZDef {
   int32_t     offset;
   const char *abr;
   const char *descr;
   const char *loc;
};

static _bsDateTimeTZDef _tzones[] = {
   {-14 * 3600,           "UTC-1400", "UTC-1400",                      "WorldWide"},
   {-13 * 3600,           "UTC-1300", "UTC-1300",                      "WorldWide"},
   {-12 * 3600,           "Y",        "Yankee Time Zone",              "Military" },
   {-11 * 3600,           "SST",      "Samoa Standard Time",           "Pacific" },
   {-10 * 3600,           "TAHT",     "Tahiti Time",                   "Pacific" },
   { -9 * 3600 - 30 * 60, "MART",     "Marquesas Time",                "Pacific" },
   { -9 * 3600,           "AKST",     "Alaska Standard Time"           "North America" },
   { -8 * 3600,           "PST",      "Pitcairn Standard Time",        "Pacific" },
   { -7 * 3600,           "MST",      "Mountain Standard Time",        "North America" },
   { -6 * 3600,           "EAST",     "Easter Island Standard Time",   "Pacific" },
   { -5 * 3600,           "CIST",     "Cayman Islands Standard Time",  "Caribbean" },
   { -4 * 3600,           "BOT",      "Bolivia Time",                  "South America" },
   { -3 * 3600,           "GFT",      "French Guiana Time",            "South America" },
   { -2 * 3600,           "CVT"       "Cape Verde Time",               "Africa" },
   { -1 * 3600,           "A",        "Alpha Time Zone",               "Military" },
   {         0,           "UTC",      "Coordinated Universal Time",    "WorldWide" },
   {  1 * 3600,           "CET",      "Central European Time ",        "Europe" },
   {  2 * 3600,           "SAST",     "South Africa Standard Time",    "Africa" },
   {  3 * 3600,           "TRT",      "Turkey Time",                   "Asia Europe" },
   {  4 * 3600,           "SCT",      "Seychelles Time",               "Africa" },
   {  5 * 3600,           "TMT",      "Turkmenistan Time",             "Asia" },
   {  5 * 3600 + 30 * 60, "IST",      "India Standard Time",           "Asia" },
   {  5 * 3600 + 45 * 60, "NPT",      "Nepal Time",                    "Asia" },
   {  6 * 3600,           "BST",      "Bangladesh Standard Time",      "Asia" },
   {  6 * 3600 + 30 * 60, "CCT",      "Cocos Islands Time",            "Indian Ocean" },
   {  7 * 3600,           "DAVT",     "Davis Time",                    "Antarctica" },
   {  8 * 3600,           "HKT",      "Hong Kong Time",                "Asia" },
   {  8 * 3600 + 30 * 60, "PYT",      "Pyongyang Time",                "Asia" },
   {  9 * 3600,           "JST",      "Japan Standard Time",           "Asia" },
   { 10 * 3600,           "VLAT",     "Vladivostok Time",              "Asia"},
   { 10 * 3600 + 30 * 60, "LHST",     "Lord Howe Standard Time",       "Australia" },
   { 11 * 3600,           "BST",      "Bougainville Standard Time",    "Pacific" },
   { 12 * 3600,           "FJT",      "Fiji Time",                     "Pacific" },
   { 12 * 3600 + 45 * 60, "CHAST",    "Chatham Island Standard Time",  "Pacific" },
   { 13 * 3600,           "PHOT",     "Phoenix Island Time",           "Pacific" },
   { 14 * 3600,           "WST",      "West Samoa Time",               "Pacific" },
   {0}  /* Sentinel */
};


#define SOURCE    "Mettle::DateTime"
#define STD_FMT   "%Y-%m-%d %H:%M:%S"

DateTime::DateTime() noexcept
{
}


DateTime::DateTime(const DateTime &inDateTime) noexcept
{
   this->date   = inDateTime.date;
   this->time   = inDateTime.time;
   _tzhour      = gltzhour;
   _tzmin       = gltzmin;
}


DateTime::DateTime(const Date &date) noexcept
{
   this->date = date;
   _tzhour    = gltzhour;
   _tzmin     = gltzmin;
}


DateTime::DateTime(const Time &time) noexcept
{
   this->time = time;
   _tzhour    = gltzhour;
   _tzmin     = gltzmin;
}


DateTime::DateTime(const Date &date, const Time &time) noexcept
{
   this->date = date;
   this->time = time;
   _tzhour    = gltzhour;
   _tzmin     = gltzmin;
}


DateTime::DateTime(const struct tm &inDateTime)
{
   this->date  = inDateTime;
   this->time  = inDateTime;
   _tzhour     = gltzhour;
   _tzmin      = gltzmin;
}


DateTime::DateTime(const char *yyyymmddhhmmss, const bool throwError)
{
   date = Date(yyyymmddhhmmss, throwError);
   time = Time(yyyymmddhhmmss + 8, throwError);

   if (yyyymmddhhmmss[14])
      _timeZoneFromStr(yyyymmddhhmmss + 14);
   else
   {
      _tzhour = gltzhour;
      _tzmin  = gltzmin;
   }
}


DateTime::DateTime(const char *yyyymmdd, const char *hhmmss, const char *tz, const bool throwError)
{
   date = Date(yyyymmdd, throwError);
   time = Time(hhmmss, throwError);

   if (tz && *tz)
      _timeZoneFromStr(tz);
   else
   {
      _tzhour = gltzhour;
      _tzmin  = gltzmin;
   }
}


DateTime::DateTime(const int32_t yyyymmdd, const int32_t hhmmss, const int8_t tzhour, const int8_t tzmin, const bool throwError)
{
   date = Date(yyyymmdd, throwError);
   time = Time(hhmmss, throwError);

   _validateTZ(tzhour, tzmin);

   _tzhour = tzhour;
   _tzmin  = tzmin;
}


DateTime::DateTime(const int32_t yyyy,   const int32_t mm, const int32_t dd,
                   const int32_t hh,     const int32_t mi, const int32_t ss,
                   const int8_t  tzhour, const int8_t  tzmin,
                   const bool throwError)
{
    date = Date(yyyy, mm, dd, throwError);
    time = Time(hh, mi, ss, throwError);

    _tzhour = tzhour == INT8_MIN ? gltzhour : tzhour;
    _tzmin  = tzmin  == INT8_MIN ? gltzmin  : tzmin;
}


DateTime::~DateTime() noexcept
{
}


int32_t DateTime::tzOffset() const noexcept
{
   return calcTZOffset(_tzhour, _tzmin);
}


const char *DateTime::tzAbr() const noexcept
{
   return getTZAbr(tzOffset());
}


DateTime &DateTime::tzSetHour(const int8_t tzhour)
{
   _validateTZ(tzhour, 0);

   _tzhour = tzhour;

   return *this;
}


DateTime &DateTime::tzSetMinute(const int8_t tzmin)
{
   _validateTZ(0, tzmin);

   _tzmin = tzmin;

   return *this;
}


DateTime &DateTime::tzConverTo(const int8_t tzhour, const int8_t tzmin)
{
   if (_tzhour == tzhour && _tzmin == tzmin)
      return *this;

   _validateTZ(tzhour, tzmin);

   if (_tzhour > tzhour)
   {
      sub(_tzhour - tzhour, Time::Hours);
      sub(_tzmin  - tzmin,  Time::Minutes);
   }
   else if (_tzhour < tzhour)
   {
      add(tzhour - _tzhour, Time::Hours);
      add(tzmin  - _tzmin,  Time::Minutes);
   }
   else
   {
      if (_tzmin > tzmin)
         sub(_tzmin  - tzmin,  Time::Minutes);
      else
         add(tzmin  - _tzmin,  Time::Minutes);
   }

   _tzhour = tzhour;
   _tzmin  = tzmin;

   return *this;
}


DateTime &DateTime::tzConverTo(const int32_t tzoffset)
{
   int32_t currOffset = tzOffset();

   if (tzoffset == currOffset)
      return *this;

   int8_t hr;
   int8_t mn;

   calcTZHourMin(tzoffset, hr, mn);

   return tzConverTo(hr, mn);
}


DateTime &DateTime::operator = (const DateTime &inDateTime) noexcept
{
   date    = inDateTime.date;
   time    = inDateTime.time;
   _tzhour = inDateTime._tzhour;
   _tzmin  = inDateTime._tzmin;

   return *this;
}


DateTime &DateTime::operator = (const Date &inDate) noexcept
{
   date = inDate;

   return *this;
}


DateTime &DateTime::operator = (const Time &inTime) noexcept
{
   time = inTime;

   return *this;
}


DateTime &DateTime::operator = (const struct tm &inDateTime)
{
   date = inDateTime;
   time = inDateTime;

   return *this;
}


DateTime &DateTime::operator = (const char *yyyymmddhhmmss)
{
   date = yyyymmddhhmmss;
   time = yyyymmddhhmmss + 8;

   if (yyyymmddhhmmss[14])
      _timeZoneFromStr(yyyymmddhhmmss + 14);
   else
   {
      _tzhour = gltzhour;
      _tzmin  = gltzmin;
   }

   return *this;
}


int DateTime::_compare(const void *obj) const
{
   int rc = date.compare(&((const DateTime *) obj)->date);

   if (rc == 0)
      return time.compare(&((const DateTime *) obj)->time);

   return rc;
}


bool operator < (const DateTime &dt1, const DateTime &dt2) noexcept
{
   int rc = dt1.date.compare(&dt2.date);

   if (rc == 0)
      return dt1.time.compare(&dt2.time) < 0;

   return rc < 0;
}


bool operator >  (const DateTime &dt1, const DateTime &dt2) noexcept
{
   int rc = dt1.date.compare(&dt2.date);

   if (rc == 0)
      return dt1.time.compare(&dt2.time) > 0 ;

   return rc > 0;
}


bool operator <= (const DateTime &dt1, const DateTime &dt2) noexcept
{
   int rc = dt1.date.compare(&dt2.date);

   if (rc == 0)
      return dt1.time.compare(&dt2.time) <= 0;

   return rc <= 0;
}


bool operator >= (const DateTime &dt1, const DateTime &dt2) noexcept
{
   int rc = dt1.date.compare(&dt2.date);

   if (rc == 0)
      return dt1.time.compare(&dt2.time) >= 0;

   return rc >= 0;
}


bool operator == (const DateTime &dt1, const DateTime &dt2) noexcept
{
   int rc = dt1.date.compare(&dt2.date);

   if (rc == 0)
      return dt1.time.compare(&dt2.time) == 0;

   return rc == 0;
}


bool operator != (const DateTime &dt1, const DateTime &dt2) noexcept
{
   int rc = dt1.date.compare(&dt2.date);

   if (rc == 0)
      return dt1.time.compare(&dt2.time) != 0;

   return rc != 0;
}


bool DateTime::validDateTime(const char *yyyymmddhhmmss) noexcept
{
   if (strlen(yyyymmddhhmmss) != 14)
      return false;

   char dtmp[9];
   char ttmp[7];

   strncpy(dtmp, yyyymmddhhmmss, 8);
   strncpy(ttmp, yyyymmddhhmmss + 8, 6);

   dtmp[8] = 0;
   ttmp[6] = 0;

   return Date::validDate(dtmp) && Time::validTime(ttmp);
}


DateTime DateTime::now() noexcept
{
#if defined(WINCE)

   __time64_t    now;
   struct tm     tm_rec;

   _time64(&now);
   _localtime64_s(&tm_rec, &now);

   Date d(tm_rec, false);
   Time t(tm_rec);

#else

   time_t      now;
   struct tm  *tm_rec;

   ::time(&now);
   tm_rec = ::localtime(&now);

   Date d(*tm_rec);
   Time t(*tm_rec);

#endif

   return DateTime(d, t);
}


bool DateTime::tryParse(const char *srcDate, const char *fmt, DateTime &outDate)
{
   DateTime tmp;

   try
   {
      tmp.parse(srcDate, fmt);
   }
   catch (xMettle &x)
   {
      return false;
   }

   outDate = tmp;

   return true;
}


bool DateTime::tryParse(const String &srcDate, const char *fmt, DateTime &outDate)
{
   return tryParse(srcDate.c_str(), fmt, outDate);
}


DateTime::operator struct tm () const noexcept
{
   struct tm tm_rec;

   memset(&tm_rec ,0, sizeof(struct tm));

   tm_rec.tm_year  = date.year() - 1900;
   tm_rec.tm_mon   = date.month() - 1;
   tm_rec.tm_mday  = date.day();

   tm_rec.tm_hour = time.hour();
   tm_rec.tm_min  = time.minute();
   tm_rec.tm_sec  = time.second();

   return tm_rec;
}


/*------------------------------------------------------------------------------
   %a abbreviated weekday name
   %A full weekday name
   %b abbreviated month name
   %B full month name
   %d day of month as decimal [01,31]
   %f Microsecond as decimal 000000, 000001, ..., 999999 !NOTE, not currently supported always prints 000000
   %H Hour (24-hour clock) as a decimal number [00,23]
   %I Hour (12-hour clock) as a decimal number [01,12]
   %j Day of the year as a decimal number [001,366]
   %m Month as a decimal number [01,12]
   %M Minute as a decimal number [00,59]
   %p Locale's equivalent of either AM or PM
   %S Second as a decimal number [00,59]
   %U Week number of the year (starting day in Sunday), 00, 01, ..., 53
   %w Weekday as a decimal number [0(Sunday),6]
   %W Week number of the year (starting day in Monday), 00, 01, ..., 53
   %y Year without century as a decimal number [00,99]
   %Y Year as a decimal number [0000,9999]
   %z UTC offset in the form +HHMM or -HHMM
   %Z Time zone name
   %% A literal "%" character
------------------------------------------------------------------------------*/
char *DateTime::format(char *dest, const char *fmt) const noexcept
{
   *dest = 0;

   if (*fmt == 0)
      return dest;

   char   *start = dest;
   char   *pos;
   char    jotter[32];
   int     var;

   for (pos = (char *) fmt; *pos; pos++, dest++)
   {
      if (*pos != '%')
      {
         *dest = *pos;
         continue;
      }

      pos++;

      switch (*pos)
      {
         case 'y' :
            sprintf(jotter, "%4.4d", date._year);

            *dest = jotter[2];
            dest++;
            *dest = jotter[3];
            break;

         case 'Y' :
            sprintf(dest, "%4.4d", date._year);
            dest += 3;
            break;

         case 'd' :
            sprintf(dest, "%2.2u", date._day);
            dest += 1;
            break;

         case 'm' :
            sprintf(dest, "%2.2d", date._month + 1);
            dest += 1;
            break;

         case 'M' :
            sprintf(dest, "%2.2d", time._minute);
            dest += 1;
            break;

         case 'a' :
            var = (int) date.dayOfWeek();
            memcpy(dest, Date::_weekDays[var], 3);
            dest += 2;
            break;

         case 'A':
            var = (int) date.dayOfWeek();
            strcpy(dest, Date::_weekDays[var]);
            dest += strlen(Date::_weekDays[var]) - 1;
            break;

         case 'b' :
            memcpy(dest, Date::_months[date._month], 3);
            dest += 2;
            break;

         case 'B' :
            strcpy(dest, Date::_months[date._month]);
            dest += strlen(Date::_months[date._month]) - 1;
            break;

         case 'H' :
            sprintf(dest, "%2.2u", time._hour);
            dest += 1;
            break;

         case 'I' :
            var = time._hour;

            if (var > 11)
               var -= 12;

            sprintf(dest, "%2.2d", var);
            dest += 1;
            break;

         case 'j' :
            sprintf(dest, "%3.3d", date.dayOfYear() + 1);
            dest += 2;
            break;

         case 'p' :
            if (time._hour > 11)
               strcpy(dest, "PM");
            else
               strcpy(dest, "AM");

            dest += 1;
            break;

         case 'S' :
            sprintf(dest, "%2.2d", time._second);
            dest += 1;
            break;

         case 'w' :
            sprintf(dest, "%d", (int) date.dayOfWeek());
            break;

         case 'U':
            var = date.dayOfYear();

            if (var != 0)
            {
               if (date.dayOfWeek() != 0)
                  var -= date.dayOfWeek();

               if (var < 0)
                  var = 0;
               else
                  var /= 7;
            }

            sprintf(dest, "%2.2d", var);
            dest += 1;
            break;

         case 'W':
            var = date.dayOfYear();

            if (var != 0)
            {
               if (date.dayOfWeek() != 1)
                  var -= date.dayOfWeek();

               if (var < 0)
                  var = 0;
               else
                  var /= 7;
            }

            sprintf(dest, "%2.2d", var);
            dest += 1;
            break;

         case 'f':
            strcpy(dest, "000000"); // NOT SUPPORTED
            dest += 5;
            break;

         case '%' :
            *dest = '%';
            break;

         case 'z' :
            sprintf(dest, "%s%2.2d%2.2d", _tzhour >= 0 ? "+" : "", _tzhour, _tzmin);
            dest += 4;
            break;

         case 'Z' :
            if (_tzhour == gltzhour && _tzmin == gltzmin)
            {
               strcat(dest, gltzabr);
               dest += strlen(gltzabr) - 1;
            }
            else
            {
               strcpy(jotter, getTZAbr(calcTZOffset(_tzhour, _tzmin)));
               strcat(dest, jotter);
               dest += strlen(jotter) - 1;
            }

            break;

         default :
            *dest = '?';
            break;
      }
   }

   *dest = 0;

   return start;
}


String &DateTime::format(String &dest, const char *fmt) const
{
   unsigned int  percCnt = 0;
   char         *ptr     = (char *) fmt;

   for (; *ptr; ptr++)
      if (*ptr == '%')
         percCnt++;

   percCnt *= 8;
   percCnt += ptr - fmt;
   percCnt += 32;

   dest.setSize(percCnt, true);

   format((char *) dest.c_str(), fmt);

   return dest;
}


int DateTime::_parseInt(const char fc, const int len, const char *ptrd, const int min, const int max)
{
   char jotter[8];
   int  idx;

   for (idx = 0; idx < len; idx++)
   {
      if (ptrd[idx] == 0)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse source date. Unexpected end of string while reading [%%%c] formatter.", fc);

      jotter[idx] = ptrd[idx];
   }

   jotter[idx] = 0;

   if (!Common::stringIsUnsigned(jotter))
      throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse source date. Non integer value detected while reading [%%%c] formatter.", fc);

   idx = atoi(jotter);

   if (idx < min || idx > max)
      throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse source date. Integer value must be between (%d...%d) for [%%%c] formatter.", min, max, fc);

   return idx;
}


DateTime &DateTime::parse(const char *srcDate, const char *fmt)
{
   if (*srcDate == 0)
      throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date with empty source date.");

   if (*fmt == 0)
      throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date with empty format.");

   DateTime tmp;

   char       *ptrd        = (char *) srcDate;
   char       *ptrf        = (char *) fmt;
   char        pmam        = 0;
   bool        reqCentuary = false;
   int16_t     resDoY      = -1;
   uint16_t    year        = 0;
   uint8_t     month       = 0;
   uint8_t     day         = 0;
   uint8_t     hour        = 0;
   uint8_t     min         = 0;
   uint8_t     sec         = 0;
   int8_t      tzhour      = gltzhour;
   int8_t      tzmin       = gltzmin;

   for (; *ptrf && *ptrd; ptrf++, ptrd++)
   {
      if (*ptrf != '%')
      {
         if (*ptrf == *ptrd)
            continue;

         throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date, found unexpected character [%c] at position [srcDate:%d, fmt:%d].", *ptrd, ptrd - srcDate, ptrf - fmt);
      }

      ptrf++;

      switch (*ptrf)
      {
         case 'y' :
            year        = _parseInt(*ptrf, 2, ptrd, 0, 99);
            reqCentuary = true;
            ptrd += 1;
            break;

         case 'Y' :
            year        = _parseInt(*ptrf, 4, ptrd, 0, 9999);
            reqCentuary = false;
            ptrd += 3;
            break;

         case 'd' :
            day   = _parseInt(*ptrf, 2, ptrd, 1, 31);
            ptrd += 1;
            break;

         case 'm' :
            month = _parseInt(*ptrf, 2, ptrd, 1, 12) - 1;
            ptrd += 1;
            break;

         case 'M' :
            min   = _parseInt(*ptrf, 2, ptrd, 0, 59);
            ptrd += 1;
            break;

         case 'b' :
            for (month = 0; month < 12; month++)
               if (0 == String::strnicmp(ptrd, Date::_months[month], 3))
                  break;

            if (month >= 12)
               throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse source date. Invalid short month code detected while reading [%%%c] formatter.", *ptrf);

            ptrd += 2;
            break;

         case 'B' :
            for (month = 0; month < 12; month++)
               if (0 == String::strnicmp(ptrd, Date::_months[month], strlen(Date::_months[month])))
                  break;

            if (month >= 12)
               throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse source date. Invalid long month code detected while reading [%%%c] formatter.", *ptrf);

            ptrd += strlen(Date::_months[month]);
            break;

         case 'H' :
            hour  = _parseInt(*ptrf, 2, ptrd, 0, 23);
            ptrd += 1;
            break;

         case 'I' :
            hour  = _parseInt(*ptrf, 2, ptrd, 0, 11);
            ptrd += 1;
            break;

         case 'j' :
            resDoY = _parseInt(*ptrf, 3, ptrd, 1, 365);
            ptrd += 2;
            break;

         case 'p' :
            if (0 == strncmp(ptrd, "AM", 2))
               pmam = 1;
            else if (0 == strncmp(ptrd, "PM", 2))
               pmam = 2;
            else
               throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date. Invalid AM/PM string detected while reading [%%%c] formatter.", *ptrf);

            ptrd += 1;
            break;

         case 'S' :
            sec   = _parseInt(*ptrf, 2, ptrd, 0, 59);
            ptrd += 1;
            break;

         case 'f' :
            _parseInt(*ptrf, 6, ptrd, 0, 999999); // sorry not supported will at least validate.
            ptrd += 5;
            break;

         case '%' :
            if (*ptrd != '%')
               throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date, invalid format character [%c] found at position [%d].", *ptrf, ptrf - fmt);
            ptrd += 1;
            break;

         case 'z' :
            if (*ptrd != '+' && *ptrd != '-')
               throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date, timezone must start with a +/-, invalid character [%c] found at position [%d].", *ptrf, ptrf - fmt);

            if (!_validateTZ(ptrd, false, &tzhour, &tzmin))
               throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date, timezone not valid at position [%d].", ptrf - fmt);

            ptrd += 4;
            break;

         case 'W' :
         case 'w' :
         case 'U' :
         case 'Z' :
         case 'a' :
         case 'A' :
         default:
            throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date, unsupported format character [%c] found at position [%d].", *ptrf, ptrf - fmt);
      }
   }

   if (*ptrf && !*ptrd)
      throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date, source date ended before the entire format could be parsed.  Last format position [%d], remainder [%s]", ptrf - fmt, ptrf);

   if (reqCentuary) // assume current centuary
   {
      Date d = Date::now();
      char jotter[8];

      sprintf(jotter,     "%4.4u", date.year());
      sprintf(jotter + 2, "%2.2u", year);

      year = atoi(jotter);
   }

   if (resDoY > 0)
   {
      unsigned int dim;

      for (month = 0; month < 12; month++)
      {
         dim = Date::daysInMonth(year, month);

         if (resDoY > dim)
         {
            resDoY -= dim;
            continue;
         }

         break;
      }

      if (month >= 12 || resDoY > 31)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Cannot parse date, source date Day of Year format [%%j] parse failed!");

      day = resDoY;
   }

   if (pmam == 2 && hour < 12)
      hour += 12;

   assign(year, month + 1, day, hour, min, sec, tzhour, tzmin);

   return *this;
}


DateTime &DateTime::parse(const String &srcDate, const char *fmt)
{
   return parse(srcDate.c_str(), fmt);
}

char *DateTime::toString(char *yyyymmddhhmmss, const bool withTZ) const noexcept
{
   if (isNull())
      yyyymmddhhmmss[0] = 0;
   else
      sprintf(yyyymmddhhmmss,
              "%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d",
              date._year,
              date._month + 1,
              date._day,
              time._hour,
              time._minute,
              time._second);

   if (withTZ)
   {
      sprintf(yyyymmddhhmmss + 14,
              "%s%2.2d%2.2d",
              _tzhour >= 0 ? "+" : "",
              _tzhour,
              _tzmin);
   }

   return yyyymmddhhmmss;
}


DateTime &DateTime::add(const unsigned int amount, const Date::PeriodLegend legend) noexcept
{
   date.add(amount, legend);

   return *this;
}


DateTime &DateTime::add(const unsigned int amount, const Time::PeriodLegend legend) noexcept
{
   time.add(amount, legend, &date);

   return *this;
}


DateTime &DateTime::sub(const unsigned int amount, const Date::PeriodLegend legend) noexcept
{
   date.sub(amount, legend);

   return *this;
}


DateTime &DateTime::sub(const unsigned int amount, const Time::PeriodLegend legend) noexcept
{
   time.sub(amount, legend, &date);

   return *this;
}


bool DateTime::isNull() const noexcept
{
   return date.isNull();
}


void DateTime::nullify() noexcept
{
   date.nullify();
   time.nullify();
}


void DateTime::clear() noexcept
{
   nullify();
}


void DateTime::assign(const unsigned int year,  const unsigned int month,   const unsigned int day,
                      const unsigned int hours, const unsigned int minutes, const unsigned int seconds,
                      const int32_t tzhour,
                      const int32_t tzmin)
{
   date.assign(year, month, day);
   time.assign(hours, minutes, seconds);

   if (tzhour == INT8_MIN || tzmin == INT8_MIN)
   {
      _tzhour = gltzhour;
      _tzmin  = gltzmin;
   }
   else
   {
      _validateTZ(tzhour, tzmin);
      _tzhour = tzhour;
      _tzmin  = tzmin;
   }
}


const char *DateTime::stdDateTimeFormat()
{
   return STD_FMT;
}


std::ostream &operator << (std::ostream &output, const DateTime &dt) noexcept
{
   char jot[16];

   dt.format(jot, STD_FMT);

   output << jot;

   return output;
}


std::istream &operator >> (std::istream &input, DateTime &dt)
{
   char *p;

   input >> p;

   dt.parse(p, STD_FMT);

   return input;
}


void DateTime::_timeZoneFromStr(const char *tz)
{
   _validateTZ(tz);
}


bool DateTime::_validateTZ(const char *tz, const bool throwError, int8_t *outHour, int8_t *outMin)
{
   if (strlen(tz) < 5)
      if (throwError)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Invalid timezone string passed '%s'", tz);
      else
         return false;

   bool neg;

   if (*tz == '+')
      neg = false;
   else if (*tz == '-')
      neg = true;
   else
      if (throwError)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Invalid timezone string passed '%s', expect + or - as first character", tz);
      else
         return false;

   int8_t tzhour = (int8_t) _parseInt('H', 2, tz + 1,   0, 14);
   int8_t tzmin  = (int8_t) _parseInt('M', 2, tz + 3,   0, 59);

   if (neg)
      tzhour = 0 - tzhour;

   if (!_validateTZ(tzhour, tzmin, throwError))
      return false;

   if (outHour)
      *outHour = tzhour;

   if (outMin)
      *outMin = tzmin;

   return true;
}


bool DateTime::_validateTZ(const int8_t tzhour, const int8_t tzmin, const bool throwError)
{
   if (tzhour < -14 or tzhour > 14)
      if (throwError)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Invalid timezone hour [%d], value should be >= -14 and <= 14", tzhour);
      else
         return false;

   if (tzmin < 0 or tzmin > 59)
      if (throwError)
         throw xMettle(__FILE__, __LINE__, SOURCE, "Invalid timezone minute [%d], value should be >= 0 and <= 59", tzmin);
      else
         return false;

   return true;
}


void DateTime::setLocalTZ(const char *tz)
{
   if (tz == 0 || *tz == 0)
   {
      gltzoffset = DateTime::_osLocalTZ(&gltzhour, &gltzmin);
   }
   else
   {
      DateTime::_validateTZ(tz, true, &gltzhour, &gltzmin);

      gltzoffset = DateTime::calcTZOffset(gltzhour, gltzmin);
   }

   strcpy(gltzabr, DateTime::getTZAbr(gltzoffset));
}


void DateTime::setLocalTZ(const int8_t tzhour, const int8_t tzmin)
{
   if (tzhour == INT8_MIN || tzmin == INT8_MIN)
   {
      gltzoffset = DateTime::_osLocalTZ(&gltzhour, &gltzmin);
   }
   else
   {
      DateTime::_validateTZ(gltzhour, gltzmin, true);

      gltzhour   = tzhour;
      gltzmin    = tzmin;
      gltzoffset = DateTime::calcTZOffset(gltzhour, gltzmin);
   }

   strcpy(gltzabr, DateTime::getTZAbr(gltzoffset));
}


void DateTime::setLocalTZ(const int32_t tzoffset)
{
   if (tzoffset == INT32_MIN)
   {
      gltzoffset = DateTime::_osLocalTZ(&gltzhour, &gltzmin);
   }
   else
   {
      int8_t hr = 0;
      int8_t mn = 0;

      DateTime::calcTZHourMin(tzoffset, hr, mn);

      DateTime::_validateTZ(hr, mn, true);

      gltzoffset = tzoffset;
      gltzhour   = hr;
      gltzmin    = mn;
   }

   strcpy(gltzabr, DateTime::getTZAbr(gltzoffset));
}


char *DateTime::getLocalTZAbr(char *tz) noexcept
{
   if (tz)
      strcpy(tz, gltzabr);

   return gltzabr;
}


int32_t DateTime::getLocalTZ(int8_t *tzhour, int8_t *tzmin) noexcept
{
   if (tzhour)
      *tzhour = gltzhour;

   if (tzmin)
      *tzmin = gltzmin;

   return gltzoffset;
}


int32_t DateTime::calcTZOffset(const int8_t tzhour, const int8_t tzmin) noexcept
{
   if (tzhour == 0)
   {
      if (tzmin > 0)
         return tzmin * 60;
      else
         return 0 - (tzmin * 60);
   }

   if (tzhour > 0)
      return (tzhour * 3600) + (tzmin * 60);

   return (tzhour * 3600) - (tzmin * 60);
}


void DateTime::calcTZHourMin(const int32_t tzoffset, int8_t &tzhour, int8_t &tzmin) noexcept
{
   if (tzoffset == INT32_MIN)
   {
      tzhour  = INT8_MIN;
      tzmin   = INT8_MIN;

      return;
   }

   double tz = tzoffset / 3600;
   double mn;
   double hr = modf(tz, &mn);

   tzhour = (int8_t) hr;
   tzmin  = (int8_t) ((mn * 100) * 6);
}


const char *DateTime::getTZAbr(const int32_t tzoffset) noexcept
{
   unsigned int      idx  = 0;
   _bsDateTimeTZDef *curr = &_tzones[0];
   _bsDateTimeTZDef *prev = 0;

   while (curr)
   {
      if (tzoffset == curr->offset)
         return curr->abr;

      if (tzoffset < curr->offset)
      {
         if (prev == 0)
            return "???";

         if (tzoffset > prev->offset)
         {
            int diffp = tzoffset - prev->offset;
            int diffc = curr->offset - tzoffset;

            if (diffp < diffc)
               return prev->abr;

            return curr->abr;
         }
      }

      curr = &_tzones[++idx];
   }

   return "???";
}


int32_t DateTime::_osLocalTZ(int8_t *tzhour, int8_t *tzmin) noexcept
{
   time_t t     = ::time(0);
   struct tm lt = {0};

   if (0 == localtime_r(&t, &lt))
   {
      tzhour = 0;
      tzmin  = 0;
      return 0;
   }

   if (tzhour)
      DateTime::calcTZHourMin(lt.tm_gmtoff, *tzhour, *tzmin);

   return lt.tm_gmtoff;
}


#undef STD_FMT
#undef SOURCE

}}
