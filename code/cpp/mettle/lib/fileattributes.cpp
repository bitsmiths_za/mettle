/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/fileattributes.h"

#include "mettle/lib/platform.h"
#include "mettle/lib/macros.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#if defined(OS_MS_WINDOWS)

   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>

   #if defined (WINCE)
      #include <string.h>
   #else
      #include <io.h>
   #endif

   #if defined(COMPILER_MSC)

      #define S_ISDIR(m) (((m) & _S_IFMT) == _S_IFDIR)
      #define S_ISREG(m) (((m) & _S_IFMT) == _S_IFREG)


   #endif


#else

   #include <unistd.h>
   #include <dirent.h>

#endif

#if !defined(WINCE)

   #include <sys/types.h>
   #include <sys/stat.h>

#endif


namespace Mettle { namespace Lib {

FileAttributes::FileAttributes(const char *path)
{
#if defined(WINCE)
   fileName   = path;
   fileSize   = 0;
   fileType   = Unknown;
#else
   struct stat  sbuf;

   if (stat(path, &sbuf) == -1)
      throw xMettle(__FILE__, __LINE__, "'stat()' failed on file (%s)", path);

   fileName     = path;
   fileSize     = sbuf.st_size;
   lastAccessed = DateTime(*localtime(&sbuf.st_atime));
   lastModified = DateTime(*localtime(&sbuf.st_mtime));

   if (S_ISDIR(sbuf.st_mode))
      fileType = Directory;
   else if (S_ISREG(sbuf.st_mode))
      fileType = Regular;
#if defined (S_ISLNK)
   else if (S_ISBLK(sbuf.st_mode))
      fileType = Special;
#endif
#if defined (S_ISCHR)
   else if (S_ISCHR(sbuf.st_mode))
      fileType = Special;
#endif
#if defined (S_ISFIFO)
   else if (S_ISFIFO(sbuf.st_mode))
      fileType = Special;
#endif
#if defined (S_ISLNK)
   else if (S_ISLNK(sbuf.st_mode))
      fileType = SymbolicLink;
#endif
#if defined (S_ISSOCK)
   else if (S_ISSOCK(sbuf.st_mode))
      fileType = Socket;
#endif
   else
      fileType = Unknown;
#endif
}


FileAttributes::~FileAttributes()
{
}


static int _sortByLastModified_Qsort(const FileAttributes **a, const FileAttributes **b)
{
   int i;

   if ((i = (*a)->lastModified.date.compare(&(*b)->lastModified.date)) != 0)
      return i;

   return (*a)->lastModified.time.compare(&(*b)->lastModified.time);
}

static int _sortByFileName_Qsort(const FileAttributes **a, const FileAttributes **b)
{
   return (*a)->fileName.compare((*a)->fileName);
}

void FileAttributes::List::sortByLastModified()
{
   quickSort(_sortByLastModified_Qsort);
}


void FileAttributes::List::sortByFileName()
{
   quickSort(_sortByFileName_Qsort);
}


}}
