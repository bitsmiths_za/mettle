/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/stringbuilder.h"

#include <stdlib.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "mettle/lib/macros.h"
#include "mettle/lib/uchar.h"
#include "mettle/lib/ustring.h"

#define SOURCE "Mettle::StringBuilder"

namespace Mettle { namespace Lib {

#define GROWBY  1024

StringBuilder::StringBuilder() noexcept
{
   value = 0;
   used  = 0;
   size  = 0;
}

StringBuilder::~StringBuilder() noexcept
{
   if (value)
      free(value);
}

bool StringBuilder::isEmpty() const noexcept
{
   return value == 0 || *value == 0;
}

StringBuilder &StringBuilder::clear() noexcept
{
   if (value)
      *value = 0;

   used  = 0;

   return *this;
}

StringBuilder &StringBuilder::purge() noexcept
{
   if (value)
      free(value);

   value = 0;
   used  = 0;
   size  = 0;

   return *this;
}

StringBuilder &StringBuilder::add(const char ch)
{
   if (used + 2 >= size)
   {
      char *t = (char *) realloc(value, size + GROWBY);

      if (!t)
         throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, SOURCE, "add - Cannot allocate memory [%u] bytes", size + GROWBY);

      value = t;
      size  += GROWBY;
   }

   value[used++] = ch;
   value[used]   = 0;

   return *this;
}

StringBuilder &StringBuilder::add(const char *str)
{
   unsigned int len    = strlen(str);
   unsigned int needed = used + len + 1;

   if (len < 1)
      return *this;

   add(needed);

   memcpy(value + used, str, len);
   used += len;
   value[used] = 0;

   return *this;
}

StringBuilder &StringBuilder::add(const char *str, const unsigned int len)
{
   unsigned int needed = used + len + 1;

   if (len < 1)
      return *this;

   add(needed);

   memcpy(value + used, str, len);
   used += len;
   value[used] = 0;

   return *this;
}

void StringBuilder::add(const unsigned int needed)
{
   if (used + needed >= size)
   {
      size += GROWBY;

      if (used + needed >= size)
      {
         int increments = (used + needed - size) / GROWBY;

         increments++;

         size += GROWBY * increments;
      }

      char *t = (char *) realloc(value, size);

      if (!t)
         throw xMettle(__FILE__, __LINE__, SOURCE, "add - Cannot allocate memory [%u] bytes", size);

      value = t;
   }
}

StringBuilder &StringBuilder::insert(const char *str, const unsigned int pos)
{
   if (pos > used)
      throw xMettle(__FILE__, __LINE__, SOURCE, "Insert - Position [%u] is greater than the used size [%u]", pos, used);

   unsigned int  len     = strlen(str);
   unsigned int  needed  = used + len + 1;
   char         *posPtr;
   char         *toPtr;
   char         *endPtr;

   if (len < 1)
      return *this;

   add(needed);

   posPtr  = value  + pos;
   toPtr   = posPtr + len;
   endPtr  = value  + used;

   memmove(toPtr, posPtr, endPtr - posPtr);
   memcpy(posPtr, str, len);

   used += len;

   value[used] = 0;

   return *this;
}

StringBuilder &StringBuilder::addFormat(const char *fmt, ...)
{
   va_list       ap;
   unsigned int  avail;
   unsigned int  maxGrowth = 10;
   unsigned int  fmtSize;
   char         *ptr;

   if ((avail = size - used) < 32)
   {
      add(size + 32);
      avail = size - used;
   }

   do
   {
      ptr = value + used;

      va_start(ap, fmt);

      fmtSize = vsnprintf(ptr, avail - 1, fmt, ap);

      va_end(ap);

      if (fmtSize != -1)
         break;

      add(size + GROWBY);
      avail = size - used;

   } while (maxGrowth--);

   if (size == -1)
   {
      used        = size - 1;
      value[used] = 0;
   }
   else
   {
      used = used + fmtSize;
   }

   return *this;
}

StringBuilder &StringBuilder::eat(char *str) noexcept
{
   purge();

   value = str;
   used  = strlen(str);
   size  = used + 1;

   return *this;
}

StringBuilder &StringBuilder::eat(String &str) noexcept
{
   purge();

   value = str._str;
   used  = str._strLen;
   size  = str._bytesUsed;

   str._str        = 0;
   str._strLen     =
   str._bytesUsed  = 0;

   return *this;
}

StringBuilder &StringBuilder::eat(UString &str) noexcept
{
   purge();

   value = str._str;
   used  = str._byteLen;
   size  = str._bytesUsed;

   str._str        = 0;
   str._strLen     =
   str._byteLen    =
   str._bytesUsed  = 0;

   return *this;
}

StringBuilder &StringBuilder::eat(StringBuilder &str) noexcept
{
   purge();

   value = str.value;
   used  = str.used;
   size  = str.size;

   str.value = 0;
   str.used  =
   str.size  = 0;

   return *this;
}

char *StringBuilder::poop() noexcept
{
   char *t = value;

   value = 0;
   used  = 0;
   size  = 0;

   return t;
}

StringBuilder &StringBuilder::join(const char *first, ...)
{
   va_list    ap;
   const char *ptr;

   va_start(ap, first);

   add(first);

   while (1)
   {
      if ((ptr = va_arg(ap, const char*)) == 0)
         break;

      add(ptr);
   }

   va_end(ap);

   return *this;
}


StringBuilder &StringBuilder::join(const String::List &slist)
{
   _FOREACH(String, s, slist)
      add(s.obj->c_str());

   return *this;
}


StringBuilder &StringBuilder::joinSep(const char *sep, const char *first, ...)
{
   va_list    ap;
   const char *ptr;

   va_start(ap, first);

   add(first);

   while (1)
   {
      if ((ptr = va_arg(ap, const char*)) == 0)
         break;

      add(sep);
      add(ptr);
   }

   va_end(ap);

   return *this;
}


StringBuilder &StringBuilder::joinSep(const char *sep, const String::List &slist)
{
   _FOREACH(String, s, slist)
   {
      if (s.idx)
         add(sep);

      add(s.obj->c_str());
   }

   return *this;
}

StringBuilder &StringBuilder::replace(const char *source, const char *dest)
{
   if (source == 0 || *source == 0)
      return *this;

   unsigned int   lenSrc  = strlen(source);
   unsigned int   lenDest = strlen(dest);
   int            lenDiff = lenDest - lenSrc;
   char          *ptr     = value;

   for (; *ptr; UChar::u8_inc(ptr))
   {
      if (!UChar::u8_is_ascii(ptr))
         continue;

      if (strncmp(ptr, source, lenSrc))
         continue;

      if (lenDiff == 0)
      {
         memcpy(ptr, dest, lenDest);
         ptr += lenDest;
         continue;
      }

      if (lenDiff < 0)
      {
         memcpy(ptr, dest, lenDest);
         memmove(ptr + lenDest, ptr + lenSrc, (used - (ptr - value)) + lenDiff);
         ptr  += lenDest;
         used += lenDiff;
         continue;
      }

      add((unsigned int ) lenDiff);
      memmove(ptr + lenDest, ptr + lenSrc, (used - (ptr - value)) + lenDest);
      memcpy(ptr, dest, lenDest);
      used += lenDiff;
      ptr  += lenDest;
   }

   value[used] = 0;

   return *this;
}


StringBuilder &StringBuilder::replaceAt(const unsigned int  pos,
                                        const unsigned int  posLen,
                                        const char         *newStr,
                                        const unsigned int  newLen)
{
   if (pos >= used)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, SOURCE, "Cannot replace a portion of the string that is out of bounds: %u >= %u ", pos, size);

   int   lenDiff = newLen - posLen;
   char *ptr     = value + pos;

   if (lenDiff == 0)
   {
      memcpy(ptr, newStr, newLen);
      return *this;
   }

   if (lenDiff < 0)
   {
      memcpy(ptr, newStr, newLen);
      memmove(ptr + newLen, ptr + posLen, (used - (ptr - value)) + lenDiff);
      value[used] = 0;
      return *this;
   }

   add((unsigned int ) lenDiff);
   memmove(ptr + newLen, ptr + posLen, (used - (ptr - value)) + newLen);
   memcpy(ptr, newStr, newLen);
   value[used] = 0;

   return *this;
}


#undef GROWBY

}}

#undef SOURCE
