/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_APPENDLIST_H_
#define __METTLE_LIB_APPENDLIST_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/xmettle.h"

#include <stddef.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>

#if defined(OS_MS_WINDOWS)

   #if COMPILER_MSC > 6

      #include "mettle/lib/common.h"

   #endif

#endif

#define __HEADER_SOURCE_ "Mettle::AppendList"

#define DECLARE_APPENDLIST_LIST_CLASS(cls) class List:public Mettle::Lib::AppendList<cls> {}

namespace Mettle { namespace Lib {

/** \class AppendList appendlist.h mettle/lib/appendlist.h
 * \brief A template list class for c structs.
 *
 *  This template class provides a list class for standard c structs.  For classes use the Collection
 *  template class instead.  This class remains here for backward compatibility with older c program
 *  integrations.
 */
template <class type> class AppendList
{
public:

   /** \brief Public access is granted to the internal pointer list for reading.
    *  \warning Do not modify this member.
    */
   type *_list;

   /** \brief Constructor.
    */
   AppendList() noexcept;

   /** \brief Destructor.
    */
   virtual ~AppendList() noexcept;

   /** \brief Allocates the internal array list to length \p size.  Usually this method
    *          is not required to be called, however if you want to pre-allocate a large array this
    *          method can provide a performance boost.
    * \param size the total number of array elements to be created.
    * \warning The entire collection is Purged before the operation is begun, thus you will lose any collection contents.
    * \throws xMettle if out of memory.
    */
   void allocate(const unsigned int size);

   /** \brief Appends an object to the append list.  The method auto increments the internal array size
    *          by the value of \p incVal.  Note that if NULL is passed, the record is memset to zero.
    * \param rec the record to be appended to the append list, NULL is acceptable.
    * \param incVal the rate at which to grow the append list when it runs out of space.
    * \returns the record that was passed in via \p rec.
    * \throws xMettle if out of memory.
    */
   void append(const type *rec = 0, const unsigned int incVal = 16);

   /** \brief Appends an object to the append list.  The method auto increments the internal array size
    *          by the value of \p incVal.
    * \param rec the record to be appended to the append list.
    * \param incVal the rate at which to grow the append list when it runs out of space.
    * \returns the record that was passed in via \p rec.
    * \throws xMettle if out of memory.
    */
   void append(const type &rec, const unsigned int incVal = 16);

   /** \brief Removes the record at the specified index.
    * \param index the position of the object to be removed.
    * \returns true if the remove was successful or false if either \p index1 or \p index2 was out of range.
    */
   bool remove(const unsigned int index) noexcept;

   /** \brief Removes the record from the back of the append list.
    * \returns true if the remove was successful, or false if the list is empty.
    */
   bool remove() noexcept;

   /** \brief Clears out the array, and set the size to zero but does not free any memory.
    */
   void clear() noexcept;

   /** \brief Free's the entire list from memory.
    */
   void purge() noexcept;

   /** \brief The total count of the number of records in the list.
    * \returns the number of records in the list.
    */
   unsigned int count()      const noexcept;

   /** \brief The total array element size of the list.
    * \returns the total array element size.
    */
   unsigned int arrCount()   const noexcept;

   /** \brief Calculates the memory size of the lists records.
    * \returns the calculates size.
    */
   unsigned int memUsed()    const noexcept;

   /** \brief The size of each record.
    * \returns the size of a single record, ie. sizeof(type)
    */
   unsigned int recordSize() const noexcept { return sizeof(type); }

   /** \brief A default quick sort method for this append list.
     * \param sortFunc a function pointer to be used to sort this collection with.  This is a compare function.
    */
   void quickSort(int (*sortFunc)(const type *rec, const type *rec2)) noexcept;

   /** \brief A default binary search method for this append list.  If the list is not sorted, a call to Qsort should be made first.
     * \param searchRec a template of the record to be searched for.
     * \param sortFunc a function pointer to be used to binary search this list with.  This is a compare function.
     * \returns the record if it is found or NULL if it was not.
    */
   type *binarySearch(const type *searchRec, int (*sortFunc)(const type *rec, const type *rec2)) const noexcept;

   /** \brief Gets the last record in the list
    * \returns the last record in the list or NULL if the append list is empty.
    */
   type *last() const noexcept;

   /** \brief Operator overload for [] to get the record at the specified index.
    * \param index the index of the record to get.
    * \returns the record at position \p index.
    * \warning This method has undefined behaviour if \p index is out of bounds.
    */
   type &operator[](const unsigned index) const noexcept;

   /** \brief Gets the record at the specified index.
    * \param index the index of the record to get.
    * \returns the record at position \p index.
    * \warning This method has undefined behaviour if \p index is out of bounds.
    */
   type *get(const unsigned index)        const noexcept;

   /** \brief Sets the record at position \p index.
    * \param index the index of the object to be set.
    * \param val the object to be set, if this is a NULL pointer the record is memet to zero at \p index.
    * \returns the object passed in via \p val.
    * \warning This method has undefined behaviour if \p index is out of bounds.
    */
   type *set(const unsigned int index, type *val) noexcept;

   /** \brief Gives the internal memory pointer and list count to the out reference pointers provided.
    * \param list the destination out pointer reference.
    * \param listCount the destination out list count size.
    * \warning The calling code is responsible for freeing up the memory assigned to \p list.
    */
   void poop(type *&list, unsigned int &listCount);

   /** \brief Gives the internal memory pointer and list count to the out reference pointers provided.
    * \param list the destination out pointer reference.
    * \param listCount the destination out list count size.
    * \warning The calling code is responsible for freeing up the memory assigned to \p list.
    */
   void poop(type *&list, int &listCount) ;

   /** \brief Purges itself and then takes ownership of the passed in array.
    * \param list the source array to be taken over.
    * \param listCount the source array size to be take over.
    * \warning The calling code is no longer responsible for freeing up the memory assigned by \p list.
    */
   void eat(type *&list, unsigned int &listCount)  noexcept;

   /** \brief Purges itself and then takes ownership of the passed in array.
    * \param list the source array to be taken over.
    * \param listCount the source array size to be take over.
    * \warning The calling code is no longer responsible for freeing up the memory assigned by \p list.
    */
   void eat(type *&list, int  &listCount)  noexcept;

   /** \brief This method works like eat, but the calling code is still responsible for freeing the memory.
    * \param list the source array to be referenced.
    * \param listCount the source array size to be referenced.
    */
   void reference(const type *list, const unsigned int listCount) noexcept;

   /** \brief This method works like eat, but the calling code is still responsible for freeing the memory.
    * \param list the source array to be referenced.
    * \param listCount the source array size to be referenced.
    */
   void reference(const type *list, const int  listCount) noexcept;


private:

   unsigned int        _arrayCnt;   // number of array elements
   unsigned int        _listCnt;    // number of used array elements
};


template <class type>
AppendList<type>::AppendList() noexcept
{
   _list      = 0;
   _arrayCnt  =
   _listCnt   = 0;
}

template <class type>
AppendList<type>::~AppendList() noexcept
{
   if (_arrayCnt && _list)
      free(_list);
}

template <class type>
unsigned int AppendList<type>::count() const noexcept
{
   return _listCnt;
}

template <class type>
unsigned int AppendList<type>::arrCount() const noexcept
{
   return _arrayCnt;
}

template <class type>
unsigned int AppendList<type>::memUsed() const noexcept
{
   return _arrayCnt * sizeof(type);
}

template <class type>
void AppendList<type>::clear() noexcept
{
   _listCnt = 0;
}

template <class type>
void AppendList<type>::purge() noexcept
{
   if (_arrayCnt && _list)
      free(_list);

   _list       = 0;
   _arrayCnt   =
   _listCnt    = 0;
}

template <class type>
void AppendList<type>::allocate(const unsigned int size)
{
  purge();

  if (size < 1)
     return;

  _listCnt = _arrayCnt = size;

   if ((_list = (type*) realloc(_list, sizeof(type) * size)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, __HEADER_SOURCE_, "Couldn not allocate [%u] bytes", sizeof(type) * size);

   memset(_list, 0, sizeof(type) * size);
}

template <class type>
void AppendList<type>::append(const type *rec, const unsigned int incVal)
{
   if (_listCnt == _arrayCnt)
   {
      type *memPtr = _list;

      _arrayCnt += incVal;

      if ((_list = (type*) realloc(_list, sizeof(type) * _arrayCnt)) == 0)
      {
         _list = memPtr;
         throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, __HEADER_SOURCE_, "Couldn not allocate [%u] bytes", sizeof(type) * _arrayCnt);
      }
   }
   else if (_arrayCnt < _listCnt)
      throw xMettle(__FILE__, __LINE__, __HEADER_SOURCE_, "Cannot append when in read only mode");

   if (rec)
      _list[_listCnt++] = *rec;
   else
      memset(&_list[_listCnt++], 0, sizeof(type));
}

template <class type>
void AppendList<type>::append(const type &rec, const unsigned int incVal)
{
   append(&rec, incVal);
}

template <class type>
bool AppendList<type>::remove() noexcept
{
   if (_listCnt < 1)
      return false;

   return remove(_listCnt - 1);
}

template <class type>
bool AppendList<type>::remove(const unsigned int index) noexcept
{
   if (index >= _listCnt)
      return false;

   if (index == _listCnt - 1)
   {
      _listCnt--;
      return true;
   }

   memcpy(&_list[index], &_list[index + 1], sizeof(type) * (_listCnt - index - 1));
   _listCnt--;

   return true;
}

template <class type>
type &AppendList<type>::operator[](const unsigned int index) const noexcept
{
   return _list[index];
}

template <class type>
type *AppendList<type>::get(const unsigned int index) const noexcept
{
   return &_list[index];
}

template <class type>
type *AppendList<type>::set(const unsigned int index, type *val) noexcept
{
   if (val == 0)
      memset(_list[index], 0, sizeof(type));
   else
      *_list[index] = *val;

   return val;
}

template <class type>
type *AppendList<type>::last() const noexcept
{
   if (_listCnt < 1)
      return 0;

   return &_list[_listCnt - 1];
}

template <class type>
void AppendList<type>::poop(type *&list, unsigned int &listCount)
{
   if (_arrayCnt == 0 && _list)
      throw xMettle(__FILE__, __LINE__, __HEADER_SOURCE_, "Cannot dispence when in read only mode");

   list      = _list;
   listCount = _listCnt;

   _list     = 0;
   _listCnt  = 0;
   _arrayCnt = 0;
}

template <class type>
void AppendList<type>::poop(type *&list, int &listCount)
{
   if (_arrayCnt == 0 && _list)
      throw xMettle(__FILE__, __LINE__, __HEADER_SOURCE_, "Cannot dispence when in read only mode");

   list      = _list;
   listCount = (int) _listCnt;

   _list     = 0;
   _listCnt  = 0;
   _arrayCnt = 0;
}

template <class type>
void AppendList<type>::eat(type *&list, unsigned int &listCount) noexcept
{
   purge();

   _list     = list;
   _listCnt  = listCount;
   _arrayCnt = listCount;

   list      = 0;
   listCount = 0;
}

template <class type>
void AppendList<type>::eat(type *&list, int &listCount) noexcept
{
   purge();

   _list     = list;
   _listCnt  = (unsigned int) listCount;
   _arrayCnt = (unsigned int) listCount;

   list      = 0;
   listCount = 0;
}

template <class type>
void AppendList<type>::reference(const type *list, const unsigned int listCount) noexcept
{
   purge();

   _list     = (type *) list;
   _listCnt  = listCount;
   _arrayCnt = 0;
}

template <class type>
void AppendList<type>::reference(const type *list, const int listCount) noexcept
{
   purge();

   _list     = (type *) list;
   _listCnt  = (unsigned int) listCount;
   _arrayCnt = 0;
}

template <class type>
void AppendList<type>::quickSort(int (*sortFunc)(const type *rec, const type *rec2)) noexcept
{
   qsort(_list,
         _listCnt,
         sizeof(type),
         (int (*)(const void *, const void *)) sortFunc);
}

template <class type>
type *AppendList<type>::binarySearch(const type *searchRec,
                                     int (*sortFunc)(const type *rec, const type *rec2)) const noexcept
{
   return (type *) bsearch(searchRec,
                           _list,
                           _listCnt,
                           sizeof(type),
                           (int (*)(const void *, const void *)) sortFunc);
}

}}

#undef __HEADER_SOURCE_

#endif
