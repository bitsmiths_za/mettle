/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_DATANODE_H_
#define __METTLE_LIB_DATANODE_H_


#include "mettle/lib/c99standard.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/string.h"
#include "mettle/lib/ustring.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

class DataVar;


/** \class DataNode datanode.h mettle/lib/datanode.h
 * \brief A generic data tree node object.
 *
 * This object can be used to store and navigate a generic data structure tree such as
 * XML or YAML file.
 */
class DataNode
{
public:

   /** \class Emitter datanode.h mettle/lib/datanode.h
    * \brief An emitter object to parser/traverse the DataNode.
    */
   class Emitter
   {
   friend class DataNode;

   public:
      /** \brief The emitted action
       *
       * This is the current action of the emitter.
       */
      typedef enum
      {
         none  = 0,  /**< No action, emitter has not started, no call to emit() been made. */
         done,       /**< The emitter has completed. */
         error,      /**< The emitter encountered an internal error. */

         element,    /**< An element has been emitted. */

         listOpen,   /**< A list/array object has been opened. */
         listClose,  /**< A list/array object has been closed. */

         objOpen,    /**< A new object has been opened. */
         objClose,   /**< An object has been closed. */
      } Action;

      /** \brief Constructor.
        * \param root the root DataNode this emitter must start emitting from.
       */
      Emitter(DataNode *root);

      /**\brief Destructor.
      */
      virtual ~Emitter();

      /* \brief The main emit method.
       *
       * After construction the emitter object, ths method should be called in a loop until
       * it returns Action::done signalling the entire DataNode tree has been parsed.
       *
       * \returns the current emitted action.
      */
      Action emit();

      /* \brief Gets the current emitted node.
       * \returns the current DataNode or NULL if current datanode is not applicate (ie, if emitting a list).
       */
      const Mettle::Lib::DataNode *node() const;

      /* \brief Gets the string name of the current emitted node.
       * \returns a string pointer to the current emitted node or NULL if there is no current node.
       */
      const Mettle::Lib::String *name() const;

      /* \brief Gets the value of the current emitted object.
       * \returns the DataVar pointer of the current emitted object or NULL if there is not current value.
       */
      Mettle::Lib::DataVar *value();

      /* \brief Check if the current emitted object is part of a list.
       * \returns true if the current emitted object part a of list.
       */
      bool inList() const;

      /* \brief Check if the current emitted object has a sibling.
       *
       * This is useful to know if you are writing something like a JSON writer.  You can use
       * this check to see if you should or should not append a comma to the end of the current emitted object.
       *
       * \returns true if there is another element/object/thing besides this object.
       */
      bool hasSibling() const;

      /* \brief The current action of the current emitted object.
       * \returns the action of the current emitted object.
      */
      DataNode::Emitter::Action action() const;

      /* \brief Gets the current index of the list if the emitted object is part of a list.
       * \returns the list index of the current element if it is part of a list, else returns 0.
      */
      unsigned int listIndex() const;

      /* \brief Gets the list size/count of the emitted objects parent list if it is part of a list.
       * \returns the list size of the current element if it is part of a list, else return 0.
      */
      unsigned int listSize()   const;

      /* \brief Gets the root node that the emitter was constructed with.
       * \returns the root DataNode the emitter was constructed with.
       */
      const Mettle::Lib::DataNode *rootNode() const;

   private:

      void *_obj;
   };


   /** \class Safe datanode.h mettle/lib/datanode.h
    * \brief The safe class implementation.
    */
   class Safe : public Mettle::Lib::Safe<DataNode>
   {
   public:
      Safe(DataNode *o) : Mettle::Lib::Safe<DataNode>(o) {}
   };

   /** \brief Default constructor.
    */
   DataNode() noexcept;

   /** \brief Constructor setting the data node's name and value.
    *  \param name the name of the data node element.
    *  \param val the DataVar object to be stored in this node.
    */
   DataNode(const char *name, Mettle::Lib::DataVar *val = 0);


   /** \brief Constructor setting the data node's name and value.
    *  \param name the name of the data node element.
    *  \param value the value of the data node element, default to ustring type.
    *  \throws xMettle if there is a memory allocation problem.
    */
   //DataNode(const char *name, const char *value);

   /** \brief Destructor.
    */
   virtual ~DataNode() noexcept;

   /** \brief Clears this node and remove all children and siblings.
    */
   void purge() noexcept;

   /** \brief Same as purge, also clears the name.
    */
   void clear() noexcept;

   /** \brief Gets the name of this node.
    *  \returns the name of the node, if the name is null an empty string is returned instead.
    */
   const Mettle::Lib::String &name() const noexcept;

   /** \brief Set the name of the node.
    *  \param newName the new name of the node, if the pointer is null the name is set to empty.
    */
   void nameSet(const char *newName);

   /** \brief Gets this nodes value.
    *  \returns the DataVar object for this node.  Returns null if the value has not been initalized.
    */
   Mettle::Lib::DataVar *value() const;

   /** \brief Check if the value is null.
    *  \returns true if the value memeber is null or if its associated value member value is null.
    */
   const bool valueIsNull() const noexcept;

   /** \brief Check if the value is null or or empty.
    *  \returns true if the value memeber is null or blank/zero.
    */
   const bool valueIsEmpty() const noexcept;

   /** \brief Ensure the value is initialized.
    *  \returns the value, if it is null it is first created.
    *  \throws an xMettle exception if out of memory.
    */
   Mettle::Lib::DataVar * valueInit();

   /** \brief Set a new value.
    *  \param var the new value for this node.
    *  \returns the value that was just added.
    *  \throws an xMettle exception if out of memory.
    */
   Mettle::Lib::DataVar * valueSet(Mettle::Lib::DataVar *var);

   /** \brief Set a new value from a string.
    *  \param var the new value for this node.
    *  \returns the value that was just added.
    *  \throws an xMettle exception if out of memory.
    */
   Mettle::Lib::DataVar * valueSet(const char *var);

   /** \brief Get the number of children this node has.
    *  \returns the children count.
    */
   const unsigned int childCount() const noexcept;

   /** \brief Get the number of siblings this node has.
    *  \returns the sibling count.
    */
   const unsigned int siblingCount() const noexcept;

   /** \brief Get the parent node.
    *  \returns the parent node or NULL if this the highest level node.
    */
   Mettle::Lib::DataNode *parent() const noexcept;

   /** \brief Get the sibling node.
    *  \returns the next sibling node or NULL if this node has no siblings.
    */
   Mettle::Lib::DataNode *sibling() const noexcept;

   /** \brief Get the child node.
    *  \returns the first child  node or NULL if this node has no children.
    */
   Mettle::Lib::DataNode *child() const noexcept;

   /** \brief Gets the child via the search path.
    *
    *  This methos uses an 'path' to find the child node.  The delimeter for the xpath is a '/' character. For
    *  example: if you wanted to get line one of a postal address record in a tree the xpath could like 'entity/addresses/postsal/line1'.  The path
    *  also supports the getting items at a specfic index, so entity[4]/addresses would get the fourth entity from the file. Additional path commands
    *  tha are supported are: '..' : go back (parent).  If any of the identifier have a forward slash '/' in their names, you must enclose them with
    *  double quotes '"'.  Note tha an empty string will always reutrn the current node.
    *
    *  \param path the tree path to the child node.
    *  \returns the child at the xpath or NULL if the path could no be found.
    *  \throws xMettle if the path is not well formed.
    */
   Mettle::Lib::DataNode *get(const char *path);

   /** \brief Gets the child at the specified index.
    *  \param idx the index of the child node to get.
    *  \returns the child data node at the specified index, the index is out of bounds a NULL is returned instead.
    */
   Mettle::Lib::DataNode *indexOf(const unsigned int idx) const noexcept;

   /** \brief Gets the root node.
    *  \returns the top most root node.
    */
   Mettle::Lib::DataNode *root() const noexcept;

   /** \brief Adds a new child node.
    *  \param name the name of the new child node.
    *  \param value the datavar value object of the new child node. This object will be handled by the DataNode from this point, do not delete it.
    *  \param pos the position to add the child node at, if \p pos is negative or out of bounds it is added to the end.
    *  \returns the child data node that was created.
    *  \throws an xMettle exception if out of memory.
    */
   Mettle::Lib::DataNode *newChild(const char *name, Mettle::Lib::DataVar *value = 0, const int pos = -1);

   /** \brief Adds a new sibling node.
    *  \param name the name of the new child node.
    *  \param value the datavar value object of the new child node. This object will be handled by the DataNode from this point, do not delete it.
    *  \param pos the position to add the child node at, if \p pos is negative or out of bounds it is added to the end.
    *  \returns the child data node that was created.
    *  \throws an xMettle exception if out of memory of if this is the root node.
    */
   Mettle::Lib::DataNode *newSibling(const char *name, Mettle::Lib::DataVar *value = 0, const int pos = -1);

   /** \brief Remove the node at the specified path.
    *  \param path the path of the node to remove.  Note, that you cannot use '..' or '/' for this call it must be a relative path.  See 'get()' method for more detail.
    *  \returns true if the node at the path was found and removed or false if the path was not found.
    */
   bool remove(const char *path) noexcept;

   /** \brief Read the variable at the path as a char*.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, char *&val);

   /** \brief Read the variable at the path as a String.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, String &val);

   /** \brief Read the variable at the path as a UString.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, UString &val);

   /** \brief Read the variable at the path as a char.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, char &val);

   /** \brief Read the variable at the path as a double.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, double &val);

   /** \brief Read the variable at the path as a int.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, int &val);

   /** \brief Read the variable at the path as a unsigned.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, unsigned int  &val);

   /** \brief Read the variable at the path as a bool.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, bool &val);

   /** \brief Read the variable at the path as a Date.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, Date &val);

   /** \brief Read the variable at the path as a Time.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, Time &val);

   /** \brief Read the variable at the path as a DateTime.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, DateTime &val);

   /** \brief Read the variable at the path as a guid.
    *  \param path the path of the node to read.  See get().
    *  \param val value to read into.
    *  \returns true if the node was found and read.
    */
   bool read(const char *path, Guid &val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const char *val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const String &val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const UString &val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const double &val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const int &val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const unsigned int &val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const bool &val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const Date &val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const Time &val);

   /** \brief Write a char the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const DateTime &val);

   /** \brief Write a guid to the node at the path.
    *  \param path the path of the node to write to.  See get().
    *  \param val the value to write.
    *  \returns the DataNode at the path that was written or null if path was not valid and nothing was written.
    */
   DataNode *write(const char *path, const Guid &val);

   /** \brief Changes the value to the best types for each value in the tree, converting from a string to their best type.  See DataVar::toBestType().
    *  \returns the total number of nodes successfully converted.
    */
   unsigned int toBestTypes();

protected:

   String      _name;
   DataVar    *_value;

   DataNode   *_parent;
   DataNode   *_sibling;
   DataNode   *_child;

   DataNode *_findNode(const char *path,
                       const bool  allowRoot      = true,
                       const bool  allowParentNav = true);

   DataNode *_findNodeForWrite(const char *path);

};


}}

#endif
