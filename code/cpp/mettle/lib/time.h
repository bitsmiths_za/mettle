/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_TIME_H_
#define __METTLE_LIB_TIME_H_

#include <iostream>
#include <time.h>

#include "mettle/lib/c99standard.h"
#include "mettle/lib/collection.h"
#include "mettle/lib/icomparable.h"
#include "mettle/lib/safe.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

class Date;

/** \class Time time.h mettle/lib/time.h
 * \brief A time object.
 *
 *  This the standard time handling object in Mettle.
 */
class Time : public IComparable
{
friend class DateTime;

public:

   DECLARE_SAFE_CLASS(Time);
   DECLARE_COLLECTION_LIST_CLASS(Time);

   /** \brief Time Period Legend enum.
    *
    * This is used to define a what level addition and subration operations occur.
    */
   typedef enum
   {
      Hours,
      Minutes,
      Seconds
   } PeriodLegend;

   /** \brief Defualt constructor.
    */
   Time() noexcept;

   /** \brief Copy constructor.
       \param inTime the time to copy.
    */
   Time(const Time &inTime) noexcept;

   /** \brief Construct the class with a standard 'strcut tm' type.
    *  \param inTime the struct tm record to use when initializing the Time class.
    *  \param throwError if set to true, the constructor will raise an exception if the struct tm time is not valid.
    *  \throws xMettle if the constructed date is not valid.
    */
   Time(const struct tm &inTime, const bool throwError = true);

   /** \brief Construct the class with a hour, minutes, seconds.
    *  \param hours the hours of the new date.
    *  \param minutes the minutes of the new date.
    *  \param seconds the seconds of the new date.
    *  \param throwError if set to true, the constructor will raise an exception if the \p hours, \p minutes, and \p seconds are not a valid date.
    *  \throws xMettle if the constructed date is not valid.
    */
   Time(const unsigned int hours, const unsigned int minutes, const unsigned int seconds, const bool throwError = true);

   /** \brief Construct the class with a string in the 'HHMMSS' format.
    *  \param hhmmss the string that is used to constructe the date, ie "195959" for the 07 PM, 59 minutes, 59 seconds.
    *  \param throwError if set to true, the constructor will raise an exception if the \p hhmmss is not a valid date.
    *  \throws xMettle if the constructed date is not valid.
    */
   Time(const char *hhmmss, const bool throwError = true);

   /** \brief Construct the class with a string in the 'HHMMSS' format.
    *  \param hhmmss the int that is used to constructe the date, ie "195959" for the 07 PM, 59 minutes, 59 seconds.
    *  \param throwError if set to true, the constructor will raise an exception if the \p hhmmss is not a valid date.
    *  \throws xMettle if the constructed date is not valid.
    */
   Time(const int32_t hhmmss, const bool throwError = true);

   /** \brief Destructor.
    */
   ~Time() noexcept;

   /** \brief Add an amount to the time class in the specified legend.
    *  \param amount the value to add to the time.
    *  \param legend the value type to add, ie hours, minutes, seconds.
    *  \param date optional date object that the addition could effect.
    *  \returns a referense to itself for convenience.
    */
   Time &add(unsigned int amount, const PeriodLegend legend, Date *date = 0) noexcept;

   /** \brief Subract an amount from the time class in the specified legend.
    *  \param amount the value to subract from the time.
    *  \param legend the value type to add, ie hours, minutes, seconds.
    *  \param date optional date object that the subraction could effect.
    *  \returns a referense to itself for convenience.
    */
   Time &sub(unsigned int amount, const PeriodLegend legend, Date *date = 0) noexcept;

   /** \brief A useful 'struct tm' type cast operator.
    *  \returns a standard 'struct tm' cast of the time object.
    */
   operator struct tm () const noexcept;

   /** \brief Set the hour of the Time object.
    *  \param hour the new hour of the object.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new time would be invalid.
    */
   Time &setHour(const unsigned int hour);

   /** \brief Set the minute of the Time object.
    *  \param minute the new minute of the object.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new time would be invalid.
    */
   Time &setMinute(const unsigned int minute);

   /** \brief Set the second of the Time object.
    *  \param second the new second of the object.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new time would be invalid.
    */
   Time &setSecond(const unsigned int second);

   /** \brief Formats the time as specified into a a string.

       The format options are:

         - %a abbreviated weekday name
         - %A full weekday name
         - %b abbreviated month name
         - %B full month name
         - %d day of month as decimal [01,31]
         - %H Hour (24-hour clock) as a decimal number [00,23]
         - %I Hour (12-hour clock) as a decimal number [01,12]
         - %j Day of the year as a decimal number [001,366]
         - %m Month as a decimal number [01,12]
         - %M Minute as a decimal number [00,59]
         - %p Locale's equivalent of either AM or PM
         - %S Second as a decimal number [00,61]
         - %w Weekday as a decimal number [0(Sunday),6]
         - %y Year without century as a decimal number [00,99]
         - %Y Year as a decimal number [0000,9999]
         - %% A literal "%" character

    *  \param dest the destination string for the formatted time.  Enough space must be available for the formatting.
    *  \param fmt the format the time should formatted into.
    *  \returns the \p dest param for convenience.
    */
   char *format(char *dest, const char *fmt) const noexcept;

   /** \brief Formats the time into a specified string.  See 'char *format(char *dest, char *fmt)'.
     * \param dest the destination string for the formatted time.
     * \param fmt the format the time should formatted into.
     * \returns the \p dest param for convenience.
     * \throws xMettle it out of memory.
     */
   String &format(String &dest, const char *fmt) const;

   /** \brief Get the hour porition of the time.
    *  \returns the hour portion of the time.
    */
   unsigned int hour() const noexcept { return _hour; }

   /** \brief Get the minute porition of the time.
    *  \returns the minute portion of the time.
    */
   unsigned int minute() const noexcept { return _minute; }

   /** \brief Get the second porition of the time.
    *  \returns the second portion of the time.
    */
   unsigned int second() const noexcept { return _second; }

   /** \brief Get the if the timeis AM.
    *  \returns true if the time is AM.
    */
   bool isAM() const noexcept { return _hour < 12 ? true : false; }

   /** \brief Get the if the timeis PM.
    *  \returns true if the time is PM.
    */
   bool isPM() const noexcept { return _hour > 11 ? true : false; }

   /** \brief Retrieves the time as a string represented visually in the HHMMSS format.
    *  \param hhmmss the destination string that is at least 7 charaters long, the time is copied into it.
    *  \returns the \p hhmmss arguement for convenience.
    */
   char *toString(char *hhmmss) const noexcept;

   /** \brief Retrieves the time as an integer represented visually in the HHMMSS format.
    *  \returns the current time as an integer (HHMMSS).
    */
   int32_t toInt32() const noexcept;

   /** \brief Compare this time to another.
    *  \param cmpTime the time to compare against.
    *  \returns -1 if this time is smaller than \p cmpTime, 0 if the times match, or +1 if this time is greater than \p cmpTime
    */
   int compare(const Time *cmpTime) const noexcept;

   /** \brief compare this time to another Time pointer, via explicit type cast.
    *  \param obj a pointer that can be directly type casted to a time class.
    *  \returns -1 if this time is smaller than \p obj, 0 if the times match, or +1 if this time is greater than \p obj
    */
   int _compare(const void *obj) const;

   /** \brief Check if this time is null.
    *  \returns true if this time is null; ie the hour, min, and sec are all zero.
    */
   bool isNull() const noexcept;

   /** \brief Assign this time a new value.
    *  \param hour the hour of the new time.
    *  \param minute the minute of the new time.
    *  \param second the second of the new time.
    *  \throws xMettle if the constructed time is not valid.
    */
   void assign(const unsigned int hours, const unsigned int minutes, const unsigned int seconds);

   /** \brief Makes this time null.
    */
   void nullify() noexcept;

   /** \brief Same as nullify().
    */
   void clear() noexcept;

   /** \brief Operator overload for = to assign the time from an existing time.
    *  \param inTime the time to become.
    *  \returns itself.
    */
   Time & operator = (const Time &inTime) noexcept;

   /** \brief Operator overload for = to assign the time from a struct tm.
    *  \param inTime the time to become.
    *  \returns itself.
    *  \throws xMettle if the struct tm time is not valid.
    */
   Time & operator = (const struct tm &inTime);

   /** \brief Operator overload for = to assign the string in HHMMSS format.
    *  \param hhmmss the time to become.
    *  \returns itself.
    *  \throws xMettle if the input time is not valid.
    */
   Time & operator = (const char *hhmmss);

   /** \brief Operator overload for = to assign the int in HHMMSS format.
    *  \param yyyymmdd the time to become.
    *  \returns itself.
    *  \throws xMettle if the input time is not valid.
    */
   Time & operator = (const int32_t hhmmss);

   /** \brief Operator overload for += to increment the time in seconds
    *  \param seconds the number of seconds to add to the time.
    *  \returns itself for convenience.
    */
   Time & operator += (const unsigned int seconds) noexcept;

   /** \brief Operator overload for -= to decrement the time in seconds
    *  \param seconds the number of seconds to subract from the time.
    *  \returns itself for convenience.
    */
   Time & operator -= (const unsigned int seconds) noexcept;

   /** \brief Operator overload for ++ to increment the time by 1 second.
    *  \returns itself for convenience.
    */
   const Time & operator ++ () noexcept;

   /** \brief Operator overload for -- to decrement the time by 1 second.
    *  \returns itself for convenience.
    */
   const Time & operator -- () noexcept;

   /** \brief Operator overload for ++ (int) to increment the time by 1 second.
    *  \returns itself.
    */
   Time operator ++ (int) noexcept;

   /** \brief Operator overload for -- (int) to decrement the time by 1 second.
    *  \returns itself.
    */
   Time operator -- (int) noexcept;

   /** \brief Less than friend operator overload between two times.
    *  \param time1 the first time to compare.
    *  \param time2 the second time to compare.
    *  \returns true if \p time1 is less than \p time2.
    */
   friend bool operator <  (const Time &time1, const Time &time2) noexcept;

   /** \brief Greater than friend operator overload between two times.
    *  \param time1 the first time to compare.
    *  \param time2 the second time to compare.
    *  \returns true if \p time1 is greater than \p time2.
    */
   friend bool operator >  (const Time &time1, const Time &time2) noexcept;

   /** \brief Less Equal than friend operator overload between two times.
    *  \param time1 the first time to compare.
    *  \param time2 the second time to compare.
    *  \returns true if \p time1 is less than or eqaul to \p time2.
    */
   friend bool operator <= (const Time &time1, const Time &time2) noexcept;

   /** \brief Greater Equal than friend operator overload between two times.
    *  \param time1 the first time to compare.
    *  \param time2 the second time to compare.
    *  \returns true if \p time1 is greater than or equal to \p time2.
    */
   friend bool operator >= (const Time &time1, const Time &time2) noexcept;

   /** \brief Equals friend operator overload between two times.
    *  \param time1 the first time to compare.
    *  \param time2 the second time to compare.
    *  \returns true if \p time1 equals \p time2.
    */
   friend bool operator == (const Time &time1, const Time &time2) noexcept;

   /** \brief Not Equals friend operator overload between two times.
    *  \param time1 the first time to compare.
    *  \param time2 the second time to compare.
    *  \returns true if \p time1 does not equal \p time2.
    */
   friend bool operator != (const Time &time1, const Time &time2) noexcept;

   /** \brief Ouput stream operator.
    *  \param output the output stream, time is formatted into the standard time time format 'HH:MM:SS'.
    *  \param dt the time object to be output.
    *  \returns the output stream as expected.
    */
   friend std::ostream &operator << (std::ostream &output, const Time &dt) noexcept;

   /** \brief Input stream operator.
    *  \param input the input stream.
    *  \param dt the time object to receive the value.
    *  \returns the input stream as expected.
    *  \throws xMettle if the input string was not in the standard time time format 'HH:MM:SS'.
    */
   friend std::istream &operator >> (std::istream &input, Time &dt);

   /** \brief Returns the standard format string for timetimes, ie 'HH:MM:SS'
     * \returns The standard format string.
     */
   static const char *stdTimeFormat();

   /** \brief Check if the hours, minutes, seconds combination is a valid time.
    *  \param hours the hours to check.
    *  \param minutes the minutes to check.
    *  \param seconds the seconds to check.
    *  \returns true if params \p hours, \p minutes, and \p seconds make up a valid time.
    */
   static bool validTime(const unsigned int hours, const unsigned int minutes, const unsigned int seconds) noexcept;

   /** \brief Check if the string in the HHMMSS format is a valid time.
    *  \param hhmmss the string to check.
    *  \returns true if \p hhmmss is a valid time.
    */
   static bool validTime(const char *hhmmss) noexcept;

   /** \brief Check if the integer in the YYYYMMDD format is a valid time.
    *  \param hhmmss the integer to check.
    *  \returns true if \p hhmmss is a valid time.
    */
   static bool validTime(const int32_t hhmmss) noexcept;

   /** \brief Gets a new Time object with time set to the current machine time.
    *  \returns A new time object with the time to now.
    */
   static Time now() noexcept;

private:

   uint8_t  _hour;
   uint8_t  _minute;
   uint8_t  _second;

   void     _from(const struct tm *tm_struct, const bool throwError = true);
   void     _from(const unsigned int hours, const unsigned int minutes, const unsigned int seconds, const bool throwError = true);
   void     _from(const char *hhmmss,const bool throwError = true);
   void     _from();

   bool     validate(const bool throwError = true);
};

}}


#endif
