/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/filemanager.h"

#include "mettle/lib/platform.h"
#include "mettle/lib/common.h"
#include "mettle/lib/macros.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#if defined(OS_MS_WINDOWS)

   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>

   #if defined (WINCE)
      #include <string.h>
   #else
      #include <io.h>
   #endif

#else

   #include <unistd.h>
   #include <dirent.h>

#endif

#if !defined(WINCE)

   #include <sys/types.h>
   #include <sys/stat.h>

#endif

namespace Mettle { namespace Lib {


FileManager::FileManager()
{
}


unsigned int FileManager::getLength(const char *fileName)
{
   FileHandle fh;

   fh.open(fileName, FileHandle::Binary);

   return fh.getLength();
}


void FileManager::writeStrToFile(const char *fileName,
                                 const char *str)
{
   FileHandle fh;

   fh.create(fileName, FileHandle::Text);

   if (str && str[0])
      fh.write(str);
}


int8_t *FileManager::readFile(const char     *fileName,
                              int8_t        *&jotter,
                              unsigned int   &jotterSize)
{
   jotter = 0;

   try
   {
      FileHandle    fh;

      fh.open(fileName, FileHandle::Binary);

      if ((jotterSize = fh.getLength()) == 0L)
         return 0;

      jotter = (int8_t*) Common::memoryAlloc(jotterSize + 1);

      fh.read(jotter, jotterSize);

      jotter[jotterSize] = 0;
   }
   catch (...)
   {
      _FREE(jotter)
      throw;
   }

   return jotter;
}


bool FileManager::pathExists(const char *path) noexcept
{
#if defined(OS_MS_WINDOWS) && defined (WINCE)

   WCHAR  buffer[4096];

   MultiByteToWideChar(CP_ACP, 0, path, -1, buffer, 4096);

   if (GetFileAttributes(buffer) == 0xFFFFFFFF)
      return false;

#else

   if (access(path, 0) == -1)
      return false;

#endif

   return true;
}


void FileManager::removeFile(const char *filePath, const bool ignoreMissing)
{
   if (ignoreMissing && !pathExists(filePath))
      return;

#if defined(OS_MS_WINDOWS) && defined(WINCE)

      WCHAR buffer[4096];

      MultiByteToWideChar(CP_ACP, 0, filePath, -1, buffer, 4096);

      if (!DeleteFile(buffer))
         throw FileHandle::xFileHandle(__FILE__, __LINE__, "Could not remove the file '%s'", filePath);

#else

   if (remove(filePath) != 0)
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Could not remove the file '%s'", filePath);

#endif
}


void FileManager::renameFile(const char *dest, const char *source)
{
   int rc;

   if (!pathExists(source))
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Could not rename file '%s' because it does not exist!", source);

#if defined(OS_MS_WINDOWS)

  #if defined(WINCE)

      WCHAR bdest[4096];
      WCHAR bsource[4096];

      MultiByteToWideChar(CP_ACP, 0, dest,   -1, bdest,   4096);
      MultiByteToWideChar(CP_ACP, 0, source, -1, bsource, 4096);

      if (MoveFile(bsource, bdest))
         rc = 1;
      else
         rc = 0;

  #else

      rc = system(String().format("move %s %s", source, dest).c_str());

  #endif

#else

   rc = system(String().format("mv \"%s\" \"%s\"", source, dest).c_str());

#endif

   if (rc != 0)
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Error renameing file '%s' to '%s'", source, dest);
}


void FileManager::fileCopy(const char *dest, const char *source)
{
   int rc;

   if (!pathExists(source))
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Could not copy file '%s' because it does not exist!", source);

#if defined(OS_MS_WINDOWS)

  #if defined(WINCE)

      WCHAR bdest[4096];
      WCHAR bsource[4096];

      MultiByteToWideChar(CP_ACP, 0, dest,   -1, bdest,   4096);
      MultiByteToWideChar(CP_ACP, 0, source, -1, bsource, 4096);

      if (CopyFile(bsource, bdest, true))
         rc = 1;
      else
         rc = 0;

  #else

      rc = system(String().format("copy %s %s", source, dest).c_str());

  #endif

#else

   rc = system(String().format("cp \"%s\" \"%s\"", source, dest).c_str());

#endif

   if (rc != 0)
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Error copying file '%s' to '%s'", source, dest);
}


void FileManager::createDir(const char *path)
{
   int rc;

   if (pathExists(path))
      return;

#if defined(OS_MS_WINDOWS)

   #if defined(WINCE)

      WCHAR buffer[4096];

      MultiByteToWideChar(CP_ACP, 0, path, -1, buffer, 4096);

      if (CreateDirectory(buffer, 0))
         rc = 1;
      else
         rc = 0;

   #else

      rc = system(String().format("md %s", path).c_str());

   #endif

#else

   rc = system(String().format("mkdir -p \"%s\"", path).c_str());

#endif

   if (rc != 0)
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Error creating directory '%s'", path);
}


unsigned int FileManager::findFilesInPath(const char         *path,
                                                String::List &fileList,
                                          const bool         returnFullPath)
{
   fileList.clear();

   if (!pathExists(path))
      return 0;

   String tempPath(path);

   tempPath.makeDirectory();

#if defined(OS_MS_WINDOWS)

   WIN32_FIND_DATA findData;
   HANDLE finder;

   String findPath(tempPath);
   findPath += ("*");

   #if defined(WINCE)

      WCHAR buffer[4096];

      MultiByteToWideChar(CP_ACP, 0, path, -1, buffer, 4096);

      finder = FindFirstFile(buffer, &findData);

   #else

      finder = FindFirstFile(findPath.c_str(), &findData);

   #endif

   if (finder == INVALID_HANDLE_VALUE)
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Could not get files for path '%s'", path);

   do
   {
      if (!(findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
      {
   #if defined(WINCE)

         char   cres[4096];

         int rc = WideCharToMultiByte(CP_ACP,
                                  0,
                                  findData.cFileName,
                                  -1,
                                  cres,
                                  sizeof(cres),
                                  0,
                                  0);

         if (rc < 1)
            throw FileHandle::xFileHandle(__FILE__, __LINE__, "Operating System Error: Attempt to convert wide char to ansi char!");

         if (returnFullPath == true)
            fileList.append(new String())->format("%s%s", tempPath.c_str(), cres);
         else
            fileList.append(new String(cres));

   #else

         if (returnFullPath == true)
            fileList.append(new String())->format("%s%s", tempPath.c_str(), findData.cFileName);
         else
            fileList.append(new String(findData.cFileName));

   #endif
      }
   }
   while (FindNextFile(finder, &findData));

   FindClose(finder);

#else

   DIR    *dir;
   dirent *entry;

   dir = opendir(tempPath.c_str());
   if (dir == 0)
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Could not get files for path '%s'", path);

   while (entry = readdir(dir))
   {
      if (entry->d_type != DT_REG)
         continue;

      if (returnFullPath == true)
         fileList.append(new String())->format("%s%s", tempPath.c_str(), entry->d_name);
      else
         fileList.append(new String(entry->d_name));
   }

   closedir(dir);

#endif

   return fileList.count();
}


unsigned int FileManager::findDirsInPath(const char         *path,
                                               String::List &dirList,
                                         const bool          returnFullPath)
{
   dirList.clear();

   if (!pathExists(path))
      return 0;

   String tempPath(path);

   tempPath.makeDirectory();

#if defined(OS_MS_WINDOWS)

   WIN32_FIND_DATA findData;
   HANDLE finder;

   String findPath(tempPath);
   findPath += ("*");

   #if defined(WINCE)

      WCHAR buffer[4096];

      MultiByteToWideChar(CP_ACP, 0, path, -1, buffer, 4096);

      finder = FindFirstFile(buffer, &findData);

   #else

      finder = FindFirstFile(findPath.c_str(), &findData);

   #endif

   if (finder == INVALID_HANDLE_VALUE)
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Could not get dirs for path '%s'", path);

   do
   {
      if ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
      {
   #if defined(WINCE)

         char   cres[4096];

         int rc = WideCharToMultiByte(CP_ACP,
                                  0,
                                  findData.cFileName,
                                  -1,
                                  cres,
                                  sizeof(cres),
                                  0,
                                  0);

         if (rc < 1)
            throw FileHandle::xFileHandle(__FILE__, __LINE__, "Operating System Error: Attempt to convert wide char to ansi char!");

         if (strcmp(cres, ".")  == 0 ||
             strcmp(cres, "..") == 0)
             continue;

         if (returnFullPath == true)
            dirList.append(new String())->format("%s%s", tempPath.c_str(), cres);
         else
            dirList.append(new String(cres));

   #else

         if (strcmp(findData.cFileName, ".")  == 0 ||
             strcmp(findData.cFileName, "..") == 0)
             continue;

         if (returnFullPath == true)
            dirList.append(new String())->format("%s%s", tempPath.c_str(), findData.cFileName);
         else
            dirList.append(new String(findData.cFileName));

   #endif
      }
   }
   while (FindNextFile(finder, &findData));

   FindClose(finder);

#else

   DIR    *dir;
   dirent *entry;

   dir = opendir(tempPath.c_str());
   if (dir == 0)
      throw FileHandle::xFileHandle(__FILE__, __LINE__, "Could not get dirs for path '%s'", path);

   while (entry = readdir(dir))
   {
      if (entry->d_type != DT_DIR)
         continue;

      if (strcmp(entry->d_name, ".")  == 0 ||
          strcmp(entry->d_name, "..") == 0)
          continue;

      if (returnFullPath == true)
         dirList.append(new String())->format("%s%s", tempPath.c_str(), entry->d_name);
      else
         dirList.append(new String(entry->d_name));
   }

   closedir(dir);

#endif

   return dirList.count();
}


}}
