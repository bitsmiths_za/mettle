/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_DATETIME_H_
#define __METTLE_LIB_DATETIME_H_

#include <iostream>
#include <stdint.h>

#include "mettle/lib/date.h"
#include "mettle/lib/time.h"

namespace Mettle { namespace Lib {

/** \class DateTime datetime.h mettle/lib/datetime.h
 * \brief A datetime object.
 *
 *  This the standard datetime handling object in Mettle.
 *  Note! The timezone default is UTC 00:00, use setLocalTZ to set change the default for the global class space.
 */
class DateTime : public IComparable
{
public:

   DECLARE_SAFE_CLASS(DateTime);
   DECLARE_COLLECTION_LIST_CLASS(DateTime);

   /** \brief Defualt constructor.
    */
   DateTime() noexcept;

   /** \brief Copy constructor.
    *  \param inDateTime the date to copy.
    */
   DateTime(const DateTime &inDateTime) noexcept;

   /** \brief Date constructor.
    *  \param date the date to copy.
    */
   DateTime(const Date &date) noexcept;

   /** \brief Time constructor.
    *  \param time the time to copy.
    */
   DateTime(const Time &time) noexcept;

   /** \brief Date & Time constructor.
    *  \param date the date to copy.
    *  \param time the time to copy.
    */
   DateTime(const Date &date, const Time &time) noexcept;

   /** \brief Standard struct tm constructor.
    *  \param inDateTime the date and time to copy.
    *  \throws xMettle if \p inDateTime is not valid.
    */
   DateTime(const struct tm &inDateTime);

   /** \brief Full YYYYMMDDHHMMSS(TZ) date & time string constructor.
    *  \param yyyymmddhhmmss(tz) the string in the specified format to construct with, the (tz) part is optional and is in format (+/-HHMM).
    *  \param throwError if set to true, the constructor will raise an exception if the \p yyyymmddhhmmss is not a valid date & time.
    *  \throws xMettle if \p inDateTime is not valid.
    */
   DateTime(const char *yyyymmddhhmmss, const bool throwError = true);

   /** \brief A YYYYMMDD date string and HHMMSS time string constructor.
    *  \param yyyymmdd the string that is used to construct the date, ie "20160421" for the year 21'st day of April 2016.
    *  \param hhmmss the string that is used to construct the date, ie "195959" for the 07 PM, 59 minutes, 59 seconds.
    *  \param tz optionally set the timze zone in format of "(+/-)HHMM" eg, "+0200" for + 2 hours and zero minues.
    *  \param throwError if set to true, the constructor will raise an exception if the \p yyyymmddhhmmss is not a valid date & time.
    *  \throws xMettle if \p yyyymmdd or \p hhmmss is not valid.
    */
   DateTime(const char *yyyymmdd, const char *hhmmss, const char *tz = 0, const bool throwError = true);

   /** \brief A YYYYMMDD date integer and HHMMSS time integer constructor.
    *  \param yyyymmdd the integer that is used to constructe the date, ie "20160421" for the year 21'st day of April 2016.
    *  \param hhmmss the integer that is used to constructe the date, ie "195959" for the 07 PM, 59 minutes, 59 seconds.
    *  \param tzmin optionally set the timezone hour.
    *  \param tzmin optionally set the timezone minute.
    *  \param throwError if set to true, the constructor will raise an exception if the \p yyyymmddhhmmss is not a valid date & time.
    *  \throws xMettle if \p yyyymmdd or \p hhmmss is not valid.
    */
   DateTime(const int32_t yyyymmdd, const int32_t hhmmss, int8_t tzhour = INT8_MIN, int8_t tzmin = INT8_MIN, const bool throwError = true);

   /** \brief Construct the date & time by setting each value individually.
    *  \param yyyy the year value.
    *  \param mm the month value.
    *  \param dd the day value.
    *  \param hh the hours value.
    *  \param mi the minutes value.
    *  \param ss the seconds value.
    *  \param tzhour optionally give the time zone hour.
    *  \param tzmin optionally give the time zone minute.
    *  \param throwError if set to true, the constructor will raise an exception if all the parts do not make up a valid date & time.
    *  \throws xMettle if \p yyyymmdd or \p hhmmss is not valid.
    */
   DateTime(const int32_t yyyy,   const int32_t mm, const int32_t dd,
            const int32_t hh,     const int32_t mi, const int32_t ss,
            const int8_t  tzhour     = INT8_MIN,
            const int8_t  tzmin      = INT8_MIN,
            const bool    throwError = true);

   /** \brief Destructor.
    */
   ~DateTime() noexcept;

   /** \brief A useful 'struct tm' type cast operator.
    *  \returns a standard 'struct tm' cast of the daet & time object.
    */
   operator struct tm () const noexcept;

   /** \brief Get the time zone hour from the datetime.
    *  \returns the time zone hour of the datetime.
    */
   int8_t tzHour() const noexcept { return _tzhour; }

   /** \brief Get the time zone minute from the datetime.
    *  \returns the time zone minute of the datetime.
    */
   int8_t tzMinute() const noexcept { return _tzmin; }

   /** \brief Get the time zone offset in seconds.
    *  \returns the time zone offset int seconds.
    */
   int32_t tzOffset() const noexcept;

   /** \brief Get the time zone abbreviation.
    *  \returns the time zone offset abbreviation.
    */
   const char *tzAbr() const noexcept;

   /** \brief Set the time zone hour of the DateTime object.
    *  \param tzhour the new timezone hour for the object.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new datetime would be invalid.
    */
   DateTime &tzSetHour(const int8_t tzhour);

   /** \brief Set the time zone minute of the DateTime object.
    *  \param tzmin the new timezone minute for the object.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new datetime would be invalid.
    */
   DateTime &tzSetMinute(const int8_t tzmin);

   /** \brief Converts the datetime object to the specified time zone base off utc hour & min.
    *  \param tzhour the timezone hour to convert to.
    *  \param tzmin the timezone minute to convert to.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new datetime would be invalid.
    */
    DateTime &tzConverTo(const int8_t tzhour, const int8_t tzmin);


   /** \brief Converts the datetime object to the specified time zone based off utc offset in seconds.
    *  \param tzoffset the timezone offset in seconds from utc.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new datetime would be invalid.
    */
    DateTime &tzConverTo(const int32_t tzoffset);


   /** \brief Retrieves the date & time as a string represented visually in the YYYYMMDDHHMMSS format.
    *  \param yyyymmddhhmmss the destination string that is at least 15 charaters long, the date & time is copied into it.
    *  \param withTZ if this is set true, appends the timezone in format (+/-)HHMM
    *  \returns the \p yyyymmddhhmmss arguement for convenience.
    */
   char *toString(char *yyyymmddhhmmss, const bool withTZ = false) const noexcept;

   /** \brief Formats the time as specified into a a string.

       The format options are:

         - %a abbreviated weekday name
         - %A full weekday name
         - %b abbreviated month name
         - %B full month name
         - %d day of month as decimal [01,31]
         - %H Hour (24-hour clock) as a decimal number [00,23]
         - %I Hour (12-hour clock) as a decimal number [01,12]
         - %j Day of the year as a decimal number [001,366]
         - %m Month as a decimal number [01,12]
         - %M Minute as a decimal number [00,59]
         - %p Locale's equivalent of either AM or PM
         - %S Second as a decimal number [00,61]
         - %w Weekday as a decimal number [0(Sunday),6]
         - %y Year without century as a decimal number [00,99]
         - %Y Year as a decimal number [0000,9999]
         - %z Time zone UTC offset in the form +HHMM or -HHMM
         - %Z Time zone abbreviation, eg UTC, CST, SAST, Z
         - %% A literal "%" character

    *  \param dest the destination string for the formatted time.  Enough space must be available for the formatting.
    *  \param fmt the format the time should formatted into.
    *  \returns the \p dest param for convenience.
    */
   char *format(char *dest, const char *fmt) const noexcept;

   /** \brief Formats the time into a specified string.  See 'char *format(char *dest, char *fmt)'.
     * \param dest the destination string for the formatted time.
     * \param fmt the format the time should formatted into.
     * \returns the \p dest param for convenience.
     * \throws xMettle it out of memory.
     */
   String &format(String &dest, const char *fmt) const;

   /** \brief Parses the date/time from the specfified format string. See 'char *format(char *dest, char *fmt)'.
     * \param dateStr the string to be parsed.
     * \param fmt the format the datetime must be decoded.
     * \returns self for convenience.
     * \throws xMettle if the format does not match the srcDate.
     */
   DateTime &parse(const String &srcDate, const char *fmt);

   /** \brief Parses the date/time from the specfified format string. See 'char *format(char *dest, char *fmt)'.
     * \param dateStr the string to be parsed.
     * \param fmt the format the datetime must be decoded.
     * \returns self for convenience.
     * \throws xMettle if the format does not match the srcDate.
     */
   DateTime &parse(const char *srcDate, const char *fmt);


   /** \brief Check if this dae & time is null.
    *  \returns true if this date is null.
    */
   bool isNull() const noexcept;

   /** \brief Assign this datetime a new value.
    *  \param year the year of the new datetime.
    *  \param month the month of the new datetime.
    *  \param day the day of the new datetime.
    *  \param hour the hour of the new datetime.
    *  \param minute the minute of the new datetime.
    *  \param second the second of the new datetime.
    *  \param tzhour the time zone hour of the new datetime, set INT8_MIN for local time zone.
    *  \param tzmin the time zone minute of the new datetime, set INT8_MIN for local time zone.
    *  \throws xMettle if the constructed date & time is not valid.
    */
   void assign(const unsigned int year,  const unsigned int month,   const unsigned int day,
               const unsigned int hours, const unsigned int minutes, const unsigned int seconds,
               const int32_t tzhour = INT8_MIN,
               const int32_t tzmin  = INT8_MIN);


   /** \brief Makes this time null.
    */
   void nullify() noexcept;

   /** \brief Same as nullify().
    */
   void clear() noexcept;

   /** \brief compare this time to another DateTime pointer, via explicit type cast.
    *  \param obj a pointer that can be directly type casted to a datetime class.
    *  \returns -1 if this time is smaller than \p obj, 0 if the times match, or +1 if this time is greater than \p obj
    */
   int _compare(const void *obj) const;


   /** \brief Add an amount to the datetime class in the specified legend.
    *  \param amount the value to add to the date.
    *  \param legend the value type to add, ie days, months, weeks, or years.
    *  \returns a referense to itself for convenience.
    */
   DateTime &add(const unsigned int amount, const Date::PeriodLegend legend) noexcept;

   /** \brief Add an amount to the datetime class in the specified legend.
    *  \param amount the value to add to the time.
    *  \param legend the value type to add, ie hours, minutes, seconds.
    *  \returns a referense to itself for convenience.
    */
   DateTime &add(const unsigned int amount, const Time::PeriodLegend legend) noexcept;

   /** \brief Subract an amount from the datetime class in the specified legend.
    *  \param amount the value to subract from the date.
    *  \param legend the value type to subract, ie days, months, weeks, or years.
    *  \returns a referense to itself for convenience.
    */
   DateTime &sub(const unsigned int amount, const Date::PeriodLegend legend) noexcept;

   /** \brief Subract an amount from the datetime class in the specified legend.
    *  \param amount the value to subract from the time.
    *  \param legend the value type to add, ie hours, minutes, seconds.
    *  \returns a referense to itself for convenience.
    */
   DateTime &sub(const unsigned int amount, const Time::PeriodLegend legend) noexcept;

   /** \brief Operator overload for = to assign the datetime from an existing datetime object.
    *  \param inDateTime the datetime to become.
    *  \returns itself.
    */
   DateTime & operator = (const DateTime &inDateTime) noexcept;

   /** \brief Operator overload for = to assign the datetime from a date object.
    *  \param inDate the date to become.
    *  \returns itself.
    */
   DateTime & operator = (const Date &inDate) noexcept;

   /** \brief Operator overload for = to assign the datetime from a time object.
    *  \param inTime the time to become.
    *  \returns itself.
    */
   DateTime & operator = (const Time &inTime) noexcept;

   /** \brief Operator overload for = to assign the datetime from a struct tm object.
    *  \param inDateTime the struct tm to become.
    *  \returns itself.
    *  \throw xMettle if the struct tm is not a valid datetime.
    */
   DateTime & operator = (const struct tm  &inDateTime);

   /** \brief Operator overload for = to assign the datetime from a string in YYYYMMDDHHMMSS(TZ) format.
    *  \param yyyymmddhhmmss the string in the datetime format to become, the (tz) part is optional and is in format (+/-HHMM).
    *  \returns itself.
    *  \throw xMettle if \p yyyymmddhhmmss is not a valid datetime.
    */
   DateTime & operator = (const char *yyyymmddhhmmss);

   /** \brief Less than friend operator overload between two datetimes.
    *  \param dt1 the first datetime to compare.
    *  \param dt2 the second datetime to compare.
    *  \returns true if \p dt1 is less than \p dt2.
    */
   friend bool operator <  (const DateTime &dt1, const DateTime &dt2) noexcept;

   /** \brief Greater than friend operator overload between two datetimes.
    *  \param dt1 the first datetime to compare.
    *  \param dt2 the second datetime to compare.
    *  \returns true if \p dt1 is greater than \p dt2.
    */
   friend bool operator >  (const DateTime &dt1, const DateTime &dt2) noexcept;

   /** \brief Less Equal than friend operator overload between two datetimes.
    *  \param dt1 the first datetime to compare.
    *  \param dt2 the second datetime to compare.
    *  \returns true if \p dt1 is less than or eqaul to \p dt2.
    */
   friend bool operator <= (const DateTime &dt1, const DateTime &dt2) noexcept;

   /** \brief Greater Equal than friend operator overload between two datetimes.
    *  \param dt1 the first datetime to compare.
    *  \param dt2 the second datetime to compare.
    *  \returns true if \p dt1 is greater than or equal to \p dt2.
    */
   friend bool operator >= (const DateTime &dt1, const DateTime &dt2) noexcept;

   /** \brief Equals friend operator overload between two datetimes.
    *  \param dt1 the first datetime to compare.
    *  \param dt2 the second datetime to compare.
    *  \returns true if \p dt1 equals \p dt2.
    */
   friend bool operator == (const DateTime &dt1, const DateTime &dt2) noexcept;

   /** \brief Not Equals friend operator overload between two datetimes.
    *  \param dt1 the first datetime to compare.
    *  \param dt2 the second datetime to compare.
    *  \returns true if \p dt1 does not equal \p dt2.
    */
   friend bool operator != (const DateTime &dt1, const DateTime &dt2) noexcept;

   /** \brief Ouput stream operator.
    *  \param output the output stream, date is formatted into the standard date time format 'YYYY-MM-DD HH:MM:SS'.
    *  \param dt the datetime object to be output.
    *  \returns the output stream as expected.
    */
   friend std::ostream &operator << (std::ostream &output, const DateTime &dt) noexcept;

   /** \brief Input stream operator.
    *  \param input the input stream.
    *  \param dt the datetime object to receive the value.
    *  \returns the input stream as expected.
    *  \throws xMettle if the input string was not in the standard date time format 'YYYY-MM-DD HH:MM:SS'.
    */
   friend std::istream &operator >> (std::istream &input, DateTime &dt);

   /** \brief Check if the string in the YYYYMMDDHHMMSS format is a valid datetime.
    *  \param yyyymmddhhmmss the string to check.
    *  \returns true if \p yyyymmddhhmmss is a valid datetime.
    */
   static bool validDateTime(const char *yyyymmddhhmmss) noexcept;

   /** \brief Gets a new DateTime object with date & time set to the current machine clock.
    *  \returns A new datetime object with the datetime set to now.
    */
   static DateTime now() noexcept;

   /** \brief Trys to parses the date/time from the specfified format string. See 'char *format(char *dest, char *fmt)'.
     * \param dateStr the string to be parsed.
     * \param fmt the format the datetime must be decoded.
     * \param outDate if successful, the resulten date is persisted to this param, if it fails this param remains unchanged.
     * \returns true if successful, or false on failure.
     */
   static bool tryParse(const char *srcDate, const char *fmt, DateTime &outDate);

   /** \brief Trys to parses the date/time from the specfified format string. See 'char *format(char *dest, char *fmt)'.
     * \param dateStr the string to be parsed.
     * \param fmt the format the datetime must be decoded.
     * \param outDate if successful, the resulten date is persisted to this param, if it fails this param remains unchanged.
     * \returns true if successful, or false on failure.
     */
   static bool tryParse(const String &srcDate, const char *fmt, DateTime &outDate);

   /** \brief Returns the standard format string for datetimes, ie 'YYYY-MM-DD HH:MM:SS'.
     * \returns The standard format string.
     */
   static const char *stdDateTimeFormat();

   /** \brief Sets the local time zone for all future datetime declartion and timezone instructions, note not thread safe.
    *  \param tz if this is null local timezone is set, else set the timezone using format in (+/-)HHMM.
    */
   static void setLocalTZ(const char *tz);

   /** \brief Sets the local time zone for all future datetime declartion and timezone instructions, note note thread safe.
    *  \param tzhour the hour to set, INT8_MIN to set local timezone.
    *  \param tzmin the minute to set, INT8_MIN to set local timezone.
    */
   static void setLocalTZ(const int8_t tzhour = INT8_MIN, const int8_t tzmin = INT8_MIN);

   /** \brief Sets the local time zone for all future datetime declartion and timezone instructions, note note thread safe.
    *  \param tzoffset the offset into seconds to set the local timezone, INT32_MIN to set local time.
    */
   static void setLocalTZ(const int32_t tzoffset = INT32_MIN);

   /** \brief Gets the local time zone abbreviation the datetime class is currently using.
    *  \param tz an buffer string to fill the timezone with, minumum 6 characters in size.
    *  \return the \p tz buffer passed in for convenience.
    */
   static char *getLocalTZAbr(char *tz=0) noexcept;

   /** \brief Get the local tz offset and optionally hour/min the datetime class is currently using.
    *  \param tzhour if not null the the destination hour integer.
    *  \param tzmin if not null the destination minute integer.
    *  \return the the timezone offset in seconds.
    */
   static int32_t getLocalTZ(int8_t *tzhour = 0, int8_t *tzmin = 0) noexcept;

   /** \brieft Gets the best timezone abbreviation for the offset.
    *  \param tzoffset the timezone offset.
    *  \param dest a string buffer that is at least 10 characters long.
    *  \return the \p destname parameter for convenience.
    */
   static const char *getTZAbr(const int32_t tzoffset) noexcept;

   /** \brieft Calculates the time zone offset from an hour and minute value.
    *  \param tzhour the time zone hour.
    *  \param tzmin the time zone minute.
    *  \return the calculated offset.
    */
   static int32_t calcTZOffset(const int8_t tzhour, const int8_t tzmin) noexcept;

   /** \brieft Calculates the time zone hour and minute from a time zone offset.
    *  \param tzoffset the time zone offset in seconds.
    *  \param tzhour the destination hour integer.
    *  \param tzmin the destination minute integer.
    *  \return the calculated offset.
    */
   static void calcTZHourMin(const int32_t tzoffset, int8_t &tzhour, int8_t &tzmin) noexcept;


   /** \brief Public date object.
    */
   Date         date;

   /** \brief Public time object.
    */
   Time         time;

protected:

   /** \brief Protected time zone hour.
   */
   int8_t       _tzhour;

   /** \brief Protected time zone minute.
   */
   int8_t       _tzmin;

private:


   /** \brief Helper method to parse an integer from part of a string.
     * \param fc the formatter in question, ie Y for year, m for minute etc, purely for error message purposes.
     * \param len the max len to read.
     * \param ptrd the char point to the integer to parse.
     * \param min the minumum value of the integer.
     * \param max the maximum value of the integer.
     * \return the parsed value.
     */
   static int _parseInt(const char fc, const int len, const char *ptrd, const int min, const int max);


   /** \brief Parses a timezone string and applys it to this datetime object.
    *  \param tz the timezone in format (+/-HHMM).
   */
   void _timeZoneFromStr(const char *tz);

   /** \brief Check if the string in the +/-HHMM format is a valid timezone.
    *  \param tz the string to check.
    *  \param throwError if true, throws an exception if not valid, else just return false.
    *  \param outHour if not null, set the hour into this pointer.
    *  \param outMin if not null, set the minute into this pointer.
    *  \returns true if \p tz is a valid timezone.
    */
   static bool _validateTZ(const char *tz, const bool throwError = true, int8_t *outHour = 0, int8_t *outMin = 0);

   /** \brief Check if the hour/min combination is a valid time zone.
    *  \param tzhour the hour to check.
    *  \param tzmin the minute to check.
    *  \param throwError if true, throws an exception if not valid, else just return false.
    *  \returns true if \p tzhour and tzminute is a valid timezone.
    */
   static bool _validateTZ(const int8_t tzhour, const int8_t tzmin, const bool throwError = true);

   static int32_t _osLocalTZ(int8_t *tzhour, int8_t *tzmin) noexcept;
};


}}


#endif
