/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/datavar.h"

#define __STDC_FORMAT_MACROS 1
#define __STDC_LIMIT_MACROS

#include <limits.h>
#include <cstdlib>
#include <stdint.h>
#include <inttypes.h>

#include "mettle/lib/c99standard.h"
#include "mettle/lib/common.h"
#include "mettle/lib/datanode.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/uchar.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

#define DT_DATE_FMT       "%Y-%m-%d"
#define DT_TIME_FMT       "%H:%M:%S"
#define DT_DATETIME_FMT   "%Y-%m-%d %H:%M:%S"


DataVar::List::List() noexcept
{
}


DataVar::List::~List() noexcept
{
}


DataVar::DataVar() noexcept
{
   _tleg         = tl_null;
   _value._void  = 0;
}


DataVar::DataVar(const TypeLegend tleg)
{
   _tleg         = tl_null;
   _value._void  = 0;

   _initType(tleg);
}


DataVar::~DataVar() noexcept
{
   destroy();
}


void DataVar::destroy() noexcept
{
   if (_value._void)
   {
      if (_tleg == tl_string)
         delete _value._string;
      else if (_tleg == tl_ustring)
         delete _value._ustring;
      else if (_tleg == tl_date)
         delete _value._date;
      else if (_tleg == tl_time)
         delete _value._time;
      else if (_tleg == tl_datetime)
         delete _value._datetime;
      else if (_tleg == tl_memoryBlock)
         delete _value._memoryBlock;
      else if (_tleg == tl_list)
         delete _value._list;
      else if (_tleg == tl_dataNode)
         delete _value._dataNode;
      else if (_tleg == tl_c_string)
         free(_value._c_string);
   }

   _value._void = 0;
   _tleg        = tl_null;
}


void DataVar::clear() noexcept
{
   if (!_value._void)
      return;

   if (_tleg == tl_string)
      _value._string->clear();
   else if (_tleg == tl_ustring)
      _value._ustring->clear();
   else if (_tleg == tl_date)
      _value._date->clear();
   else if (_tleg == tl_time)
      _value._time->clear();
   else if (_tleg == tl_datetime)
      _value._datetime->clear();
   else if (_tleg == tl_memoryBlock)
      _value._memoryBlock->clear();
   else if (_tleg == tl_list)
      _value._list->clear();
   else if (_tleg == tl_dataNode)
      _value._dataNode->clear();
   else if (_tleg == tl_c_string)
      _value._c_string[0] = 0;
   else if (_tleg == tl_guid)
      _value._guid->clear();
   else
      _value._void = 0;
}


const void *DataVar::valuePtr() const noexcept
{
   return (const void *) _value._void;
}


const DataVar::TypeLegend DataVar::valueType() const noexcept
{
   return _tleg;
}


const bool DataVar::valueIsNull() const noexcept
{
   return _value._void == 0 || _tleg == tl_null;
}


const bool DataVar::valueIsEmpty() const noexcept
{
   if (_value._void == 0 || _tleg == tl_null)
      return true;

   if (_tleg == tl_string)
      return _value._string->isEmpty();

   if (_tleg == tl_ustring)
      return _value._ustring->isEmpty();

   if (_tleg == tl_date)
      return _value._date->isNull();

   if (_tleg == tl_time)
      return _value._time->isNull();

   if (_tleg == tl_datetime)
      return _value._datetime->isNull();

   if (_tleg == tl_memoryBlock)
      return _value._memoryBlock->isEmpty();

   if (_tleg == tl_list)
      return _value._list->count() == 0;

   if (_tleg == tl_dataNode)
      return _value._dataNode->valueIsEmpty();

   if (_tleg == tl_c_string)
      return _value._c_string == 0 || _value._c_string[0] == 0;

   if (_tleg == tl_guid)
      return _value._guid->isNull();

   return false;
}


const bool DataVar::valueTypeIsInteger() const noexcept
{
   if (_tleg == tl_int8   ||
       _tleg == tl_int16  ||
       _tleg == tl_int32  ||
       _tleg == tl_int64  ||
       _tleg == tl_uint8  ||
       _tleg == tl_uint16 ||
       _tleg == tl_uint32 ||
       _tleg == tl_uint64)
      return true;

   return false;
}


const bool DataVar::valueTypeIsString() const noexcept
{
   if (_tleg == tl_string   ||
       _tleg == tl_ustring  ||
       _tleg == tl_c_string)
      return true;

   return false;
}


void DataVar::valueSetType(const TypeLegend tleg)
{
   if (_tleg == tleg)
      return;

   if (_tleg == tl_null)
   {
      _initType(tleg);

      return;
   }

   DataVar safeTmp;

   safeTmp._value._void = _value._void;
   safeTmp._tleg        = _tleg;
   _value._void         = 0;

   _initType(tleg);

   if (safeTmp._tleg == tl_null)
      return;

   if (tleg == tl_string)
   {
      if (safeTmp._tleg == tl_ustring)
      {
         _value._string->eat(*safeTmp._value._ustring);
         return;
      }
      else if (safeTmp._tleg == tl_c_string)
      {
         _value._string->eat(safeTmp._value._c_string);
         safeTmp._value._c_string = 0;
         return;
      }

      safeTmp.toString(*_value._string);
      return;
   }

   if (tleg == tl_ustring)
   {
      if (safeTmp._tleg == tl_string)
      {
         _value._ustring->eat(*safeTmp._value._string);
         return;
      }
      else if (safeTmp._tleg == tl_c_string)
      {
         _value._ustring->eat(safeTmp._value._c_string);
         safeTmp._value._c_string = 0;
         return;
      }

      String tmp;

      safeTmp.toString(tmp);

      _value._ustring->eat(tmp);

      return;
   }

   if (tleg == tl_c_string)
   {
      if (safeTmp._tleg == tl_string)
      {
         _value._c_string = safeTmp._value._string->poop();
         return;
      }
      else if (safeTmp._tleg == tl_ustring)
      {
         _value._c_string = safeTmp._value._ustring->poop();
         return;
      }

      String tmp;

      safeTmp.toString(tmp);

      _value._c_string = tmp.poop();

      return;
   }

   if (tleg == tl_int8)
     safeTmp.read(_value._int8);
   else if (tleg == tl_int16)
     safeTmp.read(_value._int16);
   else if (tleg == tl_int32)
     safeTmp.read(_value._int32);
   else if (tleg == tl_int64)
     safeTmp.read(_value._int64);
   else if (tleg == tl_uint8)
     safeTmp.read(_value._uint8);
   else if (tleg == tl_uint16)
     safeTmp.read(_value._uint16);
   else if (tleg == tl_uint32)
     safeTmp.read(_value._uint32);
   else if (tleg == tl_uint64)
     safeTmp.read(_value._uint64);
   else if (tleg == tl_bool)
     safeTmp.read(_value._bool);
   else if (tleg == tl_char)
     safeTmp.read(_value._c_char);
   else if (tleg == tl_float)
     safeTmp.read(_value._float);
   else if (tleg == tl_double)
     safeTmp.read(_value._double);
   else if (tleg == tl_date)
     safeTmp.read(*_value._date);
   else if (tleg == tl_time)
     safeTmp.read(*_value._time);
   else if (tleg == tl_datetime)
     safeTmp.read(*_value._datetime);
   else if (tleg == tl_guid)
     safeTmp.read(*_value._guid);
}

int DataVar::toInteger() const
{
   if (_tleg == tl_string)
      return _value._string->toInt();

   if (_tleg == tl_ustring)
      return _value._ustring->toInt();

   if (_tleg == tl_c_string)
      return _value._c_string ? atoi(_value._c_string) : 0;

   if (_tleg == tl_char)
      return (int) _value._c_char;

   if (_tleg == tl_int8)
      return (int) _value._int8;

   if (_tleg == tl_int16)
      return (int) _value._int16;

   if (_tleg == tl_int32)
      return (int) _value._int32;

   if (_tleg == tl_int64)
      return (int) _value._int64;

   if (_tleg == tl_uint8)
      return (int) _value._uint8;

   if (_tleg == tl_uint16)
      return (int) _value._uint16;

   if (_tleg == tl_uint32)
      return (int) _value._uint32;

   if (_tleg == tl_uint64)
      return (int) _value._uint64;

   if (_tleg == tl_double)
      return (int) _value._double;

   if (_tleg == tl_float)
      return (int) _value._float;

   if (_tleg == tl_bool)
      return (int) _value._bool;

   return 0;
}


DataVar::TypeLegend DataVar::toBestType()
{
   if (!valueTypeIsString())
      return _tleg;

   if (_tleg == tl_c_string)
      detectBestType(_value._c_string, this, 0);
   else if (_tleg == tl_string)
      detectBestType(_value._string->c_str(), this, _value._string->length());
   else if (_tleg == tl_ustring)
      detectBestType(_value._ustring->c_str(), this, _value._ustring->length());

   return _tleg;
}


String DataVar::toString() const
{
   String res;

   return toString(res);
}

String& DataVar::toString(String &dest) const
{
   dest.clear();

   if (_tleg == tl_null)
      return dest;

   if (_tleg == tl_string)
      dest = *_value._string;
   else if (_tleg == tl_ustring)
      dest = _value._ustring->c_str();
   else if (_tleg == tl_c_string)
      dest = _value._c_string;
   else if (_tleg == tl_char)
      dest = _value._c_char;
   else if (_tleg == tl_int8)
      dest = _value._int8;
   else if (_tleg == tl_int16)
      dest = _value._int16;
   else if (_tleg == tl_int32)
      dest = _value._int32;
   else if (_tleg == tl_int64)
      dest = (int) _value._int64;
   else if (_tleg == tl_uint8)
      dest = _value._uint8;
   else if (_tleg == tl_uint16)
      dest = _value._uint16;
   else if (_tleg == tl_uint32)
      dest = _value._uint32;
   else if (_tleg == tl_uint64)
      dest = (unsigned int) _value._uint64;
   else if (_tleg == tl_double)
      dest = _value._double;
   else if (_tleg == tl_float)
      dest = (double) _value._float;
   else if (_tleg == tl_bool)
      dest = _value._bool;

   if (_value._void == 0)
      return dest;

   if (_tleg == tl_date)
   {
      char jotter[32];
      dest = _value._date->format(jotter, DT_DATE_FMT);
   }
   else if (_tleg == tl_time)
   {
      char jotter[32];
      dest = _value._time->format(jotter, DT_TIME_FMT);
   }
   else if (_tleg == tl_datetime)
   {
      char jotter[32];
      dest = _value._datetime->format(jotter, DT_DATETIME_FMT);
   }
   else if (_tleg == tl_guid)
   {
      char jotter[38];
      dest = _value._guid->toString(jotter);
   }
   else if (_tleg == tl_memoryBlock)
      dest.format("MemoryBlock[size: %u]", _value._memoryBlock->size());
   else if (_tleg == tl_list)
      dest.format("List[count: %u]", _value._list->count());
   else if (_tleg == tl_dataNode)
      dest.format("DataNode[name: %s]", _value._dataNode->name().c_str());

   return dest;
}


const char *DataVar::c_str() const noexcept
{
   if (_value._void == 0)
      return 0;

   if (_tleg == tl_string)
      return _value._string->c_str();

   if (_tleg == tl_ustring)
      return _value._ustring->c_str();

   if (_tleg == tl_c_string)
      return _value._c_string;

   if (_tleg == tl_bool)
      return _value._bool ? "true" : "false";

   return 0;
}


void DataVar::nullify() noexcept
{
   destroy();
}


bool DataVar::read(Mettle::Lib::String &val)
{
   toString(val);

   return true;
}


bool DataVar::read(Mettle::Lib::UString &val)
{
   String tmp;

   toString(tmp);

   val.eat(tmp);

   return true;
}


bool DataVar::read(Mettle::Lib::Date &val)
{
   if (_tleg == tl_date)
   {
      val = *_value._date;
      return true;
   }

   if (_tleg == tl_datetime)
   {
      val = _value._datetime->date;
      return true;
   }

   String tmp;

   toString(tmp).replace("-", "");

   if (Date::validDate(tmp.c_str()))
   {
      val = tmp.c_str();
      return true;
   }

   val.nullify();
   return false;
}


bool DataVar::read(Mettle::Lib::Time &val)
{
   if (_tleg == tl_time)
   {
      val = *_value._time;
      return true;
   }

   if (_tleg == tl_datetime)
   {
      val = _value._datetime->time;
      return true;
   }

   String tmp;

   toString(tmp).replace(":", "");

   if (Time::validTime(tmp.c_str()))
   {
      val = tmp.c_str();
      return true;
   }

   val.nullify();
   return false;
}


bool DataVar::read(Mettle::Lib::DateTime &val)
{
   if (_tleg == tl_datetime)
   {
      val = *_value._datetime;
      return true;
   }

   if (_tleg == tl_date)
   {
      val.date = *_value._date;
      val.time = 0;
      return true;
   }

   if (_tleg == tl_time)
   {
      val.date = 0;
      val.time = *_value._time;
      return true;
   }

   String tmp;

   toString(tmp).replace("-", "").replace(" ", "").replace(":", "");

   if (tmp.length() == 8)
   {
      if (Date::validDate(tmp.c_str()))
      {
         val.date = tmp.c_str();
         return true;
      }
   }
   else if (tmp.length() == 6)
   {
      if (Time::validTime(tmp.c_str()))
      {
         val.time = tmp.c_str();
         return true;
      }
   }
   else if (tmp.length() == 14)
   {
      char *tptr = (char *) tmp.c_str() + 8;

      if (Time::validTime(tptr))
      {
         val.time = tptr;

         *tptr = 0;

         if (Date::validDate(tmp.c_str()))
         {
            val.date = tmp.c_str();
            return true;
         }
      }
   }

   val.nullify();

   return false;
}


bool DataVar::read(char &val) noexcept
{
   if (_tleg == tl_char)
   {
      val = _value._c_char;
      return true;
   }

   if (_tleg == tl_string)
   {
      val = _value._string->c_str()[0];
      return true;
   }

   if (_tleg == tl_ustring)
   {
      val = _value._ustring->c_str()[0];
      return true;
   }

   if (_tleg == tl_c_string)
   {
      val = _value._c_string == 0 ? 0 : *_value._c_string;
      return true;
   }

   if (_tleg == tl_bool)
   {
      val = _value._bool ? '1' : '0';
      return true;
   }

   return false;
}

bool DataVar::read(bool &val) noexcept
{
   if (_tleg == tl_bool)
   {
      val = _value._bool;
      return true;
   }

   if (_tleg == tl_char)
   {
      val = (bool) _value._c_char;
      return true;
   }

   if (_tleg == tl_string)
   {
      val = _value._string->toBool();
      return true;
   }

   if (_tleg == tl_ustring)
   {
      val = _value._ustring->toBool();
      return true;
   }

   if (_tleg == tl_c_string)
   {
      val = String(_value._c_string).toBool();
      return true;
   }

   if (valueTypeIsInteger())
   {
      val = (bool) toInteger();
      return true;
   }

   return false;
}

bool DataVar::read(int8_t &val)
{
   if (_tleg == tl_int8)
   {
      val = _value._int8;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (int8_t) toInteger();
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isInt())
      {
         val = (int8_t) tmp.toInt();
         return true;
      }
   }

   return false;
}


bool DataVar::read(int16_t &val)
{
   if (_tleg == tl_int16)
   {
      val = _value._int16;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (int16_t) toInteger();
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isInt())
      {
         val = (int16_t) tmp.toInt();
         return true;
      }
   }

   return false;
}


bool DataVar::read(int32_t &val)
{
   if (_tleg == tl_int32)
   {
      val = _value._int32;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (int32_t) toInteger();
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isInt())
      {
         val = (int32_t) tmp.toInt();
         return true;
      }
   }

   return false;
}


bool DataVar::read(int64_t &val)
{
   if (_tleg == tl_int64)
   {
      val = _value._int64;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (int64_t) toInteger();
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isInt())
      {
         val = (int64_t) tmp.toInt();
         return true;
      }
   }

   return false;
}


bool DataVar::read(uint8_t &val)
{
   if (_tleg == tl_uint8)
   {
      val = _value._uint8;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (uint8_t) toInteger();
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isUnsigned())
      {
         val = (uint8_t) tmp.toUInt();
         return true;
      }
   }

   return false;
}


bool DataVar::read(uint16_t &val)
{
   if (_tleg == tl_uint16)
   {
      val = _value._uint16;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (uint16_t) toInteger();
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isUnsigned())
      {
         val = (uint16_t) tmp.toUInt();
         return true;
      }
   }

   return false;
}


bool DataVar::read(uint32_t &val)
{
   if (_tleg == tl_uint32)
   {
      val = _value._uint32;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (uint32_t) toInteger();
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isUnsigned())
      {
         val = (uint32_t) tmp.toUInt();
         return true;
      }
   }

   return false;
}


bool DataVar::read(uint64_t &val)
{
   if (_tleg == tl_uint64)
   {
      val = _value._uint64;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (uint64_t) toInteger();
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isUnsigned())
      {
         val = (uint64_t) tmp.toInt();
         return true;
      }
   }

   return false;
}


bool DataVar::read(float &val)
{
   if (_tleg == tl_float)
   {
      val = _value._float;
      return true;
   }

   if (_tleg == tl_double)
   {
      val = (float) _value._double;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (float) _value._int32;
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isDouble())
      {
         val = (float) tmp.toDouble();
         return true;
      }
   }

   return false;
}


bool DataVar::read(double &val)
{
   if (_tleg == tl_double)
   {
      val = _value._double;
      return true;
   }

   if (_tleg == tl_float)
   {
      val = (double) _value._float;
      return true;
   }

   if (valueTypeIsInteger() || _tleg == tl_bool)
   {
      val = (double) _value._int32;
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (tmp.isDouble())
      {
         val = tmp.toDouble();
         return true;
      }
   }

   return false;
}


bool DataVar::read(Mettle::Lib::Guid &val)
{
   if (_tleg == tl_guid)
   {
      val = *_value._guid;
      return true;
   }

   if (valueTypeIsString())
   {
      String tmp;

      toString(tmp);

      if (Guid::validGuid(tmp))
      {
         val = tmp;
         return true;
      }
   }

   return false;
}


void DataVar::write(const Mettle::Lib::String &val)
{
   _initType(tl_string);

   *_value._string = val;
}


void DataVar::write(const Mettle::Lib::UString &val)
{
   _initType(tl_ustring);

   *_value._ustring = val;
}


void DataVar::write(const Mettle::Lib::Date &val)
{
   _initType(tl_date);

   *_value._date = val;
}


void DataVar::write(const Mettle::Lib::Time &val)
{
   _initType(tl_time);

   *_value._time = val;
}


void DataVar::write(const Mettle::Lib::DateTime &val)
{
   _initType(tl_datetime);

   *_value._datetime = val;
}


void DataVar::write(const Mettle::Lib::MemoryBlock &val)
{
   _initType(tl_memoryBlock);

   *_value._memoryBlock = val;
}


void DataVar::write(const DataVar::List &val)
{
   destroy();

   _tleg        = tl_list;
   _value._list = new List(val);
}


void DataVar::write(const DataNode &val)
{
   _initType(tl_dataNode);

   _tleg             = tl_dataNode;
   *_value._dataNode = val;
}


void DataVar::write(const char *val)
{
   _initType(tl_c_string);

   unsigned int len = strlen(val) + 1;

   _value._c_string  = (char *) Common::memoryAlloc(len);

   strcpy(_value._c_string, val);
}


void DataVar::write(const bool val) noexcept
{
   _initType(tl_bool);

   _value._bool = val;
}


void DataVar::write(const char val) noexcept
{
   _initType(tl_char);

   _value._c_char = val;
}


void DataVar::write(const int8_t val) noexcept
{
   _initType(tl_int8);

   _value._int8 = val;
}


void DataVar::write(const int16_t val) noexcept
{
   _initType(tl_int16);

   _value._int16 = val;
}


void DataVar::write(const int32_t val) noexcept
{
   _initType(tl_int32);

   _value._int32 = val;
}


void DataVar::write(const int64_t val) noexcept
{
   _initType(tl_int64);

   _value._int64 = val;
}


void DataVar::write(const uint8_t val) noexcept
{
   _initType(tl_uint8);

   _value._uint8 = val;
}


void DataVar::write(const uint16_t val) noexcept
{
   _initType(tl_uint16);

   _value._uint16 = val;
}


void DataVar::write(const uint32_t val) noexcept
{
   _initType(tl_uint32);

   _value._uint32 = val;
}


void DataVar::write(const uint64_t val) noexcept
{
   _initType(tl_uint64);

   _value._uint64 = val;
}


void DataVar::write(const float val) noexcept
{
   _initType(tl_float);

   _value._float = val;
}


void DataVar::write(const double val) noexcept
{
   _initType(tl_double);

   _value._double = val;
}


void DataVar::write(const Mettle::Lib::Guid &val) noexcept
{
   _initType(tl_guid);

   *_value._guid = val;
}


void DataVar::_initType(const TypeLegend tleg)
{
   if (_tleg == tleg)
      return;

   destroy();

   _tleg = tleg;

   if (_tleg == tl_string)
      _value._string      = new String();
   else if (_tleg == tl_ustring)
      _value._ustring     = new UString();
   else if (_tleg == tl_date)
      _value._date        = new Date();
   else if (_tleg == tl_time)
      _value._time        = new Time();
   else if (_tleg == tl_datetime)
      _value._datetime    = new DateTime();
   else if (_tleg == tl_memoryBlock)
      _value._memoryBlock = new MemoryBlock();
   else if (_tleg == tl_list)
      _value._list        = new List();
   else if (_tleg == tl_dataNode)
      _value._dataNode    = new DataNode();
   else
      _value._void        = 0;
}

#define DV_IS_DIGIT(ch) (ch >= '0' && ch <= '9')

bool DataVar::_uintPart(uint64_t &out, const char *src, const uint64_t len, const uint64_t min, const uint64_t max)
{
   if (len == 0)
   {
      out = 0;
      return true;
   }

   if (len > 127)
      return false;

   char          jotter[128];
   unsigned int  idx;
   char         *end = 0;

   for (idx = 0; idx < len; idx++)
   {
      if (!DV_IS_DIGIT(src[idx]))
         return false;

      jotter[idx] = src[idx];
   }

   jotter[idx] = 0;

   out = std::strtoull(jotter, &end, 10);

#if defined(ENV_64)

  #if LONG_MAX == INT64_MAX || INT_MAX == INT64_MAX

   out = std::strtoul(jotter, &end, 10);

   if (out == ULONG_MAX || out < min || out > max)
      return false;

  #elif ULLONG_MAX == UINT64_MAX

   out = std::strtoull(jotter, &end, 10);

   if (out == ULLONG_MAX || out < min || out > max)
      return false;

  #else
    #error No known string converstion to unsigned int64.
  #endif

#elif defined(ENV_32)

   out = std::strtoul(jotter, &end, 10);

   if (out == ULONG_MAX || out < min || out > max)
      return false;

#endif

   if (out == ULLONG_MAX || out < min || out > max)
      return false;

   return true;
}


DataVar::TypeLegend DataVar::detectBestType(const char *inVal, DataVar *dest, unsigned int valLen)
{
   if (inVal == 0 || !*inVal)
   {
      if (dest)
         dest->nullify();
      return DataVar::tl_null;
   }

   if (valLen == 0)
      valLen = strlen(inVal);

   char *val = (char *) inVal;

   // Skip white spaces
   for (; *val; val++, valLen--)
      if (!String::isWhiteSpace(*val))
         break;

   if (*val == 0)
   {
      if (dest)
         dest->nullify();
      return DataVar::tl_null;
   }

   // Check for char
   /*if (valLen == 1 && !DV_IS_DIGIT(val[0]))
   {
      if (((int)val[0]) < 127)
      {
         if (dest)
            dest->write(*val);

         return DataVar::tl_char;
      }
   }*/

   // Check for booleans
   if (valLen == 2)
   {
      if (0 == String::strnicmp(val, "no", valLen))
      {
         if (dest)
            dest->write(false);
         return DataVar::tl_bool;
      }
   }

   if (valLen == 3)
   {
      if (0 == String::strnicmp(val, "yes", valLen))
      {
         if (dest)
            dest->write(true);
         return DataVar::tl_bool;
      }
   }

   if (valLen == 4)
   {
      if (0 == String::strnicmp(val, "true", valLen))
      {
         if (dest)
            dest->write(true);
         return DataVar::tl_bool;
      }
   }

   if (valLen == 5)
   {
      if (0 == String::strnicmp(val, "false", valLen))
      {
         if (dest)
            dest->write(false);
         return DataVar::tl_bool;
      }
   }

   // Check for Date
   if (valLen == 10 && val[4] == '-' && val[7] == '-')
   {
      uint64_t year  = 0;
      uint64_t month = 0;
      uint64_t day   = 0;

      if (_uintPart(year,  val,     4, 0, 9999) &&
          _uintPart(month, val + 5, 2, 1, 12)   &&
          _uintPart(day,   val + 8, 2, 1, 31))
      {
         if (Date::validDate(year, month, day))
         {
            if (dest)
            {
               Date dt(year, month, day);
               dest->write(dt);
            }
            return DataVar::tl_date;
         }
      }
   }

   // Check for Time
   if (valLen == 8 && val[2] == ':' && val[5] == ':')
   {
      uint64_t hour  = 0;
      uint64_t min   = 0;
      uint64_t sec   = 0;

      if (_uintPart(hour,  val,     2, 0, 23) &&
          _uintPart(min,   val + 3, 2, 0, 59) &&
          _uintPart(sec,   val + 6, 2, 0, 59))
      {
         if (dest)
         {
            Time dt(hour, min, sec);
            dest->write(dt);
         }

         return DataVar::tl_time;
      }
   }

   // Check for DateTime
   if (valLen == 19 && val[4] == '-' && val[7] == '-' && val[10] == ' ' && val[13] == ':' && val[16] == ':')
   {
      uint64_t year  = 0;
      uint64_t month = 0;
      uint64_t day   = 0;

      if (_uintPart(year,  val,     4, 0, 9999) &&
          _uintPart(month, val + 5, 2, 1, 12)   &&
          _uintPart(day,   val + 8, 2, 1, 31))
      {
         if (Date::validDate(year, month, day))
         {
            uint64_t hour  = 0;
            uint64_t min   = 0;
            uint64_t sec   = 0;

            if (_uintPart(hour,  val + 11, 2, 0, 23) &&
                _uintPart(min,   val + 14, 2, 0, 59) &&
                _uintPart(sec,   val + 17, 2, 0, 59))
            {
               if (dest)
               {
                  DateTime dt(year, month, day, hour, min, sec);
                  dest->write(dt);
               }

               return DataVar::tl_datetime;
            }
         }
      }
   }

   // Check for Guid
   if (valLen == 36 && Common::stringIsUUID(val, false))
   {
      Guid goo(val);

      dest->write(goo);

      return DataVar::tl_guid;
   }

   // Check for integer/unsigned integer/double
   {
      uint64_t  partDeci = 0;
      uint64_t  partFrac = 0;
      bool      neg;
      bool      point    = false;
      bool      ok       = true;
      char     *end      = val + valLen;
      char     *start;
      char     *ptr;

      if (*val == '-')
      {
         neg   = true;
         start = val + 1;
      }
      else if (*val == '+')
      {
         neg   = false;
         start = val + 1;
      }
      else
      {
         neg   = false;
         start = val;
      }

      for (ptr = start; *ptr && ptr < end; ptr++)
         if (!DV_IS_DIGIT(*ptr))
         {
            if (*ptr == '.')
               point = true;
            else
               ok = false;

            break;
         }

      if (ok && (ok = _uintPart(partDeci, start, ptr - start, 0, ULLONG_MAX)))
      {
         if (point)
         {
            for (start = ++ptr; *ptr && ptr < end; ptr++)
               if (!DV_IS_DIGIT(*ptr))
               {
                  ok = false;
                  break;
               }

            if (ok)
               ok = _uintPart(partFrac, start, ptr - start, 0, ULONG_MAX);
         }

         if (ok)
         {
            if (point)
            {
               if (partDeci <= ULONG_MAX || partFrac <= ULONG_MAX)
               {
                  dest->write(atof(inVal));

                  return DataVar::tl_double;
               }
            }
            else if (neg)
            {
               if (partDeci <= LLONG_MAX)
               {
                  if (partDeci <= LONG_MAX)
                  {
                     if (dest)
                     {
                        int32_t dec = 0 - (int32_t) partDeci;
                        dest->write(dec);
                     }

                     return DataVar::tl_int32;
                  }

                  if (dest)
                  {
                     int64_t dec = 0 - (int64_t) partDeci;
                     dest->write(dec);
                  }

                  return DataVar::tl_int64;
               }
            }

            if (partDeci <= LONG_MAX)
            {
               if (dest)
               {
                  int32_t dec = (int32_t) partDeci;
                  dest->write(dec);
               }

               return DataVar::tl_int32;
            }

            if (partDeci <= LLONG_MAX)
            {
               if (dest)
               {
                  int64_t dec = (int64_t) partDeci;
                  dest->write(dec);
               }

               return DataVar::tl_int64;
            }

            if (dest)
               dest->write(partDeci);

            return DataVar::tl_uint64;
         }
      }
   }

   // Just assume its a string
   if (dest)
   {
      if (dest->_tleg == tl_c_string)
      {
         if (val != dest->_value._c_string)
            dest->write(val);

         dest->valueSetType(DataVar::tl_string);
      }
      else if (dest->_tleg == tl_string)
      {
         if (val != dest->_value._string->c_str())
            dest->write(val);

         dest->valueSetType(DataVar::tl_string);
      }
      else if (dest->_tleg == tl_ustring && val != dest->_value._ustring->c_str())
      {
         dest->write(val);
         dest->valueSetType(DataVar::tl_string);
         return DataVar::tl_ustring;
      }
   }

   return DataVar::tl_string;
}

#undef DV_IS_DIGIT

#undef DT_DATE_FMT
#undef DT_TIME_FMT
#undef DT_DATETIME_FMT

}}
