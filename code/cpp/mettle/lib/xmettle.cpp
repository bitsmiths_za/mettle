/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/xmettle.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined (OS_MS_WINDOWS)

  #define WIN32_LEAN_AND_MEAN
  #include <windows.h>

   #if !defined(WINCE)

      #include <errno.h>

   #endif

#else

   #include <errno.h>

#endif


namespace Mettle { namespace Lib {

xMettle::xMettle(const char *filename,
                 const int   line,
                 const char *source,
                 const char *fmt, ...)  noexcept : std::exception()
{
   this->filename       = filename;
   this->line           = line;
   this->errorCode      = StandardException;
   this->sysErrorNo     = getOSError(this->sysErrorString, sizeof(this->sysErrorString) - 1);

   strcpy(this->source, source);

   va_list ap;

   va_start(ap, fmt);

   if (vsnprintf(errorMsg, sizeof(errorMsg) - 2, fmt, ap) == -1)
      errorMsg[sizeof(errorMsg) - 1] = 0;

   va_end(ap);
}

xMettle::xMettle(const char *filename,
                 const int   line,
                 const char *source)  noexcept : std::exception()
{
   this->filename       = filename;
   this->line           = line;
   this->errorCode      = StandardException;
   this->sysErrorNo     = getOSError(this->sysErrorString, sizeof(this->sysErrorString) - 1);
   this->errorMsg[0]    = 0;

   strcpy(this->source, source);
}

xMettle::xMettle(const char  *filename,
                 const int    line,
                 const eCode  errorCode,
                 const char  *source,
                 const char  *fmt, ...) noexcept : std::exception()
{
   this->filename       = filename;
   this->line           = line;
   this->errorCode      = errorCode;
   this->sysErrorNo     = getOSError(this->sysErrorString, sizeof(this->sysErrorString) - 1);

   strcpy(this->source, source);

   va_list ap;

   va_start(ap, fmt);

   if (vsnprintf(errorMsg, sizeof(errorMsg) - 2, fmt, ap) == -1)
      errorMsg[sizeof(errorMsg) - 1] = 0;

   va_end(ap);
}

xMettle::xMettle(const xMettle &x) noexcept
{
   this->filename = x.filename;
   this->line     = x.line;

   strcpy(this->source, x.source);
   strcpy(this->errorMsg, x.errorMsg);
}

#if defined (COMPILER_GNU)
const char *xMettle::what() const noexcept
#else
const char *xMettle::what() const
#endif
{
   return errorMsg;
}

int xMettle::getOSError(char *dest, unsigned int destSize, int errCode)
{
#if defined (OS_MS_WINDOWS)

   if (errCode == 0)
      errCode = GetLastError();

   #ifdef UNICODE

      WCHAR buffer[255];

      if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,
                        0,
                        errCode,
                        0,
                        buffer,
                        sizeof(buffer) - 1,
                        0) == 0)
         snprintf(dest, destSize - 1, "Unknown Windows Error (%d)", errCode);
      else // wack of the \r\n windows puts into its error message
      {
         snprintf(dest, destSize - 1, "%ls", buffer);

         if (strlen(dest) > 2)
            dest[strlen(dest) - 2] = 0;
      }

   #else

      if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,
                        0,
                        errCode,
                        0,
                        dest,
                        destSize - 1,
                        0) == 0)
         snprintf(dest, destSize - 1, "Unknown Windows Error (%d)", errCode);
      else // wack of the \r\n windows puts into its error message
      {
         if (strlen(dest) > 2)
            dest[strlen(dest) - 2] = 0;
      }

   #endif

   return errCode;

#else

   if (errCode == 0)
      errCode = errno;

   strncpy(dest, strerror(errCode), destSize - 1);

   dest[destSize - 1] = 0;

   return errCode;

#endif
}

bool xMettle::isDBError() const noexcept
{
   if (errorCode >= DBStandardError && errorCode <= DB_MAX_ERROR)
      return true;

   return false;
}

bool xMettle::isComsError() const noexcept
{
   if (errorCode >= ComsStandardError && errorCode <= COMS_MAX_ERROR)
      return true;

   return false;
}

bool xMettle::isStandardError() const noexcept
{
   if (errorCode >= 0 && errorCode <= STANDARD_MAX_ERROR)
      return true;

   return false;
}

bool xMettle::isDBBrokenConnection() const noexcept
{
   return errorCode == DBBokenConnection;
}

xTerminate::xTerminate(const char *filename,
                       const int   line,
                       const char *source,
                       const char *fmt, ...) noexcept
           :xMettle(filename,
                   line,
                   TerminalException,
                   source,
                   "")
{
   va_list ap;

   va_start(ap, fmt);

   if (vsnprintf(errorMsg, sizeof(errorMsg) - 2, fmt, ap) == -1)
      errorMsg[sizeof(errorMsg) - 1] = 0;

   va_end(ap);
}


xTerminate::xTerminate(const char  *filename,
                       const int    line,
                       const eCode  errorCode,
                       const char  *source,
                       const char  *fmt, ...) noexcept
           :xMettle(filename,
                   line,
                   errorCode,
                   source,
                   "")
{
   va_list ap;

   va_start(ap, fmt);

   if (vsnprintf(errorMsg, sizeof(errorMsg) - 2, fmt, ap) == -1)
      errorMsg[sizeof(errorMsg) - 1] = 0;

   va_end(ap);
}


xTerminate::xTerminate(const xTerminate &x) noexcept
           :xMettle(x)
{
}

}}
