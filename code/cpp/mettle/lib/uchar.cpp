/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

/*
  Note, many of the u8_ methods are based or directly copied from the work of:

     utf8.c, uf8.h

     Basic UTF-8 manipulation routines
     by Jeff Bezanson
     placed in the public domain Fall 2005

     This code is designed to provide the utilities you need to manipulate
     UTF-8 as an internal string encoding. These functions do not perform the
     error checking normally needed when handling UTF-8 data, so if you happen
     to be from the Unicode Consortium you will want to flay me alive.
     I do this because error checking can be performed at the boundaries (I/O),
     with these routines reserved for higher performance on data known to be
     valid.
*/


#include "mettle/lib/uchar.h"

#include <stdlib.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>

#include <iostream>
#include <cwctype>
#include <clocale>

#include "mettle/lib/common.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/macros.h"

#define ISUTF8(c)   (((c)&0xC0)!=0x80)

static const uint32_t g_OffsetsFromUTF8[6] =
{
   0x00000000UL,
   0x00003080UL,
   0x000E2080UL,
   0x03C82080UL,
   0xFA082080UL,
   0x82082080UL
};

namespace Mettle { namespace Lib {


bool UChar::u8_is_ascii(const char *ch) noexcept
{
   return ISUTF8(ch[0]) && (ch[1] == 0 || ISUTF8(ch[1]));
}


bool UChar::u8_is_utf8(const char ch) noexcept
{
   return ISUTF8(ch);
}


bool UChar::u8_local_is_utf8(const char *local) noexcept
{
   const char *ptr = local ? local : setlocale(LC_ALL, NULL);
   const char *encoding;

   for (; *ptr != '\0' && *ptr != '@' && *ptr != '+' && *ptr != ','; ptr++)
   {
      if (*ptr == '.')
      {
         encoding = ++ptr;

         for (; *ptr != '\0' && *ptr != '@' && *ptr != '+' && *ptr != ','; ptr++);

         if ((ptr - encoding == 5 && !strncmp(encoding, "UTF-8", 5)) ||
             (ptr - encoding == 4 && !strncmp(encoding, "utf8", 4)))
            return true;

         break;
       }
   }

   return false;
}


unsigned int UChar::u8_strlen(const char *str) noexcept
{
   unsigned int cnt;
   unsigned int idx;

   cnt = idx = 0;

   while (u8_next_char(str, idx) != 0)
      cnt++;

   return cnt;
}


uint32_t UChar::u8_next_char(const char *str, unsigned int &idx) noexcept
{
   uint32_t     ch = 0;
   unsigned int sz = 0;

   do
   {
      ch <<= 6;
      ch += (unsigned char) str[idx++];
      ++sz;
   } while (str[idx] && !ISUTF8(str[idx]));

   ch -= g_OffsetsFromUTF8[sz - 1];

   return ch;
}


const char *UChar::u8_strstr(const char *src, const char *find) noexcept
{
   return UChar::u8_strstr((char *)src, find);
}


char *UChar::u8_strstr(char *src, const char *find) noexcept
{
   char        *p1      = (char *) src;
   unsigned int findlen = strlen(find);

   for (p1 = src; *p1; u8_inc(p1))
      if (strncmp(p1, find, findlen) == 0)
         return p1;

   return 0;
}


unsigned int UChar::u8_inc(const char *s, unsigned int &i) noexcept
{
    (void)(ISUTF8(s[++i]) || ISUTF8(s[++i]) ||
           ISUTF8(s[++i]) || ++i);

    return i;
}


unsigned int UChar::u8_dec(const char *s, unsigned int &i) noexcept
{
   if (i < 1)
      return i;

   bool utfValid[4];

   utfValid[0] = i >= 4 ? ISUTF8(s[i - 4]) : false;
   utfValid[1] = i >= 3 ? ISUTF8(s[i - 3]) : false;
   utfValid[2] = i >= 2 ? ISUTF8(s[i - 2]) : false;
   utfValid[3] = i >= 1 ? ISUTF8(s[i - 1]) : false;

   if (utfValid[0])
   {
      if (utfValid[1])
      {
         if (utfValid[2])
         {
            if (utfValid[3])
               --i;
            else
               i -= 2;
         }
         else
            i -= 3;
      }
      else
         i -= 4;
   }
   else if (utfValid[1])
   {
      if (utfValid[2])
      {
         if (utfValid[3])
            --i;
         else
            i -= 2;
      }
      else
         i -= 3;
   }
   else if (utfValid[2])
   {
      if (utfValid[3])
         --i;
      else
         i -= 2;
   }
   else
   {
      if (utfValid[3])
         --i;
      else
         i = 0; // yeah, this string is officially fucked at this point could throw an exception
   }

   return i;
}


char *UChar::u8_inc(char *&s) noexcept
{
   if (ISUTF8(*(++s)))
      return s;

   if (ISUTF8(*(++s)))
      return s;

   if (ISUTF8(*(++s)))
      return s;

   return ++s;
}


char *UChar::u8_dec(char *&s, unsigned int &i) noexcept
{
   unsigned int dex = i;

   u8_dec(s, dex);

   while (dex < i)
   {
      --s;
      --i;
   }

   return s;
}


UChar::UChar() noexcept : _char(0)
{
}


UChar::UChar(const uint32_t val) noexcept : _char(val)
{
}


UChar::UChar(const char val) noexcept : _char((uint32_t) val)
{
}


UChar::UChar(const UChar &cp) noexcept : _char(cp._char)
{
}


UChar::~UChar() noexcept
{
}


UChar &UChar::lower() noexcept
{
   return *this;
}


UChar &UChar::upper() noexcept
{
   return *this;
}


UChar &UChar::operator = (const UChar &val) noexcept
{
   _char = val._char;

   return *this;
}


UChar &UChar::operator = (const char val) noexcept
{
   _char = (uint32_t) val;

   return *this;
}


UChar &UChar::operator = (const uint32_t val) noexcept
{
   _char = val;

   return *this;
}


bool operator < (const UChar &ch, const UChar &cmp) noexcept
{
   return ch._char < cmp._char;
}


bool operator > (const UChar &ch, const UChar &cmp) noexcept
{
   return ch._char > cmp._char;
}


bool operator <= (const UChar &ch, const UChar &cmp) noexcept
{
   return ch._char <= cmp._char;
}


bool operator >= (const UChar &ch, const UChar &cmp) noexcept
{
   return ch._char >= cmp._char;
}


bool operator == (const UChar &ch, const UChar &cmp) noexcept
{
   return ch._char == cmp._char;
}


bool operator != (const UChar &ch, const UChar &cmp) noexcept
{
   return ch._char != cmp._char;
}


bool operator < (const UChar &ch, const char cmp) noexcept
{
   return ch._char < (int32_t) cmp;
}


bool operator > (const UChar &ch, const char cmp) noexcept
{
   return ch._char > (int32_t) cmp;
}


bool operator <= (const UChar &ch, const char cmp) noexcept
{
   return ch._char <= (int32_t) cmp;
}


bool operator >= (const UChar &ch, const char cmp) noexcept
{
   return ch._char >= (int32_t) cmp;
}


bool operator == (const UChar &ch, const char cmp) noexcept
{
   return ch._char == (int32_t) cmp;
}


bool operator != (const UChar &ch, const char cmp) noexcept
{
   return ch._char != (int32_t) cmp;
}


int UChar::_compare(const void *obj) const
{
   return Common::compare(_char, ((UChar *) obj)->_char);
}

#undef ISUTF8

}}
