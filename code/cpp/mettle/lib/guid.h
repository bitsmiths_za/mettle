/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_GUID_H_
#define __METTLE_LIB_GUID_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/collection.h"
#include "mettle/lib/icomparable.h"
#include "mettle/lib/safe.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include <uuid/uuid.h>

namespace Mettle { namespace Lib {

/** \class Guid guid.h mettle/lib/guid.h
 * \brief A guid object.
 *
 *  This the standard guid handling object in Mettle.
 */
class Guid : public IComparable
{
public:

   DECLARE_SAFE_CLASS(Guid);
   DECLARE_COLLECTION_LIST_CLASS(Guid);

   /** \brief Defualt constructor.
    */
   Guid() noexcept;

   /** \brief Copy constructor.
       \param inGuid the guid to copy.
    */
   Guid(const Guid &inGuid) noexcept;

   /** \brief Construct the class with a standard 'uuid_t' type.
    *  \param inGuid the uuid_t record to use when initializing the Guid class.
    */
   Guid(const uuid_t inGuid);

   /** \brief Construct the class with a string guid.
    *  \param inGuid the string that is used to be parsed into a guid.
    *  \param throwError if set to true, the constructor will raise an exception if the \p hhmmss is not a valid date.
    *  \throws xMettle if the constructed date is not valid.
    */
   Guid(const char *inGuid, const bool throwError = true);

   /** \brief Construct the class with a string guid.
    *  \param inGuid the string that is used to be parsed into a guid.
    *  \param throwError if set to true, the constructor will raise an exception if the \p hhmmss is not a valid date.
    *  \throws xMettle if the constructed date is not valid.
    */
   Guid(const String &inGuid, const bool throwError = true);

   /** \brief Destructor.
    */
   ~Guid() noexcept;

   /** \brief Converts the guid into a string.
    *  \param dest the destination string that is at least 37 charaters long, the guid is copied into it.
    *  \param forceUpperCase set this to true if you want the guid to be forced into upper case.
    *  \param forceLowerCase set this to true if you want the guid to be forced into lower case.
    *  \returns the \p dest arguement for convenience.
    */
   char *toString(char *dest, bool forceUpperCase = false, bool forceLowerCase = false) const noexcept;

   /** \brief Compare this guid to another.
    *  \param cmpGuid the guid to compare against.
    *  \returns -1 if this guid is smaller than \p cmpGuid, 0 if the times match, or +1 if this guid is greater than \p cmpGuid
    */
   int compare(const Guid *cmpGuid) const noexcept;

   /** \brief compare this guid to another Guid pointer, via explicit type cast.
    *  \param obj a pointer that can be directly type casted to a guid class.
    *  \returns -1 if this guid is smaller than \p obj, 0 if the times match, or +1 if this guid is greater than \p obj
    */
   int _compare(const void *obj) const;

   /** \brief Check if this guid is null.
    *  \returns true if this guid is null.
    */
   bool isNull() const noexcept;

   /** \brief Assign this guid a new value.
    *  \param inGuid the new value.
    */
   void assign(const uuid_t inGuid) noexcept;

   /** \brief Makes this guid null.
    */
   void nullify() noexcept;

   /** \brief Same as nullify().
    */
   void clear() noexcept;

   /** \brief Parses a guid from a string.
    *  \param inGuid the guid to become.
    *  \returns itself.
    *  \throws xMettle if the input guid is not valid.
    */
   Guid &parse(const char *inGuid);

   /** \brief Operator overload for = to assign the guid from an existing guid.
    *  \param inGuid the guid to become.
    *  \returns itself.
    */
   Guid & operator = (const Guid &inGuid) noexcept;

   /** \brief Operator overload for = to assign the guid from a uuid_t.
    *  \param inGuid the guid to become.
    *  \returns itself.
    */
   Guid & operator = (const uuid_t inGuid) noexcept;

   /** \brief Operator overload for = to assign the string in std guid format.
    *  \param inGuid the guid to become.
    *  \returns itself.
    *  \throws xMettle if the input guid is not valid.
    */
   Guid & operator = (const char *inGuid);

   /** \brief Operator overload for = to assign the string in std guid format.
    *  \param inGuid the guid to become.
    *  \returns itself.
    *  \throws xMettle if the input guid is not valid.
    */
   Guid & operator = (const String &inGuid);

   /** \brief Less than friend operator overload between two guids.
    *  \param guid the first guid to compare.
    *  \param cmdGuid the second guid to compare.
    *  \returns true if \p guid is less than \p cmdGuid.
    */
   friend bool operator <  (const Guid &guid, const Guid &cmdGuid) noexcept;

   /** \brief Greater than friend operator overload between two guids.
    *  \param guid the first guid to compare.
    *  \param cmdGuid the second guid to compare.
    *  \returns true if \p guid is greater than \p cmdGuid.
    */
   friend bool operator >  (const Guid &guid, const Guid &cmdGuid) noexcept;

   /** \brief Less Equal than friend operator overload between two guids.
    *  \param guid the first guid to compare.
    *  \param cmdGuid the second guid to compare.
    *  \returns true if \p guid is less than or eqaul to \p cmdGuid.
    */
   friend bool operator <= (const Guid &guid, const Guid &cmdGuid) noexcept;

   /** \brief Greater Equal than friend operator overload between two guids.
    *  \param guid the first guid to compare.
    *  \param cmdGuid the second guid to compare.
    *  \returns true if \p guid is greater than or equal to \p cmdGuid.
    */
   friend bool operator >= (const Guid &guid, const Guid &cmdGuid) noexcept;

   /** \brief Equals friend operator overload between two guids.
    *  \param guid the first guid to compare.
    *  \param cmdGuid the second guid to compare.
    *  \returns true if \p guid equals \p cmdGuid.
    */
   friend bool operator == (const Guid &guid, const Guid &cmdGuid) noexcept;

   /** \brief Not Equals friend operator overload between two guids.
    *  \param guid the first guid to compare.
    *  \param cmdGuid the second guid to compare.
    *  \returns true if \p guid does not equal \p cmdGuid.
    */
   friend bool operator != (const Guid &guid, const Guid &cmdGuid) noexcept;

   /** \brief Ouput stream operator.
    *  \param output the output stream, guid is formatted in the standard way.
    *  \param outGuid the guid object to be output.
    *  \returns the output stream as expected.
    */
   friend std::ostream &operator << (std::ostream &output, const Guid &outGuid) noexcept;

   /** \brief Input stream operator.
    *  \param input the input stream.
    *  \param inGuid the guid object to receive the value.
    *  \returns the input stream as expected.
    *  \throws xMettle if the input string was not a valid guid.
    */
   friend std::istream &operator >> (std::istream &input, Guid &inGuid);

   /** \brief Check if the string is a valid guid.
    *  \param inGuid the string to check.
    *  \returns true if \p inGuid is a valid guid.
    */
   static bool validGuid(const char *inGuid) noexcept;

   /** \brief Check if the string is a valid guid.
    *  \param inGuid the string to check.
    *  \returns true if \p inGuid is a valid guid.
    */
   static bool validGuid(const String &inGuid) noexcept;

   /** \brief Gets a new Guid object with a new random value.
    *  \param timeSafe if this value is set to a positive value, it will ensure a time safe is generated wait the \p timeSafe value in mili seconds between intervals.
    *  \returns A new guid object with a newly generated guid.
    */
   static Guid newGuid(int timeSafe = 0) noexcept;

private:

   uuid_t  _guid;

   void _from(const char *inGuid, const bool throwError);
};

}}


#endif
