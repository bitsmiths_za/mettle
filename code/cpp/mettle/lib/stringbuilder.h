/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_STRINGBUILDER_H_
#define __METTLE_LIB_STRINGBUILDER_H_

#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

class UString;

/** \class StringBuilder stringbuilder.h mettle/lib/stringbuilder.h
 *  \brief An effiecient string builder.
 */
class StringBuilder
{
   friend class String;
   friend class UString;

public:

   /** \brief Constructor.
    */
   StringBuilder() noexcept;

   /** \brief Destructor.
    */
   virtual ~StringBuilder() noexcept;

   /** \brief Appends the string to the to the StringBuilder
    *  \param str the string to add.
    *  \returns itself.
    *  \throws xMettle if out of memory
    */
   StringBuilder &add(const char *str);

   /** \brief Appends the string to a maximum of \p len to the StringBuilder
    *  \param str the string to add.
    *  \param len the maximum length of \p str to use.
    *  \returns itself.
    *  \throws xMettle if out of memory
    */
   StringBuilder &add(const char *str, const unsigned int len);

   /** \brief Appends a character to the StringBuilder
    *  \param ch the character to add.
    *  \returns itself.
    *  \throws xMettle if out of memory
    */
   StringBuilder &add(const char ch);

   /** \brief Inserts a string to the StringBuilder at the specified position
    *  \param str the string to insert.
    *  \param pos the position to add string from.
    *  \returns itself.
    *  \throws xMettle if out of memory, or if the position is not valid.
    */
   StringBuilder &insert(const char *str, const unsigned int pos);

   /** \brief Appends formatted string
    *  \param fmt the formatted string, see printf
    *  \param ... the arguements for \p fmt
    *  \returns itself.
    *  \throws xMettle if out of memory
    */
   StringBuilder &addFormat(const char *fmt, ...);


   /** \brief Free's all internal resources and clears the contents.
    *  \returns itself.
    */
   StringBuilder &purge() noexcept;

   /** \brief Clears the internal resources for reuse.
    *  \returns itself.
    */
   StringBuilder &clear() noexcept;

   /** \brief Consumes a char*, taking ownership
    *  \param str the char* to be consumed.
    *  \returns itself for convenience.
    */
   StringBuilder &eat(char *str) noexcept;

   /** \brief Consumes another String, freeing its contents
    *  \param str the String to be consumed.
    *  \returns itself for convenience.
    */
   StringBuilder &eat(String &str) noexcept;

   /** \brief Consumes a UString, freeing its contents
    *  \param str the UString to be consumed.
    *  \returns itself for convenience.
    */
   StringBuilder &eat(UString &str) noexcept;

   /** \brief Consumes a StringBuilder, freeing its contents
    *  \param str the StringBuilder to be consumed.
    *  \returns itself for convenience.
    */
   StringBuilder &eat(StringBuilder &str) noexcept;

   /** \brief Dispenses the internal pointer to the calling code and forgets about it.
    *  \returns the pointer to the string.
    *  \warning the calling code is responsible for freeing the returned pointer.
    */
   char *poop() noexcept;

   /** \brief Get if the string is empty.
    *  \returns true if the StringBuilder is empty, else returns false;
    */
   bool isEmpty() const noexcept;

   /** \brief Joins a list of strings together.
    *  \param first the first string to add.
    *  \param ... any number of additional strings to join
    *  \returns itself.
    *  \warning the last arguement of \p ... must be a NULL or your program will go BANG!
    *  \throws xMettle if out of memory
    */
   StringBuilder &join(const char *first, ...);

   /** \brief Joins a list of strings together.
    *  \param slist the array of strings to join
    *  \returns itself.
    *  \throws xMettle if out of memory
    */
   StringBuilder &join(const String::List &slist);

   /** \brief Joins a list of strings together with a seperator.
    *  \param sep the seperator to use between the strings.
    *  \param first the first string to add.
    *  \param ... any number of additional strings to join
    *  \returns itself.
    *  \warning the last arguement of \p ... must be a NULL or your program will go BANG!
    *  \throws xMettle if out of memory
    */
   StringBuilder &joinSep(const char *sep, const char *first, ...);

   /** \brief Joins a list of strings together with a seperator.
    *  \param sep the seperator to use between the strings.
    *  \param slist the array of strings to join
    *  \returns itself.
    *  \throws xMettle if out of memory
    */
   StringBuilder &joinSep(const char *sep, const String::List &slist);


   /** \brief Replaces all instances of \p source with \p dest.
    *  \param source the source string to be replaced
    *  \param dest the string that will replace source
    *  \returns itself.
    *  \throws xMettle if out of memory
    */
   StringBuilder &replace(const char *source, const char *dest);

   /** \brief Public string build value pointer.
    */
   char          *value;

   /** \brief Public size used variable of the vaue buffer. Should be treated as read-only.
    */
   unsigned int   used;

   /** \brief Public size variable of the value buffer. Should be treated as read-only.
    */
   unsigned int   size;

protected:

   /** \brief Adds more memory to the StringBuilder if necessary.
    *  \param needed the amount of space needed.
    *  \throws xMettle if out of memory
    */
   void add(const unsigned int needed);

   /** \brief Replaces the string at the specified position with a new string
    *  \param pos the position in the string to replace from.
    *  \param posLen the length of position to replace with.
    *  \param newStr the new string to replace \p pos with.
    *  \param newLen the length of the \p newStr to be used to replace with.
    *  \returns itself.
    *  \throws xMettle if out of memory, or if pos is out of bounds
    */
   StringBuilder &replaceAt(const unsigned int  pos,
                            const unsigned int  posLen,
                            const char         *newStr,
                            const unsigned int  newLen);

};


}}

#endif
