/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_DATE_H_
#define __METTLE_LIB_DATE_H_

#include <iostream>
#include <time.h>

#include "mettle/lib/c99standard.h"
#include "mettle/lib/collection.h"
#include "mettle/lib/icomparable.h"
#include "mettle/lib/safe.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

/** \class Date date.h mettle/lib/date.h
 * \brief A date object.
 *
 *  This the standard date handling object in Mettle.
 */
class Date : public IComparable
{
friend class DateTime;

public:

   DECLARE_SAFE_CLASS(Date);
   DECLARE_COLLECTION_LIST_CLASS(Date);

   /** \brief Date Period Legend enum.
    *
    * This is used to define a what level addition and subration operations occur.
    */
   typedef enum
   {
      Days = 0,
      Weeks,
      Months,
      Years
   } PeriodLegend;

   /** \brief Day of Week Legend enum.
    *
    * A list of all the days of the week.
    */
   typedef enum
   {
      Sunday = 0,
      Monday,
      Tuesday,
      Wednesday,
      Thursday,
      Friday,
      Saturday
   } DayOfWeekLegend;

   /** \brief Defualt constructor.
    */
   Date() noexcept;

   /** \brief Copy constructor.
    *  \param inDate the date to copy.
    */
   Date(const Date &inDate) noexcept;

   /** \brief Construct the class with a standard 'strcut tm' type.
    *  \param inDate the struct tm record to use when initializing the Date class.
    *  \param throwError if set to true, the constructor will raise an exception if the struct tm date is not valid.
    *  \throws xMettle if the constructed date is not valid.
    */
   Date(const struct tm &inDate, const bool throwErrors = true);

   /** \brief Construct the class with a year, month, day.
    *  \param year the year of the new date.
    *  \param month the month of the new date.
    *  \param day the day of the new date.
    *  \param throwError if set to true, the constructor will raise an exception if the \p year, \p month, and \p day are not a valid date.
    *  \throws xMettle if the constructed date is not valid.
    */
   Date(const unsigned int year, const unsigned int month, const unsigned int day, const bool throwErrors = true);

   /** \brief Construct the class with a string in the 'YYYYMMDD' format.
    *  \param yyyymmdd the string that is used to constructe the date, ie "20160421" for the year 21'st day of April 2016.
    *  \param throwError if set to true, the constructor will raise an exception if the \p yyyymmdd is not a valid date.
    *  \throws xMettle if the constructed date is not valid.
    */
   Date(const char *yyyymmdd, const bool throwErrors = true);

   /** \brief Construct the class with an integer in the 'YYYYMMDD' format.
    *  \param yyyymmdd the integer that is used to constructe the date, ie 20160421 for the year 21'st day of April 2016.
    *  \param throwError if set to true, the constructor will raise an exception if the \p yyyymmdd is not a valid date.
    *  \throws xMettle if the constructed date is not valid.
    */
   Date(const int32_t yyyymmdd, const bool throwErrors = true);

   /** \brief Destructor.
    */
  ~Date() noexcept;

   /** \brief Add an amount to the date class in the specified legend.
    *  \param amount the value to add to the date.
    *  \param legend the value type to add, ie days, months, weeks, or years.
    *  \returns a referense to itself for convenience.
    */
   Date &add(unsigned int amount, const PeriodLegend legend) noexcept;

   /** \brief Subract an amount from the date class in the specified legend.
    *  \param amount the value to subract from the date.
    *  \param legend the value type to subract, ie days, months, weeks, or years.
    *  \returns a referense to itself for convenience.
    */
   Date &sub(unsigned int amount, const PeriodLegend legend) noexcept;

   /** \brief A useful 'struct tm' type cast operator.
    *  \returns a standard 'struct tm' cast of the date object.
    */
   operator struct tm () const noexcept;

   /** \brief Set the day of the Date object.
    *  \param day the new day of the object.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new date would be invalid.
    */
   Date &setDay(const unsigned int day);

   /** \brief Set the month of the Date object.
    *  \param month the new month of the object.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new date would be invalid.
    */
   Date &setMonth(const unsigned int month);

   /** \brief Set the year of the Date object.
    *  \param year the new year of the object.
    *  \returns a referense to itself for convenience.
    *  \throws xMettle if the the new date would be invalid.
    */
   Date &setYear(const unsigned int year);

   /** \brief Formats the date as specified into a a string.

       The format options are:

         - %a abbreviated weekday name
         - %A full weekday name
         - %b abbreviated month name
         - %B full month name
         - %d day of month as decimal [01,31]
         - %H Hour (24-hour clock) as a decimal number [00,23]
         - %I Hour (12-hour clock) as a decimal number [01,12]
         - %j Day of the year as a decimal number [001,366]
         - %m Month as a decimal number [01,12]
         - %M Minute as a decimal number [00,59]
         - %p Locale's equivalent of either AM or PM
         - %S Second as a decimal number [00,61]
         - %w Weekday as a decimal number [0(Sunday),6]
         - %y Year without century as a decimal number [00,99]
         - %Y Year as a decimal number [0000,9999]
         - %% A literal "%" character

    *  \param dest the destination string for the formatted date.  Enough space must be available for the formatting.
    *  \param fmt the format the date should formatted into.
    *  \returns the \p dest param for convenience.
    */
   char *format(char *dest, const char *fmt) const noexcept;

   /** \brief Formats the date into a specified string.  See 'char *format(char *dest, char *fmt)'.
     * \param dest the destination string for the formatted date.
     * \param fmt the format the date should formatted into.
     * \returns the \p dest param for convenience.
     * \throws xMettle it out of memory.
     */
   String &format(String &dest, const char *fmt) const;

   /** \brief Get the year porition of the date.
    *  \returns the year portion of the date.
    */
   unsigned int year() const noexcept { return _year; }

   /** \brief Get the month porition of the date.
    *  \returns the month portion of the date.
    */
   unsigned int month() const noexcept { return _month + 1; }

   /** \brief Get the day porition of the date.
    *  \returns the day portion of the date.
    */
   unsigned int day() const noexcept { return _day; }

   /** \brief Get the week day type of the date.
    *  \returns the week day typeof the date.
    */
   DayOfWeekLegend dayOfWeek() const noexcept;

   /** \brief Get the number of days in the date's month.
    *  \returns the number of days in the current month.
    */
   unsigned int daysInMonth() const noexcept;

   /** \brief Get the day of the current year of the date.
    *  \returns the current day of year.
    */
   unsigned int dayOfYear() const noexcept;

   /** \brief Retrieves the date as an integer represented visually in the YYYYMMDD format.
    *  \returns the current date as an integer (YYYYMMDD).
    */
   int32_t toInt32() const noexcept;

   /** \brief Retrieves the date as a string represented visually in the YYYYMMDD format.
    *  \param yyyymmdd the destination string that is at least 9 charaters long, the date is copied into it.
    *  \returns the \p yyyymdd arguement for convenience.
    */
   char *toString(char *yyyymmdd) const noexcept;

   /** \brief Converts the date to a Julian date integer.
    *  \returns the date as a julian integer.
    */
   int32_t julianDate() const noexcept;

   /** \brief Compare this date to another.
    *  \param cmpDate the date to compare against.
    *  \returns -1 if this date is smaller than \p cmpDate, 0 if the dates match, or +1 if this date is greater than \p cmpDate
    */
   int compare(const Date *cmpDate) const noexcept;

   /** \brief compare this date to another Date pointer, via explicit type cast.
    *  \param obj a pointer that can be directly type casted to a date class.
    *  \returns -1 if this date is smaller than \p cmpDate, 0 if the dates match, or +1 if this date is greater than \p obj
    */
   int _compare(const void *obj) const;

   /** \brief Check if this date is null.
    *  \returns true if this date is null; ie the year, month, and day are all zero.
    */
   bool isNull() const noexcept;

   /** \brief Check if this date is a leap year.
    *  \returns true if this date is a leap year.
    */
   bool isLeapYear() noexcept;

   /** \brief Assign this date a new value.
    *  \param year the year of the new date.
    *  \param month the month of the new date.
    *  \param day the day of the new date.
    *  \throws xMettle if the constructed date is not valid.
    */
   void assign(const unsigned int year, const unsigned int month, const unsigned int day);

   /** \brief Makes this date null.
    */
   void nullify() noexcept;

   /** \brief Same as nullify().
    */
   void clear() noexcept;

   /** \brief Operator overload for = to assign the date from an existing date.
    *  \param inDate the date to become.
    *  \returns itself for convenience.
    */
   Date & operator = (const Date &inDate) noexcept;

   /** \brief Operator overload for = to assign the date from a struct tm.
    *  \param inDate the date to become.
    *  \returns itself for convenience.
    *  \throws xMettle if the struct tm date is not valid.
    */
   Date & operator = (const struct tm &inDate)  ;

   /** \brief Operator overload for = to assign the string in YYYYMMDD format.
    *  \param yyyymmdd the date to become.
    *  \returns itself for convenience.
    *  \throws xMettle if the input date is not valid.
    */
   Date & operator = (const char *yyyymmdd);

   /** \brief Operator overload for = to assign the int in YYYYMMDD format.
    *  \param yyyymmdd the date to become.
    *  \returns itself for convenience.
    *  \throws xMettle if the input date is not valid.
    */
   Date & operator = (const int32_t yyyymmdd);

   /** \brief Operator overload for += to increment the date in days
    *  \param days the number of days to add to the date.
    *  \returns itself for convenience.
    */
   Date & operator += (const unsigned int days)  noexcept;

   /** \brief Operator overload for -= to decrement the date in days
    *  \param days the number of days to subract from the date.
    *  \returns itself for convenience.
    */
   Date & operator -= (const unsigned int days)  noexcept;

   /** \brief Operator overload for ++ to increment the date by 1 day.
    *  \returns itself for convenience.
    */
   const Date & operator ++ () noexcept;

   /** \brief Operator overload for -- to decrement the date by 1 day.
    *  \returns itself for convenience.
    */
   const Date & operator -- () noexcept;

   /** \brief Operator overload for ++ (int) to increment the date by 1 day.
    *  \returns itself.
    */
   Date operator ++ (int) noexcept;

   /** \brief Operator overload for -- (int) to decrement the date by 1 day.
    *  \returns itself.
    */
   Date operator -- (int) noexcept;

   /** \brief Less than friend operator overload between two dates.
    *  \param date1 the first date to compare.
    *  \param date2 the second date to compare.
    *  \returns true if \p date1 is less than \p date2.
    */
   friend bool operator <  (const Date &date1, const Date &date2) noexcept;

   /** \brief Greater than friend operator overload between two dates.
    *  \param date1 the first date to compare.
    *  \param date2 the second date to compare.
    *  \returns true if \p date1 is greater than \p date2.
    */
   friend bool operator >  (const Date &date1, const Date &date2) noexcept;

   /** \brief Less Equal than friend operator overload between two dates.
    *  \param date1 the first date to compare.
    *  \param date2 the second date to compare.
    *  \returns true if \p date1 is less than or eqaul to \p date2.
    */
   friend bool operator <= (const Date &date1, const Date &date2) noexcept;

   /** \brief Greater Equal than friend operator overload between two dates.
    *  \param date1 the first date to compare.
    *  \param date2 the second date to compare.
    *  \returns true if \p date1 is greater than or equal to \p date2.
    */
   friend bool operator >= (const Date &date1, const Date &date2) noexcept;

   /** \brief Equals friend operator overload between two dates.
    *  \param date1 the first date to compare.
    *  \param date2 the second date to compare.
    *  \returns true if \p date1 equals \p date2.
    */
   friend bool operator == (const Date &date1, const Date &date2) noexcept;

   /** \brief Not Equals friend operator overload between two dates.
    *  \param date1 the first date to compare.
    *  \param date2 the second date to compare.
    *  \returns true if \p date1 does not equal \p date2.
    */
   friend bool operator != (const Date &date1, const Date &date2) noexcept;

   /** \brief Ouput stream operator.
    *  \param output the output stream, date is formatted into the standard date time format 'YYYY-MM-DD'.
    *  \param dt the date object to be output.
    *  \returns the output stream as expected.
    */
   friend std::ostream &operator << (std::ostream &output, const Date &dt) noexcept;

   /** \brief Input stream operator.
    *  \param input the input stream.
    *  \param dt the date object to receive the value.
    *  \returns the input stream as expected.
    *  \throws xMettle if the input string was not in the standard date time format 'YYYY-MM-DD'.
    */
   friend std::istream &operator >> (std::istream &input, Date &dt);

   /** \brief Returns the standard format string for datetimes, ie 'YYYY-MM-DD HH:MM:SS'.
     * \returns The standard format string.
     */
   static const char *stdDateFormat();

   /** \brief Static method to check the number days in the month for the given year.
    *  \param year the year to check.
    *  \param month to check, Jan = 0, ..., Dec = 11
    *  \returns the number of days in the month.
    *  \throws xMettle if \p month or \p year are out of range.
    */
   static unsigned int daysInMonth(const unsigned int year, const unsigned int month);

   /** \brief Static method to check if a year is a leap year.
    *  \param year the year to check.
    *  \returns true if \p year is a leap year.
    */
   static bool isLeapYear(const unsigned int year) noexcept;

   /** \brief Check if the year, month, day combination is a valid date.
    *  \param year the year to check.
    *  \param month the month to check.
    *  \param day the day to check.
    *  \returns true if params \p year, \p month, and \p day make up a valid date.
    */
   static bool validDate(const unsigned int year, const unsigned int month, const unsigned int day) noexcept;

   /** \brief Check if the string in the YYYYMMDD format is a valid date.
    *  \param yyyymmdd the string to check.
    *  \returns true if \p yyyymmdd is a valid date.
    */
   static bool validDate(const char *yyyymmdd) noexcept;

   /** \brief Check if the integer in the YYYYMMDD format is a valid date.
    *  \param yyyymmdd the integer to check.
    *  \returns true if \p yyyymmdd is a valid date.
    */
   static bool validDate(const int32_t yyyymmdd) noexcept;

   /** \brief Gets a new Date object with date set to the current machine date.
    *  \returns A new date object with the date to now.
    */
   static Date now() noexcept;

private:

   uint16_t          _year;
   uint8_t           _month;
   uint8_t           _day;

   void              _from(const struct tm *tmStruct, const bool throwErros = true);
   void              _from(const unsigned int year, const unsigned int month, const unsigned int day, const bool throwErros = true);
   void              _from(const char *yyyymmdd, const bool throwErros = true);
   void              _from() noexcept;

   bool              _validate(const bool throwErros = true);

   static const char  *_weekDays[];
   static const char  *_months[];
   static const int8_t _monthEnds[];

};

}}


#endif
