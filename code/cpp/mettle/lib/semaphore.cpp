/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/semaphore.h"

#include <errno.h>
#include <string.h>
#include <stdio.h>

#if defined(OS_AIX)

   #include <unistd.h>

#elif defined(OS_LINUX)

   #include <sys/stat.h>
   #include <unistd.h>
   #include <fcntl.h>

#endif

namespace Mettle { namespace Lib {

#define MODULE "Mettle::Semaphore"


Semaphore::Semaphore(const char *name,
                     const bool  createIfMissing)
{
   _semaphore = 0;
   _name      = name;

#if defined(OS_AIX)

   /* This is only safe in Unix, as Linux seems to delete the semaphores
       regardless of the if they are being used by other processes or not */
   sem_unlink(_name.c_str());

#endif

#if defined(OS_LINUX) || defined(OS_AIX)

   if ((_semaphore = sem_open(_name.c_str(), O_CREAT, S_IRUSR | S_IWUSR, 0)) == SEM_FAILED)
      throw xMettle(__FILE__, __LINE__, MODULE, "Could not open semaphore [%s]", _name.c_str());

#elif defined (OS_MS_WINDOWS)

   if ((_semaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, _name)) == 0)
   {
      if (createIfMissing)
      {
         if ((_semaphore = CreateSemaphore(0, 100, 100, _name.c_str())) != 0)
            throw xMettle(__FILE__, __LINE__, MODULE, "Could not create semaphore [%s]", _name.c_str());
      }
   }

   throw xMettle(__FILE__, __LINE__, MODULE, "Could not open semaphore [%s]", _name.c_str());

#endif
}


Semaphore::~Semaphore() noexcept
{
#if defined(OS_AIX) || defined(OS_LINUX)

   if (_semaphore && _semaphore != SEM_FAILED)
   {
      sem_close(_semaphore);
      sem_unlink(_name.c_str());
   }

#elif defined (OS_MS_WINDOWS)

   if (_semaphore != 0)
      CloseHandle(_semaphore);

#endif
}


void Semaphore::lock()
{
#if defined(OS_AIX) || defined(OS_LINUX)

   if (sem_wait(_semaphore) == 0)
      return;

   // Attempt to recreate a valid semaphor
   if (errno == EINVAL)
   {
      unsigned int idx;

      for (idx = 0; idx < 3; idx++)
      {
         if ((_semaphore = sem_open(_name, O_CREAT, S_IRUSR | S_IWUSR, 0)) == SEM_FAILED)
         {
            sleep(5);
            continue;
         }

         break;
      }

      if (idx == 3)
         throw xMettle(__FILE__, __LINE__, MODULE, "Could not re-create semaphore [%s]", _name.c_str());

      if (sem_wait(_semaphore) != 0)
         throw xMettle(__FILE__, __LINE__, MODULE, "Could not lock semaphore [%s]", _name.c_str());

      return;
   }

   throw xMettle(__FILE__, __LINE__, MODULE, "Could not lock semaphore [%s]", _name.c_str());

#elif defined (OS_MS_WINDOWS)

   DWORD dwWaitResult;

   if ((dwWaitResult = WaitForSingleObject(_semaphore, INFINITE)) == WAIT_OBJECT_0)
       return;

   throw xMettle(__FILE__, __LINE__, MODULE, "Could not lock semaphore [%s]", _name.c_str());

#endif
}


void Semaphore::unlock(const bool throwErrors)
{
#if defined(OS_AIX) || defined(OS_LINUX)

   if (sem_post(_semaphore) == 0)
      return;

#elif defined (OS_MS_WINDOWS)

   if (ReleaseSemaphore(_semaphore, 1, 0))
      return;

#endif

   if (throwErrors)
      throw xMettle(__FILE__, __LINE__, MODULE, "Could not unlock semaphore [%s]", _name.c_str());
}


const String &Semaphore::name() const noexcept
{
   return _name;
}


Semaphore::Auto::Auto(Semaphore *semaphore)
{
   _semaphore = semaphore;

   if (_semaphore)
      _semaphore->lock();
}

Semaphore::Auto::~Auto() noexcept
{
   if (_semaphore)
      _semaphore->unlock();
}

#undef MODULE

}}
