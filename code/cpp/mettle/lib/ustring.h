/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_USTRING_H_
#define __METTLE_LIB_USTRING_H_

#include <iostream>
#include <uuid/uuid.h>

#include "mettle/lib/collection.h"
#include "mettle/lib/icomparable.h"
#include "mettle/lib/safe.h"
#include "mettle/lib/uchar.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

class String;
class StringBuilder;

/** \class UString ustring.h mettle/lib/ustring.h
 * \brief An UTF8 encondeded string class lot of utility functions.
 */
class UString : public IComparable
{
   friend class String;
   friend class StringBuilder;

public:

   /** \class Safe ustring.h mettle/lib/ustring.h
    * \brief The safe class implementation.
    */
   class Safe : public Mettle::Lib::Safe<UString>
   {
   public:
      Safe(UString *o) : Mettle::Lib::Safe<UString>(o) {}
   };

   /** \class List ustring.h mettle/lib/ustring.h
    * \brief A standard collection template of UString.
    */
   class List : public Collection<UString>
   {
   public:
      List() noexcept;
      virtual ~List() noexcept;
   };

   /** \brief Default constructor.
    *  \throws xMettle if out of memory.
    */
   UString();

   /** \brief Char constructor, sets the ustring equal the \p ch provided
    *  \param ch the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const char ch);

   /** \brief String pointer constructor, sets the ustring equal the \p str provided
    *  \param inStr the null terminated initialized value of the string
    *  \throws xMettle if out of memory.
    */
   UString(const char *inStr);

   /** \brief Standard copy constructor.
    *  \param inStr the UString to copy
    *  \throws xMettle if out of memory.
    */
   UString(const UString &inStr);

   /** \brief Integer constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const int16_t strVal);

   /** \brief Unsigned integer constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const uint16_t strVal);

   /** \brief Integer constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const int32_t strVal);

   /** \brief Unsigned integer constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const uint32_t strVal);

   /** \brief Integer constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const int64_t strVal);

   /** \brief Unsigned integer constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const uint64_t strVal);

   /** \brief Double constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const float strVal);

   /** \brief Double constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const double strVal);

   /** \brief Double constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const long double strVal);

   /** \brief Boolean constructor, sets the ustring equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const bool strVal);

   /** \brief UUID constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   UString(const uuid_t strVal);

   /** \brief Constructor thie UString object with \p up to the specified length.
    *  \param str the initialized value of the string.
    *  \param len the length of the \p str to initalize with.
    *  \throws xMettle if out of memory.
    */
   UString(const char *str, const unsigned len);

   /** \brief Destructor.
    */
   virtual ~UString() noexcept;

   /** \brief Gets the physical char pointer of the UString.
    *  \returns the native char pointer of the UString object.
    */
   const char *c_str() const noexcept  { return _str; }

   /** \brief Gets the length of the String.
    *  \returns the length of the ustring not including the null terminator.
    */
   const unsigned int length() const noexcept  { return _strLen; }

   /** \brief Gets the number of bytes that make up the UString.
    *  \returns the number of bytes that make up the UString not including the null terminator.
    */
   const unsigned int byteLength() const noexcept  { return _byteLen; }

   /** \brief Gets the total number of bytes this UString object has currently allocated.
    *  \returns the bytes allocated in memory.
    */
   const unsigned int bytesUsed() const noexcept  { return _bytesUsed; }

   /** \brief Sub strings itself.
    *
    *  \param start the start position to start from.
    *  \param len the number of utf8 characters to read.
    *  \returns itself for convenience.
    *  \throws xMettle if \p start or \p len is out of bounds.
    */
   UString &subString(const unsigned int start, const unsigned int len = 0);

   /** \brief Sets the internal size to a minium of length.
    *
    *  Note that this method should only used to increase the internal buffer size and
    *  not shrink the string.  Using a length that is less than the UString objects current
    *  size has no effect.
    *
    *  \param length the size the string should grow to not including the null terminator.
    *  \param clear if true, memsets the buffer to 0.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString &setSize(const unsigned length, const bool clear = false);

   /** \brief Helper function, returns the directory seperator of the operating system String was compiled on.
    *  \returns '\' on Windows, or '/' or Linux, Unix, etc.
    */
   static char dirSep() noexcept;

   /** \brief Helper function to do a string case sensitive compare.  Note this is not utf-8 safe at this time.
    *  \param a the first string to compare.
    *  \param b the second string to compare.
    *  \returns 0 if the strings match, -1 if a > b, or 1 if b < a.
    */
   static int stricmp(const char *a, const char *b) noexcept;

   /** \brief Helper function to do a string non case sensitive compare.
    *  \param a the first string to compare.
    *  \param b the second string to compare.
    *  \returns 0 if the strings match, -1 if a > b, or 1 if b < a.
    */
   static int strnicmp(const char *a, const char *b, const unsigned int n) noexcept;

   /** \brief Helper function that returns an empty string.
    *  \returns an empty string.
    */
   static const char *empty() noexcept;

   /** \brief Check if the string is empty.
    *  \returns true if the string is empty else returns false.
    */
   bool isEmpty() const noexcept;

   /** \brief Finds the first occurence of \p subStr in the UString.
    *  \param subStr the sub string to search for.
    *  \param offset optional arguement, tell the string to start searching from the specified offset.
    *  \returns the position of the \p subStr if found, else return -1 if not found.
    */
   int find(const UString &inStr, const unsigned int offset = 0) const noexcept;

   /** \brief Finds the first occurence of \p subStr in the UString starting from the back.
    *  \param subStr the sub string to search for.
    *  \param offset optional arguement, tell the string to start searching from the specified offset.
    *  \returns the position of the \p subStr if found, else return -1 if not found.
    */
    int reverseFind(const UString &inStr, const unsigned int offset = 0) const noexcept;

   /** \brief Calculates the pointer index for the offset value.  This may differ because UTF-8 character can use up more than 1 byte.
    *  \param offset the required offset.
    *  \returns the effective utf-8 character offset.
    */
   unsigned int calcIndexFromOffset(const unsigned int offset) const noexcept;

   /** \brief Count the sub strings in the UString.
    *  \param subStr the sub string ot search for.
    *  \returns the number of time \p subStr appears in the String.
    */
   unsigned int count(const UString &inStr) const noexcept;

   /** \brief Clears the string, setting the size to 0, but leaving the original allocated memory available for re-use.
    *  \returns itself for convenience.
    */
   UString &clear() noexcept;

   /** \brief Consumes a char *, taking ownership
    *  \param str the char * to be consumed.
    *  \returns itself for convenience.
    */
   UString &eat(char *str) noexcept;

   /** \brief Consumes a String, freeing its contents
    *  \param str the String to be consumed.
    *  \returns itself for convenience.
    */
   UString &eat(String &str) noexcept;

   /** \brief Consumes a UString, freeing its contents
    *  \param str the UString to be consumed.
    *  \returns itself for convenience.
    */
   UString &eat(UString &str) noexcept;

   /** \brief Consumes a StringBuilder, freeing its contents
    *  \param str the StringBuilder to be consumed.
    *  \returns itself for convenience.
    */
   UString &eat(StringBuilder &str) noexcept;

   /** \brief Releases its internal memory pointer, and forgets about it.
    *  \returns its internal memory pointer
    *  \warning it is the responsibilty of the calling code to free the returned pointer.
    */
   char *poop() noexcept;

   //String             &Format(const char *fmt, ...);
   //String             &Lower() noexcept;
   //UString            &Upper() noexcept;

   /** \brief Converts the Ustring into a UChar list.
    *  \param the destination UChar array list.
    *  \returns the count of the \p list arguement.
    */
   unsigned int toCharArray(UChar::List &list) const;

   /** \brief Splits the ustring into an collection of ustring using the specified seperator.
    *  \param sep the seperator string to use to split the string.
    *  \param dest the destination list of the split out strings, note this list is cleared initially.
    *  \param enclosingChar an optional enclosing character, ie a double quote ( " ) that escapes the seperator for splitting, useful for csv files where fields have commans in them.  Note the enlosing character can be esacped with a preceeding back slash.
    *  \returns the number of splits (\p dest length)
    *  \throws xMettle if out of memory.
    */
   unsigned int split(const UString &sep, UString::List &dest, const char enclosingChar = 0) const;

   /** \brief Splits the string into an collection of string using operating systems line seperator.
    *  \param dest the destination list of the split out strings, note this list is cleared initially.
    *  \returns the number of splits (\p dest length)
    *  \throws xMettle if out of memory.
    */
   unsigned int splitLines(UString::List &dest) const;

   /** \brief Check if the UString ends with the specified sub string.
    *  \param subStr the string to check.
    *  \returns true if this string ends with \p subStr else reutrns false.
    */
   bool endsWith(const UString &inStr) const noexcept;

   /** \brief Check if the UString starts with the specified sub string.
    *  \param subStr the string to check.
    *  \returns true if this string starts with \p subStr else reutrns false.
    */
   bool startsWith(const UString &inStr) const noexcept;

   /** \brief Compare this string to another string.
    *  \param cmpStr the string to compare to.
    *  \param ignoreCase optional arguement, if set to true this method does a non case senitive compare.
    *  \returns 0 if the strings match, -1 if this strings is less than \p cmpStr, +1 if this string is greater than \p cmpStr.
    */
   int compare(const UString &cmpStr, const bool ignoreCase = false) const noexcept;

   /** \brief Implemented pure virtual function for ICompare interface.
    *  \param obj the string to compare to, note it is assumed obj is in fact a pointer to a valid String object.
    *  \returns 0 if the strings match, -1 if this strings is less than \p obj, +1 if this string is greater than \p obj.
    */
   int _compare(const void *obj) const;

   /** \brief Replaces all instance of \p oldStr with \p newStr within the String.
    *  \param oldStr the string that is to be replaced.
    *  \param newStr the new string that will replace \p oldStr.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString &replace(const UString &oldStr, const UString &newStr);

   /*
   String& Reverse() noexcept;
   String& JoinDirs(const char *firstDir, ...);
   String& Capitalize() noexcept;
   String& Title() noexcept;
   */

   /** \brief Cuts the string at the last \p ext ch found.
    *
    *  This method is primarily used as a utility function to get the extension
    *  of a file name.  For example if the string was 'jotter.txt' this method
    *  would return a new string of 'txt'.
    *
    *  \param extChar the file extension, can be optional overriden to something else.
    *  \returns a new string of just what was behind the last instance of \p extch
    *  \throws xMettle if out of memory.
    */
   UString fileExtension(const char extChar = '.') const;

   /** \brief Appends the operating system directory seperator if is not already at the end of the string.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString &makeDirectory();

   /** \brief Rechecks the length of this ustring, usually called if the calling code is being naughty and doing some
    *         low level work on the internal pointer for some reason.
    *  \returns itself for convenience.
    */
   UString &checkLength() noexcept;

   /** \brief Checks if this string is a valid integer.
    *  \returns true if the contents of string would make up a valid integer, else returns false.
    */
   bool isInt() const noexcept;

   /** \brief Checks if this string is a valid unsigned integer.
    *  \returns true if the contents of string would make up a valid unsigned integer, else returns false.
    */
   bool isUnsigned() const noexcept;

   /** \brief Checks if this string is a valid double.
    *  \returns true if the contents of string would make up a valid double, else returns false.
    */
   bool isDouble() const noexcept;

   /** \brief Checks if this string is a valid boolean.
    *
    *  There are several valid boolean values, namely: [true, false, yes, no, t, f, 1, 0]
    *  \returns true if the contents of string would make up a valid boolean, else returns false.
    */
   bool isBool() const noexcept;

   /** \brief Checks if this string is a valid uuid.
    *  Checks if the string is valid universal unique identifier.
    *  \returns true if the contents of string would make up a valid uuid, else returns false.
    */
   bool isUUID() const noexcept;

   /** \brief Checks if this string is all alpahbetical character only.
    *  \returns true if the contents of string is made up entirely of alphabetical characters, else returns false.
    *  \warning Note UTF-8 safe yet.
    */
   bool isAlpha() const noexcept;

   /** \brief Checks if this string is entirely in upper case.
    *  \returns true if the contents of string is made up entirely of upper case characters, else returns false.
    *  \warning Note UTF-8 safe yet.
    */
   bool isUpper() const noexcept;

   /** \brief Checks if this string is entirely in lower case.
    *  \returns true if the contents of string is made up entirely of lower case characters, else returns false.
    *  \warning Note UTF-8 safe yet.
    */
   bool isLower() const noexcept;

   /** \brief Converts the string to an int.
    *  \returns the string as an int.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   int toInt() const;

   /** \brief Converts the string to an unsinged int.
    *  \returns the string as an unsigned int.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   unsigned int toUInt() const;

   /** \brief Converts the string to an int32.
    *  \returns the string as an int32.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   int32_t toInt32() const;

   /** \brief Converts the string to an uint32.
    *  \returns the string as an unsigned long.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   uint32_t toUInt32() const;

   /** \brief Converts the string to an int64_t.
    *  \returns the string as an int64_t.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   int64_t toInt64() const;

   /** \brief Converts the string to an uint64_t.
    *  \returns the string as an uint64_t.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   uint64_t toUInt64() const;

   /** \brief Converts the string to a float.
    *  \returns the string as a float.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   float toFloat() const;

   /** \brief Converts the string to a float.
    *  \returns the string as a double.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   double toDouble() const;

   /** \brief Converts the string to a long double.
    *  \returns the string as a long double.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   long double toLongDouble() const;

   /** \brief Converts the string to a boolean.
    *  \returns a valid boolean value of the string, or returns false if there was an error.
    */
   bool toBool() const noexcept;

   /** \brief Converts the string to an int.
    *  \param dest the destination uuid_t point to fill.
    *  \returns the dest pointer for convenience..
    *  \throws xMettle if the string is not a valid uuid.
    */
   uuid_t *toUUID(uuid_t *dest) const;

   /** \brief Operator overload to a 'const char *'.
    *  \returns the internal 'char *' as a 'const char *'.
    */
   operator const char* () const noexcept { return _str; }

   /** \brief Operator overload to a 'char *'
    *  \returns the internal 'char *'
    */
   operator char* () const noexcept { return _str; }

   /*char & operator [] (const unsigned index) noexcept { return _str[index]; }
   char & operator [] (const int index)      noexcept { return _str[index]; }*/

   /** \brief Operator overload for = to assign the string to a character.
    *  \param ch value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const char ch);

   /** \brief Operator overload for = to assign the string to a new string.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const char *inStr);

   /** \brief Operator overload for = to assign the string to a new string.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const UString &inStr);

   /** \brief Operator overload for = to assign the string to an int32_t.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const int32_t strVal);

   /** \brief Operator overload for = to assign the string to an uint32_t.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const uint32_t strVal);

   /** \brief Operator overload for = to assign the string to an int64_t.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const int64_t strVal);

   /** \brief Operator overload for = to assign the string to an uint64_t.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const uint64_t strVal);

   /** \brief Operator overload for = to assign the string to a float.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const float strVal);

   /** \brief Operator overload for = to assign the string to a double.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const double strVal);

   /** \brief Operator overload for = to assign the string to a long double.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const long double strVal);

   /** \brief Operator overload for = to assign the string to a boolean.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const bool strVal);

   /** \brief Operator overload for = to assign the string to a uuid.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator = (const uuid_t strVal);

   /** \brief Operator overload for += to append a character to the String.
    *  \param ch the chaaracter to be appeneded.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const char ch);

   /** \brief Operator overload for += to append a null terminated char array to the String.
    *  \param strVal the string to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const char *inStr);

   /** \brief Operator overload for += to append a UString to this String.
    *  \param strVal the string to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const UString &inStr);

   /** \brief Operator overload for += to append an int32_t to the String.
    *  \param strVal the integer to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const int32_t strVal);

   /** \brief Operator overload for += to append an uint32_t to the String.
    *  \param strVal the unsigned integer to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const uint32_t strVal);

   /** \brief Operator overload for += to append an int64_t to the String.
    *  \param strVal the int64_t to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const int64_t strVal);

   /** \brief Operator overload for += to append an uint64_t to the String.
    *  \param strVal the uint64_t to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const uint64_t strVal);

   /** \brief Operator overload for += to append a float to the String.
    *  \param strVal the float to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const float strVal);

   /** \brief Operator overload for += to append a double to the String.
    *  \param strVal the double to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const double strVal);

   /** \brief Operator overload for += to append a long double to the String.
    *  \param strVal the long double to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const long double strVal);

   /** \brief Operator overload for += to append a boolean to the String.
    *  \param strVal the boolean to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const bool strVal);

   /** \brief Operator overload for += to append a uuid to the String.
    *  \param strVal the uuid to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   UString & operator += (const uuid_t strVal);

   /** \brief Friend operator overload for + between String and a String.
    *  \param str1 the first String.
    *  \param str2 the second String.
    *  \returns a new string with \p str1 and \p str2 combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const UString &inStr);

   /** \brief Friend operator overload for + between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns a new string with \p str and \p ch combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const char ch)     ;

   /** \brief Friend operator overload for + between String and a char*.
    *  \param str1 the String.
    *  \param str2 the null terminated char pointer.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const char *inStr)  ;

   /** \brief Friend operator overload for + between String and an integer.
    *  \param str the String.
    *  \param strVal the integer.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const int32_t strVal) ;

   /** \brief Friend operator overload for + between String and an unsigned integer.
    *  \param str the String.
    *  \param strVal the unsigned integer.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const uint32_t strVal) ;

   /** \brief Friend operator overload for + between String and an int64_t.
    *  \param str the String.
    *  \param strVal the int64_t.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const int64_t strVal) ;

   /** \brief Friend operator overload for + between String and an uint64_t.
    *  \param str the String.
    *  \param strVal the uint64_t.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const uint64_t strVal) ;

   /** \brief Friend operator overload for + between String and a double.
    *  \param str the String.
    *  \param strVal the double.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const float) ;

   /** \brief Friend operator overload for + between String and a float.
    *  \param str the String.
    *  \param strVal the float.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const double strVal) ;

   /** \brief Friend operator overload for + between String and a long double.
    *  \param str the String.
    *  \param strVal the long double.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const long double strVal) ;

   /** \brief Friend operator overload for + between String and a boolean.
    *  \param str the String.
    *  \param strVal the boolean.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const bool strVal) ;

   /** \brief Friend operator overload for + between a uuid and String.
    *  \param str the String.
    *  \param strVal the uuid.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const UString &str, const uuid_t strVal) ;

   /** \brief Friend operator overload for + between a char and String.
    *  \param ch the character.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const char ch, const UString &str) ;

   /** \brief Friend operator overload for + between a char pointer and String.
    *  \param str1 the null terminated char pointer.
    *  \param str2 the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const char *inStr, const UString &str) ;

   /** \brief Friend operator overload for + between an integer and String.
    *  \param strVal the integer.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const int32_t strVal, const UString &str) ;

   /** \brief Friend operator overload for + between an unsigned integer and String.
    *  \param strVal the unsigned integer.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const uint32_t strVal, const UString &str) ;

   /** \brief Friend operator overload for + between a double and String.
    *  \param strVal the double.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const double strVal, const UString &str) ;

   /** \brief Friend operator overload for + between a boolean and String.
    *  \param strVal the boolean.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const bool strVal, const UString &str) ;

   /** \brief Friend operator overload for + between a uuid and String.
    *  \param strVal the uuid.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend UString operator + (const uuid_t strVal, const UString &str) ;

   /** \brief Less than friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str is less than \p ch.
    */
   friend bool operator <  (const UString &str, const char ch)   noexcept;

   /** \brief Greater than friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str is greater than \p ch.
    */
   friend bool operator >  (const UString &str, const char ch)   noexcept;

   /** \brief Less than equal friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str is less than or equal to \p ch.
    */
   friend bool operator <= (const UString &str, const char ch)   noexcept;

   /** \brief Greater than equals friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str is greater than or equal to \p ch.
    */
   friend bool operator >= (const UString &str, const char ch)   noexcept;

   /** \brief Equals friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str equals \p ch.
    */
   friend bool operator == (const UString &str, const char ch)   noexcept;

   /** \brief Not equals friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str does not equal \p ch.
    */
   friend bool operator != (const UString &str, const char ch)   noexcept;

   /** \brief Less than friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str is less than \p strVal.
    */
   friend bool operator <  (const UString &str, const char *inStr)   noexcept;

   /** \brief Greater than friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str is greater than \p strVal.
    */
   friend bool operator >  (const UString &str, const char *inStr)   noexcept;

   /** \brief Less than equals friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str is less than or equal to \p strVal.
    */
   friend bool operator <= (const UString &str, const char *inStr)   noexcept;

   /** \brief Greater than equals friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str is greater than or equal to \p strVal.
    */
   friend bool operator >= (const UString &str, const char *inStr)   noexcept;

   /** \brief Equals friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str equals \p strVal.
    */
   friend bool operator == (const UString &str, const char *inStr)   noexcept;

   /** \brief Not equals friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str does not equal \p strVal.
    */
   friend bool operator != (const UString &str, const char *inStr)   noexcept;

   /** \brief Less than friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str is less than \p strVal.
    */
   friend bool operator <  (const UString &str, const UString &inStr)   noexcept;

   /** \brief Greater than friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str is greater than \p strVal.
    */
   friend bool operator >  (const UString &str, const UString &inStr)   noexcept;

   /** \brief Less than equals friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str is less than or equal to \p strVal.
    */
   friend bool operator <= (const UString &str, const UString &inStr)   noexcept;

   /** \brief Greater than equals friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str is greater than or equal to \p strVal.
    */
   friend bool operator >= (const UString &str, const UString &inStr)   noexcept;

   /** \brief Equals friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str equals \p strVal.
    */
   friend bool operator == (const UString &str, const UString &inStr)   noexcept;

   /** \brief Not equals friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str does not equal \p strVal.
    */
   friend bool operator != (const UString &str, const UString &inStr)   noexcept;

   /** \brief Ouput stream operator.
    *  \param output the output stream to receive the string.
    *  \param str the string object to be output.
    *  \returns the output stream as expected.
    */
   friend std::ostream &operator << (std::ostream &output, const UString &str) noexcept;

   /** \brief Input stream operator.
    *  \param input the input stream.
    *  \param str the string object to receive the value.
    *  \returns the input stream as expected.
    *  \throws xMettle if out of memory.
    */
   friend std::istream &operator >> (std::istream &input, UString &str);

private:

   UString(const char *str1, const char *str2)  ;

   char             *_str;
   unsigned int      _strLen;
   unsigned int      _byteLen;
   unsigned int      _bytesUsed;

   UString          *_newStr(const char *str)         ;
   UString          &_appendStr(const char *extra_str);
   void              _freeStr()                        noexcept;

   void              _replaceStr(const UString &dest, const UString &source)        ;
   //void              StripChars(const bool left, const bool right, char *chars)     noexcept;


   static bool       _isTrue(const char *str)   noexcept;
   static bool       _isFalse(const char *str)  noexcept;

};



}}

#endif
