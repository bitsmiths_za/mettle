/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_STRING_H_
#define __METTLE_LIB_STRING_H_

#include <iostream>
#include <uuid/uuid.h>

#include "mettle/lib/collection.h"
#include "mettle/lib/c99standard.h"
#include "mettle/lib/icomparable.h"
#include "mettle/lib/safe.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Lib {

class StringBuilder;
class UString;

/** \class String string.h mettle/lib/string.h
 * \brief An ascii string class with a lot of utility functions.
 *
 *  This String class tries to be the be and end all of all string classes.
 *  However if you want to performance based string manipulation instead
 *  consider using StringBuilder.  Note that this class is not UTF8 complaint
 *  in any way.  Use the UString class for UTF8 encoded string manipulation.
 *
 */
class String : public IComparable
{
   friend class StringBuilder;
   friend class UString;

public:

   /** \class Safe string.h mettle/lib/string.h
    * \brief The safe class implementation.
    */
   class Safe : public Mettle::Lib::Safe<String>
   {
   public:
      Safe(String *o) : Mettle::Lib::Safe<String>(o) {}
   };

   /** \class List string.h mettle/lib/string.h
    * \brief A standard collection template of String.
    */
   class List : public Collection<String>
   {
   public:
      List() noexcept;
      virtual ~List() noexcept;
   };

   /** \brief Default constructor.
    *  \throws xMettle if out of memory.
    */
   String();

   /** \brief Char constructor, sets the string equal the \p ch provided
    *  \param ch the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const char ch);

   /** \brief Char* constructor, sets the string equal the \p str provided
    *  \param str the null terminated initialized value of the string
    *  \throws xMettle if out of memory.
    */
   String(const char *str);

   /** \brief Standard copy constructor.
    *  \param str the String to copy
    *  \throws xMettle if out of memory.
    */
   String(const String &str);

   /** \brief Integer constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const int16_t strVal) ;

   /** \brief Unsigned int constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const uint16_t strVal);

   /** \brief Long constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const int32_t strVal);

   /** \brief Usigned long constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const uint32_t strVal);

   /** \brief int64_t constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const int64_t strVal);

   /** \brief uint64_t constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const uint64_t strVal);

   /** \brief Float constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const float strVal);

   /** \brief Double constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const double strVal);

   /** \brief Long double constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const long double strVal);

   /** \brief Boolean constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const bool strVal);

   /** \brief UUID constructor, sets the string equal the \p strVal provided
    *  \param strVal the initialized value of the string.
    *  \throws xMettle if out of memory.
    */
   String(const uuid_t strVal);

   /** \brief Constructor thie String object with \p up to the specified length.
    *  \param str the initialized value of the string.
    *  \param len the length of the \p str to initalize with.
    *  \throws xMettle if out of memory.
    */
   String(const char *str, const unsigned len);

   /** \brief Destructor.
    */
   virtual ~String() noexcept;

   /** \brief Gets the physical char pointer of the String.
    *  \returns the native char pointer of the String object.
    */
   const char *c_str() const throw ()     { return _str; }

   /** \brief Gets the length of the String.
    *  \returns the length of the string not including the null terminator.
    */
   const unsigned int length() const throw ()    { return _strLen; }

   /** \brief Gets the total number of bytes this String object has currently allocated.
    *  \returns the bytes allocated in memory.
    */
   const unsigned int bytesUsed() const noexcept  { return _bytesUsed; }

   /** \brief Sets the internal size to a minium of length.
    *
    *  Note that this method should only used to increase the internal buffer size and
    *  not shrink the string.  Using a length that is less than the String objects current
    *  size has no effect.
    *
    *  \param length the size the string should grow to not including the null terminator.
    *  \param clear if true, memsets the buffer to 0.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String &setSize(const unsigned length, const bool clear = false);

   /** \brief Sets the internal size to a minium of length and optionally fills it with the specified char.
    *
    *  Note that this method should only used to increase the internal buffer size and
    *  not shrink the string.  Using a length that is less than the String objects current
    *  size has no effect.
    *
    *  \param length the size the string should grow to not including the null terminator.
    *  \param ch if this does not equal zero, the string will be filled with this character up to \p length.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String &setSize(const unsigned length, const char ch);

   /** \brief Helper function, returns the directory seperator of the operating system String was compiled on.
    *  \returns '\' on Windows, or '/' or Linux, Unix, etc.
    */
   static const char dirSep() noexcept;

   /** \brief Helper function, returns the line ending of the operating system String was compiled on.
    *  \returns "\n\r" on Windows, "\n" or Linux & Unix, and "\r\n" or Mac
    */
   static const char *lineEnding() noexcept;

   /** \brief Helper function to do a string non case sensitive compare.
    *  \param a the first string to compare.
    *  \param b the second string to compare.
    *  \returns 0 if the strings match, -1 if a > b, or 1 if b < a.
    */
   static int stricmp(const char *a, const char *b) noexcept;

   /** \brief Helper function to do a string non case sensitive compare up the \p n characters.
    *  \param a the first string to compare.
    *  \param b the second string to compare.
    *  \param n the maximum number of characters to compare.
    *  \returns 0 if the strings match, -1 if a > b, or 1 if b < a.
    */
   static int strnicmp(const char *a, const char *b, const unsigned n) noexcept;

   /** \brief Helper function to copy one string into another and always append a null terminator at \p destSize.
    *  \param dest the destination where \p source is to be copied into.
    *  \param source the source string to be copied.
    *  \param destSize the maximum number of characters to copied into \p dest, and where a null terminator will always be set.
    *  \returns \p dest for convenience.
    */
   static char *safeCopy(char *dest, const char *source, const unsigned destSize) noexcept;

   /** \brief Helper function that returns an empty string.
    *  \returns an empty string.
    */
   static const char *empty() noexcept;

   /** \brief Helper function that checks if \p is a white space of some kind.
    *
    *  The full list of white spaces is: [' ', '\\n', '\\r', '\\t', '\\f', '\\v']
    *
    *  \returns true if \p is a white space, else returns false.
    */
   static bool isWhiteSpace(const char ch) noexcept;

   /** \brief Check if the string is empty.
    *  \returns true if the string is empty else returns false.
    */
   bool isEmpty() const noexcept;

   /** \brief Finds the first occurence of \p subStr in the String.
    *  \param subStr the sub string to search for.
    *  \param offset optional arguement, tell the string to start searching from the specified offset.
    *  \returns the position of the \p subStr if found, else return -1 if not found.
    */
   int find(const String &subStr, const unsigned int offset = 0) const noexcept;

   /** \brief Finds the first occurence of \p subStr in the String starting from the back.
    *  \param subStr the sub string to search for.
    *  \param offset optional arguement, tell the string to start searching from the specified offset.
    *  \returns the position of the \p subStr if found, else return -1 if not found.
    */
   int reverseFind(const String &subStr, const unsigned int offset = 0) const noexcept;

   /** \brief Count the sub strings in the String.
    *  \param subStr the sub string ot search for.
    *  \returns the number of time \p subStr appears in the String.
    */
   unsigned int count(const String &subStr) const noexcept;

   /** \brief Consumes a char*, taking ownership
    *  \param str the char* to be consumed.
    *  \returns itself for convenience.
    */
   String &eat(char *str) noexcept;

   /** \brief Consumes another String, freeing its contents
    *  \param str the String to be consumed.
    *  \returns itself for convenience.
    */
   String &eat(String &str) noexcept;

   /** \brief Consumes a UString, freeing its contents
    *  \param str the UString to be consumed.
    *  \returns itself for convenience.
    */
   String &eat(UString &str) noexcept;

   /** \brief Consumes a StringBuilder, freeing its contents
    *  \param str the StringBuilder to be consumed.
    *  \returns itself for convenience.
    */
   String &eat(StringBuilder &str) noexcept;

   /** \brief Releases its internal memory pointer, and forgets about it.
    *  \returns its internal memory pointer
    *  \warning it is the responsibilty of the calling code to free the returned pointer.
    */
   char *poop() noexcept;

   /** \brief Clears the string, setting the size to 0, but leaving the original allocated memory available for re-use.
    *  \returns itself for convenience.
    */
   String &clear() noexcept;

   /** \brief Formats a string, see the standard printf() function for more detail.
    *  \param fmt the format to be used.
    *  \param ... the arguements to be used by \p fmt.
    *  \returns itself for convenience.
    *  \warning the method cuts off any formating after 4096 charactes.
    *  \throws xMettle if out of memory.
    */
   String &format(const char *fmt, ...)     ;

   /** \brief Converts all the characters to lower case.
    *  \returns itself for convenience.
    */
   String &lower() noexcept;

   /** \brief Converts all the characters to upper case.
    *  \returns itself for convenience.
    */
   String &upper() noexcept;

   /** \brief Cuts the string at the last \p extch found.
    *
    *  This method is primarily used as a utility function to get the extension
    *  of a file name.  For example if the string was 'jotter.txt' this method
    *  would return a new string of 'txt'.
    *
    *  \param extch the file extension, can be optional overriden to something else.
    *  \returns a new string of just what was behind the last instance of \p extch
    *  \throws xMettle if out of memory.
    */
   String fileExtension(const char extch = '.') const;

   /** \brief Splices a string just like Python string splicing does.
    *
    *  \param pos1 the starting position of the string to start splicing from.
    *  \param pos2 optional end position of string to splice too, note if left as zero it means to the end of the string.
    *  \returns a new string of the based from \p pos1 and \p pos2
    *  \throws xMettle if out of memory.
    */
   String splice(const int pos1, const int pos2 = 0) const;

   /** \brief Strips all characters from right side of the string.
    *  \param chars optional arguement a string of characters to be stripped off at the end of string.  If left as null, defaults to all white spaces.
    *  \returns itself for convenience.
    */
   String &stripRight(const char *chars = 0) noexcept;

   /** \brief Strips all characters from left the side of the string.
    *  \param chars optional arguement a string of characters to be stripped off at the end of string.  If left as null, defaults to all white spaces.
    *  \returns itself for convenience.
    */
   String &stripLeft(const char *chars = 0) noexcept;

   /** \brief Strips all characters from left and right size of the string.
    *  \param chars optional arguement a string of characters to be stripped off at the end of string.  If left as null, defaults to all white spaces.
    *  \returns itself for convenience.
    */
   String &strip(const char *chars = 0) noexcept;

   /** \brief Strips the chars off a string, but leaves at least one of the chars.
    *  \param left the char to strip off the left (start) of the string, 0 means do nothing.
    *  \param right the char to strip off the right (end) of the string, 0 means do nothing.
    *  \returns itself for convenience.
    */
   String &stripAllButOne(const char left, const char right) noexcept;

   /** \brief Right pads the string with the specified \p ch to specified \p size.
    *  \param size the size of the string after padding.
    *  \param ch the character to use to pad with, defaults to space.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String &padRight(const unsigned int size,  const char ch = ' ') ;

   /** \brief Left pads the string with the specified \p ch to specified \p size.
    *  \param size the size of the string after padding.
    *  \param ch the character to use to pad with, defaults to space.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String &padLeft(const unsigned int size,  const char ch = ' ') ;

   /** \brief Center pads the string with the specified \p ch to specified \p size.
    *  \param size the size of the string after padding.
    *  \param ch the character to use to pad with, defaults to space.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String &padCenter(const unsigned int size, const char ch = ' ') ;

   /** \brief Replaces all instance of \p oldStr with \p newStr within the String.
    *  \param oldStr the string that is to be replaced.
    *  \param newStr the new string that will replace \p oldStr.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String &replace(const String &oldStr, const String &newStr) ;

   /** \brief Reverses the contents of the string.
    *  \returns itself for convenience.
    */
   String &reverse() noexcept;

   /** \brief Joins all the string arguements together into this String.
    *  \param firstStr the first string to be joined.
    *  \param ... all the other strings to be joined, note the final argument must be NULL.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    *  \warning if the final argument is not NULL, your program code will go BOOM!
    */
   String &join(const char *firstStr, ...);

   /** \brief Joins all the strings passed into this String and seperates them with the specified \p sep string.
    *  \param sep the string seperator to use to seperate the joined strings.
    *  \param firstStr the first string to be joined.
    *  \param ... all the other strings to be joined, note the final argument must be NULL.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    *  \warning if the final argument is not NULL, your program code will go BOOM!
    */
   String &joinSep(const char *sep, const char *firstStr, ...) ;

   /** \brief Joins all the strings passed into this String and seperates them with the operating system directory seperator.
    *  \param firstDir the first directory name to be joined.
    *  \param ... all the other directory names to be joined, note the final argument must be NULL.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    *  \warning if the final argument is not NULL, your program code will go BOOM!
    */
   String &joinDirs(const char *firstDir, ...);

   /** \brief Appends the operating system directory seperator if is not already at the end of the string.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String &makeDirectory();

   /** \brief Makes the start of the string start with an upper case character.
    *  \returns itself for convenience.
    */
   String &capitalize() noexcept;

   /** \brief Makes every new word in the string (seperated by white spaces) start with an upper case character.
    *  \returns itself for convenience.
    */
   String &title() noexcept;

   /** \brief Assuming that the String is a full or relative path of the operating system file system, this method splits the path and the filename out.
    *  \param destPath the string that will be set as the path portion.  This is set to an empty string if there is not path portion.
    *  \param destFile the string that will be set as the filename portion.
    */
   void pathAndFileName(String &destPath, String &destFile) const;

   /** \brief Splits the string into an collection of string using the specified seperator.
    *  \param sep the seperator string to use to split the string.
    *  \param dest the destination list of the split out strings, note this list is cleared initially.
    *  \param enclosingChar an optional enclosing character, ie a double quote ( " ) that escapes the seperator for splitting, useful for csv files where fields have commans in them.  Note the enlosing character can be esacped with a preceeding back slash.
    *  \returns the number of splits (\p dest length)
    *  \throws xMettle if out of memory.
    */
   unsigned int split(const String &sep, String::List &dest, const char enclosingChar = 0) const;

   /** \brief Splits the string into an collection of string using operating systems line seperator.
    *  \param dest the destination list of the split out strings, note this list is cleared initially.
    *  \returns the number of splits (\p dest length)
    *  \throws xMettle if out of memory.
    */
   unsigned int splitLines(String::List &dest) const;

   /** \brief Check if the String ends with the specified sub string.
    *  \param subStr the string to check.
    *  \returns true if this string ends with \p subStr else reutrns false.
    */
   bool endsWith(const String &subStr) const noexcept;

   /** \brief Check if the String starts with the specified sub string.
    *  \param subStr the string to check.
    *  \returns true if this string starts with \p subStr else reutrns false.
    */
   bool startsWith(const String &subStr) const noexcept;

   /** \brief Compare this string to another string.
    *  \param cmpStr the string to compare to.
    *  \param ignoreCase optional arguement, if set to true this method does a non case senitive compare.
    *  \returns 0 if the strings match, -1 if this strings is less than \p cmpStr, +1 if this string is greater than \p cmpStr.
    */
   int compare(const String &cmpStr, const bool ignoreCase = false) const noexcept;

   /** \brief Implemented pure virtual function for ICompare interface.
    *  \param obj the string to compare to, note it is assumed obj is in fact a pointer to a valid String object.
    *  \returns 0 if the strings match, -1 if this strings is less than \p obj, +1 if this string is greater than \p obj.
    */
   int _compare(const void *obj) const;

   /** \brief Copies the \p str and truncates it up to the specified length.
    *
    *  Note that this method does not allow this string to grow larger that the current amount of bytes allocated.
    *
    *  \param str the string to be copied.
    *  \param len the maximum number of characters to copy.
    *  \returns itself for convenience.
    */
   String &copyTruncate(const char *str, const unsigned int len) noexcept;


   /** \brief Rechecks the length of this string, usually called if the calling code is being naughty and doing some
    *         low level work on the internal pointer for some reason.
    *  \returns itself for convenience.
    */
   String &checkLength() noexcept;

   /** \brief Checks if this string is a valid integer.
    *  \returns true if the contents of string would make up a valid integer, else returns false.
    */
   bool isInt() const noexcept;

   /** \brief Checks if this string is a valid unsigned integer.
    *  \returns true if the contents of string would make up a valid unsigned integer, else returns false.
    */
   bool isUnsigned() const noexcept;

   /** \brief Checks if this string is a valid double.
    *  \returns true if the contents of string would make up a valid double, else returns false.
    */
   bool isDouble() const noexcept;

   /** \brief Checks if this string is a valid boolean.
    *
    *  There are several valid boolean values, namely: [true, false, yes, no, t, f, 1, 0]
    *  \returns true if the contents of string would make up a valid boolean, else returns false.
    */
   bool isBool() const noexcept;

   /** \brief Checks if this string is a valid uuid.
    *
    *  Checks if the string is valid universal unique identifier.
    *  \returns true if the contents of string would make up a valid uuid, else returns false.
    */
   bool isUUID() const noexcept;

   /** \brief Checks if this string is all alpahbetical character only.
    *  \returns true if the contents of string is made up entirely of alphabetical characters, else returns false.
    */
   bool isAlpha() const noexcept;

   /** \brief Checks if this string is entirely in upper case.
    *  \returns true if the contents of string is made up entirely of upper case characters, else returns false.
    */
   bool isUpper() const noexcept;

   /** \brief Checks if this string is entirely in lower case.
    *  \returns true if the contents of string is made up entirely of lower case characters, else returns false.
    */
   bool isLower() const noexcept;

   /** \brief Converts the string to an int.
    *  \returns the string as an int.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   int toInt() const;

   /** \brief Converts the string to an unsinged int.
    *  \returns the string as an unsigned int.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   unsigned int toUInt() const;

   /** \brief Converts the string to an int32.
    *  \returns the string as an int32.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   int32_t toInt32() const;

   /** \brief Converts the string to an uint32.
    *  \returns the string as an uint32.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   uint32_t toUInt32() const;

   /** \brief Converts the string to an int64_t.
    *  \returns the string as an int64_t.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   int64_t toInt64() const;

   /** \brief Converts the string to an uint64_t.
    *  \returns the string as an uint64_t.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   uint64_t toUInt64() const;

   /** \brief Converts the string to a float.
    *  \returns the string as a float.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   float toFloat() const;

   /** \brief Converts the string to a float.
    *  \returns the string as a double.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   double toDouble() const;

   /** \brief Converts the string to a long double.
    *  \returns the string as a long double.
    *  \throws xMettle if the string is not valid, or if the conversion would over/under flow.
    */
   long double toLongDouble() const;

   /** \brief Converts the string to a boolean.
    *  \returns a valid boolean value of the string, or returns false if there was an error.
    */
   bool toBool() const noexcept;

   /** \brief Converts the string to an int.
    *  \param dest the destination uuid_t point to fill.
    *  \returns the dest pointer for convenience..
    *  \throws xMettle if the string is not a valid uuid.
    */
   uuid_t *toUUID(uuid_t *dest) const;

   /** \brief Operator overload to a 'const char *'.
    *  \returns the internal 'char *' as a 'const char *'.
    */
   operator const char* () const noexcept { return _str; }

   /** \brief Operator overload to a 'char *'
    *  \returns the internal 'char *'
    */
   operator char* () noexcept { return _str; }

   /** \brief Operator overload for [] to get a character at te specified position in the string.
    *  \param pos the position of the char to get, note no boundry checking is performed use with caution.
    *  \returns the character at \p pos.
    */
   char & operator [] (const unsigned int pos) noexcept { return _str[pos]; }

   /** \brief Operator overload for [] to get a character at te specified position in the string.
    *  \param pos the position of the char to get, note no boundry checking is performed use with caution.
    *  \returns the character at \p pos.
    */
   char & operator [] (const int pos) noexcept { return _str[pos]; }

   /** \brief Operator overload for = to assign the string to a character.
    *  \param ch value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const char ch);

   /** \brief Operator overload for = to assign the string to a new string.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const char *strVal);

   /** \brief Operator overload for = to assign the string to a new string.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const String &strVal);

   /** \brief Operator overload for = to assign the string to an integer.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const int32_t strVal);

   /** \brief Operator overload for = to assign the string to an unsigned integer.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const uint32_t strVal);

   /** \brief Operator overload for = to assign the string to an int64_t.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const int64_t strVal);

   /** \brief Operator overload for = to assign the string to an uint64_t.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const uint64_t strVal);

   /** \brief Operator overload for = to assign the string to a float.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const float strVal);

   /** \brief Operator overload for = to assign the string to a double.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const double strVal);

   /** \brief Operator overload for = to assign the string to a long double.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const long double strVal);

   /** \brief Operator overload for = to assign the string to a boolean.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const bool strVal);

   /** \brief Operator overload for = to assign the string to a uuid.
    *  \param strVal value the string will become.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator = (const uuid_t strVal);

   /** \brief Operator overload for += to append a character to the String.
    *  \param ch the chaaracter to be appeneded.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const char ch)     ;

   /** \brief Operator overload for += to append a null terminated char array to the String.
    *  \param strVal the string to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const char *strVal)  ;

   /** \brief Operator overload for += to append a String to this String.
    *  \param strVal the string to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const String &strVal)  ;

   /** \brief Operator overload for += to append an integer to the String.
    *  \param strVal the integer to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const int32_t strVal) ;

   /** \brief Operator overload for += to append an unsigned integer to the String.
    *  \param strVal the unsigned integer to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const uint32_t strVal) ;

   /** \brief Operator overload for += to append an int64_t to the String.
    *  \param strVal the int64_t to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const int64_t strVal) ;

   /** \brief Operator overload for += to append an uint64_t to the String.
    *  \param strVal the uint64_t to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const uint64_t strVal) ;

   /** \brief Operator overload for += to append a float to the String.
    *  \param strVal the float to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const float strVal) ;

   /** \brief Operator overload for += to append a double to the String.
    *  \param strVal the double to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const double strVal) ;

   /** \brief Operator overload for += to append a long double to the String.
    *  \param strVal the long double to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const long double strVal) ;

   /** \brief Operator overload for += to append a boolean to the String.
    *  \param strVal the boolean to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const bool strVal) ;

   /** \brief Operator overload for += to append a uuid to the String.
    *  \param strVal the uuid to be appended.
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   String & operator += (const uuid_t strVal) ;

   /** \brief Friend operator overload for + between String and a String.
    *  \param str1 the first String.
    *  \param str2 the second String.
    *  \returns a new string with \p str1 and \p str2 combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str1, const String &str2)    ;

   /** \brief Friend operator overload for + between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns a new string with \p str and \p ch combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str, const char ch)     ;

   /** \brief Friend operator overload for + between String and a char*.
    *  \param str1 the String.
    *  \param str2 the null terminated char pointer.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str1, const char *str2)  ;

   /** \brief Friend operator overload for + between String and an integer.
    *  \param str the String.
    *  \param strVal the integer.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str, const int32_t strVal) ;

   /** \brief Friend operator overload for + between String and an unsigned integer.
    *  \param str the String.
    *  \param strVal the unsigned integer.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str, const uint32_t strVal) ;

   /** \brief Friend operator overload for + between String and an int64_t.
    *  \param str the String.
    *  \param strVal the int64_t integer.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str, const int64_t strVal) ;

   /** \brief Friend operator overload for + between String and an uint64_t.
    *  \param str the String.
    *  \param strVal the uint64_t integer.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str, const uint64_t strVal) ;

   /** \brief Friend operator overload for + between String and a double.
    *  \param str the String.
    *  \param strVal the double.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str, const double strVal) ;

   /** \brief Friend operator overload for + between String and a boolean.
    *  \param str the String.
    *  \param strVal the boolean.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str, const bool strVal) ;

   /** \brief Friend operator overload for + between String and a uuid.
    *  \param str the String.
    *  \param strVal the uuid.
    *  \returns a new string with \p str and \p strVal combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const String &str, const uuid_t strVal) ;

   /** \brief Friend operator overload for + between a char and String.
    *  \param ch the character.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const char ch, const String &str) ;

   /** \brief Friend operator overload for + between a char pointer and String.
    *  \param str1 the null terminated char pointer.
    *  \param str2 the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const char *str1,  const String &str2) ;

   /** \brief Friend operator overload for + between an integer and String.
    *  \param strVal the integer.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const int32_t strVal, const String &str) ;

   /** \brief Friend operator overload for + between an unsigned integer and String.
    *  \param strVal the unsigned integer.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const uint32_t strVal, const String &str) ;

   /** \brief Friend operator overload for + between a double and String.
    *  \param strVal the double.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const double strVal, const String &str) ;

   /** \brief Friend operator overload for + between a boolean and String.
    *  \param strVal the boolean.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const bool strVal, const String &str) ;

   /** \brief Friend operator overload for + between a uuid and String.
    *  \param strVal the uuid.
    *  \param str the String.
    *  \returns a new string with \p strVal and \p str combined.
    *  \throws xMettle if out of memory.
    */
   friend String operator + (const uuid_t strVal, const String &str) ;

   /** \brief Less than friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str is less than \p ch.
    */
   friend bool operator <  (const String &str, const char ch)   noexcept;

   /** \brief Greater than friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str is greater than \p ch.
    */
   friend bool operator >  (const String &str, const char ch)   noexcept;

   /** \brief Less than equal friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str is less than or equal to \p ch.
    */
   friend bool operator <= (const String &str, const char ch)   noexcept;

   /** \brief Greater than equals friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str is greater than or equal to \p ch.
    */
   friend bool operator >= (const String &str, const char ch)   noexcept;

   /** \brief Equals friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str equals \p ch.
    */
   friend bool operator == (const String &str, const char ch)   noexcept;

   /** \brief Not equals friend operator overload between String and a char.
    *  \param str the String.
    *  \param ch the character.
    *  \returns true if \p str does not equal \p ch.
    */
   friend bool operator != (const String &str, const char ch)   noexcept;

   /** \brief Less than friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str is less than \p strVal.
    */
   friend bool operator <  (const String &str, const char *strVal)   noexcept;

   /** \brief Greater than friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str is greater than \p strVal.
    */
   friend bool operator >  (const String &str, const char *strVal)   noexcept;

   /** \brief Less than equals friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str is less than or equal to \p strVal.
    */
   friend bool operator <= (const String &str, const char *strVal)   noexcept;

   /** \brief Greater than equals friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str is greater than or equal to \p strVal.
    */
   friend bool operator >= (const String &str, const char *strVal)   noexcept;

   /** \brief Equals friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str equals \p strVal.
    */
   friend bool operator == (const String &str, const char *strVal)   noexcept;

   /** \brief Not equals friend operator overload between String and a char pointer.
    *  \param str the String.
    *  \param strVal the null terminated char pointer.
    *  \returns true if \p str does not equal \p strVal.
    */
   friend bool operator != (const String &str, const char *strVal)   noexcept;

   /** \brief Less than friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str is less than \p strVal.
    */
   friend bool operator <  (const String &str, const String &strVal)   noexcept;

   /** \brief Greater than friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str is greater than \p strVal.
    */
   friend bool operator >  (const String &str, const String &strVal)   noexcept;

   /** \brief Less than equals friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str is less than or equal to \p strVal.
    */
   friend bool operator <= (const String &str, const String &strVal)   noexcept;

   /** \brief Greater than equals friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str is greater than or equal to \p strVal.
    */
   friend bool operator >= (const String &str, const String &strVal)   noexcept;

   /** \brief Equals friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str equals \p strVal.
    */
   friend bool operator == (const String &str, const String &strVal)   noexcept;

   /** \brief Not equals friend operator overload between String and a String.
    *  \param str the String.
    *  \param strVal the other String.
    *  \returns true if \p str does not equal \p strVal.
    */
   friend bool operator != (const String &str, const String &strVal)   noexcept;

   /** \brief Ouput stream operator.
    *  \param output the output stream to receive the string.
    *  \param str the string object to be output.
    *  \returns the output stream as expected.
    */
   friend std::ostream &operator << (std::ostream &output, const String &str) noexcept;

   /** \brief Input stream operator.
    *  \param input the input stream.
    *  \param str the string object to receive the value.
    *  \returns the input stream as expected.
    *  \throws xMettle if out of memory.
    */
   friend std::istream &operator >> (std::istream &input, String &str);

   static int          toInt(const char *str);
   static unsigned int toUInt(const char *str);
   static int32_t      toInt32(const char *str);
   static uint32_t     toUInt32(const char *str);
   static int64_t      toInt64(const char *str);
   static uint64_t     toUInt64(const char *str);
   static float        toFloat(const char *str);
   static double       toDouble(const char *str);
   static long double  toLongDouble(const char *str);

private:

   String(const char *str1, const char *str2)  ;

   char             *_str;
   unsigned int      _strLen;
   unsigned int      _bytesUsed;

   String           *_newStr(const char *str)         ;

   String           &_appendStr(const char *str)      ;

   String           &_appendPartialStr(const char *str, unsigned int strLen);

   void              _freeStr()                        noexcept;

   void              _replaceStr(const String &dest, const String &source)        ;

   void              _stripChars(const bool  left,
                                 const bool  right,
                                 const char *chars) noexcept;

   static bool       _isTrue(const char *str)   noexcept;
   static bool       _isFalse(const char *str)  noexcept;
};



}}

#endif
