/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/mutex.h"

#if !defined (OS_MS_WINDOWS)

#include <errno.h>
#include <stdio.h>

#endif

namespace Mettle { namespace Lib {

Mutex::Mutex()
{
#if defined (OS_MS_WINDOWS)

   if ((_mutex = CreateMutex(0, false, 0)) == 0)
      _throwMutexError(0, "CreateMutex");

#elif defined (OS_LINUX) || defined (OS_AIX)

   int rc;

   if (0 != (rc = pthread_mutex_init(&_mutex, NULL) != 0))
      _throwMutexError(rc, "pthread_mutex_init");

#endif
}

Mutex::~Mutex(void) noexcept
{
#if defined (OS_MS_WINDOWS)

   CloseHandle(_mutex);

#elif defined (OS_LINUX) || defined (OS_AIX)

   pthread_mutex_destroy(&_mutex);

#endif
}

void Mutex::unlock()
{
#if defined (OS_MS_WINDOWS)

   if (!ReleaseMutex(_mutex))
      _throwMutexError(0, "ReleaseMutex");

#elif defined (OS_LINUX) || defined (OS_AIX)

   int rc;

   if (0 != (rc = pthread_mutex_unlock(&_mutex)))
      _throwMutexError(rc, "pthread_mutex_unlock");

#endif
}

void Mutex::lock(const uint32_t waittime)
{
#if defined (OS_MS_WINDOWS)

   if (WaitForSingleObject(_mutex, waittime ? waittime : INFINITE) == WAIT_ABANDONED)
      _throwMutexError(0, "WaitForSingleObject");

#elif defined (OS_LINUX) || defined (OS_AIX)

   int rc;

   if (0 != (rc = pthread_mutex_lock(&_mutex)))
      _throwMutexError(rc, "pthread_mutex_lock");

#endif
}

void Mutex::_throwMutexError(int errCode, const char *str)
{
#if defined (OS_MS_WINDOWS)

   char    errorStr[255];

   errCode = xMettle::getOSError(errorStr, sizeof(errorStr));

   throw xMettle(__FILE__, __LINE__, "Mutex", "%s - %d/%s", str, errCode, errorStr);

#elif defined (OS_LINUX)

   char    errorStr[255];

   xMettle::getOSError(errorStr, sizeof(errorStr), errCode);

   throw xMettle(__FILE__, __LINE__, "Mutex", "%s - %d/%s", str, errCode, errorStr);

#endif
}

Mutex::Auto::Auto(Mutex *mutex, const uint32_t waittime)
{
   _mutex = mutex;

   _mutex->lock(waittime);
}

Mutex::Auto::~Auto()
{
   _mutex->unlock();
}

}}
