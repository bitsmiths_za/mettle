/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/filehandle.h"

#include "mettle/lib/platform.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/memoryblock.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#if defined(OS_MS_WINDOWS)

   #define WIN32_LEAN_AND_MEAN
   #include <windows.h>

   #if defined (WINCE)
      #include <string.h>
   #else
      #include <io.h>
   #endif

   #if defined(COMPILER_MSC)

      #define S_ISDIR(m) (((m) & _S_IFMT) == _S_IFDIR)
      #define S_ISREG(m) (((m) & _S_IFMT) == _S_IFREG)

   #endif

#else

   #include <unistd.h>
   #include <dirent.h>

#endif

#if !defined(WINCE)

   #include <sys/types.h>
   #include <sys/stat.h>

#endif


namespace Mettle { namespace Lib {

FileHandle::xFileHandle::xFileHandle(const char *file, const int line, const char *fmt, ...) noexcept
                        :xMettle(file, line, "xFileHandle")
{
   va_list ap;

   va_start(ap, fmt);

   if (vsnprintf(errorMsg, sizeof(errorMsg) - 2, fmt, ap) == -1)
      errorMsg[sizeof(errorMsg) - 1] = 0;

   va_end(ap);
}


FileHandle::xFileHandle::xFileHandle(const xFileHandle &x) noexcept
                        :xMettle(x)
{
}

FileHandle::FileHandle() noexcept
{
   _fptr     = 0;
   _fileName.clear();
}

FileHandle::~FileHandle() noexcept
{
   close(false);
}

FileHandle &FileHandle::open(const char *fileName,
                             const Mode  openMeth)
{
   close();

   if (!fileName || !fileName[0])
      throw xFileHandle(__FILE__, __LINE__, "Null filename specified");

   _fileName = fileName;

   if ((_fptr = fopen(fileName, openMeth == Binary ? "rb" : "rt")) == 0)
      throw xFileHandle(__FILE__, __LINE__, "Error opening file (%s)", _fileName.c_str());

   return *this;
}

FileHandle &FileHandle::create(const char *fileName,
                               const Mode  openMeth)
{
   close();

   if (!fileName || !fileName[0])
      throw xFileHandle(__FILE__, __LINE__, "Null filename specified");

   _fileName = fileName;

   if ((_fptr = fopen(fileName, openMeth == Binary ? "wb" : "wt")) == 0)
      throw xFileHandle(__FILE__, __LINE__, "Error creating file (%s)", _fileName.c_str());

   // set buffer size
#if defined(OS_MS_WINDOWS)

   #if !defined(WINCE)

      if (setvbuf(_fptr, 0, _IOFBF, 4096))
      {
         close(false);
         throw xFileHandle(__FILE__, __LINE__, "Error error allocating memory (%s)", _fileName.c_str());
      }

   #endif

#else

   if (setvbuf(_fptr, 0, _IOFBF, 4096))
   {
      close(false);
      throw xFileHandle(__FILE__, __LINE__, "Error error allocating memory (%s)", _fileName.c_str());
   }

#endif

   return *this;
}

void FileHandle::close(const bool checkForcloseError)
{
   if (_fptr)
   {
      fflush(_fptr);

      if (fclose(_fptr) == EOF)
      {
         _fptr = 0;

         if (checkForcloseError)
            throw xFileHandle(__FILE__, __LINE__, "Error closing file (%s)", _fileName.c_str());
      }

      _fptr     = 0;
      _fileName = "";
   }
}

FILE *FileHandle::filePointer() const noexcept
{
   return _fptr;
}

FILE *FileHandle::poop() noexcept
{
   FILE *p = _fptr;

   _fptr     = 0;
   _fileName.clear();

   return p;
}

unsigned int FileHandle::getLength()
{
   long int len;

   if (fseek(_fptr, 0L, SEEK_END) == -1 ||
       (len = ftell(_fptr))       == -1 ||
       fseek(_fptr, 0L, SEEK_SET) == -1)
      throw xFileHandle(__FILE__, __LINE__, "Error accessing file (%s)", _fileName.c_str());

   return (unsigned int) len;
}

void FileHandle::writef(const char *fmt, ...)
{
   va_list ap;

   va_start(ap, fmt);

   if (vfprintf(_fptr, fmt, ap) == EOF)
   {
      va_end(ap);
      throw xFileHandle(__FILE__, __LINE__, "Error writting format to file (%s)", _fileName.c_str());
   }

   va_end(ap);
}

void FileHandle::write(const char *str)
{
   if (fwrite(str, strlen(str), 1, _fptr) != 1)
      throw xFileHandle(__FILE__, __LINE__, "Error writting to file (%s)", _fileName.c_str());
}

void FileHandle::eat(FILE *fptr, const char *fileName)
{
   close();

   if (!fileName || !fileName[0])
      throw xFileHandle(__FILE__, __LINE__, "Null filename specified");

   _fptr     = fptr;
   _fileName = fileName;
}

void FileHandle::write(const void         *source,
                       const unsigned int  len)
{
   if (!len || !source)
      return;

   if (fwrite(source, len, 1, _fptr) != 1)
      throw xFileHandle(__FILE__, __LINE__, "Error writting to file (%s/%u)", _fileName.c_str(), len);
}

void FileHandle::write(const MemoryBlock  *source)
{
   if (source->isEmpty())
      return;

   if (fwrite(source->_block, source->_size, 1, _fptr) != 1)
      throw xFileHandle(__FILE__, __LINE__, "Error writting to file (%s/%u)", _fileName.c_str(), source->_size);
}

unsigned int FileHandle::read(      void          *dest,
                              const unsigned int   len,
                              const bool           throwOnError)
{
   if (!len || !dest)
      return 0;

   unsigned int read;

   if ((read = fread(dest, 1, len, _fptr)) != len)
   {
      if (throwOnError)
         throw xFileHandle(__FILE__, __LINE__, "Error reading from file (%s/%u)", _fileName.c_str(), len);
   }

   return read;
}


unsigned int FileHandle::read(      MemoryBlock   *dest,
                              const bool           throwOnError)
{
   if (dest->isEmpty())
      return 0;

   unsigned int read;

   if ((read = fread(dest->_block, 1, dest->_size, _fptr)) != dest->_size)
   {
      if (throwOnError)
         throw xFileHandle(__FILE__, __LINE__, "Error reading from file (%s/%u)", _fileName.c_str(), dest->size());
   }

   return read;
}


bool FileHandle::endOfFile() const noexcept
{
   if (feof(_fptr))
      return true;

   return false;
}


bool FileHandle::readLine(char *dest, const int len)
{
   size_t jlen;

   if (!len || !dest)
      return false;

   if (fgets(dest, len, _fptr) == 0)
      return false;

   if ((jlen = strlen(dest)) > 0)
   {
#if defined(OS_MS_WINDOWS)
      if (dest[jlen - 1] == '\n' &&
          dest[jlen - 2] == '\r')
         dest[jlen - 2] = 0;
#else
      if (dest[jlen - 1] == '\n')
         dest[jlen -1 ] = 0;
#endif
   }

   return true;
}


bool FileHandle::readLine(String &dest)
{
   char   jotter[4096];
   size_t jlen;

   if (fgets(jotter, sizeof(jotter) - 1, _fptr) == 0)
      return false;

   if ((jlen = strlen(jotter)) > 0)
   {
#if defined(OS_MS_WINDOWS)
      if (jotter[jlen - 1] == '\n' &&
          jotter[jlen - 2] == '\r')
         jotter[jlen - 2] = 0;
#else
      if (jotter[jlen - 1] == '\n')
         jotter[jlen - 1] = 0;
#endif
   }

   dest = jotter;

   return true;
}

FileHandle &FileHandle::seekStart()
{
   if (fseek(_fptr, 0L, SEEK_SET) == -1)
      throw xFileHandle(__FILE__, __LINE__, "Error accessing file (%s)", _fileName.c_str());

   return *this;
}

FileHandle &FileHandle::seekEnd()
{
   if (fseek(_fptr, 0L, SEEK_END) == -1)
      throw xFileHandle(__FILE__, __LINE__, "Error accessing file (%s)", _fileName.c_str());

   return *this;
}


FileHandle &FileHandle::seekOffset(const long int offset)
{
   if (fseek(_fptr, 0L, SEEK_CUR) == -1)
      throw xFileHandle(__FILE__, __LINE__, "Error accessing file (%s)", _fileName.c_str());

   return *this;
}

long int FileHandle::tell() noexcept
{
   return ftell(_fptr);
}


const String &FileHandle::fileName() const noexcept
{
   return _fileName;
}


}}
