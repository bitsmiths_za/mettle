/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/lib/string.h"

#define __STDC_FORMAT_MACROS 1
#define __STDC_LIMIT_MACROS

#include <stdlib.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include <limits.h>

#include "mettle/lib/common.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/stringbuilder.h"
#include "mettle/lib/ustring.h"

#define MAX_INT_STR_SIZE         128
#define BSTRING_TRUE             "true"
#define BSTRING_FALSE            "false"
#define BSTRING_YES              "yes"
#define BSTRING_NO               "no"
#define WHITE_SPACES             " \n\r\t\f\v"

namespace Mettle { namespace Lib {

#define MODULE "Mettle.String"

String::String() : _str(0), _strLen(0), _bytesUsed(0)
{
   _newStr(_EMPTY_STRING);
}

String::String(const char *str) : _str(0), _strLen(0), _bytesUsed(0)
{
   _newStr(str);
}

String::String(const String &str) : _str(0), _strLen(0), _bytesUsed(0)
{
   if (str.isEmpty())
   {
      _newStr(_EMPTY_STRING);
      return;
   }

   char *tmp;

   if ((tmp = (char *) realloc(_str, str.length() + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", str.length() + 1);

   _str       = tmp;
   _strLen    = str.length();
   _bytesUsed = _strLen + 1;

   strcpy(_str, str._str);
}

String::String(const int16_t strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId16, strVal);
   _newStr(jotter);
}

String::String(const uint16_t strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu16, strVal);
   _newStr(jotter);
}

String::String(const int32_t strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);
   _newStr(jotter);
}

String::String(const uint32_t strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   _newStr(jotter);
}

String::String(const int64_t strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId64, strVal);
   _newStr(jotter);
}

String::String(const uint64_t strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu64, strVal);
   _newStr(jotter);
}

String::String(const float strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%f", strVal);
   _newStr(jotter);
}

String::String(const double strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);
   _newStr(jotter);
   stripAllButOne(0, '0');
}

String::String(const long double strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%Lf", strVal);
   _newStr(jotter);
   stripAllButOne(0, '0');
}

String::String(const char ch) : _str(0), _strLen(0), _bytesUsed(0)
{
   char strVal[2];

   strVal[0] = ch;
   strVal[1] = 0;

   _newStr(strVal);
   stripAllButOne(0, '0');
}

String::String(const bool strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   if (strVal)
      _newStr(BSTRING_TRUE);
   else
      _newStr(BSTRING_FALSE);
}

String::String(const uuid_t strVal) : _str(0), _strLen(0), _bytesUsed(0)
{
   if (uuid_is_null(strVal))
      _newStr(_EMPTY_STRING);
   else
   {
      char jotter[38];

      uuid_unparse(strVal, jotter);

      _newStr(jotter);
   }
}

String::String(const char *str1, const char *str2) : _str(0), _strLen(0), _bytesUsed(0)
{
   char         *tmp;
   unsigned int  len = 0;

   if (str1)
      len += strlen(str1);

   if (str2)
      len += strlen(str2);

   if (!len)
   {
      _newStr(_EMPTY_STRING);
      return;
   }

   if ((tmp = (char *) realloc(_str, len + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", len  + 1);

   _str       = tmp;
   _strLen    = len;
   _bytesUsed = len + 1;

   if (str1)
   {
      strcpy(_str, str1);

      if (str2)
         strcat(_str, str2);
   }
   else
      strcpy(_str, str2);
}


String::String(const char *str1, const unsigned len) : _str(0), _strLen(0), _bytesUsed(0)
{
   if (!str1 || !len)
   {
      _newStr(_EMPTY_STRING);
      return;
   }

   char *tmp;

   if ((tmp = (char *) realloc(_str, len + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", len  + 1);

   _str       = tmp;
   _strLen    = len;
   _bytesUsed = len + 1;

   memcpy(_str, str1, len);
   _str[len] = 0;
}

String::~String() noexcept
{
   _freeStr();
}

void String::_freeStr() noexcept
{
   if (!_str)
      return;

   free(_str);

   _str      = 0;
   _strLen   = 0;
   _bytesUsed = 0;
}

String *String::_newStr(const char *str)

{
   if (!str)
      return _newStr(_EMPTY_STRING);

   char     *tmp;
   unsigned  len = strlen(str);

   if (len < _bytesUsed)
   {
      strcpy(_str, str);
      _strLen = len;
      return this;
   }

   if ((tmp = (char *) realloc(_str, len + 1)) == 0)
            throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", len  + 1);

   _str       = tmp;
   _strLen    = len;
   _bytesUsed = len + 1;

   strcpy(_str, str);

   return this;
}

String &String::_appendStr(const char *str)
{
   if (!str || !*str)
      return *this;

   char         *tmp;
   unsigned int  len = _strLen + strlen(str);

   if (len < _bytesUsed)
   {
      _strLen = len;
      strcat(_str, str);
      return *this;
   }

   if ((tmp = (char *) realloc(_str, len + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", len  + 1);

   _str       = tmp;
   _strLen    = len;
   _bytesUsed = len + 1;

   strcat(_str, str);

   return *this;
}


String &String::_appendPartialStr(const char *str, const unsigned int strLen)
{
   if (!str || !*str)
      return *this;

   char         *tmp;
   unsigned int  len = _strLen + strLen;

   if (len < _bytesUsed)
   {
      _strLen = len;
      strcat(_str, str);
      return *this;
   }

   if ((tmp = (char *) realloc(_str, len + 1)) == 0)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", len  + 1);

   _str       = tmp;
   _strLen    = len;
   _bytesUsed = len + 1;

   strncpy(_str, str, strLen);

   _str[len] = 0;

   return *this;
}


String &String::operator = (const char ch)
{
   char strVal[2];

   strVal[0] = ch;
   strVal[1] = 0;

   return *_newStr(strVal);
}

String &String::operator = (const char *strVal)
{
   return *_newStr(strVal);
}

String &String::operator = (const String &strVal)
{
   return *_newStr(strVal._str);
}

String &String::operator = (const int32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);
   return *_newStr(jotter);
}

String &String::operator = (const uint32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   return *_newStr(jotter);
}

String &String::operator = (const int64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId64, strVal);
   return *_newStr(jotter);
}

String &String::operator = (const uint64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu64, strVal);
   return *_newStr(jotter);
}

String &String::operator = (const float strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%f", strVal);
   return _newStr(jotter)->stripAllButOne(0, '0');
}

String &String::operator = (const double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);
   return _newStr(jotter)->stripAllButOne(0, '0');
}

String &String::operator = (const long double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%Lf", strVal);
   return _newStr(jotter)->stripAllButOne(0, '0');
}

String &String::operator = (const bool strVal)
{
   if (strVal)
      return *_newStr(BSTRING_TRUE);

   return *_newStr(BSTRING_FALSE);
}

String &String::operator = (const uuid_t strVal)
{
   if (uuid_is_null(strVal))
      return *_newStr(_EMPTY_STRING);

   char jotter[38];

   uuid_unparse(strVal, jotter);

   return *_newStr(jotter);
}

String &String::operator += (const char ch)
{
   char strVal[2];

   strVal[0] = ch;
   strVal[1] = 0;

   return _appendStr(strVal);
}

String &String::operator += (const char *strVal)
{
   return _appendStr(strVal);
}

String &String::operator += (const String &strVal)
{
   return _appendStr(strVal._str);
}

String &String::operator += (const int32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);

   return _appendStr(jotter);
}

String &String::operator += (const uint32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   return _appendStr(jotter);
}

String &String::operator += (const int64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId64, strVal);
   return _appendStr(jotter);
}

String &String::operator += (const uint64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu64, strVal);
   return _appendStr(jotter);
}

String &String::operator += (const float strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%f", strVal);

   return _appendStr(jotter).stripAllButOne(0, '0');
}

String &String::operator += (const double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);

   return _appendStr(jotter).stripAllButOne(0, '0');
}

String &String::operator += (const long double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%Lf", strVal);

   return _appendStr(jotter).stripAllButOne(0, '0');
}

String &String::operator += (const bool strVal)
{
   if (strVal)
      return _appendStr(BSTRING_TRUE);

   return _appendStr(BSTRING_FALSE);
}

String &String::operator += (const uuid_t strVal)
{
   if (uuid_is_null(strVal))
      return *this;

   char jotter[38];

   uuid_unparse(strVal, jotter);

   return _appendStr(jotter);
}

String operator + (const String &str, const char ch)
{
   char strVal[2];

   strVal[0] = ch;
   strVal[1] = 0;

   return String(str._str, strVal);
}

String operator + (const String &str1, const char *str2)
{
   return String(str1._str, str2);
}

String operator + (const String &str1, const String &str2)
{
   return String(str1._str, str2._str);
}

String operator + (const String &str, const int32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);
   return String(str._str, jotter);
}

String operator + (const String &str, const uint32_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   return String(str._str, jotter);
}

String operator + (const String &str, const int64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId64, strVal);
   return String(str._str, jotter);
}

String operator + (const String &str, const uint64_t strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu64, strVal);
   return String(str._str, jotter);
}

String operator + (const String &str, const double strVal)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);
   return String(str._str, jotter);
}

String operator + (const char ch, const String &str)
{
   char strVal[2];

   strVal[0] = ch;
   strVal[1] = 0;

   return String(strVal, str._str);
}

String operator + (const char *str1, const String &str2)
{
   return String(str1, str2._str);
}

String operator + (const int32_t strVal, const String &str)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRId32, strVal);
   return String(jotter, str._str);
}

String operator + (const uint32_t strVal, const String &str)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%" PRIu32, strVal);
   return String(jotter, str._str);
}

String operator + (const double strVal, const String &str)
{
   char jotter[MAX_INT_STR_SIZE];

   sprintf(jotter, "%lf", strVal);
   return String(jotter, str._str);
}

String operator + (const bool strVal, const String &str)
{
   if (strVal)
      return String(BSTRING_TRUE, str._str);

   return String(BSTRING_FALSE, str._str);
}

String operator + (const String &str, const bool strVal)
{
   if (strVal)
      return String(BSTRING_TRUE, str._str);

   return String(BSTRING_FALSE, str._str);
}

String operator + (const uuid_t strVal, const String &str)
{
   if (uuid_is_null(strVal))
      return String(str._str);
   else
   {
      char jotter[38];

      uuid_unparse(strVal, jotter);

      return String(jotter, str._str);
   }
}

String operator + (const String &str, const uuid_t strVal)
{
   if (uuid_is_null(strVal))
      return String(str._str);
   else
   {
      char jotter[38];

      uuid_unparse(strVal, jotter);

      return String(str._str, jotter);
   }
}

String &String::clear() noexcept
{
   *_str   = 0;
   _strLen = 0;

   return *this;
}

String &String::eat(char *str) noexcept
{
   _freeStr();

   _str        = str;
   _strLen     = strlen(_str);
   _bytesUsed  = _strLen + 1;

   return *this;
}

String &String::eat(String &str) noexcept
{
   _freeStr();

   _str        = str._str;
   _strLen     = str._strLen;
   _bytesUsed  = str._bytesUsed;

   str._str        = 0;
   str._strLen     =
   str._bytesUsed  = 0;

   return *this;
}

String &String::eat(UString &str) noexcept
{
   _freeStr();

   _str        = str._str;
   _strLen     = str._byteLen;
   _bytesUsed  = str._bytesUsed;

   str._str        = 0;
   str._strLen     =
   str._byteLen    =
   str._bytesUsed  = 0;

   return *this;
}

String &String::eat(StringBuilder &str) noexcept
{
   _freeStr();

   _str        = str.value;
   _strLen     = str.used;
   _bytesUsed  = str.size;

   str.value = 0;
   str.used  =
   str.size  = 0;

   if (_str == 0)
      _newStr(_EMPTY_STRING);

   return *this;
}

char *String::poop() noexcept
{
   char *tmp = _str;

   _str       = 0;
   _strLen    =
   _bytesUsed = 0;

   _newStr(_EMPTY_STRING);

   return tmp;
}

String &String::format(const char *fmt, ...)
{
   va_list ap;
   char    jotter[4096];

   va_start(ap, fmt);

   int cnt = vsnprintf(jotter, sizeof(jotter), fmt, ap);

   va_end(ap);

   if (cnt > -1 && cnt < sizeof(jotter))
   {
      _newStr(jotter);
      return *this;
   }

   char *tmp = 0;

   try
   {
      if (cnt < 0)
         throw xMettle(__FILE__, __LINE__, MODULE, "Format: problem formating with vsnprintf '%s'", fmt);

      if ((tmp = (char *) realloc(tmp, cnt+1)) == 0)
         throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", cnt + 1);

      va_start(ap, fmt);

      vsnprintf(tmp, cnt+1, fmt, ap);

      va_end(ap);

      _newStr(tmp);

      _FREE(tmp);

      return *this;
   }
   catch (...)
   {
      va_end(ap);
      _FREE(tmp)
      throw;
   }
}

bool String::isDouble() const noexcept
{
   return Common::stringIsDouble(_str);
}


bool String::isInt() const noexcept
{
   return Common::stringIsInt(_str);
}


bool String::isUnsigned() const noexcept
{
   return Common::stringIsUnsigned(_str);
}


bool String::isBool() const noexcept
{
   if (_isTrue(_str) || _isFalse(_str))
      return true;

   return false;
}


bool String::isUUID() const noexcept
{
   if (_strLen != 36)
      return false;

   return Common::stringIsUUID(_str, false);
}


bool String::isAlpha() const noexcept
{
   char *ptr;

   for (ptr = _str; *ptr; ptr++)
      if (!((*ptr >= 'a' && *ptr <= 'z') || (*ptr >= 'A' && *ptr <= 'Z')))
         return false;

   return true;
}


bool String::isUpper() const noexcept
{
   char *ptr;

   for (ptr = _str; *ptr; ptr++)
      if (*ptr >= 'a' && *ptr <= 'z')
         return false;

   return true;
}


bool String::isLower() const noexcept
{
   char *ptr;

   for (ptr = _str; *ptr; ptr++)
      if (*ptr >= 'A' && *ptr <= 'Z')
         return false;

   return true;
}


String String::splice(const int pos1, const int pos2) const
{
   char *start_pos;
   char *end_pos;

   if (pos1 == 0 && pos2 == 0)
      return String();

   if (pos1 > 0)
   {
      if (pos1 > (int) _strLen)
         return String();

      start_pos = _str + pos1;
   }
   else if (pos1 < 0)
   {
      if ((0 - pos1) > (int) _strLen)
         start_pos = _str;
      else
      {
         start_pos  = _str + _strLen;
         start_pos += pos1;
      }
   }
   else
      start_pos = _str;

   if (pos2 > 0)
   {
      if (pos2 > (int) _strLen)
         end_pos = _str + _strLen;
      else
         end_pos = _str + pos2;
   }
   else if (pos2 < 0)
   {
      if ((0 - pos2) > (int) _strLen)
         return String();

      end_pos  = _str + _strLen;
      end_pos += pos2;
   }
   else
      end_pos = _str + _strLen;

   if (end_pos <= start_pos)
      return String();

   return String(start_pos, end_pos - start_pos);
}

const char String::dirSep() noexcept
{
   return _DIR_SEPERATOR;
}

const char *String::lineEnding() noexcept
{
   return _LINE_ENDING;
}

int String::toInt(const char *str)
{
   char    *end = 0;
   int      val = strtol(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during Int conversion.");

   if (val == LONG_MAX || val == LONG_MIN)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to Int would cause an numeric overflow.");

   return val;
}

int String::toInt() const
{
   return toInt(_str);
}

unsigned int String::toUInt(const char *str)
{
   char         *end = 0;
   unsigned int  val = strtoul(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during UInt conversion.");

   if (val == ULONG_MAX)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to UInt would cause an numeric overflow.");

   return val;
}

unsigned int String::toUInt() const
{
   return toUInt(_str);
}

int32_t String::toInt32(const char *str)
{
   char    *end = 0;

#if LONG_MAX == INT32_MAX || INT_MAX == INT32_MAX

   int32_t val = strtol(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during Int32 conversion.");

   if (val == LONG_MAX || val == LONG_MIN)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to Int32 would cause an numeric overflow.");

#elif LLONG_MAX == INT32_MAX

   int32_t val = strtoll(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during Int32 conversion.");

   if (val == ULONG_MAX || val == ULONG_MIN)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to Int32 would cause an numeric overflow.");

#else
   #error No known string converstion to int32.
#endif

   return val;
}

int32_t String::toInt32() const
{
   return toInt32(_str);
}

uint32_t String::toUInt32(const char *str)
{
   char     *end = 0;

#if ULONG_MAX == UINT32_MAX || UINT_MAX == UINT32_MAX

   uint32_t val = strtoul(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during UInt32 conversion.");

   if (val == ULONG_MAX)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to UInt32 would cause an numeric overflow.");

#elif ULLONG_MAX == UINT32_MAX

   uint32_t val = strtoull(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during UInt32 conversion.");

   if (val == ULLONG_MAX)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to UInt32 would cause an numeric overflow.");

#else
   #error No known string converstion to unsigned int32.
#endif

   return val;
}

uint32_t String::toUInt32() const
{
   return toUInt32(_str);
}

int64_t String::toInt64(const char *str)
{
   char    *end = 0;

#if LONG_MAX == INT64_MAX || INT_MAX == INT64_MAX

   int64_t val = strtol(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during Int64 conversion.");

   if (val == LONG_MAX || val == LONG_MIN)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to Int64 would cause an numeric overflow.");

#elif LLONG_MAX == INT64_MAX

   int64_t val = strtoll(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during Int64 conversion.");

   if (val == LLONG_MAX || val == LLONG_MIN)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to Int64 would cause an numeric overflow.");

#else
   #error No known string converstion to int64.
#endif

   return val;
}

int64_t String::toInt64() const
{
    return toInt64(_str);
}

uint64_t String::toUInt64(const char *str)
{
   char     *end = 0;

#if ULONG_MAX == UINT64_MAX || UINT_MAX == UINT64_MAX

   uint64_t val = strtoul(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during UInt64 conversion.");

   if (val == ULONG_MAX)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to UInt64 would cause an numeric overflow.");

#elif ULLONG_MAX == UINT64_MAX

   uint64_t val = strtoull(str, &end, 10);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during UInt64 conversion.");

   if (val == ULLONG_MAX)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to UInt64 would cause an numeric overflow.");

#else
   #error No known string converstion to unsigned int64.
#endif

   return val;
}

uint64_t String::toUInt64() const
{
   return toUInt64(_str);
}

float String::toFloat(const char *str)
{
   char     *end = 0;
   float     val = strtof(str, &end);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during float conversion.");

   if (val == HUGE_VALF)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to Float would cause an numeric overflow.");

   return val;
}

float String::toFloat() const
{
   return toFloat(_str);
}

double String::toDouble(const char *str)
{
   char     *end = 0;
   double    val = strtod(str, &end);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during Double conversion.");

   if (val == HUGE_VAL)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to Double would cause an numeric overflow.");

   return val;
}

double String::toDouble() const
{
   return toDouble(_str);
}

long double String::toLongDouble(const char *str)
{
   char     *end = 0;
   double    val = strtold(str, &end);

   if (*end)
      throw xMettle(__FILE__, __LINE__, MODULE, "Illegal character found during Long Double conversion.");

   if (val == HUGE_VALL)
      throw xMettle(__FILE__, __LINE__, MODULE, "Conversion to Long Double would cause an numeric overflow.");

   return val;
}

long double String::toLongDouble() const
{
   return toLongDouble(_str);
}

bool String::toBool() const noexcept
{
   if (isBool())
      return _isTrue(_str);

   return false;
}

uuid_t *String::toUUID(uuid_t *dest) const
{
   if (!isUUID())
      throw xMettle(__FILE__, __LINE__, MODULE, "Not a valid value for uuid conversion.");

   if (0 != uuid_parse(_str, *dest))
      throw xMettle(__FILE__, __LINE__, MODULE, "Value could not be parsed for uuid conversion.");

   return dest;
}

bool operator < (const String &str1, const String &str2) noexcept
{
   return strcmp(str1._str, str2._str) < 0;
}

bool operator > (const String &str1, const String &str2) noexcept
{
   return strcmp(str1._str, str2._str) > 0;
}

bool operator <= (const String &str1, const String &str2) noexcept
{
   return strcmp(str1._str, str2._str) <= 0;
}

bool operator >= (const String &str1, const String &str2) noexcept
{
   return strcmp(str1._str, str2._str) >= 0;
}

bool operator == (const String &str1, const String &str2) noexcept
{
   return strcmp(str1._str, str2._str) == 0;
}

bool operator != (const String &str1, const String &str2) noexcept
{
   return strcmp(str1._str, str2._str) != 0;
}

bool operator < (const String &str1, const char *strVal) noexcept
{
   return strcmp(str1._str, strVal) < 0;
}

bool operator > (const String &str1, const char *strVal) noexcept
{
   return strcmp(str1._str, strVal) > 0;
}

bool operator <= (const String &str1, const char *strVal) noexcept
{
   return strcmp(str1._str, strVal) <= 0;
}

bool operator >= (const String &str1, const char *strVal) noexcept
{
   return strcmp(str1._str, strVal) >= 0;
}

bool operator == (const String &str1, const char *strVal) noexcept
{
   return strcmp(str1._str, strVal) == 0;
}

bool operator != (const String &str1, const char *strVal) noexcept
{
   return strcmp(str1._str, strVal) != 0;
}

bool operator < (const String &str1, const char ch) noexcept
{
   return str1._str[0] < ch;
}

bool operator > (const String &str1, const char ch) noexcept
{
   return str1._str[0] > ch;
}

bool operator <= (const String &str1, const char ch) noexcept
{
   return str1._str[0] <= ch;
}

bool operator >= (const String &str1, const char ch) noexcept
{
   return str1._str[0] >= ch;
}

bool operator == (const String &str1, const char ch) noexcept
{
   return str1._str[0] == ch;
}

bool operator != (const String &str1, const char ch) noexcept
{
   return str1._str[0] != ch;
}

std::ostream &operator << (std::ostream &output, const String &str) noexcept
{
   output << str._str;

   return output;
}

std::istream &operator >> (std::istream &input, String &str)
{
   char *p;

   input >> p;

   str = p;

   return input;
}

bool String::_isTrue(const char *str) noexcept
{
   if (!stricmp(str, "y")          ||
       !stricmp(str, "t")          ||
       !stricmp(str, BSTRING_TRUE) ||
       !stricmp(str, BSTRING_YES)  ||
       !stricmp(str, "1"))
      return true;

   return false;
}

bool String::_isFalse(const char *str) noexcept
{
   if (!stricmp(str, "n")           ||
       !stricmp(str, "f")           ||
       !stricmp(str, BSTRING_FALSE) ||
       !stricmp(str, BSTRING_NO)    ||
       !stricmp(str, "0"))
      return true;

   return false;
}

bool String::isEmpty() const noexcept
{
   if (_str == 0 || *_str == 0)
      return true;

   return false;
}

String &String::lower() noexcept
{
   for (char *p = _str; *p; p++)
      *p = tolower(*p);

   return *this;
}

String &String::upper() noexcept
{
   for (char *p = _str; *p; p++)
      *p = toupper(*p);

   return *this;
}

String &String::joinSep(const char *sep, const char *firstStr, ...)
{
   va_list    ap;
   const char *ptr;

   va_start(ap, firstStr);

   *this = firstStr;

   while (1)
   {
      if ((ptr = va_arg(ap, const char*)) == 0)
         break;

      *this += sep;
      *this += ptr;
   }

   va_end(ap);

   return *this;
}

String &String::join(const char *firstStr, ...)
{
   va_list    ap;
   const char *ptr;

   va_start(ap, firstStr);

   *this = firstStr;

   while (1)
   {
      if ((ptr = va_arg(ap, const char*)) == 0)
         break;

      *this += ptr;
   }

   va_end(ap);

   return *this;
}

String &String::joinDirs(const char *firstDir, ...)
{
   va_list    ap;
   const char *ptr;

   va_start(ap, firstDir);

   *this = firstDir;

   while (1)
   {
      if ((ptr = va_arg(ap, const char*)) == 0)
         break;

      makeDirectory();
      *this += ptr;
   }

   va_end(ap);

   return *this;
}

String &String::makeDirectory()
{
   if (_strLen == 0)
   {
      char tmp[3];

      tmp[0] = '.';
      tmp[1] = _DIR_SEPERATOR;
      tmp[2] = 0;

      *this = tmp;
      return *this;
   }

   if (_str[_strLen - 1] != _DIR_SEPERATOR)
      *this += _DIR_SEPERATOR;

   return *this;
}

void String::pathAndFileName(String &destPath, String &destfile) const
{
   char *p = &_str[_strLen - 1];

   for (; p > _str; p--)
      if (*p == _DIR_SEPERATOR)
         break;

   if (p < _str)
      return;

   if (p > _str)
   {
      destPath = this->splice(0, p - _str);
      destfile = this->splice((p - _str) + 1);
   }
   else
   {
      destPath = String::empty();
      destfile = this->_str;
   }
}

String String::fileExtension(const char extch) const
{
   char *ptr;

   ptr = strrchr(_str, extch);

   return String(ptr);
}

int String::find(const String &subStr, const unsigned int offset) const noexcept
{
   char *tmp;
   char *pos;

   if (offset >= _strLen)
      return -1;

   pos = _str + offset;

   if ((tmp = strstr(pos, subStr._str)) == 0)
      return -1;

   return tmp - _str;
}

int String::reverseFind(const String &subStr, const unsigned int offset) const noexcept
{
   char         *tmp = _str;
   unsigned int  len = subStr.length();

   if (offset >= _strLen || len < 1)
      return -1;

   if (offset == 0)
      tmp += _strLen;
   else
      tmp += offset;

   for (; tmp >= _str; tmp--)
      if (strncmp(tmp, subStr._str, len) == 0)
         return tmp - _str;

   return -1;
}

unsigned int String::count(const String &subStr) const noexcept
{
   char     *tmp;
   char     *pos = _str;
   unsigned  cnt = 0;

   while ((tmp = strstr(pos, subStr._str)) != 0)
   {
      pos = tmp + 1;
      cnt++;
   }

   return cnt;
}

String &String::stripRight(const char *chars) noexcept
{
   _stripChars(false, true, chars);
   return *this;
}

String &String::stripLeft(const char *chars) noexcept
{
   _stripChars(true, false, chars);
   return *this;
}

String &String::strip(const char *chars) noexcept
{
   _stripChars(true, true, chars);
   return *this;
}

String &String::stripAllButOne(const char left, const char right) noexcept
{
   char *ptr;

   if (left)
   {
      for (ptr = _str; *ptr == left && *(ptr + 1) == left; ptr++);

      if (ptr > _str)
      {
         _strLen -= (ptr - _str);
         memmove(_str, ptr, _strLen + 1);
      }

      if (*_str == 0)
         return *this;
   }

   if (right)
   {
      ptr = _str + _strLen;

      for (ptr--; ptr >= _str + 1 && *ptr == right && *(ptr - 1) == right; ptr--);

      if (ptr - _str < _strLen - 1)
      {
         _str[ptr - _str] = 0;
         _strLen = ptr - _str;
      }
   }

   return *this;
}

String &String::replace(const String &oldStr, const String &newStr)
{
   _replaceStr(oldStr, newStr);
   return *this;
}

String &String::reverse() noexcept
{
   char *p = _str;
   char *e = _str + _strLen;
   char  tmp;

   for (e--; p < e; p++, e--)
   {
      tmp = *p;
      *p  = *e;
      *e  = tmp;
   }

   return *this;
}

String &String::capitalize() noexcept
{
   _str[0] = toupper(_str[0]);
   return *this;
}

String &String::title() noexcept
{
   if (_str[0] == 0)
      return *this;

   char       *p = _str;
   const char *ch;
   bool        cap = false;

   *p = toupper(*p);

   for (p++; *p; p++)
   {
      for (ch = WHITE_SPACES; *ch; ch++)
         if (*p == *ch)
         {
            cap = true;
            break;
         }

      if (*ch == 0)
      {
         if (cap)
         {
            cap = false;
            *p = toupper(*p);
         }
      }
   }
   return *this;
}

bool String::endsWith(const String &subStr) const noexcept
{
   if (_strLen < subStr._strLen)
      return false;

   if (strcmp(_str + (_strLen - subStr._strLen), subStr._str) == 0)
      return true;

   return false;
}

bool String::startsWith(const String &subStr) const noexcept
{
   if (_strLen < subStr._strLen)
      return false;

   if (strncmp(_str, subStr._str, subStr._strLen) == 0)
      return true;

   return false;
}

int String::compare(const String &cmpStr, const bool ignoreCase) const noexcept
{
   if (ignoreCase)
      return stricmp(_str, cmpStr._str);

   return strcmp(_str, cmpStr._str);
}

int String::_compare(const void *obj) const
{
   return strcmp(_str, ((const String *) obj)->_str);
}

String &String::copyTruncate(const char *str, const unsigned int len) noexcept
{
   if (len >= _bytesUsed)
   {
      strncpy(_str, str, _bytesUsed - 1);
      _str[_bytesUsed - 1] = 0;
      _strLen              = _bytesUsed - 1;
   }
   else
   {
      strcpy(_str, str);
      _strLen = len;
   }

   return *this;
}

String &String::checkLength() noexcept
{
   _strLen = strlen(_str);

   return *this;
}

String &String::padRight(const unsigned int size, const char ch)
{
   if (size <= _strLen)
      return *this;

   String ret;

   ret.setSize(size, ch);

   memcpy(ret._str + (ret._strLen - _strLen), _str, _strLen);

   *this = ret;
   return *this;
}

String &String::padLeft(const unsigned int size, const char ch)
{
   if (size <= _strLen)
      return *this;

   String ret;

   ret.setSize(size, ch);

   memcpy(ret._str, _str, _strLen);

   *this = ret;
   return *this;
}

String &String::padCenter(const unsigned int size, const char ch)
{
   if (size <= _strLen)
      return *this;

   String ret;

   ret.setSize(size, ch);

   memcpy(ret._str + (unsigned) (((ret._strLen) * 0.5) - ((_strLen) * 0.5)), _str, _strLen);

   *this = ret;
   return *this;
}

unsigned int String::split(const String &sep, String::List &dest, const char enclosingChar) const
{
   dest.clear();

   if (*_str == 0)
      return 0;

   if (enclosingChar == 0)
   {
      char *pos = _str;
      char *ptr = _str;

      while (*ptr)
      {
         if (strncmp(ptr, sep._str, sep._strLen) == 0)
         {
            if (ptr > pos)
               dest.append(new String(pos, ptr - pos));
            else
               dest.append(new String());

            ptr += sep._strLen;
            pos  = ptr;
         }
         else
            ptr++;
      }

      dest.append(new String(pos));
   }
   else
   {
      char   *pos       = _str;
      char   *ptr       = _str;
      String *tmpStr    = 0;
      char   *enclosing = 0;
      char    enclosingStr[2];

      enclosingStr[0] = enclosingChar;
      enclosingStr[1] = 0;

      while (*ptr)
      {
         if (!tmpStr)
            tmpStr = dest.append(new String());

         if (*ptr == '\\' && *(ptr + 1) == enclosingChar)
         {
            if (pos != ptr)
               tmpStr->_appendPartialStr(pos, ptr - pos);

            tmpStr->_appendStr(enclosingStr);

            ptr = ptr + 2;
            pos = ptr;

            continue;
         }

         if (*ptr == enclosingChar)
         {
            if (enclosing)
            {
               if (ptr > pos)
                  tmpStr->_appendPartialStr(pos, ptr - pos);

               enclosing = 0;
               ptr++;
               pos = ptr;
            }
            else
            {
               if (ptr > pos)
                  tmpStr->_appendPartialStr(pos, ptr - pos);

               enclosing = ptr;
               ptr++;
               pos = ptr;
            }

            continue;
         }

         if (!enclosing && strncmp(ptr, sep._str, sep._strLen) == 0)
         {
            if (ptr > pos)
               tmpStr->_appendPartialStr(pos, ptr - pos);

            tmpStr  = 0;
            ptr    += sep._strLen;
            pos     = ptr;
         }
         else
            ptr++;
      }

      if (enclosing)
         throw xMettle(__FILE__, __LINE__, MODULE, "Open enclosing character [%c] at position [%u] is missing a closing char.", enclosingChar, enclosing - _str);

      if (ptr > pos)
         tmpStr->_appendPartialStr(pos, ptr - pos);
   }

   return dest.count();
}

unsigned String::splitLines(String::List &dest) const
{
   return split(_LINE_ENDING, dest);
}

void String::_replaceStr(const String &dest, const String &source)
{
   char *to_ptr;

   if ((to_ptr = strstr(_str, dest._str)) == 0)
      return;

   char          *from_ptr;
   char          *mem;
   String         newStr;
   unsigned int   offset;

   from_ptr = _str;

   for (; to_ptr;)
   {
      if (to_ptr - from_ptr > 0)
      {
         offset          = newStr._strLen;
         newStr._strLen += to_ptr - from_ptr;

         if (newStr._strLen >= newStr._bytesUsed)
         {
            newStr._bytesUsed = newStr._strLen + 1;

            if ((mem = (char *) realloc(newStr._str, newStr._bytesUsed)) == 0)
                throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", newStr._bytesUsed);

            newStr._str = mem;
         }

         memcpy(newStr._str + offset, from_ptr, to_ptr - from_ptr);
      }

      offset          = newStr._strLen;
      newStr._strLen += source._strLen;

      if (newStr._strLen >= newStr._bytesUsed)
      {
        newStr._bytesUsed = newStr._strLen + 1;

        if ((mem = (char *) realloc(newStr._str, newStr._bytesUsed)) == 0)
            throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", newStr._bytesUsed);

        newStr._str = mem;
      }

      memcpy(newStr._str + offset, source._str, source._strLen);

      from_ptr  = to_ptr + dest._strLen;
      to_ptr    = strstr(from_ptr, dest._str);
   }

   if (*from_ptr)
   {
      offset = newStr._strLen;

      newStr._strLen += strlen(from_ptr);

      if (newStr._strLen >= newStr._bytesUsed)
      {
         newStr._bytesUsed = newStr._strLen + 1;

         if ((mem = (char *) realloc(newStr._str, newStr._bytesUsed)) == 0)
            throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", newStr._bytesUsed);

         newStr._str = mem;
      }
      strcpy(newStr._str + offset, from_ptr);
   }

   newStr._str[newStr._strLen] = 0;

   _freeStr();

   _str              = newStr._str;
   _strLen           = newStr._strLen;
   _bytesUsed        = newStr._bytesUsed;

   newStr._str       = 0;
   newStr._strLen    = 0;
   newStr._bytesUsed = 0;
}

String &String::setSize(const unsigned length, const char ch)
{
   setSize(length);

   if (ch == 0)
   {
      memset(_str, 0, _strLen + 1);

      return *this;
   }

   _strLen = length;

   memset(_str, ch, _strLen + 1);

   _str[_strLen] = 0;

   return *this;
}

String &String::setSize(const unsigned length, const bool clear)
{
   if (length >= _bytesUsed)
   {
      char *tmp;

      if ((tmp = (char *) realloc(_str, length + 1)) == 0)
         throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Error allocating [%u] bytes.", length + 1);

      _str       = tmp;
      _strLen    = length;
      _bytesUsed = length + 1;
   }

   if (clear)
      memset(_str, 0, _bytesUsed);

   return *this;
}

void String::_stripChars(const bool left, const bool right, const char *chars) noexcept
{
   if (*_str == 0)
      return;

   if (chars == 0 || *chars == 0)
      chars = WHITE_SPACES;

   char       *ptr;
   const char *ch;

   if (left)
   {
      for (ptr = _str; *ptr; ptr++)
      {
         for (ch = chars; *ch; ch++)
         {
            if (*ptr == *ch)
               break;
         }

         if (*ch == 0)
            break;
      }

      if (*ptr == 0)
      {
         *_str   = 0;
         _strLen = 0;
         return;
      }

      if (ptr > _str)
      {
         _strLen -= (ptr - _str);
         memmove(_str, ptr, _strLen + 1);
      }

      if (*_str == 0)
         return;
   }

   if (right)
   {
      ptr = _str + _strLen;

      for (ptr--; ptr >= _str; ptr--)
      {
         for (ch = chars; *ch; ch++)
         {
            if (*ptr == *ch)
               break;
         }

         if (*ch == 0)
            break;
      }

      if (ptr >= _str)
      {
         ptr++;
         _strLen = ptr - _str;
         *ptr = 0;
      }
   }
}

int String::stricmp(const char *a, const char *b) noexcept
{
   int n;

   for (; ;a++, b++)
   {
      n = tolower((unsigned char)*a) - tolower((unsigned char)*b);

      if (n || !*a)
         break;
   }

   return n;
}

int String::strnicmp(const char *a, const char *b, const unsigned n) noexcept
{
   unsigned i, ret;

   for (i = 0; i < n; i++)
   {
      ret = tolower((unsigned char)*a) - tolower((unsigned char)*b);

      if (ret || !*a)
         break;

      a++;
      b++;
   }

   return ret;
}

char *String::safeCopy(char *dest, const char *source, const unsigned destSize) noexcept
{
   strncpy(dest, source, destSize - 1);

   dest[destSize - 1] = 0;

   return dest;
}

const char *String::empty() noexcept
{
   return _EMPTY_STRING;
}

bool String::isWhiteSpace(const char ch) noexcept
{
   for (const char *ws = WHITE_SPACES; *ws; ++ws)
      if (*ws == ch)
         return true;

   return false;
}

String::List::List() noexcept
{
}

String::List::~List() noexcept
{
}

#undef MODULE

#undef MAX_INT_STR_SIZE
#undef BSTRING_TRUE
#undef BSTRING_FALSE
#undef BSTRING_YES
#undef BSTRING_NO
#undef WHITE_SPACES

}}
