/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_LIB_MEMORYBLOCK_H_
#define __METTLE_LIB_MEMORYBLOCK_H_

#include "mettle/lib/collection.h"
#include "mettle/lib/c99standard.h"
#include "mettle/lib/xmettle.h"
#include "mettle/lib/safe.h"

namespace Mettle { namespace Lib {

/** \class MemoryBlock memoryblock.h mettle/lib/memoryblock.h
 * \brief A helper object to allocate and manage a block of memory.
 *
 * Note that when a block of memory is allocated, it is not memset() to null, thats up to you.
 */
class MemoryBlock
{
public:

   DECLARE_SAFE_CLASS(MemoryBlock);
   DECLARE_COLLECTION_LIST_CLASS(MemoryBlock);

   uint8_t      *_block;
   unsigned int  _size;

   /** \brief Default constructor.
    */
   MemoryBlock() noexcept;

   /** \brief Optinal constructor to pre-allocate the memory block to a certain size.
    *  \throws xMettle if out of memory.
    */
   MemoryBlock(const unsigned int size);

   /** \brief Copy constructor
    *  \throws xMettle if out of memory.
    */
   MemoryBlock(const MemoryBlock &cp);

   /** \brief Destructor.
    */
   virtual ~MemoryBlock() noexcept;

   /** \brief Returns if the memory block is empty or not.
    *  \returns true if the memory block is null.
    */
   bool isEmpty() const noexcept;

   /** \brief Clears the block, setting the size to zero.
    */
   void clear() noexcept;

   /** \brief Allocates the memory to the new size, does not alter the contents of the memory block.
     *  \throws xMettle if out of memory.
     */
   void allocate(const unsigned int size);

   /** \brief Gets the pointer to the memory block as an unsigned int 8 pointer
     * \returns the memory block type casted to an unsigned int 8 pointer
     */
   uint8_t *blockU8() const noexcept;

   /** \brief Gets the pointer to the memory block as a signed int 8 pointer
     * \returns the memory block type casted to a signed int 8 pointer
     */
   int8_t *blockS8() const noexcept;

   /** \brief Gets the pointer to the memory block as a void pointer
     * \returns the memory block type casted to a void pointer
     */
   void *block() const noexcept;

   /** \brief Gets the current size of the memory block
     * \returns the size of the memory block in bytes
     */
   unsigned int size() const noexcept;

   /** \brief Operator overload for = to assign a memory block to another.
    *  \param cp the memory block to copy
    *  \returns itself for convenience.
    *  \throws xMettle if out of memory.
    */
   MemoryBlock & operator = (const MemoryBlock &cp);


   /** \brief Completely consumes the source memory block.
    *
    * This object will purge itself and then consume the entire source memory block and then
    * purge the source memory block of its contents.
    *
    * \param me the source memory block instance to consumed.
    */
   void eat(MemoryBlock *me) noexcept;

   /** \brief Dispenses the entire memory block to the \p memory block.
    *
    * This object will dispence all its contents to the
    * destination memory block.
    *
    * \param dest the destination memory block instance that will recieve this memory block's data.
    */
   void poop(MemoryBlock *dest) noexcept;
};

}}

#endif
