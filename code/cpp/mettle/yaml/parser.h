/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_YAML_PARSER_H_
#define __METTLE_YAML_PARSER_H_

#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Yaml {

/** \class Parser parser.h mettle/yaml/parser.h
 *  \brief The yaml parser.
 *
 */
class Parser
{
public:
   /** \brief Constructor.
    */
   Parser();

   /** \brief Destructor.
    */
   ~Parser();

   /** \brief Clears the object for reuse.
    */
   void clear();

   /* \brief Parses the yaml string, returns a DataNod representing the structure.
      \param yaml the null terminated yaml string to read.
      \param yamlLen optional parameter, max length of \p yaml to read, else the entire \p yaml string is read.
      \returns a new DataNode object reprensenting the yaml parsed.  Note the calling code must free this object.
      \throws xMettle exception if the yaml in not valid or out of memory.
   */
   Mettle::Lib::DataNode *parse(const char *yaml, const size_t yamlLen = 0);

   /* \brief Parses the yaml file, returns a DataNod representing the structure.
      \param filename the filename to read.
      \returns a new DataNode object reprensenting the yaml parsed.  Note the calling code must free this object.
      \throws xMettle exception if the yaml in not valid, out of memory or the file does not exist or could not be read.
   */
   Mettle::Lib::DataNode *parseFile(const char *filename);

private:

   void *_yamlParser;

   void parseYaml(Mettle::Lib::DataNode *curr, Mettle::Lib::DataVar::List *list);
};

}}

#endif
