/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/yaml/parser.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/common.h"
#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/filemanager.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/string.h"

#include "yaml.h"

using namespace Mettle::Lib;

namespace Mettle { namespace Yaml {

#define YMLP   ((yaml_parser_t *) _yamlParser)

#define SOURCE "Yaml::Parser"

Parser::Parser()
{
   _yamlParser = 0;
}


Parser::~Parser()
{
   clear();
}


void Parser::clear()
{
   if (_yamlParser)
   {
      yaml_parser_delete(YMLP);
      free(_yamlParser);
       _yamlParser = 0;
   }
}


DataNode *Parser::parseFile(const char *filename)
{
   clear();

   _yamlParser = Common::memoryAlloc(sizeof(yaml_parser_t));

   if (!yaml_parser_initialize(YMLP))
      throw xMettle(__FILE__, __LINE__, SOURCE, "Unexpected error in api call 'yaml_parser_initialize'");

   FileHandle     fh;
   DataNode::Safe root(new DataNode());

   fh.open(filename, FileHandle::Text);

   yaml_parser_set_input_file(YMLP, fh.filePointer());

   parseYaml(root.obj, 0);

   clear();

   return root.forget();
}


DataNode *Parser::parse(const char *yaml, const size_t yamlLen)
{
   clear();

   _yamlParser = Common::memoryAlloc(sizeof(yaml_parser_t));

   if (!yaml_parser_initialize(YMLP))
      throw xMettle(__FILE__, __LINE__, SOURCE, "Unexpected error in api call 'yaml_parser_initialize'");

   yaml_parser_set_input_string(YMLP, (unsigned char *) yaml, yamlLen);

   DataNode::Safe root(new DataNode());

   parseYaml(root.obj, 0);

   clear();

   return root.forget();
}


void Parser::parseYaml(DataNode *curr, DataVar::List *list)
{
   if (curr == 0 && list == 0)
      return;

   yaml_event_t    event;
   bool            streamStarted = false;
   bool            docStarted    = false;

   for (;;)
   {
      memset(&event, 0, sizeof(event));

      if (!yaml_parser_parse(YMLP, &event))
      {
         yaml_event_delete(&event);
         throw xMettle(__FILE__, __LINE__, SOURCE, "An error occured while passing the yaml [errno:%d, problem:%s, line:%d, column:%d]", YMLP->error, YMLP->problem, YMLP->problem_mark.line, YMLP->problem_mark.column);
      }

      if (event.type == YAML_SCALAR_EVENT)
      {
         DataVar *var = 0;

         if (list)
         {
            var = list->append(new DataVar());

            var->write((char *) event.data.scalar.value);
         }
         else
         {
            if (curr->name().isEmpty())
               curr->nameSet((char *) event.data.scalar.value);
            else if (curr->value() == 0 && curr->child() == 0)
               var = curr->valueSet((char *) event.data.scalar.value);
            else
               curr = curr->newSibling((char *) event.data.scalar.value);
         }

         if (var && !event.data.scalar.quoted_implicit)
            var->toBestType();

         yaml_event_delete(&event);
         continue;
      }

      if (event.type == YAML_MAPPING_START_EVENT)
      {
         DataNode *child;

         if (list)
         {
            DataVar  *v = list->append(new DataVar());
            v->valueSetType(DataVar::tl_dataNode);
            yaml_event_delete(&event);

            child = (DataNode *) v->valuePtr();
            child = child->newChild("");

            parseYaml(child, 0);
         }
         else
         {
            child = curr->newChild("");
            yaml_event_delete(&event);
            parseYaml(child, 0);
         }
         continue;
      }

      if (event.type == YAML_MAPPING_END_EVENT)
      {
         yaml_event_delete(&event);
         break;
      }

      if (event.type == YAML_SEQUENCE_START_EVENT)
      {
         if (list)
         {
            DataVar *v = list->append(new DataVar());
            v->valueSetType(DataVar::tl_list);
            yaml_event_delete(&event);
            parseYaml(0, (DataVar::List *) v->valuePtr());
         }
         else
         {
            DataVar *v = curr->valueSet(new DataVar());
            v->valueSetType(DataVar::tl_list);
            yaml_event_delete(&event);
            parseYaml(0, (DataVar::List *) v->valuePtr());
         }

         continue;
      }

      if (event.type == YAML_SEQUENCE_END_EVENT)
      {
         yaml_event_delete(&event);
         break;
      }

      if (event.type == YAML_ALIAS_EVENT)
      {
         yaml_event_delete(&event);
         continue;
      }

      if (event.type == YAML_STREAM_START_EVENT)
      {
         yaml_event_delete(&event);

         if (streamStarted)
            throw xMettle(__FILE__, __LINE__, SOURCE, "Stream start event occurred more than once!");

         streamStarted = true;
         continue;
      }

      if (event.type == YAML_STREAM_END_EVENT)
      {
         yaml_event_delete(&event);

         if (!streamStarted)
            throw xMettle(__FILE__, __LINE__, SOURCE, "Stream end event occurred more than once!");

         streamStarted = false;
         break;
      }

      if (event.type == YAML_DOCUMENT_START_EVENT)
      {
         yaml_event_delete(&event);

         if (docStarted)
            throw xMettle(__FILE__, __LINE__, SOURCE, "Document start event occurred more than once!");

         docStarted = true;
         continue;
      }

      if (event.type == YAML_DOCUMENT_END_EVENT)
      {
         yaml_event_delete(&event);

         if (!docStarted)
            throw xMettle(__FILE__, __LINE__, SOURCE, "Document end event occurred more than once!");

         docStarted = false;
         continue;
      }

      if (event.type != YAML_NO_EVENT)
      {
         int lin = event.start_mark.line;
         int col = event.start_mark.column;
         yaml_event_delete(&event);

         throw xMettle(__FILE__, __LINE__, SOURCE, "An unexpected event [%d] detected during yaml parse (line:%d, column:%d).", event.type, lin, col);
      }

      yaml_event_delete(&event);
   }
}

#undef YAMLP
#undef SOURCE

}}
