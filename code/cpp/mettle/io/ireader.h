/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_IO_IREADER_H_
#define __METTLE_IO_IREADER_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/platform.h"

#include "mettle/lib/datetime.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/safe.h"
#include "mettle/lib/string.h"
#include "mettle/lib/ustring.h"

namespace Mettle { namespace IO {

/** \interface IReader ireader.h mettle/io/ireader.h
 *  \brief Reader interface used for serializing
 *
 * ISerializable uses any implementation of IReader for reading (de-serializing)
 * data streams.
 */
class IReader
{
public:

   /** \brief Declare Safe Class.
   */
   DECLARE_SAFE_CLASS(IReader);

   /** \brief Destructor.
   */
   virtual ~IReader() noexcept;

   /** \brief Implemented method should clear/reset the reader object.
    */
   virtual void  clear() = 0;

   /** \brief The name of the implemented reader
    *
    * This name is usually the same as it's sister Writer implementation
    */
   virtual const char *name()  = 0;

   /** \brief This method is called at the start of a deserialization.
     * \param name the name of the object being deserialized.
     * \return the number of bytes actually read.
    */
   virtual unsigned int readStart(const char *name) = 0;

   /** \brief This method is called at the end when the last data member has been dezerialized.
     * \param name the name of the object that has just completed being deserialized.
     * \return the number of bytes actually read.
    */
   virtual unsigned int readEnd(const char *name)   = 0;

   /** \brief Implemented method to read a boolean.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, bool &v) = 0;

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
   /** \brief Implemented method to read a char.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     = \warning on some version of Microsoft Visual C++ this method is not available because of known bug in the compiler.  If this is the case use the 'int8_t' method instead.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, char &v) = 0;
#endif

   /** \brief Implemented method to read an int8_t.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, int8_t &v) = 0;

   /** \brief Implemented method to read an int16_t.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, int16_t &v) = 0;

   /** \brief Implemented method to read an int32_t.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, int32_t &v) = 0;

   /** \brief Implemented method to read an int64_t.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, int64_t &v) = 0;

   /** \brief Implemented method to read an uint8_t.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, uint8_t &v) = 0;

   /** \brief Implemented method to read an uint16_t.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, uint16_t &v) = 0;

   /** \brief Implemented method to read an uint32_t.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, uint32_t &v) = 0;

   /** \brief Implemented method to read an uint64_t.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, uint64_t &v) = 0;

   /** \brief Implemented method to read a double.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, double &v) = 0;

   /** \brief Implemented method to read a float.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, float &v) = 0;

   /** \brief Implemented method to read a String.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, Mettle::Lib::String &v) = 0;

   /** \brief Implemented method to read a UTF8 String.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, Mettle::Lib::UString &v) = 0;

   /** \brief Implemented method to read a Date & Time.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, Mettle::Lib::DateTime &v) = 0;

   /** \brief Implemented method to read a Date.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, Mettle::Lib::Date &v) = 0;

   /** \brief Implemented method to read a Time.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the value of \p field.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, Mettle::Lib::Time &v) = 0;

   /** \brief Implemented method to read a memory block.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the memory block.
     * \warning Not all readers are able to read memory blocks.  Some may throw exceptions if this method is not possible.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, Mettle::Lib::MemoryBlock &v) = 0;

   /** \brief Implemented method to read a uuid.
     * \param field the name of the field to be read.
     * \param v the variable in which to store the uuid.
     * \return the number of bytes actually read.
    */
   virtual unsigned int read(const char *field, Mettle::Lib::Guid &v) = 0;

protected:

   /** Constructor.
    * \brief Pure virtual class (Interface).
    */
   IReader() noexcept;
};

}}

#endif
