/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/io/stringbuilderwriter.h"

#include "mettle/lib/common.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/stringbuilder.h"

using namespace Mettle::Lib;

namespace Mettle { namespace IO {

#define MODULE "StringBuilderWriter"

StringBuilderWriter::StringBuilderWriter(StringBuilder *sb,
                                         const char    *delim,
                                         const int32_t  tzTarg) noexcept
{
   _sb    = sb;
   _delim = delim;

   DateTime::calcTZHourMin(tzTarg, _tzhour, _tzmin);
}

StringBuilderWriter::~StringBuilderWriter() noexcept
{
}

void StringBuilderWriter::clear()
{
   _sb->clear();
}

const char *StringBuilderWriter::name()
{
   return "Mettle.StringBuilderWriter";
}

unsigned int StringBuilderWriter::writeStart(const char *name)
{
   return 0;
}

unsigned int StringBuilderWriter::writeEnd(const char *name)
{
   return 0;
}

unsigned int StringBuilderWriter::write(const char *field, const bool &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::_writeStr(const char *v)
{
   if (!_sb->isEmpty())
      _sb->add(_delim);

   _sb->add(v);

   return 0;
}

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
unsigned int StringBuilderWriter::write(const char *field, const char &v)
{
   return _writeStr(String(v).c_str());
}
#endif

unsigned int StringBuilderWriter::write(const char *field, const int8_t &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const int16_t &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const int32_t &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const int64_t &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const uint8_t &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const uint16_t &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const uint32_t &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const uint64_t &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const double &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const float &v)
{
   return _writeStr(String(v).c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const String &v)
{
   return _writeStr(v.c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const UString &v)
{
   return _writeStr(v.c_str());
}

unsigned int StringBuilderWriter::write(const char *field, const DateTime &v)
{
   char jotter[24];

   if (_tzhour != INT8_MIN)
   {
      DateTime t(v);

      t.tzConverTo(_tzhour, _tzmin);

      return _writeStr(t.toString(jotter, true));
   }

   return _writeStr(v.toString(jotter, true));
}

unsigned int StringBuilderWriter::write(const char *field, const Date &v)
{
   char jotter[16];

   return _writeStr(v.toString(jotter));
}

unsigned int StringBuilderWriter::write(const char *field, const Time &v)
{
   char jotter[16];

   return _writeStr(v.toString(jotter));
}

unsigned int StringBuilderWriter::write(const char *field, const Mettle::Lib::MemoryBlock &v)
{
   // NOTE, todo, base64 encode the memory block.
   throw xMettle(__FILE__, __LINE__, MODULE, "MemoryBlock not supported.");
}

unsigned int StringBuilderWriter::write(const char *field, const Mettle::Lib::Guid &v)
{
   if (v.isNull())
      return _writeStr(0);

   char struuid[38];

   v.toString(struuid);

   return _writeStr(struuid);
}


#undef MODULE

}}
