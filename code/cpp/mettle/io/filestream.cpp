/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/io/filestream.h"

#include "mettle/io/memorystream.h"

#include "mettle/lib/filehandle.h"
#include "mettle/lib/filemanager.h"

#include <stdio.h>

using namespace Mettle::Lib;

namespace Mettle { namespace IO {

FileStream::FileStream(FileHandle *fh) noexcept
{
   _fh       = fh;
   _fileName = fh->fileName();
}


FileStream::~FileStream() noexcept
{
}


void FileStream::clear()
{
   _fh->close();

   FileManager::removeFile(_fileName.c_str());

   _fh->open(_fileName.c_str(), FileHandle::Binary);
}


void FileStream::purge()
{
   clear();
}


void FileStream::write(const void *p, const unsigned int size)
{
   _fh->write(p, size);
}


void FileStream::read(void *p, const unsigned int size)
{
   _fh->read(p, size);
}

unsigned int FileStream::size() const
{
   long int     currPos  = _fh->tell();
   unsigned int size     = _fh->getLength();

   _fh->seekOffset(currPos);

   return size;
}


void FileStream::eat(MemoryStream *me)
{
   _fh->close();

   FileManager::removeFile(_fileName.c_str());

   _fh->create(_fileName.c_str(), FileHandle::Binary);

   _fh->write(me->_memory, me->_sizeUsed);

   me->purge();
}

void FileStream::poop(      MemoryStream *dest,
                      const unsigned int  offset)
{
   unsigned int size = (unsigned int) _fh->getLength();

   dest->clear();
   dest->addMemory(size);

   _fh->read(dest->_memory, size);
}

void FileStream::positionStart() noexcept
{
   _fh->seekStart();
}


void FileStream::positionEnd() noexcept
{
   _fh->seekEnd();
}


void FileStream::positionMove(const unsigned int offset)
{
   _fh->seekOffset(offset);
}

unsigned int FileStream::position() const noexcept
{
   return _fh->tell();
}

}}
