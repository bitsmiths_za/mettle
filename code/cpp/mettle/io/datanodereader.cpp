/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/io/datanodereader.h"

#include "mettle/lib/common.h"
#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/xmettle.h"

using namespace Mettle::Lib;

namespace Mettle { namespace IO {

#define MODULE "DataNodeReader"

DataNodeReader::DataNodeReader(DataNode *root) noexcept
{
   _node      = 0;
   _root      = root;
   _firstRead = true;
}

DataNodeReader::~DataNodeReader() noexcept
{
}


void DataNodeReader::clear()
{
   _node      = 0;
   _firstRead = true;
}

const char *DataNodeReader::name()
{
   return "Mettle.DataNodeReader";
}

Mettle::Lib::DataVar *DataNodeReader::_getField(const char *name)
{
   if (!_node)
      throw xMettle(__FILE__, __LINE__, MODULE, "Initial node not setup!", name);

   DataNode *tmp = _node->get(name);

   if (tmp == 0)
      throw xMettle(__FILE__, __LINE__, MODULE, "Expected field/child [%s] not found.", name);

   DataVar *var = tmp->value();

   if (!var)
      throw xMettle(__FILE__, __LINE__, MODULE, "Expected field/child [%s] has no value.", name);

   return var;
}

unsigned int DataNodeReader::readStart(const char *name)
{
   if (!_node)
   {
      if (!_root)
         throw xMettle(__FILE__, __LINE__, MODULE, "Expected (%s) and found not root node", name);

      _node = _root;

      return 0;
   }

   _node = _node->get(name);

   return 0;
}

unsigned int DataNodeReader::readEnd(const char *name)
{
   if (_node->parent())
      _node = _node->parent();
}

unsigned int DataNodeReader::read(const char *field, bool &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}


#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
unsigned int DataNodeReader::read(const char *field, char &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}
#endif

unsigned int DataNodeReader::read(const char *field, int8_t &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, int16_t &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, int32_t &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, int64_t &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, uint8_t &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, uint16_t &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, uint32_t &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, uint64_t &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, double &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, float &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, String &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, UString &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, DateTime &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, Date &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, Time &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, Mettle::Lib::MemoryBlock &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   // TODO val->read(v);

   return 0;
}

unsigned int DataNodeReader::read(const char *field, Mettle::Lib::Guid &v)
{
   Mettle::Lib::DataVar *val = _getField(field);

   val->read(v);

   return 0;
}


#undef MODULE

}}
