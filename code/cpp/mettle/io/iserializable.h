/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_IO_ISERIALIZABLE_H_
#define __METTLE_IO_ISERIALIZABLE_H_

namespace Mettle { namespace IO {

class IReader;
class IWriter;

/** \interface ISerializable iserializable.h mettle/io/iserializable.h
 *  \brief An interface to allow object to become serializable.
 *
 *  Any object that has the need to serialized can inherit from ISerializable
 *  and implement the three required interface methods.
 */
class ISerializable
{
 public:

   /** destructor.
    */
   virtual ~ISerializable() noexcept;


   /** \brief Inherited object must use this method to read itself using IReader
    *
    * \param r an implementation of the IReader interface.
    * \param name the name the inherited object.
    * \return the total number of bytes read.
    */
   virtual unsigned int _deserialize(IReader *r, const char *name)       = 0;

   /** \brief Inherited object must use this method to write itself using IWriter
    *
    * \param w an implementation of the IWriter interface.
    * \param name the name the inherited object.
    * \return the total number of bytes written.
    */
   virtual unsigned int _serialize(IWriter *w,   const char *name) const = 0;

   /** \brief Name of the inherited object,
    *
    * \return a null terminated string that is the name of the inherited object.
    */
   virtual const char  *_name()   const = 0;

protected:

   /** constructor.
    * \brief Pure virtual class (Interface).
    */
   ISerializable() noexcept;
};


}}

#endif
