/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_IO_ISTREAM_H_
#define __METTLE_IO_ISTREAM_H_

#include "mettle/lib/xmettle.h"

namespace Mettle { namespace IO {

class MemoryStream;

/** \interface IStream istream.h mettle/io/istream.h
 *  \brief Stream interface to allow for data stream manipulation.
 */
class IStream
{
public:

   /**
   */
   virtual ~IStream() noexcept;

   /** \brief Clears the stream for re-use, but does not necissarily free any resources.
    *
    * The stream position is set back to the start of the stream.
    *
    * \throw xMettle Implemented class should throw a derived exception of this type on error.
    */
   virtual void clear() = 0;

   /** \brief Releases all resources that the stream holds.
    * \throw xMettle Implemented class should throw a derived exception of this type on error.
    */
   virtual void purge() = 0;

   /** \brief Implemented method write for reading from the stream.
    *
    * The implemented method will read stream into \p p to the size of \p size.
    * \param p the pointer to be filled.
    * \param size the total size to be read into \p p.
    * \throw xMettle Implemented class should throw a derived exception of this type on error.
    */
   virtual void read(      void         *p,
                     const unsigned int  size) = 0;

   /** \brief Implemented method for writting to the stream.
    *
    * The implemented method will write contents of \p p to the size of \p size.
    * \param p the pointer to be written.
    * \param size the total size of \p p to be written.
    * \throw xMettle Implemented class should throw a derived exception of this type on error.
    */
   virtual void write(const void         *p,
                      const unsigned int  size) = 0;


   /** \brief Returns the size of the stream in bytes
    *
    * \returns size of the stream in bytes.
    */
   virtual unsigned int size() const = 0;

   /** \brief Completely consumes the source stream.
    *
    * This object will purge itself and then consume the entire source stream and then
    * purge the source stream of its contents.
    *
    * \param me the source stream instance to consumed.
    * \throw xMettle Implemented class should throw a derived exception of this type on error.
    */
   virtual void eat(MemoryStream *me) = 0;

   /** \brief Dispense some of the stream into the memory stream.
    *
    * This object will dispence a portion of its contents to the
    * destination streem.
    *
    * \param dest the destination stream instance that will recieve the dispense stream portion
    * \param offset the offset to start dispensing from.
    * \throw xMettle Implemented class should throw a derived exception of this type on error.
    */
   virtual void poop(      MemoryStream *dest,
                     const unsigned int  offset) = 0;


   /** \brief Moves the internal stream position back to the start of the stream.
    */
   virtual void positionStart() noexcept = 0;

   /** \brief Moves the internal stream position back to the end of the stream.
    */
   virtual void positionEnd() noexcept = 0;

   /** \brief Moves the internal stream position forward in bytes.
    * \param offset the number of bytes to move forward from the current position.
    * \throw xMettle Implemented class should throw a derived exception of this type on error.
    */
   virtual void positionMove(const unsigned int offset) = 0;

   /** \brief The current byte (offset) of the stream position
    * \returns bytes offset of the stream position
    */
   virtual unsigned int position() const noexcept = 0;


protected:

   /** \brief Pure virtual class (Interface).
    */
   IStream() noexcept;

};

}}

#endif
