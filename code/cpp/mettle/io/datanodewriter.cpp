/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/io/datanodewriter.h"

#include "mettle/lib/common.h"
#include "mettle/lib/datanode.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/xmettle.h"

using namespace Mettle::Lib;

namespace Mettle { namespace IO {

#define MODULE "DataNodeWriter"

DataNodeWriter::DataNodeWriter(DataNode *root) noexcept
{
   _node      = 0;
   _root      = root;
}

DataNodeWriter::~DataNodeWriter() noexcept
{
}

void DataNodeWriter::clear()
{
   _root->purge();
   _node = 0;
}

const char *DataNodeWriter::name()
{
   return "Mettle.DataNodeWriter";
}

unsigned int DataNodeWriter::writeStart(const char *name)
{
   if (!_node)
   {
      _node = _root;
      _node->nameSet(name);
   }
   else
   {
      _node = _node->newChild(name);
   }

   return 0;
}

unsigned int DataNodeWriter::writeEnd(const char *name)
{
   _node = _node->parent();
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const bool &v)
{
   _node->write(field, v);
   return 0;
}

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
unsigned int DataNodeWriter::write(const char *field, const char &v)
{
   _node->write(field, v);
   return 0;
}
#endif

unsigned int DataNodeWriter::write(const char *field, const int8_t &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const int16_t &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const int32_t &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const int64_t &v)
{
   _node->write(field, (int) v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const uint8_t &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const uint16_t &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const uint32_t &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const uint64_t &v)
{
   _node->write(field, (unsigned int) v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const double &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const float &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const String &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const UString &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const DateTime &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const Date &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const Time &v)
{
   _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const Mettle::Lib::MemoryBlock &v)
{
   // TODO _node->write(field, v);
   return 0;
}

unsigned int DataNodeWriter::write(const char *field, const Mettle::Lib::Guid &v)
{
   _node->write(field, v);
   return 0;
}

#undef MODULE

}}
