/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_IO_MEMORYSTREAM_H_
#define __METTLE_IO_MEMORYSTREAM_H_

#include "mettle/io/istream.h"

#include "mettle/lib/c99standard.h"

namespace Mettle { namespace IO {

/** \class MemoryStream memorystream.h mettle/io/memorystream.h
 *  \brief Basic memory stream object
 *
 *  This basic memory stream object leaves many of its member variables
 *  public for developer convenience.  Please respect the members and
 *  consider them read-only.
 */
class MemoryStream : public IStream
{
public:

   /** constructor.
    * \brief Default constructor
    */
   MemoryStream() noexcept;

   /** constructor.
    * \brief Convenience constructor.
    *
    * This constructor attempts to read \p mem durying the construction.  Note that this
    * constructor will could throw an exception.
    */
   MemoryStream(const void          *mem,
                const unsigned int   sizeUsed);

   /** destructor.
    * \brief The streams memory is purged at this time.
    */
   virtual ~MemoryStream() noexcept;

   /** \brief Implements interface method.
    */
   void clear();

   /** \brief Implements interface method.
    */
   void purge();

   /** \brief Implements interface method.
    */
   void read(      void         *p,
             const unsigned int  size);

   /** \brief Implements interface method.
    */
   void write(const void         *p,
              const unsigned int  size);

   /** \brief Implements interface method.
    */
   unsigned int size() const;

   /** \brief Implements interface method.
    */
   void eat(MemoryStream *me);

   /** \brief Additional eat to consume blobs.
    */
   void eat(unsigned int size, void *me);

   /** \brief Implements interface method.
    */
   void poop(      MemoryStream *dest,
             const unsigned int  offset);

   /** \brief Implements interface method.
    */
   void positionStart() noexcept;

   /** \brief Implements interface method.
    */
   void positionEnd() noexcept;

   /** \brief Implements interface method.
    */
   void positionMove(const unsigned int offset);

   /** \brief Implements interface method.
    */
   unsigned int position() const noexcept;

   /** \brief Adds more memory to the stream.
    *
    *  \param memSize the number of bytes to add to the stream.
    *  \returns the new pointer to the allocated memory for convenience.
    *  \throw xMettle usually an out of memory exception.
    */
   void *addMemory(const unsigned int memSize);


   /** \brief The memory pointer
    */
   uint8_t          *_memory;

   /** \brief The current size of the memory pointer
    */
   unsigned int      _sizeUsed;

protected:

   /** \brief Internal method to allocate more memory for the stream.
    */
   void              *_allocateMemory(const unsigned int memSize);

   /** \brief The amount of left over space still allocated.
    */
   unsigned int      _sizeAllocated;

   /** \brief The current position of the stream.
    */
   uint8_t          *_offset;
};

}}

#endif
