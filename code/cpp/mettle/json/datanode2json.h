/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_JSON_DATANODE2TEXT_H_
#define __METTLE_JSON_DATANODE2TEXT_H_

#include "mettle/lib/xmettle.h"

#include <stdio.h>

namespace Mettle {

namespace Lib
{
   class DataNode;
   class StringBuilder;
}

namespace Json {

/** \class TextWriter textwriter.h mettle/json/parser.h
 *  \brief The json parser.
 *
 */
class DataNode2Json
{
public:
   /** \brief Constructor.
     * \param the DataNode to be turned into json.
     */
   DataNode2Json(Mettle::Lib::DataNode *node);

   /** \brief Destructor.
    */
   ~DataNode2Json();

   /** \brief Print the datanode out to a string.
    *  \param dest the StringBuilder to receive the json text.
    *  \throws xMettle if out of memory or if the DataNode had errors.
    */
   void print(Mettle::Lib::StringBuilder *dest);

   /** \brief Print the datanode out to a file handle.
    *  \param dest the File handle to receive the json text.
    *  \throws xMettle if out of disk space or the DataNode had errors.
    */
   void print(FILE *dest);

private:

   Mettle::Lib::DataNode *_node;

   void toString(FILE *destFH, Mettle::Lib::StringBuilder *destSB);

   void writef(FILE *fp, const char *fmt, ...);
};

}}

#endif
