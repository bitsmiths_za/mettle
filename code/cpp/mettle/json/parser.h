/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_JSON_PARSER_H_
#define __METTLE_JSON_PARSER_H_

#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/stringbuilder.h"
#include "mettle/lib/xmettle.h"

namespace Mettle { namespace Json {

/** \class Parser parser.h mettle/json/parser.h
 *  \brief The json parser.
 *
 */
class Parser
{
public:
   /** \brief Constructor.
    */
   Parser();

   /** \brief Destructor.
    */
   ~Parser();

   /** \brief Clears the object for reuse.
    */
   void clear();

   /* \brief Parses the json string, returns a DataNod representing the structure.
      \param json the null terminated json string to read.
      \param jsonLen optional parameter, max length of \p json to read, else the entire \p json string is read.
      \returns a new DataNode object reprensenting the json parsed.  Note the calling code must free this object.
      \throws xMettle exception if the json in not valid or out of memory.
   */
   Mettle::Lib::DataNode *parse(const char *json, const size_t jsonLen = 0);

   /* \brief Parses the json file, returns a DataNode representing the structure.
      \param filename the filename to read.
      \returns a new DataNode object reprensenting the json parsed.  Note the calling code must free this object.
      \throws xMettle exception if the json in not valid, out of memory or the file does not exist or could not be read.
   */
   Mettle::Lib::DataNode *parseFile(const char *filename);

private:

   void parseTags(Mettle::Lib::DataNode *node, int &idx);

   void parseArrTag(Mettle::Lib::DataVar::List *arr, int &idx);

   char *readToken(const void *token);

   Mettle::Lib::StringBuilder   *_sb;
   int                           _totTokens;
   void                         *_tokens;
   char                         *_json;
};

}}

#endif
