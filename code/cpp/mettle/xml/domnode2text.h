/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_XML_DOMNODE2TEXT_H_
#define __METTLE_XML_DOMNODE2TEXT_H_

#include "mettle/xml/domnode.h"

#include <stdio.h>

namespace Mettle {

namespace Lib
{
   class StringBuilder;
}

namespace Xml {

/** \class DomNode2Text domnode2text.h mettle/xml/domnode2text.h
 *  \brief A helper class to print a DomNode to text either as a string or to a file..
 */
class DomNode2Text
{
public:

   /** \brief Constructor.
    *  \param node the DomNode to be converted to text
    *  \param raw print xml in raw format, so it can be consumed agains by another xml parser.
    */
   DomNode2Text(      DomNode *node,
                const bool     raw);

   /** \brief Destructor.
    */
   virtual ~DomNode2Text();


   /** \brief Print the text out to a string.
    *  \param dest the StringBuilder to receive the xml text.
    */
   void print(Mettle::Lib::StringBuilder *dest);

   /** \brief Print the text out to a file handle.
    *  \param dest the File handle to receive the xml text.
    */
   void print(FILE *dest);

private:

   DomNode                     *_node;
   DomNode                     *_curr;
   bool                         _raw;
   unsigned int                 _tab;
   FILE                        *_destFile;
   Mettle::Lib::StringBuilder  *_destStr;
   Mettle::Lib::StringBuilder  *_jotter;

   const char *_getRaw(const char *val);

   void _print();

   void _startElement();

   bool _closeEmptyElement();

   bool _nextElement();

   bool _writeElement();

   void _closeElement();

   void _openChildNode();
};

}}

#endif
