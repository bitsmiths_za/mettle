/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_XML_SAXNODE_H_
#define __METTLE_XML_SAXNODE_H_

#include "mettle/lib/string.h"

#include "mettle/xml/ixmlnode.h"

namespace Mettle { namespace Xml {

class Parser;

/** \class SaxNode saxnode.h mettle/xml/saxnode.h
 *  \brief An abstract sax node object.
 *
 * This object must have it's SAXEvent method overloaded.  All event information will be
 * send to this method when a file, or xml string is read.
 */
class SaxNode : public IXmlNode
{
public:
   /** \brief Desturctor.
    */
   ~SaxNode();

   /** \brief Loads and parses xml from the specified file.
    *  \param root the node to use to parse the file
    *  \param fileName the name of the file to parse
    */
   static void loadFile(SaxNode *root, const char *fileName);

   /** \brief Parses xml from the specified xml string
    *  \param root the node to use to parse the file
    *  \param xml a null termianted xml string
    */
   static void load(SaxNode *root, const char *xml);

   /** \brief Event enum.
    *
    * Events that are catered for when passing through an xml file.
    */
   enum Event
   {
      DocumentStart,     /**< A new xml document has started */
      ElementStart,      /**< A new xml element has started */
      ElementAttribute,  /**< A new attribute added */
      ElementText,       /**< A text value is read from the file */
      ElementComplete,   /**< An element with all text and attributes is completed. */
      ElementEnd,        /**< An element has ended. */
      DocumentEnd        /**< The xml document has ended */
   };


   /** \brief The pure virtual method that receives the SAX events.
    *
    * This method will receieve all of the Xml SAX events as they occur
    * while the xml is beign parsed.
    *
    * \param event the Sax Event that has occured.
    * \name the name of the document/element associated with the event.
    * \value the value of the document/element sociated with the event, an empty string in not applicable.
    */
   virtual void saxEvent(const Event  event,
                         const char  *name,
                         const char  *value) = 0;


   /** \brief Implements interface method.
    */
   IXmlNode *newSibling(const char *name);

   /** \brief Implements interface method.
    */
   IXmlNode *newChild(const char *name);

   /** \brief Implements interface method.
    */
   XmlTag *newAttribute(const char *name,
                        const char *value);

   /** \brief Implements interface method.
    */
   void setName(const char *name, const char *value = 0);

   /** \brief Implements interface method.
    */
   void setValue(const char *value);

   /** \brief Implements interface method.
    */
   const char *name() const;

   /** \brief Implements interface method.
    */
   const char  *value() const;

   /** \brief Implements interface method.
    */
   bool isSAX() const;

   /** \brief Implements interface method.
    */
   bool isDOM() const;

   friend class Parser;

protected:

   /** \brief Protected constructor insures abstraction.
    */
   SaxNode();

private:

   Mettle::Lib::String _name;
   Mettle::Lib::String _value;
};


}}

#endif
