/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/xml/parser.h"

#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/stringbuilder.h"
#include "mettle/lib/string.h"
#include "mettle/lib/uchar.h"

#include "mettle/xml/domnode.h"
#include "mettle/xml/ixmlnode.h"
#include "mettle/xml/saxnode.h"
#include "mettle/xml/xxml.h"

using namespace Mettle::Lib;

namespace Mettle { namespace Xml {

#define XC_OPEN                 1
#define XC_CLOSE                2
#define XC_FINISHED             3


Parser::Parser()
{
   _jotter    = new StringBuilder();
   _attrIdent = new StringBuilder();
}

Parser::~Parser()
{
   _DELETE(_attrIdent)
   _DELETE(_jotter)
}

void Parser::parse(const char *xmlStr, DomNode *root)
{
   int rc;

   _root   =
   _node   = root;
   _xmlStr = xmlStr;
   _pos    = (char *) _xmlStr;

   _jotter->clear();

   if (openClose(true) != XC_OPEN)
      throw xXml(__FILE__, __LINE__, "Xml Parse: could not find a root tag.");

   for (;;)
   {
      if ((rc = openClose(false)) == XC_FINISHED)
         break;

      if (rc == XC_CLOSE)
      {
         if (_node == _root)
            break;

         _node = ((DomNode *) _node)->parent;
      }
      else if (rc == XC_OPEN)
      {
         if (readAttributes() == XC_CLOSE)
            _node = ((DomNode *)_node)->parent;
         else
            readValue();
      }
   }

   if (_node != _root)
      throw xXml(__FILE__, __LINE__, "ParserDom - Final close tag '%s' does not match root tag '%s'.", _node->name(), root->name());
}


void Parser::parse(const char *xmlStr, SaxNode *worker)
{
   int rc;

   _root   =
   _node   = worker;
   _xmlStr = xmlStr;
   _pos    = (char *) _xmlStr;

   _jotter->clear();

   if (openClose(true) != XC_OPEN)
      throw xXml(__FILE__, __LINE__, "Xml Parse: could not find a root tag.");

   for (;;)
   {
      if ((rc = openClose(false)) == XC_FINISHED)
      {
         worker->_name = _jotter->value;
         worker->_value.clear();
         worker->saxEvent(SaxNode::DocumentEnd, worker->name(), worker->value());
         break;
      }

      if (rc == XC_CLOSE)
      {
         worker->_name = _jotter->value;
         worker->_value.clear();
         worker->saxEvent(SaxNode::ElementEnd, worker->name(), worker->value());

         worker->_name.clear();
         worker->_value.clear();
      }
      else if (rc == XC_OPEN)
      {
         if (readAttributes() != XC_CLOSE)
         {
            readValue();
            worker->saxEvent(SaxNode::ElementComplete, worker->name(), worker->value());
         }
      }
   }
}


void Parser::parse(const DataNode *source, DomNode *dest)
{
   dest->destroy();

   DomNode  *xnode = dest;
   DataNode *dnode = (DataNode *) source;
   String    jot;

   if (!source->valueIsEmpty())
      source->value()->read(jot);

   xnode->setName(dnode->name(), jot.c_str());

   do
   {
      if (dnode->child())
      {
         dnode = dnode->child();

         xnode = (DomNode*) xnode->newChild(dnode->name());

         if (!dnode->valueIsEmpty())
         {
            jot.clear();
            dnode->value()->read(jot);
            xnode->setValue(jot.c_str());
         }

         continue;
      }

      if (dnode->sibling())
      {
         dnode = dnode->sibling();

         xnode = (DomNode*) xnode->newSibling(dnode->name());

         if (!dnode->valueIsEmpty())
         {
            jot.clear();
            dnode->value()->read(jot);
            xnode->setValue(jot.c_str());
         }

         continue;
      }

      while (dnode->parent())
      {
         if (dnode->parent()->sibling())
         {
            dnode = dnode->parent()->sibling();
            xnode = xnode->parent;

            xnode = (DomNode*) xnode->newSibling(dnode->name());

            if (!dnode->valueIsEmpty())
            {
               jot.clear();
               dnode->value()->read(jot);
               xnode->setValue(jot.c_str());
            }

            break;
         }

         dnode = dnode->parent();
         xnode = xnode->parent;
      }

   } while (dnode->parent());
}


int Parser::openClose(const bool openRoot)
{
   if (_pos == 0 || *_pos == 0)
      return XC_FINISHED;

   for (; *_pos; UChar::u8_inc(_pos))
   {
      if (!UChar::u8_is_ascii(_pos))
         continue;

      if (*_pos == '<')
      {
         _pos++;

         // skip comments
         if (!UChar::u8_is_ascii(_pos))
         {
            if (_pos[0] == '!' && _pos[1] == '-' && _pos[2] == '-')
               for (_pos += 3; *_pos; UChar::u8_inc(_pos))
               {
                  if (!UChar::u8_is_ascii(_pos))
                     continue;

                  if (_pos[0] == '-' && _pos[1] == '-' && _pos[2] == '>')
                  {
                     _pos += 3;
                     break;
                  }
               }

            continue;
         }

         // skip infomation
         if (*_pos == '?')
         {
            for (_pos++; *_pos; UChar::u8_inc(_pos))
            {
               if (!UChar::u8_is_ascii(_pos))
                  continue;

               if (_pos[0] == '?' && _pos[1] == '>')
               {
                  _pos += 2;
                  break;
               }
            }

            continue;
         }

         // check close tag
         if (*_pos == '/')
         {
            _pos++;

            readTag(false);

            if (_node->isDOM() && strcmp(_jotter->value, _node->name()) != 0)
                throw xXml(__FILE__, __LINE__, "Xml: close: tag '%s' does not match open tag '%s'.", _jotter->value, _node->name());

            return XC_CLOSE;
         }

         // check open tag
         readTag(true);

         if (openRoot)
            _node->setName(_jotter->value);
         else
            _node = _node->newChild(_jotter->value);

         return XC_OPEN;
      }
   }

   return XC_FINISHED;
}

void Parser::readTag(const bool openTag)
{
   char *orig = _pos;

   for (; *_pos; UChar::u8_inc(_pos))
   {
      if (!UChar::u8_is_ascii(_pos))
         continue;

      if (openTag)
      {
         if (whiteSpace(*_pos) || *_pos == '>' || *_pos == '/')
            break;
      }
      else if (*_pos == '>')
         break;
   }

   if (_pos <= orig)
      throw xXml(__FILE__, __LINE__, "readTag - Invalid xml tag - end pos <= start pos (node:%s)", _node->name());

   _jotter->clear().add(orig, _pos - orig);
}

bool Parser::whiteSpace(const char ch)
{
   if (ch == ' '  ||
       ch == '\n' ||
       ch == '\r' ||
       ch == '\t' ||
       ch == '\f' ||
       ch == '\v')
      return true;

   return false;
}

void Parser::readValue()
{
   for (; *_pos; UChar::u8_inc(_pos))
   {
      if (!UChar::u8_is_ascii(_pos))
         continue;

      if (whiteSpace(*_pos))
         continue;

      if (_pos[0] == '<' && _pos[1] == '!' && _pos[2] != '-' )
         break;

      if (*_pos == '<' || *_pos == '>')
         return;

      break;
   }

   _jotter->clear();

   while (*_pos)
   {
      while (!UChar::u8_is_ascii(_pos))
         _jotter->add(*_pos++);

      if (_pos[0] == '<' && _pos[1] == '!')
      {
         if (!readCData())
            throw xXml(__FILE__, __LINE__, "Xml readValue: Invalid CDATA construct (node:%s).", _node->name());

         continue;
      }

      if (*_pos == '<' || *_pos == '>')
      {
         _node->setValue(_jotter->value);
         return;
      }

      if (*_pos == '&' && readSpecial())
         continue;

      _jotter->add(*_pos++);
   }

   throw xXml(__FILE__, __LINE__, "Xml readValue: Could not read value (node:%s) - unexpected end of file.", _node->name());
}

bool Parser::readSpecial()
{
   if (strncmp(_pos, "&gt;", 4) == 0)
   {
      _jotter->add('>');
      _pos += 4;
      return true;
   }

   if (strncmp(_pos, "&lt;", 4) == 0)
   {
      _jotter->add('<');
      _pos += 4;
      return true;
   }

   if (strncmp(_pos, "&amp;", 5) == 0)
   {
      _jotter->add('&');
      _pos += 5;
      return true;
   }

   if (strncmp(_pos, "&apos;", 6) == 0)
   {
      _jotter->add('\'');
      _pos += 6;
      return true;
   }

   if (strncmp(_pos, "&quot;", 6) == 0)
   {
      _jotter->add('"');
      _pos += 6;
      return true;
   }

   return false;
}


bool Parser::readCData()
{
   if (strncmp(_pos + 2, "[CDATA[", 7) != 0)
      return false;

   _pos += 9;

   while (*_pos)
   {
      while (*_pos && !UChar::u8_is_ascii(_pos))
         _jotter->add(*_pos++);

      if (_pos[0] == ']' && _pos[1] == ']' && _pos[2] == '>')
      {
         _pos += 3;
         return true;
      }

      _jotter->add(*_pos++);
   }

   return false;
}


void Parser::readAttr()
{
   for (; *_pos; UChar::u8_inc(_pos))
   {
      if (!UChar::u8_is_ascii(_pos))
         break;

      if (whiteSpace(*_pos))
         continue;

      break;
   }

   char *begin = _pos;


   for (; *_pos; UChar::u8_inc(_pos))
   {
      if (!UChar::u8_is_ascii(_pos))
         continue;

      if (whiteSpace(*_pos) || *_pos == '<' || *_pos == '>' || *_pos == '/' || *_pos == '=')
      {
         _attrIdent->clear().add(begin, _pos - begin);
         return;
      }
   }

   throw xXml(__FILE__, __LINE__, "Xml readAttribute: Could not read xml attribute (node:%s) - unexpected end of file", _node->name());
}


void Parser::readAttrValue()
{
   char id;

   _jotter->clear();

   for (; *_pos; UChar::u8_inc(_pos))
   {
      if (!UChar::u8_is_ascii(_pos))
         continue;

      if (*_pos == '"')
      {
         id = '"';
         break;
      }
      else if (*_pos == '\'')
      {
         id = '\'';
         break;
      }
   }

   if (!*_pos)
      throw xXml(__FILE__, __LINE__, "Xml Read Attr Value: Could not read xml attribute value (node:%s,attr:%s) - unexpected end of file.", _node->name(), _attrIdent->value);

   _pos++;

   while (*_pos)
   {
      while (!UChar::u8_is_ascii(_pos))
         _jotter->add(*_pos++);

      if (*_pos == id)
         return;

      if (*_pos == '&' && readSpecial())
         continue;

      _jotter->add(*_pos++);
   }

   throw xXml(__FILE__, __LINE__, "Xml Read Attr Value: Could not read value (node:%s,attr:%s) - unexpected end of file.", _node->name(), _attrIdent->value);
}


void Parser::findEquals()
{
   for (; *_pos; UChar::u8_inc(_pos))
   {
      if (!UChar::u8_is_ascii(_pos))
         continue;

      if (*_pos == '=')
      {
         _pos++;
         return;
      }
   }

   throw xXml(__FILE__, __LINE__, "Xml Read Attribute: Could not find equals (node:%s), unexpected end of file.", _node->name());
}


int Parser::readAttributes()
{
   for (; *_pos; UChar::u8_inc(_pos))
   {
      if (UChar::u8_is_ascii(_pos))
      {
         if (_pos[0] == '/' && _pos[1] == '>')
         {
            _pos += 2;
            return XC_CLOSE;
         }

         if (*_pos == '>')
         {
            _pos++;
            return XC_FINISHED;
         }

         if (whiteSpace(*_pos))
            continue;
      }

      readAttr();

      findEquals();

      readAttrValue();

      _node->newAttribute(_attrIdent->value, _jotter->value);
   }

   throw xXml(__FILE__, __LINE__, "Xml Read Attributes: Could not read attributes (node:%s) - unexpected end of file.", _node->name());
}

#undef XC_OPEN
#undef XC_CLOSE
#undef XC_FINISHED

}}
