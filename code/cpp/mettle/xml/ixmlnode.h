/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_XML_IXMLODE_H_
#define __METTLE_XML_IXMLODE_H_

namespace Mettle { namespace Xml {

class XmlTag;

/** \interface IXmlNode ixmlnode.h mettle/io/ixmlnode.h
 *  \brief IXmlNode interface.
 *
 * Represents an xml tag, with its name, value and all attributes
 * data streams.
 */
class IXmlNode
{
public:

   /** \brief Destructor.
   */
   virtual ~IXmlNode();

   /** \brief Adds an new xmlnode adjacent to this.
    *  \param name the name of the sibling node.
    *  \returns the new XmlNode object created.
   */
   virtual IXmlNode *newSibling(const char *name) = 0;

   /** \brief Adds a new xmlnode as a child of this node.
    *  \param name the name of the new child node.
    *  \returns the new child XmlNode object created.
   */
   virtual IXmlNode *newChild(const char *name) = 0;

   /** \brief Adds a new attribute 'XmlTag' to this node.
    *  \param name the name the new attribute.
    *  \param value of the new attribute
    *  \returns the new child XmlTag object created.
   */
   virtual XmlTag *newAttribute(const char *name,
                                const char *value) = 0;

   /** \brief Rename this node.
    *  \param name the new name of this node.
    *  \param value an optional parameter to set the value of node.
   */
   virtual void setName(const char *name, const char *value = 0) = 0;

   /** \brief Set the value of this node.
    *  \param value the new value.
   */
   virtual void setValue(const char *value) = 0;

   /** \brief The name of this node.
    *  \returns a null termianted pointer string that is the name of this node, or if there is no value a NULL is returned.
   */
   virtual const char *name() const = 0;

   /** \brief The value of this node.
    *  \returns a null termianted pointer string that is the value of this node, or if there is no value a NULL is returned.
   */
   virtual const char *value() const = 0;

   /** \brief Check if this is a SAX Node
    *  \returns true if this is SAX node, else returns false
   */
   virtual bool isSAX() const = 0;

   /** \brief Check if this is a SAX Node
    *  \returns true if this is SAX node, else returns false
   */
   virtual bool isDOM() const = 0;

protected:
   /** \brief protected Constructor ensure this object is abstracted.
   */
   IXmlNode();
};


}}

#endif
