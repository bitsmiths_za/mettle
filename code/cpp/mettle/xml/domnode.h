/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_XML_DOMNODE_H_
#define __METTLE_XML_DOMNODE_H_

#include "mettle/xml/ixmlnode.h"

namespace Mettle {

namespace Lib
{
   class DataNode;
   class StringBuilder;
}

namespace Xml {

class XmlAttr;
class XmlTag;

/** \class DomNode domnode.h mettle/xml/domnode.h
 *  \brief An simply domn xml node object.
 *
 */
class DomNode : public IXmlNode
{
public:

   /** \brief Loads and parses xml from the specified file.
    *  \param root the node to use to parse the file
    *  \param fileName the name of the file to parse
    */
   static void loadFile(DomNode *root, const char *fileName);

   /** \brief Parses xml from the specified xml string
    *  \param root the node to use to parse the file
    *  \param xml a null termianted xml string
    */
   static void load(DomNode *root, const char *xml);


   /** \brief Constructor.
    */
   DomNode();

   /** \brief Destructor.
    */
   virtual ~DomNode();

   /** \brief Deletes all child node and sibling nodes, deletes attributes and internal data
    */
   void destroy();

   /** \brief Implements interface method.
    */
   IXmlNode *newSibling(const char *name);

   /** \brief Implements interface method.
    */
   IXmlNode *newChild(const char *name);

   /** \brief Implements interface method.
    */
   XmlTag *newAttribute(const char *name,
                        const char *value);

   /** \brief Implements interface method.
    */
   void setName(const char *name, const char *value = 0);

   /** \brief Implements interface method.
    */
   void setValue(const char *value);

   /** \brief Implements interface method.
    */
   const char *name() const;

   /** \brief Implements interface method.
    */
   const char  *value() const;

   /** \brief Implements interface method.
    */
   bool isSAX() const;

   /** \brief Implements interface method.
    */
   bool isDOM() const;

   /** \brief Deletes the sibling node
    * \param name optional arguement, delete the first sibling with the specified name only.
    */
   void deleteSibling(const char *name = 0);

   /** \brief Deletes the child node
    * \param name optional arguement, delete the first child with the specified name only.
    */
   void deleteChild(const char *name = 0);

   /** \brief Consumes and destroy the node
     * \param me the DomNode object that will be eaten.
     * \returns itself for convenience.
    */
   DomNode &eat(DomNode &me);

   /** \brief Gets the value of a DomNode using xpath.
     * \param xpath the xpath of the DomNode.
     * \returns the value of the DomNode.
     * \throws xXml exception if the path could not be found.
    */
   const char *get(const char *xpath) const;

   /** \brief Gets the DomNode object using xpath
     * \param xpath the xpath of the DomNode.
     * \returns the value of the DomNode.
     * \throws xXml exception if the path could not be found.
    */
   DomNode *getNode(const char *xpath) const;

   /** \brief Gets the XmlTag of the object using xpath if it exists.
     * \param xpath the xpath of the DomNode.
     * \returns the XmlTag object if the xpath is valid, else returns NULL.
    */
   XmlTag *exists(const char *xpath) const;

   /** \brief Gets the DomNode using xpath if it exists.
     * \param xpath the xpath of the DomNode.
     * \returns the DomNode object if \p xpath is valid, else returns NULL.
    */
   DomNode *existsNode(const char *xpath) const;

   /** \brief Checks if a child node exists, only looks one level deep.
     * \param name the name of the child node to find.
     * \returns the DomNode object if the name is found, else returns NULL.
    */
   DomNode *existsChild(const char *name) const;

   /** \brief Checks if a sibling node exists.
     * \param name the name of the sibling node to find.
     * \returns the DomNode object if the name is found, else returns NULL.
    */
   DomNode *existsSibling(const char *name) const;

   /** \brief Builds up the xpath of the node.
     * \param dest the StringBuilder to store the xpath.
     * \returns the string pointer of the StringBuilder for convenience.
    */
   const char *getXmlPath(Mettle::Lib::StringBuilder *dest) const;

   /** \brief The tag object.
    *  \warning This pointer could be NULL if nothing has been assigned to the object yet.
    */
   XmlTag        *tag;

   /** \brief The attribute object.
    *  \warning This pointer could be NULL if not attributes have been added to the node.
    */
   XmlAttr       *attr;

   /** \brief The parent node of this node.
    *  \warning This pointer will be NULL if this is the root node.
    */
   DomNode        *parent;

   /** \brief The first child node belonging to this node.
    *  \warning This pointer will be NULL there are no child nodes.
    */
   DomNode        *child;

   /** \brief The next sibling node after this one.
    *  \warning This pointer will be NULL there are no siblings.
    */
   DomNode        *sibling;

   /** \brief Writes this node to the target data node.
    *  \param dest the target DataNode that will become this xml tree.
    */
   void  dataNodeWrite(Mettle::Lib::DataNode *dest);

   /** \brief Writes the target data node to the this xml node.
    *  \param src the source DataNode that will build its xml into this node
    */
   void  dataNodeRead(Mettle::Lib::DataNode *src);

private:

   DomNode(XmlTag *tag, DomNode *parent = 0);

   bool _splitNodeData(char *&ptr,
                       Mettle::Lib::StringBuilder *dest,
                       Mettle::Lib::StringBuilder *attr) const;

   unsigned int _readNodeCount(Mettle::Lib::StringBuilder *jotter,
                               const char *path) const;

   DomNode *_locateNode(const char *path,
                        Mettle::Lib::StringBuilder *attr) const;
};



}}

#endif
