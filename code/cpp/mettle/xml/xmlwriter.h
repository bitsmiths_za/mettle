/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_XML_XMLWRITER_H_
#define __METTLE_XML_XMLWRITER_H_

#include "mettle/io/iwriter.h"

namespace Mettle {

namespace Xml
{
   class DomNode;
}

namespace IO
{

/** \class XmlReader xmlwriter.h mettle/xml/xmlwriter.h
 *  \brief An xml writer using a DomNode.
 */
class XmlWriter : public IWriter
{
public:

   /** \brief Default constructor requires a DomNode.
    * \param root the destination root xml to write to.
    */
    XmlWriter(Mettle::Xml::DomNode *root) noexcept;

   /** \brief Destructor.
   */
   virtual ~XmlWriter() noexcept;

   /** \brief Implements interface method.
    */
   void clear();

   /** \brief Implements interface method.
    */
   const char *name();

   /** \brief Implements interface method.
    */
   unsigned int writeStart(const char *name);

   /** \brief Implements interface method.
    */
   unsigned int writeEnd(const char *name);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const bool &v);

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const char &v);
#endif

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const int8_t &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const int16_t &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const int32_t &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const int64_t &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const uint8_t &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const uint16_t &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const uint32_t &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const uint64_t &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const double &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const float &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const Mettle::Lib::String &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const Mettle::Lib::UString &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const Mettle::Lib::DateTime &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const Mettle::Lib::Date &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const Mettle::Lib::Time &v);

   /** \brief Implements interface method.
    * \warning This method always throws an exception as binary data is not supported. TODO, do base64 conversion
    */
   unsigned int write(const char *field, const Mettle::Lib::MemoryBlock &v);

   /** \brief Implements interface method.
    */
   unsigned int write(const char *field, const Mettle::Lib::Guid &v);

private:

   Mettle::Xml::DomNode  *_root;
   Mettle::Xml::DomNode  *_node;
};

}}

#endif
