#include <stdio.h>
#include <string.h>

#include "mettle/lib/datanode.h"
#include "mettle/lib/logger.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/string.h"
#include "mettle/lib/stringbuilder.h"
#include "mettle/lib/ustring.h"
#include "mettle/lib/xmettle.h"

#include "mettle/xml/domnode.h"
#include "mettle/xml/domnode2text.h"
#include "mettle/xml/saxnode.h"
#include "mettle/xml/xmltag.h"

using namespace Mettle::Lib;
using namespace Mettle::Xml;

Logger *glog;

#define XML_STR "<root>"\
"   <Entity type=\"human\">"\
"      <Name>Nic &amp; Nic</Name>"\
"      <Id type='green book' valid='true'>8007115012087</Id>"\
"   </Entity>"\
"   <Accounts>"\
"      <Account id=\"1\">"\
"         <AccNo>1</AccNo>"\
"         <Name>Account Number One</Name>"\
"      </Account>"\
"      <Account id=\"2\">"\
"         <AccNo>2</AccNo>"\
"         <Name>Account Number Two</Name>"\
"      </Account>"\
"      <Account id=\"3\">"\
"         <AccNo>3</AccNo>"\
"         <Name>Account Number Three</Name>"\
"      </Account>"\
"      <Account id=\"4\">"\
"         <AccNo>4</AccNo>"\
"         <Name>Account Number Four</Name>"\
"      </Account>"\
"   </Accounts>"\
"   <Levels>"\
"      <Level>"\
"         <Level>1.1</Level>"\
"         <Level>1.2</Level>"\
"         <Level>1.3</Level>"\
"      </Level>"\
"      <Level>"\
"         <Level>2.1</Level>"\
"         <Level>2.2</Level>"\
"         <Level>2.3</Level>"\
"      </Level>"\
"      <Level>"\
"         <Level>3.1</Level>"\
"         <Level>3.2</Level>"\
"         <Level>3.3</Level>"\
"      </Level>"\
"   </Levels>"\
"</root>"



/*------------------------------------------------------------------------------
   TestDomRead
------------------------------------------------------------------------------*/
class SaxTest : public SaxNode
{
public:
   void SaxEvent(const Event event, const char *name, const char *value);
};

void SaxTest::SaxEvent(const Event event, const char *name, const char *value)
{
   switch (event)
   {
      case DocumentStart:    glog->Info(" - Doc Start:        %s", name); break;
      case ElementStart:     glog->Info(" - Element Start:    %s", name); break;
      case ElementAttribute: glog->Info(" - Element Attr:     %s=%s", name, value); break;
      case ElementText:      glog->Info(" - Element Text:     %s", value); break;
      case ElementEnd:       glog->Info(" - Element End:      %s", name); break;
      case ElementComplete:  glog->Info(" - Element Complete: %s=%s", name, value); break;
      case DocumentEnd:      glog->Info(" - Doc End:          %s", name); break;

      default:
         glog->Info("Unknown SAX Event Type Caught (%d)!!!!", event);
         break;
   }
}


static void TestSaxRead()
{
   glog->Info("TestSaxRead - Start");

   SaxTest worker;

   SaxNode::Load(&worker, XML_STR);

   glog->Info("TestSaxRead - Done");
}



/*------------------------------------------------------------------------------
   TestDomRead
------------------------------------------------------------------------------*/
static void TestDomRead()
{
   glog->Info("TestDomRead - Start");

   DomNode       root;
   StringBuilder s;

   DomNode::Load(&root, XML_STR);

   DomNode2Text(&root, false).Print(&s);

   glog->Info("  - Read: %s", s.value);

   glog->Info("TestDomRead - Done");
}


/*------------------------------------------------------------------------------
   TestXPath
------------------------------------------------------------------------------*/
static void TestXPath()
{
   glog->Info("TestXPath - Start");

   DomNode root;

   DomNode::Load(&root, XML_STR);

   glog->Info("  - Entity>Name = %s", root.Get("Entity>Name"));
   glog->Info("  - Entity>Id   = %s", root.Get("Entity>Id"));

   DomNode       *pos3  = root.GetNode("Accounts>Account[3]");
   DomNode       *pos70 = root.ExistsNode("Accounts>Account[70]");
   StringBuilder  sb;

   if (pos70)
      glog->Error("  - Accounts>Account[70] - Should have failed");

   glog->Info("  - Pos3 AccNo = %s", pos3->Get("AccNo"));
   glog->Info("  - Pos3 Name  = %s", pos3->Get("Name"));

   glog->Info("  - Accounts>Account[1]>AccNo = %s", root.Get("Accounts>Account[1]>AccNo"));
   glog->Info("  - Accounts>Account[1]>Name  = %s", root.Get("Accounts>Account[1]>Name"));

   glog->Info("  - Accounts>Account[2]>AccNo = %s", root.Get("Accounts>Account[2]>AccNo"));
   glog->Info("  - Accounts>Account[2]>Name  = %s", root.Get("Accounts>Account[2]>Name"));

   glog->Info("  - Accounts>Account[4]>AccNo = %s", root.Get("Accounts>Account[4]>AccNo"));
   glog->Info("  - Accounts>Account[4]>Name  = %s", root.Get("Accounts>Account[4]>Name"));

   glog->Info("  - Levels>Level[2]>Level[3]  = %s", root.Get("Levels>Level[2]>Level[3]"));
   glog->Info("  - Levels>Level[3]>Level[1]  = %s", root.Get("Levels>Level[3]>Level[1]"));
   glog->Info("  - Levels>Level[1]>Level[2]  = %s", root.Get("Levels>Level[1]>Level[2]"));

   pos3->GetNode("Name")->GetXmlPath(&sb);

   glog->Info(" -  'Accounts>Account[3]>Name' = '%s'", sb.value);

   glog->Info("TestXPaths - End");
}


/*------------------------------------------------------------------------------
   TestDataNode
------------------------------------------------------------------------------*/
static void TestDataNode()
{
   glog->Info("TestDataNode - Start");

   DomNode root;
   DomNode rootCopy;

   DomNode::Load(&root, XML_STR);

   DataNode droot;

   root.DataNodeWrite(&droot);

   rootCopy.SetName("foo");

   rootCopy.DataNodeRead(&droot);

   StringBuilder s;

   DomNode2Text(&rootCopy, false).Print(&s);

   glog->Info("  - Copy: %s", s.value);

   glog->Info("TestDataNode - End");
}


/*------------------------------------------------------------------------------
   main
------------------------------------------------------------------------------*/
int main(int argc, char **argv)
{
   glog = new StdOutLogger(Logger::debug);

   try
   {
      if (argc < 2)
         glog->Info("No arguements specified, running all...");
      else
         glog->Info("Mettle Xml Unittest - Start (%s).", argv[1]);

      if (argc < 2 || String::stricmp(argv[1], "TestDomRead") == 0)
         TestDomRead();

      if (argc < 2 || String::stricmp(argv[1], "TestXPath") == 0)
         TestXPath();

      if (argc < 2 || String::stricmp(argv[1], "TestSaxRead") == 0)
         TestSaxRead();

      if (argc < 2 || String::stricmp(argv[1], "TestDataNode") == 0)
         TestDataNode();

      glog->Info("Mettle Xml Unittest - Done.");
   }
   _SPOT_STD_EXCEPTIONS(glog, delete glog; return 1)

   delete glog;

   return 0;
}
