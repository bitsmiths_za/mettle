/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/xml/domnode2text.h"

#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/string.h"
#include "mettle/lib/stringbuilder.h"

#include "mettle/xml/xmlattr.h"
#include "mettle/xml/xmltag.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

using namespace Mettle::Lib;

namespace Mettle { namespace Xml {

DomNode2Text::DomNode2Text(      DomNode *node,
                           const bool     raw)
{
   _node     = node;
   _curr     = 0;
   _raw      = raw;
   _tab      = 0;
   _destFile = 0;
   _destStr  = 0;
   _jotter   = new StringBuilder();
}


DomNode2Text::~DomNode2Text()
{
   _DELETE(_jotter)
}

void DomNode2Text::print(StringBuilder *dest)
{
   _destStr   = dest;
   _curr      = _node;
   _tab       = 0;
   _destFile  = 0;

   _print();
}

void DomNode2Text::print(FILE *dest)
{
   _destStr   = 0;
   _curr      = _node;
   _tab       = 0;
   _destFile  = dest;

   _print();
}

const char *DomNode2Text::_getRaw(const char *val)
{
   if (!_raw)
      return val;

   String        jotter(val);
   unsigned  int entityCnt = 0;

   if ((entityCnt += jotter.count('>'))  < 5 &&
       (entityCnt += jotter.count('<'))  < 5 &&
       (entityCnt += jotter.count('&'))  < 5 &&
       (entityCnt += jotter.count('\'')) < 5 &&
       (entityCnt += jotter.count('"'))  < 5)
   {
      if (entityCnt == 0)
         return val;

      jotter.replace('&',  "&amp;");
      jotter.replace('>',  "&gt;");
      jotter.replace('<',  "&lt;");
      jotter.replace('\'', "&apos;");
      jotter.replace('"',  "&quot;");

      _jotter->clear();

      _jotter->add(jotter.c_str());

      return _jotter->value;
   }

   _jotter->clear();
   _jotter->add("<![CDATA[");
   _jotter->add(val);
   _jotter->add("]]>");

   return _jotter->value;
}

void DomNode2Text::_print()
{
   while (_curr)
   {
      _startElement();

      if (_closeEmptyElement())
      {
         if (!_nextElement())
            break;

         continue;
      }

      if (_writeElement())
      {
         if (!_curr->child)
         {
            _closeElement();

            if (!_nextElement())
               break;

            continue;
         }
      }

      if (_curr->child)
      {
         _openChildNode();
         continue;
      }

      if (!_nextElement())
         break;
   }
}


void DomNode2Text::_startElement()
{
   unsigned int idx;

   if (_destFile)
   {
      for (idx = 0; idx < _tab; idx++)
         fprintf(_destFile, "\t");

      fprintf(_destFile, "<%s", _curr->name());

      if (_curr->attr)
         for (idx = 0; idx < _curr->attr->count(); idx++)
         {
            XmlTag *tag = _curr->attr->ofIndex(idx);

            fprintf(_destFile, " %s = \"%s\"", tag->name(), _getRaw(tag->value()));
         }

      return;
   }

   if (_destStr)
   {
      for (idx = 0; idx < _tab; idx++)
         _destStr->add('\t');

      _destStr->join("<", _curr->name(), 0);

      if (_curr->attr)
         for (idx = 0; idx < _curr->attr->count(); idx++)
         {
            XmlTag *tag = _curr->attr->ofIndex(idx);

            _destStr->join(" ", tag->name(), " = \"", _getRaw(tag->value()), "\"", 0);
         }
   }
}


bool DomNode2Text::_closeEmptyElement()
{
   if (*_curr->value() == 0 && !_curr->child)
   {
      if (_destFile)
         fprintf(_destFile, "/>\n");
      else if (_destStr)
         _destStr->add("/>\n");

      return true;
   }

   if (_destFile)
      fprintf(_destFile, ">");
   else if (_destStr)
      _destStr->add('>');

   return false;
}


bool DomNode2Text::_writeElement()
{
   if (*_curr->value() == 0)
      return false;

   if (_destFile)
      fprintf(_destFile, "%s", _getRaw(_curr->value()));
   else if (_destStr)
      _destStr->add(_getRaw(_curr->value()));

   return true;
}


void DomNode2Text::_closeElement()
{
   if (_destFile)
   {
      fprintf(_destFile, "</%s>\n", _curr->name());
   }
   else if (_destStr)
   {
      _destStr->join("</", _curr->name(), ">\n", 0);
   }
}


bool DomNode2Text::_nextElement()
{
   for (;;)
   {
      if (_curr == _node)
         return false;

      if (_curr->sibling)
      {
         _curr = _curr->sibling;
         return true;
      }

      _curr = _curr->parent;

      if (!_curr)
         return true;

      _tab--;

      if (_destFile)
      {
         for (unsigned int idx = 0; idx < _tab; idx++)
            fprintf(_destFile, "\t");

         fprintf(_destFile, "</%s>\n", _curr->name());
      }
      else if (_destStr)
      {
         for (unsigned int idx = 0; idx < _tab; idx++)
            _destStr->add('\t');

         _destStr->join("</", _curr->name(), ">\n", 0);
      }
   }

   return true;
}


void DomNode2Text::_openChildNode()
{
   if (_destFile)
      fprintf(_destFile, "\n");
   else if (_destStr)
      _destStr->add('\n');

   _tab++;
   _curr = _curr->child;
}

}}
