/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/xml/xmlwriter.h"

#include "mettle/lib/common.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/macros.h"

#include "mettle/xml/domnode.h"
#include "mettle/xml/xmltag.h"

using namespace Mettle::Lib;
using namespace Mettle::Xml;

namespace Mettle { namespace IO {

#define MODULE "XmlWriter"

XmlWriter::XmlWriter(DomNode *root) noexcept
{
   _node  = 0;
   _root  = root;
}

XmlWriter::~XmlWriter() noexcept
{
}

void XmlWriter::clear()
{
   _root->destroy();
   _node = 0;
}

const char *XmlWriter::name()
{
   "Mettle.XmlWriter";
}

unsigned int XmlWriter::writeStart(const char *name)
{
   if (!_node)
   {
      _root->setName(name);
      _node = _root;
   }
   else
   {
      _node = (DomNode*)_node->newChild(name);
   }

   return 0;
}

unsigned int XmlWriter::writeEnd(const char *name)
{
   _node = _node->parent;
   return 0;
}

unsigned int XmlWriter::write(const char *field, const bool &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

#ifndef _METTLE_DUPLICATE_TYPDEF_IS_BROKEN_
unsigned int XmlWriter::write(const char *field, const char &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}
#endif

unsigned int XmlWriter::write(const char *field, const int8_t &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const int16_t &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const int32_t &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const int64_t &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const uint8_t &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const uint16_t &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const uint32_t &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const uint64_t &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const double &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const float &v)
{
   _node->newChild(field)->setValue(String(v).c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const String &v)
{
   _node->newChild(field)->setValue(v.c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const UString &v)
{
   _node->newChild(field)->setValue(v.c_str());
   return 0;
}

unsigned int XmlWriter::write(const char *field, const DateTime &v)
{
   char jotter[32];

   _node->newChild(field)->setValue(v.toString(jotter));
   return 0;
}

unsigned int XmlWriter::write(const char *field, const Date &v)
{
   char jotter[32];

   _node->newChild(field)->setValue(v.toString(jotter));
   return 0;
}

unsigned int XmlWriter::write(const char *field, const Time &v)
{
   char jotter[32];

   _node->newChild(field)->setValue(v.toString(jotter));
   return 0;
}

unsigned int XmlWriter::write(const char *field, const Mettle::Lib::MemoryBlock &v)
{
   throw xMettle(__FILE__, __LINE__, MODULE, "Cannot serialize binary data via XML Writer");
   return 0;
}

unsigned int XmlWriter::write(const char *field, const Mettle::Lib::Guid &v)
{
   char jotter[38];

   _node->newChild(field)->setValue(v.toString(jotter));
   return 0;
}


#undef MODULE

}}
