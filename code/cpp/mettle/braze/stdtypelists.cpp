/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/stdtypelists.h"

#include "mettle/io/ireader.h"
#include "mettle/io/iwriter.h"

using namespace Mettle::Lib;

namespace Mettle { namespace Braze {

#define _TYPE_LIST_(t, l) const char *t##List::_name() const\
{\
   return #t"List";\
}\
unsigned int t##List::_serialize(Mettle::IO::IWriter *w, const char *_defName) const\
{\
   unsigned int sizeWritten = 0;\
   uint32_t     cnt      = (uint32_t) count();\
   uint32_t     idx;\
   sizeWritten += w->writeStart(_defName ? _defName : _name());\
   sizeWritten += w->write("ListSize", cnt);\
   for (idx = 0; idx < cnt; ++idx)\
      sizeWritten += w->write("item", l[idx]);\
   sizeWritten += w->writeEnd(_defName ? _defName : _name());\
   return sizeWritten;\
}\
unsigned int t##List::_deserialize(Mettle::IO::IReader *r, const char *_defName)\
{\
   unsigned int sizeRead = 0;\
   uint32_t     cnt      = 0;\
   uint32_t     idx;\
   sizeRead += r->readStart(_defName ? _defName : _name());\
   sizeRead += r->read("ListSize", cnt);\
   allocate(cnt);\
   for (idx = 0; idx < cnt; ++idx)\
      sizeRead += r->read("item", l[idx]);\
   sizeRead += r->readEnd(_defName ? _defName : _name());\
   return sizeRead;\
}

_TYPE_LIST_(Int8,        _list)
_TYPE_LIST_(Int16,       _list)
_TYPE_LIST_(Int32,       _list)
_TYPE_LIST_(Int64,       _list)
_TYPE_LIST_(Uint8,       _list)
_TYPE_LIST_(Uint16,      _list)
_TYPE_LIST_(Uint32,      _list)
_TYPE_LIST_(Uint64,      _list)
_TYPE_LIST_(Bool,        _list)
_TYPE_LIST_(Char,        _list)
_TYPE_LIST_(Double,      _list)
_TYPE_LIST_(Float,       _list)

_TYPE_LIST_(String,      *_list);
_TYPE_LIST_(Date,        *_list);
_TYPE_LIST_(Time,        *_list);
_TYPE_LIST_(DateTime,    *_list);
_TYPE_LIST_(MemoryBlock, *_list);
_TYPE_LIST_(Guid,        *_list);

#undef _TYPE_LIST_

}}
