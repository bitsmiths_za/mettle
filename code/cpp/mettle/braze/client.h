/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_BRAZE_CLIENT_H_
#define __METTLE_BRAZE_CLIENT_H_

#include "mettle/lib/c99standard.h"

namespace Mettle {

namespace IO
{
   class IStream;
}

namespace Braze {

class IClientMarshaler;
class ITransport;

/** \class Client client.h mettle/braze/client.h
 *  \brief The basic braze client that can make requests to a braze server.
 */
class Client
{
public:

   /** \brief Constructor.
    *  \param tran the tranport object that the client will be using to make calls to the server.
    */
   Client(ITransport* trans);

   /** \brief Destructor.
    */
   virtual ~Client();

   /** \brief Sends a request to a braze server.
    *  \param marshaler the marshaler object to use to make the braze server call.
    *  \param remoteSignature the call to make.
    *  \param input the memory blob to send to the server.
    */
   virtual void send(      IClientMarshaler*    marshaler,
                     const char*                remoteSignature,
                           Mettle::IO::IStream* input);

   /** \brief Recieves a response from a braze server.
    *  \param marshaler the marshaler object to use to recieve the braze server call.
    *  \param remoteSignature the call we are expecting back from.
    *  \param input the memory blob to receive and pass on.
    */
   virtual void receive(      IClientMarshaler*    marshaler,
                        const char*                remoteSignature,
                              Mettle::IO::IStream* output);

   /** \brief Get the transport object.
    *  \returns the transport object the server is using.
    */
   ITransport* transport();

protected:

   /** \brief Protected access to the transport object.
    */
   ITransport* _trans;
};

}}


#endif
