/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_IO_STDTYPELISTS_H_
#define __METTLE_IO_STDTYPELISTS_H_

#include <uuid/uuid.h>

#include "mettle/lib/appendlist.h"
#include "mettle/lib/c99standard.h"
#include "mettle/lib/collection.h"
#include "mettle/lib/guid.h"
#include "mettle/lib/datetime.h"
#include "mettle/lib/memoryblock.h"
#include "mettle/lib/safe.h"
#include "mettle/lib/string.h"

#include "mettle/io/iserializable.h"

namespace Mettle { namespace Braze {

#define _TYPE_LIST_(t, n, l) class n##List : public Mettle::Lib::l<t>, public Mettle::IO::ISerializable\
{\
public:\
   const char   *_name() const;\
   unsigned int  _serialize(Mettle::IO::IWriter   *w, const char *_defName = 0) const;\
   unsigned int  _deserialize(Mettle::IO::IReader *r, const char *_defName = 0);\
   DECLARE_SAFE_CLASS(n##List);\
}

_TYPE_LIST_(int8_t,                 Int8,     AppendList);
_TYPE_LIST_(int16_t,                Int16,    AppendList);
_TYPE_LIST_(int32_t,                Int32,    AppendList);
_TYPE_LIST_(int64_t,                Int64,    AppendList);
_TYPE_LIST_(uint8_t,                Uint8,    AppendList);
_TYPE_LIST_(uint16_t,               Uint16,   AppendList);
_TYPE_LIST_(uint32_t,               Uint32,   AppendList);
_TYPE_LIST_(uint64_t,               Uint64,   AppendList);
_TYPE_LIST_(bool,                   Bool,     AppendList);
_TYPE_LIST_(char,                   Char,     AppendList);
_TYPE_LIST_(double,                 Double,   AppendList);
_TYPE_LIST_(float,                  Float,    AppendList);

_TYPE_LIST_(Mettle::Lib::String,       String,       Collection);
_TYPE_LIST_(Mettle::Lib::Date,         Date,         Collection);
_TYPE_LIST_(Mettle::Lib::Time,         Time,         Collection);
_TYPE_LIST_(Mettle::Lib::DateTime,     DateTime,     Collection);
_TYPE_LIST_(Mettle::Lib::MemoryBlock,  MemoryBlock,  Collection);
_TYPE_LIST_(Mettle::Lib::Guid,         Guid,         Collection);


#undef _TYPE_LIST_

}}

#endif
