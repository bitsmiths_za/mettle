/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/rpcheader.h"

#include "mettle/io/ireader.h"
#include "mettle/io/iwriter.h"

namespace Mettle { namespace Braze {

RpcHeader::RpcHeader() : ISerializable()
{
   clear();
}

RpcHeader::~RpcHeader() noexcept
{
}

const char *RpcHeader::_name() const
{
   return "RpcHeader";
}

unsigned int RpcHeader::_serialize(Mettle::IO::IWriter *w, const char *name) const
{
   unsigned int sizeWritten = 0;

   sizeWritten += w->writeStart(name);
   sizeWritten += w->write("TransportSignature", transportSignature);
   sizeWritten += w->write("ErrorCode",          errorCode);
   sizeWritten += w->write("ClientSignature",    clientSignature);
   sizeWritten += w->write("ServerSignature",    serverSignature);
   sizeWritten += w->writeEnd(name);

   return sizeWritten;
}

unsigned int RpcHeader::_deserialize(Mettle::IO::IReader *r, const char *name)
{
   unsigned int sizeRead = 0;

   sizeRead += r->readStart(name);
   sizeRead += r->read("TransportSignature", transportSignature);
   sizeRead += r->read("ErrorCode",          errorCode);
   sizeRead += r->read("ClientSignature",    clientSignature);
   sizeRead += r->read("ServerSignature",    serverSignature);
   sizeRead += r->readEnd(name);

   return sizeRead;
}

void RpcHeader::clear()
{
   transportSignature =
   errorCode          = 0;

   clientSignature.clear();
   serverSignature.clear();
}

}}
