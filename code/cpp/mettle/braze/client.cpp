/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/client.h"

#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

#include "mettle/braze/iclientmarshaler.h"
#include "mettle/braze/itransport.h"
#include "mettle/braze/rpcheader.h"

using namespace Mettle::Lib;
using namespace Mettle::IO;

namespace Mettle { namespace Braze {

#define MODULE "Braze::Client"

Client::Client(ITransport *trans)
{
   _trans = trans;
}

Client::~Client()
{
}

ITransport *Client::transport()
{
   return _trans;
}

void Client::send(      IClientMarshaler*  marshaler,
                  const char*              remoteSignature,
                        IStream*           input)
{
   RpcHeader::Safe head(_trans->createHeader());

   head.obj->clientSignature    = remoteSignature;
   head.obj->serverSignature    = marshaler->_signature();
   head.obj->transportSignature = _trans->signature();

   _trans->send(head.obj, input);
}

void Client::receive(      IClientMarshaler* marshaler,
                     const char*             remoteSignature,
                           IStream*          output)
{
   RpcHeader::Safe head(_trans->createHeader());

   _trans->receiveHeader(head.obj);

   if (head.obj->clientSignature != remoteSignature)
      throw xMettle(__FILE__, __LINE__, xMettle::ComsSignature, MODULE, "Invalid client signature retured from server (%s)", head.obj->clientSignature.c_str());

   if (head.obj->serverSignature != marshaler->_signature())
      throw xMettle(__FILE__, __LINE__, xMettle::ComsSignature, MODULE, "Invalid server signature returned from server (%s), expected (%s)", head.obj->serverSignature.c_str(), marshaler->_signature());

   if (head.obj->transportSignature != _trans->signature())
      throw xMettle(__FILE__, __LINE__, xMettle::ComsSignature, MODULE, "Invalid transport signature returned from server (%d), expected (%d)", head.obj->transportSignature, _trans->signature());

   if (head.obj->errorCode != 0)
   {
      String errMsg;

      _trans->receiveErrorMessage(head.obj, &errMsg);

      throw xMettle(__FILE__, __LINE__, (xMettle::eCode) head.obj->errorCode, MODULE, errMsg.c_str());
   }

   _trans->receive(head.obj, output);
}

#undef MODULE

}}
