/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/server.h"

#include "mettle/braze/rpcheader.h"
#include "mettle/braze/iserverinterface.h"
#include "mettle/braze/iservermarshaler.h"
#include "mettle/braze/itransport.h"

#include "mettle/io/memorystream.h"

#include "mettle/lib/logger.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

using namespace Mettle::Lib;
using namespace Mettle::IO;

#define MODULE "Braze::Server"


namespace Mettle { namespace Braze {

Server::Server(Mettle::Lib::Logger  *logger,
               IServerMarshaler     *marshaler,
               ITransport           *trans)
{
   _log       = logger;
   _marshaler = marshaler;
   _trans     = trans;
}

Server::~Server()
{
   destruct();
}

void Server::construct()
{
   destruct();

   _marshaler->_serverImpl()->_construct();
}

void Server::destruct()
{
   if (_marshaler)
      _marshaler->_serverImpl()->_destruct();

   if (_trans)
      _trans->terminate();
}

const char *Server::serverID() const
{
   return "[BrazeServer] ";
}

bool Server::shutdownServer() const
{
   return _marshaler->_serverImpl()->_shutdownServer();
}

void Server::slackTimeHandler()
{
   _marshaler->_serverImpl()->_slackTimeHandler();
}


bool Server::waitForConnection(const int32_t timeOut)
{
   return _trans->waitForConnection(timeOut);
}


void Server::receive(String   &remoteSignature,
                     IStream  *input,
                     int32_t  &errorCode,
                     String   *errMsg)
{
   try
   {
      RpcHeader::Safe head(_trans->createHeader());

      _trans->receiveHeader(head.obj);

      if (head.obj->serverSignature != _marshaler->_signature())
         throw xMettle(__FILE__, __LINE__, xMettle::ComsSignature, MODULE, "Invalid server signature received from client [%d], expected [%d]", head.obj->serverSignature, _marshaler->_signature());

      if (head.obj->transportSignature != _trans->signature())
         throw xMettle(__FILE__, __LINE__, xMettle::ComsSignature, MODULE, "Invalid transport signature received from client [%d], expected [%d]", head.obj->transportSignature, _trans->signature());

      remoteSignature = head.obj->clientSignature;

      _trans->receive(head.obj, input);
   }
   catch (xMettle &x)
   {
      _log->spot(__FILE__, __LINE__, x);
      errorCode = (int32_t) x.errorCode;
      *errMsg = x.errorMsg;
   }
}

void Server::send(const String    &remoteSignature,
                        IStream   *output,
                  const int32_t    errorCode,
                        String    *errorMessage)
{
   RpcHeader::Safe head(_trans->createHeader());

   head.obj->clientSignature     = remoteSignature;
   head.obj->serverSignature     = _marshaler->_signature();
   head.obj->transportSignature  = _trans->signature();
   head.obj->errorCode           = errorCode;

   if (errorCode)
   {
      _trans->sendErrorMessage(head.obj, errorMessage);
      return;
   }

   _trans->send(head.obj, output);
}

IServerMarshaler *Server::marshaler()
{
   return _marshaler;
}

ITransport *Server::transport()
{
   return _trans;
}

Mettle::Lib::Logger *Server::logger()
{
   return _log;
}

int Server::_maxConsecutiveExceptions()
{
   return 1000;
}

void Server::run(const int32_t waitTimeout)
{
   int exceptionCountDown = _maxConsecutiveExceptions();

   construct();

   _log->info("%sServer started", serverID());

   String remoteSignature;

   for (;;)
   {
      try
      {
         MemoryStream  input;
         MemoryStream  output;
         int32_t       errorCode = 0;
         String        errorMessage;

         remoteSignature.clear();

         if (!waitForConnection(waitTimeout))
         {
            if (shutdownServer())
               break;

            slackTimeHandler();

            continue;
         }

         receive(remoteSignature,
                 &input,
                 errorCode,
                 &errorMessage);

         if (errorCode)
         {
            send(remoteSignature, &output, errorCode, &errorMessage);
            continue;
         }

         _marshaler->_serve(this,
                            remoteSignature,
                            &input,
                            &output,
                            errorCode,
                            &errorMessage);

         send(remoteSignature, &output, errorCode, &errorMessage);

         exceptionCountDown = _maxConsecutiveExceptions();
         remoteSignature.clear();
      }
      catch (xMettle &x)
      {
         if (shutdownServer())
         {
            _log->spot(__FILE__, __LINE__, x);
            _log->error("%sException Caught & Shutdown Signal received.  Ignoring Exception '%s'", serverID(), x.errorMsg);
            destruct();
            return;
         }

         exceptionCountDown--;

         if (exceptionCountDown <= 0)
         {
            _log->spot(__FILE__, __LINE__, x);
            _log->error("%sCountdown reached (%d), blowing server....", serverID(), exceptionCountDown);
            throw;
         }

         if (x.errorCode == xMettle::TerminalException)
         {
            _log->spot(__FILE__, __LINE__, x);
            _log->error("%sTerminal exception encountered, blowing server....", serverID());
            throw;
         }

         _log->warning("%sServer exception [count:%d, code:%d, source:%s, signature:%s]: %s",
            serverID(),
            exceptionCountDown,
            x.errorCode,
            x.source,
            remoteSignature.c_str(),
            x.errorMsg);

         _trans->terminate();

         continue;
      }
      catch (std::exception &x)
      {
         if (shutdownServer())
         {
            _log->error("%sException caught & shutdown signal received.  Ignoring exception: [%s]", serverID(), x.what());
            destruct();
            return;
         }

         exceptionCountDown--;

         if (exceptionCountDown <= 0)
         {
            _log->error("%sCountdown reached (%d), blowing server....", serverID(), exceptionCountDown);
            _log->spot(__FILE__, __LINE__, x);
            throw;
         }

         _log->warning("%sServer exception [count:%d, signature:%s, std::exception]: %s",
            serverID(),
            exceptionCountDown,
            remoteSignature.c_str(),
            x.what());
         _trans->terminate();

         continue;
      }
      catch (...)
      {
         _log->spot(__FILE__, __LINE__);
         throw;
      }
   }

   _log->info("%sServer shutting down", serverID());

   destruct();
}

}}
