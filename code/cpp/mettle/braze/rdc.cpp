/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/rdc.h"

#include "mettle/lib/macros.h"

#include "mettle/io/memorystream.h"

#include <stdio.h>
#include <string.h>

using namespace Mettle::Lib;
using namespace Mettle::IO;

namespace Mettle { namespace Braze {

#define BEST_HASH    4096
#define MODULE      "RDC"

RDC::RDC() noexcept
{
   _state            = RDC::None;
   _data             = 0;
   _compressedLength = 0;
   _length           = 0;
}


RDC::~RDC() noexcept
{
   clear();
}


void RDC::clear() noexcept
{
   _state            = RDC::None;
   _compressedLength = 0;
   _length           = 0;

   _DELETE(_data)
}


RDC::DataState RDC::state() const noexcept
{
   return _state;
}


void RDC::eat(MemoryStream   *in,
              const uint32_t  length,
              const uint32_t  compressedLength)
{
   clear();

   _data = new MemoryStream();

   _data->eat(in);

   _length           = length;
   _compressedLength = compressedLength;

   if (_compressedLength != 0 && _compressedLength != length)
      _state = RDC::Compressed;
   else if (_length > 0)
      _state = RDC::Uncompressed;
   else
      _state = RDC::None;
}

void RDC::compress()
{
   if (_state != RDC::Uncompressed)
      throw xMettle(__FILE__, __LINE__, "RDC", "Cannot compress, data state not ready");

   _length = _data->_sizeUsed;

   if (_data->_sizeUsed <= 32) // skip small buffers
   {
      _compressedLength = _data->_sizeUsed;
      return;
   }

   MemoryStream compBlob;

   compBlob.addMemory(_data->_sizeUsed);

   unsigned char *hash_tbl[BEST_HASH];
   unsigned char *rdc_buff      = (unsigned char *) _data->_memory;
   unsigned char *rdc_buff_end  = rdc_buff + _data->_sizeUsed;
   unsigned char *anchor;
   unsigned char *pat_idx;
   unsigned char *out_buf      = compBlob._memory + 1;
   unsigned char *out_buf_end  = compBlob._memory + (_data->_sizeUsed - 24);
   unsigned char *ctrl_idx     = compBlob._memory;
   unsigned char  ctrl_bits    = 0;
   unsigned char  ctrl_cnt     = 0;
   int32_t        cnt;
   int32_t        gap;
   int32_t        c;
   int32_t        hash;

   // clear hash table for given length
   memset(hash_tbl, 0, sizeof(unsigned char *) * BEST_HASH);

   // scan thru buff
   while (rdc_buff < rdc_buff_end)
   {
      // --make room for control bits-----------------------------------------
      if (ctrl_cnt++ == 8)
      {
         // and check for output_buf overflow
         *ctrl_idx = ctrl_bits;
         ctrl_cnt  = 1;
         ctrl_idx  = out_buf;
         out_buf++;

         if (out_buf > out_buf_end)
         {
            _compressedLength = _data->_sizeUsed;
            return;
         }
      }

      // --Check for RLE------------------------------------------------------
      anchor = rdc_buff;
      c      = *rdc_buff++;

      while (rdc_buff < rdc_buff_end && *rdc_buff == c && (rdc_buff - anchor) < 4110)
         rdc_buff++;

      // store compression code if character is repeated more than 2 times
      if ((cnt = (rdc_buff - anchor)) > 2)
      {
         if (cnt <= 18)                    // short RLE
            *out_buf++ = (unsigned char)(cnt - 3);
         else                              // long  RLE
         {
            cnt -= 19;
            *out_buf++ = (unsigned char)(16 + (cnt & 0x0F));
            *out_buf++ = (unsigned char)(cnt >> 4);
         }

         *out_buf++ = (unsigned char)c;
         ctrl_bits  = (unsigned char)((ctrl_bits << 1) | 1);
         continue;
      }

      // --look for pattern if 2 or more chars remain in the input buffer-----
      rdc_buff = anchor;

      if ((rdc_buff_end - rdc_buff) > 2)
      {
         // locate offset of possible pattern in sliding dictionary
         hash = ((((rdc_buff[0] << 8) | rdc_buff[1]) ^
                  ((rdc_buff[0] >> 4) | (rdc_buff[2] << 4))) % BEST_HASH);

         pat_idx        = hash_tbl[hash];
         hash_tbl[hash] = rdc_buff;

         // compare chars id wer'e within 4090 bytes (set to 4090)
         if (pat_idx && (gap = (rdc_buff - pat_idx)) <= 4090)
         {
            while (rdc_buff < rdc_buff_end && pat_idx < anchor && *pat_idx == *rdc_buff && (rdc_buff - anchor) < 271)
            {
               rdc_buff++;
               pat_idx++;
            }

            if ((cnt = (rdc_buff - anchor)) > 2)
            {                               // store pattern if it is more
               gap -= 3;                     // than two chars
               if (cnt <= 15)                // short pattern
               {
                  *out_buf++ = (unsigned char)((cnt << 4) + (gap & 0x0F));
                  *out_buf++ = (unsigned char)(gap >> 4);
               }
               else
               {
                  *out_buf++ = (unsigned char)(32 + (gap & 0x0F));
                  *out_buf++ = (unsigned char)(gap >> 4);
                  *out_buf++ = (unsigned char)(cnt - 16);
               }
               ctrl_bits = (unsigned char)((ctrl_bits << 1) | 1);
               continue;
            }
         }
      }

      // --cant compress this char so copy it to out buf----------------------
      *out_buf++ = (unsigned char)c;
      rdc_buff = ++anchor;
      ctrl_bits <<= 1;
   }

   // save last load of bits
   ctrl_bits <<= (8 - ctrl_cnt);
   *ctrl_idx = ctrl_bits;

   // finally set the output size
   _compressedLength = out_buf - compBlob._memory;
   _state            = RDC::Compressed;

   _data->eat(&compBlob);
}

void RDC::uncompress()
{
   if (_state != RDC::Compressed)
      throw xMettle(__FILE__, __LINE__, "RDC", "Cannot uncompress, data state not ready");

   _state = RDC::Uncompressed;

   if (_compressedLength == 0 || _compressedLength == _length)
      return;

   MemoryStream uncoBlob;

   uncoBlob.addMemory(_length);

   unsigned char *rdc_buff     = _data->_memory;
   unsigned char *rdc_buff_end = rdc_buff + _compressedLength;
   unsigned char *out_buf      = uncoBlob._memory;
   unsigned char  ctrl_bits;
   unsigned char  ctrl_mask  = 0;
   int32_t        cmd;
   int32_t        cnt;
   int32_t        offset;

   // process each item in rdc_buff
   while (rdc_buff < rdc_buff_end)
   {
      // get new load of control bits if needed
      if ((ctrl_mask >>= 1) == 0)
      {
         ctrl_bits =  *rdc_buff;
         rdc_buff++;
         ctrl_mask = 0x80;
      }

      // just copy this char if control bit is zero
      if ((ctrl_bits & ctrl_mask) == 0)
      {
         *out_buf++ = *rdc_buff++;
         continue;
      }
      // Undo the compression code
      cmd = ((*rdc_buff >> 4) & 0x0F);
      cnt = (*rdc_buff++ & 0x0F);

      switch (cmd)
      {
         case 0:                             // short rule
            cnt += 3;
            memset(out_buf, *rdc_buff++, cnt);
            out_buf += cnt;
            break;

         case 1:                             // long rule
            cnt += (*rdc_buff++ << 4);
            cnt += 19;
            memset(out_buf, *rdc_buff++, cnt);
            out_buf += cnt;
            break;

         case 2:                             // long pattern
            offset = (cnt + 3);
            offset += (*rdc_buff++ << 4);
            cnt = *rdc_buff++;
            cnt += 16;
            memcpy(out_buf, out_buf - offset, cnt);
            out_buf += cnt;
            break;

         default:                            // short pattern
            offset = (cnt + 3);
            offset += (*rdc_buff++ << 4);
            memcpy(out_buf, out_buf - offset, cmd);
            out_buf += cmd;
            break;
      }
   }

   // ensure output size was the same as its original input size
   if (out_buf - uncoBlob._memory != _length)
      throw xMettle(__FILE__, __LINE__, "RDC", "File buffer is the incorrect size / data corrupt (%u/%u)", out_buf - uncoBlob._memory, _length);

   _data->eat(&uncoBlob);
}

#undef BEST_HASH
#undef MODULE

}}
