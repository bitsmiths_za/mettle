/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/sharedobj/libserver.h"

#include "mettle/braze/rpcheader.h"
#include "mettle/braze/iserverinterface.h"
#include "mettle/braze/iservermarshaler.h"
#include "mettle/braze/itransport.h"

#include "mettle/io/memorystream.h"

#include "mettle/lib/logger.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

using namespace Mettle::Lib;
using namespace Mettle::IO;
using namespace Mettle::Braze;

#define MODULE "Braze::SharedObj::LibServer"

namespace Mettle { namespace Braze { namespace SharedObj {

LibServer::LibServer(Mettle::Lib::Logger  *logger,
                     IServerMarshaler     *marshaler,
                     ITransport           *trans)
         :Server(logger, marshaler, trans)
{
   _constructed = false;
}

LibServer::~LibServer()
{
   if (_marshaler)
   {
      delete _marshaler->_serverImpl();
      delete _marshaler;

      _marshaler = 0;
   }

   _DELETE(_trans)
}

void LibServer::construct()
{
   _marshaler->_serverImpl()->_construct();

   _constructed = true;
}

void LibServer::destruct()
{
   _constructed = false;
}

const char *LibServer::serverID() const
{
   return "[LibServer] ";
}


void LibServer::run(const int32_t waitTimeout)
{
   if (!_constructed)
      construct();

   slackTimeHandler();

   MemoryStream  input;
   MemoryStream  output;
   String        remoteSignature;
   int32_t       errorCode       = 0;
   String        errorMessage;

   receive(remoteSignature,
           &input,
           errorCode,
           &errorMessage);

   if (errorCode)
   {
      send(remoteSignature, &output, errorCode, &errorMessage);
      return;
   }

   _marshaler->_serve(this,
                      remoteSignature,
                      &input,
                      &output,
                      errorCode,
                      &errorMessage);

   send(remoteSignature, &output, errorCode, &errorMessage);
}

}}}
