/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/sharedobj/transportserver.h"

#include "mettle/braze/sharedobj/sharedobjheader.h"

#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/string.h"

#include "mettle/io/streamreader.h"
#include "mettle/io/streamwriter.h"
#include "mettle/io/memorystream.h"

#include <string.h>
#include <stdlib.h>

using namespace Mettle::Lib;
using namespace Mettle::IO;
using namespace Mettle::Braze;

namespace Mettle { namespace Braze { namespace SharedObj {

#define MODULE "SharedObj::ServerTransport"

TransportServer::Settings::Settings()
               :ITransport::ISettings()
{
}

TransportServer::Settings::~Settings() noexcept
{
}

TransportServer::TransportServer()
{
   _recv =
   _send = 0;
}

TransportServer::~TransportServer() noexcept
{
   destruct();
}

void TransportServer::construct(ITransport::ISettings *settings)
{
   destruct();

   Settings *sett = (Settings *) settings;

   _recv  = new MemoryStream();
   _send  = new MemoryStream();
}


void TransportServer::destruct() noexcept
{
   _DELETE(_recv)
   _DELETE(_send)
}


int32_t TransportServer::signature() const
{
   return 0x0B700000;
}


IReader *TransportServer::newReader(Mettle::IO::IStream *s) const
{
   return new StreamReader(s);
}


IWriter *TransportServer::newWriter(Mettle::IO::IStream *s) const
{
   return new StreamWriter(s);
}


RpcHeader *TransportServer::createHeader()
{
   return new SharedObjHeader();
}


void TransportServer::send(RpcHeader *header,
                           IStream   *data)
{
   _send->clear();

   SharedObjHeader *head  = (SharedObjHeader *) header;
   MemoryStream    *mdata = (MemoryStream *) data;

   StreamWriter sw(_send);

   head->_serialize(&sw, head->_name());

   if (mdata && mdata->size() > 0)
      _send->write(mdata->_memory, mdata->size());

   _send->positionStart();
}

void TransportServer::sendErrorMessage(RpcHeader  *header,
                                       String     *errMsg)
{
   _send->clear();

   SharedObjHeader *head      = (SharedObjHeader *) header;

   StreamWriter sw(_send);

   head->_serialize(&sw, head->_name());

   sw.writeStart("ServerException");
   sw.write("ErrorMessage", *errMsg);
   sw.writeEnd("ServerException");

   _send->positionStart();
}

void TransportServer::receiveHeader(RpcHeader *header)
{
   SharedObjHeader *head = (SharedObjHeader *) header;

   StreamReader sr(_recv);

   head->_deserialize(&sr, head->_name());
}

void TransportServer::receive(RpcHeader *header,
                              IStream   *data)
{
   SharedObjHeader  *head = (SharedObjHeader *) header;

   if (_recv->size() == _recv->position())
      return;

   data->write(_recv->_memory + _recv->position(), _recv->size() - _recv->position());

   data->positionStart();
}

void TransportServer::receiveErrorMessage(RpcHeader   *head,
                                          String      *errMsg)
{
}

void TransportServer::terminate()
{
}

void TransportServer::houseKeeping()
{
}

bool TransportServer::waitForConnection(const int32_t timeOut)
{
   return false;
}

String *TransportServer::clientAddress(String *buf)
{
   *buf  = "<Shared Object: N/A>";

   return buf;
}

Mettle::IO::MemoryStream *TransportServer::streamReceive() const
{
   return _recv;
}

Mettle::IO::MemoryStream *TransportServer::streamSend() const
{
   return _send;
}

#undef MODULE

}}}
