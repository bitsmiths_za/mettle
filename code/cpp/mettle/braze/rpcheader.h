/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_BRAZE_RPCHEADER_H_
#define __METTLE_BRAZE_RPCHEADER_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/safe.h"
#include "mettle/lib/string.h"
#include "mettle/io/iserializable.h"

namespace Mettle {

namespace IO
{
   class IReader;
   class IWriter;
}

namespace Braze {

/** \class RpcHeader rpcheader.h mettle/braze/rpcheader.h
 *  \brief The default Remote Procedure Call Header implementation.
 *
 *  This object should be inherited and extended if additional rpc header
 *  members are required.
 */
class RpcHeader : public Mettle::IO::ISerializable
{
public:

   DECLARE_SAFE_CLASS(RpcHeader);

   /** \brief The client signature, remote call request.
   */
   Mettle::Lib::String clientSignature;

   /** \brief The server signature, which server we intend to be talking to.
   */
   Mettle::Lib::String serverSignature;

   /** \brief The server signature, which server we intend to be talking to.
   */
   int32_t     transportSignature;

   /** \brief The error code if there has been an error.
   */
   int32_t     errorCode;

   /** \brief Constructor.
   */
   RpcHeader();

   /** \brief Destructor.
   */
   virtual  ~RpcHeader() noexcept;

   /** \brief The name of this rpc header implementation, default is 'RpcHeader'.
   */
   virtual  const char* _name() const;

   /** \brief The default serializatin overload for ISerializable.
   */
   virtual unsigned int  _serialize(Mettle::IO::IWriter* w, const char* name) const;

   /** \brief The default deserializatin overload for ISerializable.
   */
   virtual unsigned int  _deserialize(Mettle::IO::IReader*r, const char* name);

   /** \brief Clears/re-intialize the internal members.
   */
   virtual void clear();
};

}}

#endif
