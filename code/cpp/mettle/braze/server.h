/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_BRAZE_SERVER_H_
#define __METTLE_BRAZE_SERVER_H_

#include "mettle/lib/c99standard.h"

namespace Mettle {

namespace Lib
{
   class Logger;
   class String;
}

namespace IO
{
   class IStream;
}


namespace Braze {

class  IServerMarshaler;
class  ITransport;
class  RpcHeader;


/** \class Server server.h mettle/braze/server.h
 *  \brief The basic braze server handler that listens on a transport of messsages and serves requests.
 */
class Server
{
public:

   /** \brief Constructor.
    *  \param logger the logging object the server will.
    *  \param marshaler the marshalling object (usually generated) the server will use.
    *  \param tran the tranport object that the marshaller will use to serialize/deserialize its streams too.
    */
   Server(Mettle::Lib::Logger *logger,
          IServerMarshaler    *marshaler,
          ITransport          *trans);

   /** \brief Destructor.
    */
   virtual ~Server();

   /** \brief The standard run implementation.
    *
    * This method put the server in running loop and by default it sleeps \p waitTimeout on the transport
    * object when it has nothing to to check for shut down signals and running house keeping routings.
    * Ideals this method should be overloaded to suit a more custome server implementation but if you need a
    * a basic and robust server handler, this will do it.
    *
    * This method loops ifinately until it recieves a shutdown signal, (until the shutdownServer() method returns true.)
    *
    * \param waitTimeout the time in mili seconds for the handler to check for shutdown request and run house keeping routines.
    */
   virtual void run(const int32_t waitTimeout = 1000);

   /** \brief Get the marshaler object.
    *  \returns the marshaler object the server is using.
    */
   IServerMarshaler *marshaler();

   /** \brief Get the transport object.
    *  \returns the transport object the server is using.
    */
   ITransport *transport();

   /** \brief Get the logger object.
    *  \returns the logger object the server is using.
    */
   Mettle::Lib::Logger *logger();

   /** \brief Get the server identifier, a name for this server.  This is used in the logging.
    *  \returns the servers identifier string name.
    */
   virtual const char *serverID() const;

protected:

   /** \brief This virtual method is called by run before the main loop handler.
    */
   virtual void construct();

   /** \brief This virtual method is called by by run when the server raises an exception or shuts down normally.
    */
   virtual void destruct();

   /** \brief A virtual method to indicate to the run() method whether or not the server should shutdown.
    *  \returns true if the server should shut down, else false.  Default implementation always returns false.
    */
   virtual bool shutdownServer() const;

   /** \brief A virtual method that is called every x mili seconds were x is setup by the run() 'waitTimeout' arguement.  This is useful for house keeping routines.
    */
   virtual void slackTimeHandler();

   /** \brief Used by the run() method to recieves a remote call from a client.
    *
    * Note the defaul implementation of this method should not need to be overloaded unless there is a very
    * specific case that the marshaler and transport cannot handle normally.
    *
    *  \param remoteSignature the rpc call unique signature to we know what call the client is trying to make.
    *  \param input the memory input stream the transport object will write while reading from the client.
    *  \param errorCode if an exception is raised, the errorCode will be set for the run method.
    *  \param errMsg if an exception is raised, the errMsg will be set.
    */
   virtual void receive(Mettle::Lib::String  &remoteSignature,
                        Mettle::IO::IStream  *input,
                        int32_t              &errorCode,
                        Mettle::Lib::String  *errMsg);

   /** \brief Used by the run() method to send a remote call result back to a client.
    *
    * Note the defaul implementation of this method should not need to be overloaded unless there is a very
    * specific case that the marshaler and transport cannot handle normally.
    *
    *  \param remoteSignature the rpc call unique signature we are sending back to the client.
    *  \param output the memory output stream the transport object will read from while writing back to the client..
    *  \param errorCode if an exception was raised, the errorCode will be sent to the client.
    *  \param errMsg if an exception was raised, the errMsg will be sent to the client.
    */
   virtual void send(const Mettle::Lib::String  &remoteSignature,
                           Mettle::IO::IStream  *output,
                     const int32_t               errorCode,
                           Mettle::Lib::String  *errorMessage);

   /** \brief Used by the run() method to wait for a connection from a client.
    *
    * This method use the servers transport object and waits based on the input arguement 'waitTimeout'.
    *
    *  \param timeOut the time in miliseconds to wait for a client connection.
    *  \returns true if a client has made a connection, false if not client has made a connection during the timeout.
    */
   virtual bool waitForConnection(const int32_t timeOut);

   /** \brief Maximum number of consecutive exception before the server blows.
    *
    *  \returns the max number exceptions, default is 1000
    */
   virtual int _maxConsecutiveExceptions();

   /** \brief Protected access to the marshaler object.
    */
   IServerMarshaler    *_marshaler;

   /** \brief Protected access to the transport object.
    */
   ITransport          *_trans;

   /** \brief Protected access to the logger object.
    */
   Mettle::Lib::Logger *_log;
};


}}


#endif
