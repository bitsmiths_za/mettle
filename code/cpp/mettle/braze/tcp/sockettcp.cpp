/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/tcp/sockettcp.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"

#include "mettle/io/memorystream.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#if defined (COMPILER_MSC)

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>

#elif defined (COMPILER_GNU)

   #if defined(OS_MS_WINDOWS)

      #define WIN32_LEAN_AND_MEAN
      #include <winsock2.h>

   #else

      #include <errno.h>
      #include <netdb.h>
      #include <unistd.h>
      #include <sys/types.h>
      #include <sys/socket.h>
      #include <sys/select.h>
      #include <sys/ioctl.h>
      #include <netinet/in.h>
      #include <arpa/inet.h>

      #if __GNUC__ > 2

         #include <netinet/tcp.h>

      #endif

      #define closesocket ::close

   #endif

#elif defined (COMPILER_XLC)

#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <sys/tiuser.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define closesocket ::close

#endif

using namespace Mettle::Lib;
using namespace Mettle::IO;

namespace Mettle { namespace Braze { namespace TCP {

SocketTcp::xSocketTcp::xSocketTcp(
   const char* filename,
   const int   line,
   const int   socketError,
   const char* fmt, ...) noexcept
           :xMettle(filename, line, "SocketTcp", "")
{
   va_list ap;

   va_start(ap, fmt);

   if (vsnprintf(errorMsg, sizeof(errorMsg) - 2, fmt, ap) == -1)
      errorMsg[sizeof(errorMsg) - 1] = 0;

   va_end(ap);
}


SocketTcp::xSocketTcp::xSocketTcp(const xSocketTcp &x) noexcept
           :xMettle(x)
{
}


SocketTcp::SocketTcp(const int timeout, const int retrys) noexcept
{
   _socketOwner = true;
   _socket      = -1;
   _timeout     = timeout;
   _retrys      = retrys;
}


SocketTcp::~SocketTcp() noexcept
{
   close(true);
}


void SocketTcp::close(const bool noThrow)
{
   if (_socket == -1)
      return;

   if (!_socketOwner)
   {
      _socket      = -1;
      _socketOwner = true;

      return;
   }

   if (closesocket(_socket) != 0)
   {
      _socket = -1;

      if (!noThrow)
         throwSocketError(__LINE__, "closesocket()");
   }

   _socket = -1;
}


void SocketTcp::setSocket(const int socket) noexcept
{
   _socket = socket;
}


bool SocketTcp::waitForRead(const int timeout, const int socket)
{
   struct timeval  timeOutVar;
   struct timeval *timeOutPtr;
   fd_set          fields;
   int             rc;

   if (timeout > 0)
   {
      timeOutVar.tv_sec  = (long) (timeout * 0.001); // divide by a 1000
      timeOutVar.tv_usec = (timeout % 1000) * 1000;
      timeOutPtr         = &timeOutVar;
   }
   else if (timeout == 0)
   {
      memset(&timeOutVar, 0, sizeof(timeOutVar));
      timeOutPtr= &timeOutVar;
   }
   else
      timeOutPtr = 0;

   FD_ZERO(&fields);
   FD_SET((unsigned) socket, &fields);

   rc = select(FD_SETSIZE, &fields, 0, 0, timeOutPtr);

   if (rc == 0)
      return false;

   if (rc < 0)
      throwSocketError(__LINE__, "select()");

   return FD_ISSET(socket, &fields) ? true : false;
}


bool SocketTcp::waitForWrite(const int timeout, const int socket)
{
   struct timeval  timeOutVar;
   struct timeval *timeOutPtr;
   fd_set          fields;
   int             rc;

   if (timeout > 0)
   {
      timeOutVar.tv_sec  = (long) (timeout * 0.001); // divide by a 1000
      timeOutVar.tv_usec = (timeout % 1000) * 1000;
      timeOutPtr         = &timeOutVar;
   }
   else if (timeout == 0)
   {
      memset(&timeOutVar, 0, sizeof(timeOutVar));
      timeOutPtr= &timeOutVar;
   }
   else
      timeOutPtr = 0;

   FD_ZERO(&fields);
   FD_SET((unsigned) socket, &fields);

   rc = select(FD_SETSIZE, 0, &fields, 0, timeOutPtr);

   if (rc == 0)
      return false;

   if (rc < 0)
      throwSocketError(__LINE__, "select()");

   return FD_ISSET(socket, &fields) ? true : false;
}


void SocketTcp::send(const Mettle::IO::MemoryStream* data,
                     const int                       socket)
{
   int bytesSent;

   if (_timeout && !waitForWrite(_timeout, socket))
      throw xSocketTcp(__FILE__, __LINE__, -1, "Timout on on socket send");

   uint32_t sendLen = htonl(data->_sizeUsed);

   if ((bytesSent = ::send(socket, (char* ) &sendLen, sizeof(sendLen), 0)) < 0)
      throwSocketError(__LINE__, "send(sendLen = %d)", sendLen);

   if (bytesSent < sizeof(sendLen))
      throw xSocketTcp(__FILE__, __LINE__, -1, "Unable to send entire socket buffer '%d'", sizeof(sendLen));

   if ((bytesSent = ::send(socket, (char* ) data->_memory, data->_sizeUsed, 0)) < 0)
      throwSocketError(__LINE__, "send(sizeUsed = %d)", data->_sizeUsed);

   if (bytesSent < (int) data->_sizeUsed)
      throw xSocketTcp(__FILE__, __LINE__, -1, "Unable to send entire socket buffer '%d'", data->_sizeUsed);
}


void SocketTcp::send(const void* data,
                     uint32_t    len,
                     const int   socket)
{
   int bytesSent;

   if (_timeout && !waitForWrite(_timeout, socket))
      throw xSocketTcp(__FILE__, __LINE__, -1, "Timout on on socket send");

   if ((bytesSent = ::send(socket, (char* ) data, len, 0)) < 0)
      throwSocketError(__LINE__, "send(len = %d)", len);

   if (bytesSent < (int) len)
      throw xSocketTcp(__FILE__, __LINE__, -1, "Unable to send entire socket buffer '%d'", len);
}


bool SocketTcp::receive(Mettle::IO::MemoryStream* data,
                        const int                 socket)
{
   uint32_t readLen;
   int      bytesRead;

   if (_timeout && !waitForRead(_timeout, socket))
      throw xSocketTcp(__FILE__, __LINE__, -1, "Timout on on socket receive");

   if ((bytesRead = recv(socket, (char* ) &readLen, sizeof(readLen), 0)) < 0)
      throwSocketError(__LINE__, "recv(readLen = %d)", sizeof(readLen));

   if (bytesRead == 0)
      return false;

   if (bytesRead != sizeof(readLen))
      throw xSocketTcp(__FILE__, __LINE__, -1, "Invalid packet read in Receive!");

   readLen = ntohl(readLen);

   data->addMemory(readLen);

   return receive(data->_memory, readLen, socket);
}


bool SocketTcp::receive(void          * data,
                        const uint32_t  len,
                        const int       socket)
{
   unsigned char   *ptr;
   uint32_t         totRead;
   int              bytesRead;

   ptr     = (unsigned char* ) data;
   totRead = 0;

   while (totRead < len)
   {
      if (_timeout)
      {
         int retrys;

         for (retrys = 0; retrys <= _retrys && !waitForRead(_timeout, socket); retrys++);

         if (retrys > _retrys)
            throw xSocketTcp(__FILE__, __LINE__, xMettle::SocketTimeout, "Timeout on socket receive - %d/%d!", retrys, _timeout);
      }

      if ((bytesRead = recv(socket, (char* ) ptr, len - totRead, 0)) < 0)
         throwSocketError(__LINE__, "recv(len = %d)", len - totRead);

      if (bytesRead == 0)
         return false;

      totRead += bytesRead;
      ptr     += bytesRead;
   }

   return true;
}


void SocketTcp::getHostAndIP(char* host, char* ip1, char* ip2)
{
   char             hostname[256];
   struct hostent  *hostinfo;

   if (gethostname(hostname, sizeof(hostname)))
      throwSocketError(__LINE__, "gethostname(%s)", host);

   if ((hostinfo = gethostbyname(hostname)) == 0)
      throwSocketError(__LINE__, "gethostbyname(%s)", host);

   strcpy(host, hostname);

   *ip1 = 0;
   *ip2 = 0;

   if (hostinfo->h_addr_list[0])
   {
      struct in_addr addr;

      memcpy(&addr, hostinfo->h_addr_list[0], sizeof(addr));
      strcpy(ip1, inet_ntoa(addr));
   }

   if (hostinfo->h_addr_list[1])
   {
      struct in_addr addr;

      memcpy(&addr, hostinfo->h_addr_list[1], sizeof(addr));
      strcpy(ip2, inet_ntoa(addr));
   }
}


void *SocketTcp::getHostInfo(const char* host)
{
   hostent *hostinfo;

   if (isdigit(host[0])) // eg '123.456.789.0'
   {
      if ((hostinfo = gethostbyname(host)) == 0)
      {
         unsigned long ipaddr = inet_addr(host);

         if (ipaddr == INADDR_NONE)
            throw xSocketTcp(__FILE__, __LINE__, -1, "Invalid tcp/ip addr - '%s'", host);

         if ((hostinfo = gethostbyaddr((char*) &ipaddr, sizeof(ipaddr), AF_INET)) == 0)
            throwSocketError(__LINE__, "gethostbyname and gethostbyaddr failed (%s)", host);
      }
   }
   else if (isalpha(host[0])) // eg "localhost"
   {
      if ((hostinfo = gethostbyname(host)) == 0)
         throwSocketError(__LINE__, "gethostbyname(%s)", host);
   }
   else
      throw xSocketTcp(__FILE__, __LINE__, -1, "Invalid host addr addr - '%s'", host);

   return (void *) hostinfo;
}


/*----------------------------------------------------------------------------
   Static
   Gets the network portno for the 'sockaddr_in.sin_port'
   Returns the portno for the request 'portno / service'
   port_no - can be a service ie 'quakeserver' or a protno ie '12345'
----------------------------------------------------------------------------*/
unsigned short SocketTcp::getPortNo(const char* port,
                                    const char* protocal)
{
   if (isalpha(port[0]))
   {
      servent *servernt_ptr;

      if ((servernt_ptr = getservbyname(port, protocal)) == NULL)
         throwSocketError(__LINE__, "getservbyname(%s)", port);

      return servernt_ptr->s_port;
   }

   if (isdigit(port[0]))
      return htons(atoi(port));

   throw xSocketTcp(__FILE__, __LINE__, -1, "Invalid port/service - '%s'", port);

   return 0;
}


void SocketTcp::throwSocketError(const int line, const char* fmt, ...)
{
   char    errorMsg[256];
   char    errorStr[256];
   int     errCode;

   va_list ap;

   va_start(ap, fmt);

   if (vsnprintf(errorMsg, sizeof(errorMsg) - 2, fmt, ap) == -1)
      errorMsg[sizeof(errorMsg) - 1] = 0;

   va_end(ap);

   errCode = xMettle::getOSError(errorStr, sizeof(errorStr));

   throw xSocketTcp(__FILE__, line, errCode , "%s - %s", errorMsg, errorStr);
}


void SocketTcp::initializeOSSockets()
{
#if defined (OS_MS_WINDOWS)
   WSAData          wsadata;

   if (WSAStartup(0x0101, &wsadata))
     throw xSocketTcp(__FILE__, __LINE__, -1, "Cannot initialize windows socket handler");
#endif
}


void SocketTcp::destroyOSSockets() noexcept
{
#if defined (OS_MS_WINDOWS)
   WSACleanup();
#endif
}

}}}
