/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#ifndef __METTLE_BRAZE_SOCKET_TCP_H_
#define __METTLE_BRAZE_SOCKET_TCP_H_

#include "mettle/lib/c99standard.h"
#include "mettle/lib/string.h"
#include "mettle/lib/xmettle.h"

struct sockaddr_in;

namespace Mettle
{

namespace IO
{
   class MemoryStream;
}

namespace Braze { namespace TCP {

class SocketTcp
{
public:

   class xSocketTcp : public Mettle::Lib::xMettle
   {
   public:

      xSocketTcp(const char* filename,
                 const int   line,
                 const int   errCode,
                 const char* fmt, ...) noexcept;

      xSocketTcp(const xSocketTcp &x) noexcept;
   };

   virtual ~SocketTcp() noexcept;

   static void getHostAndIP(char* host, char* ip1, char* ip2);

   static void* getHostInfo(const char* host);

   static unsigned short getPortNo(const char* port, const char* protocal="tcp");

   static void initializeOSSockets();

   static void destroyOSSockets() noexcept;

   void setSocket(const int socket) noexcept;

   virtual void close(const bool noThrow=false);

   static void throwSocketError(const int line, const char* fmt, ...);

   int _socket;
   int _timeout;
   int _retrys;

protected:

   SocketTcp(const int timeout, const int retrys) noexcept;

   bool waitForRead(const int timeout, const int socket);

   bool waitForWrite(const int timeout, const int socket);

   void send(const Mettle::IO::MemoryStream* data,
             const int                        socket);

   void send(const void* data,
             uint32_t    len,
             const int   socket);

   bool receive(Mettle::IO::MemoryStream* data,
                const int                 socket);

   bool receive(void*      data,
                uint32_t   len,
                const int  socket);

   bool _socketOwner;
};



}}}

#endif
