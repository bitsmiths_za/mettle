/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/tcp/sockettcpclient.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"

#include "mettle/io/memorystream.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#if defined (COMPILER_MSC)

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>

#elif defined (COMPILER_GNU)

   #if defined(OS_MS_WINDOWS)

      #define WIN32_LEAN_AND_MEAN
      #include <winsock2.h>

   #else

      #include <errno.h>
      #include <netdb.h>
      #include <unistd.h>
      #include <sys/types.h>
      #include <sys/socket.h>
      #include <sys/select.h>
      #include <sys/ioctl.h>
      #include <netinet/in.h>
      #include <arpa/inet.h>

      #if __GNUC__ > 2

         #include <netinet/tcp.h>

      #endif

      #define closesocket close

   #endif

#elif defined (COMPILER_XLC)

#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <sys/tiuser.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define closesocket close

#endif

using namespace Mettle::Lib;
using namespace Mettle::IO;

namespace Mettle { namespace Braze { namespace TCP {

SocketTcpClient::SocketTcpClient(const int timeout, const int retrys) noexcept
                :SocketTcp(timeout, retrys)
{
   _clientAddr = 0;
}


SocketTcpClient::~SocketTcpClient() noexcept
{
   destroy();
}


void SocketTcpClient::destroy() noexcept
{
   _FREE(_clientAddr)
}


void SocketTcpClient::initialize(const char* port,
                                 const char* host)
{
   close();
   destroy();

   _clientAddr = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));

   if (!_clientAddr)
      throw xMettle(__FILE__, __LINE__, "TcpClientSocket", "Error allocating memory for sockaddr_in - %d", sizeof(sizeof(struct sockaddr_in)));

   hostent *hostInfo;

   try
   {
      memset(_clientAddr, 0, sizeof(struct sockaddr_in));

      hostInfo = (hostent *) getHostInfo(host);

      _clientAddr->sin_port = getPortNo(port);

      _clientAddr->sin_family = hostInfo->h_addrtype;

      memcpy(&_clientAddr->sin_addr, hostInfo->h_addr, hostInfo->h_length);
   }
   catch (...)
   {
      close(true);
      throw;
   }
}


void SocketTcpClient::open()
{
   close();

   if ((_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
      throwSocketError(__LINE__, "socket()");

#if defined (TCP_NODELAY)
      {
         int opt = 1;

         if (setsockopt(_socket, IPPROTO_TCP, TCP_NODELAY, (char *) &opt, sizeof(opt)) != 0)
            throwSocketError(__LINE__, "setsocketopt(IPPROTO_TCP, TCP_NODELAY)");
      }
#endif

   if (connect(_socket, (struct sockaddr*) _clientAddr, sizeof(struct sockaddr_in)) != 0)
      throwSocketError(__LINE__, "connect()");
}


void SocketTcpClient::send(const Mettle::IO::MemoryStream* data)
{
   SocketTcp::send(data, _socket);
}


void SocketTcpClient::send(const void* data, uint32_t len)
{
   SocketTcp::send(data, len, _socket);
}


bool SocketTcpClient::receive(Mettle::IO::MemoryStream* data)
{
   return SocketTcp::receive(data, _socket);
}


bool SocketTcpClient::receive(void* data, uint32_t len)
{
   return SocketTcp::receive(data, len, _socket);
}

}}}
