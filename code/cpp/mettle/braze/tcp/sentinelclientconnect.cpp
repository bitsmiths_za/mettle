/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/tcp/sentinelclientconnect.h"

#include "mettle/lib/macros.h"

#include "mettle/io/bigendianreader.h"
#include "mettle/io/bigendianwriter.h"
#include "mettle/io/memorystream.h"

#include "mettle/braze/client.h"

#include "mettle/braze/tcp/transportclient.h"
#include "mettle/braze/tcp/sentinelclientmarshaler.h"

using namespace Mettle::Lib;
using namespace Mettle::IO;
using namespace Mettle::Braze;

namespace Mettle { namespace Braze { namespace TCP {

SentinelClientConnect::SentinelClientConnect()
{
   _inStream        = 0;
   _outStream       = 0;
   _clientTrans     = 0;
   _clientMarshaler = 0;
   _client          = 0;
   _reader          = 0;
   _writer          = 0;
}

SentinelClientConnect::~SentinelClientConnect()
{
   destroy();
}

void SentinelClientConnect::destroy()
{
   _DELETE(_writer)
   _DELETE(_reader)
   _DELETE(_inStream)
   _DELETE(_outStream)
   _DELETE(_client)
   _DELETE(_clientMarshaler)
   _DELETE(_clientTrans)
}

void SentinelClientConnect::initialize(const char *service)
{
   destroy();

   TransportClient::Settings settings("localhost", service, 5000);

   _clientTrans     = new TransportClient();

   _clientTrans->construct(&settings);

   _clientMarshaler = new SentinelClientMarshaler();
   _client          = new Mettle::Braze::Client(_clientTrans);
   _inStream        = new Mettle::IO::MemoryStream();
   _outStream       = new Mettle::IO::MemoryStream();
   _reader          = new BigEndianReader(_outStream);
   _writer          = new BigEndianWriter(_inStream);
}

void SentinelClientConnect::clearStreams()
{
   _inStream->purge();
   _outStream->purge();
}

void SentinelClientConnect::doCall(const char* signature)
{
   _client->send(_clientMarshaler,    signature, _inStream);
   _client->receive(_clientMarshaler, signature, _outStream);
}

}}}
