/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/tcp/transportserver.h"

#include "mettle/braze/tcp/sentinelserver.h"
#include "mettle/braze/tcp/tcpheader.h"
#include "mettle/braze/tcp/sockettcpserver.h"

#include "mettle/braze/rdc.h"

#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/string.h"

#include "mettle/io/bigendianreader.h"
#include "mettle/io/bigendianwriter.h"
#include "mettle/io/memorystream.h"

#include <string.h>
#include <stdlib.h>

#if defined (OS_MS_WINDOWS)

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>

#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#endif

using namespace Mettle::Lib;
using namespace Mettle::IO;
using namespace Mettle::Braze;

namespace Mettle { namespace Braze { namespace TCP {

#define MODULE "TCP::ServerTransport"

TransportServer::Settings::Settings(const char *service,
                                    const int   timeout,
                                    const int   retrys,
                                    const bool  managed,
                                    const int   sockFD)
               :ITransport::ISettings()
{
   if (managed && sockFD < 1)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Socket file descriptor is required if service is managed [service:%s]).", service);

   _service  = (char *) malloc(strlen(service) + 1);
   _timeout  = timeout;
   _retrys   = retrys;
   _managed  = managed;
   _sockFD   = sockFD;

   if (!_service)
      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Could not allocate memory for service string - '%s'", service);

   strcpy(_service, service);
}

TransportServer::Settings::~Settings() noexcept
{
   _FREE(_service)
}

TransportServer::TransportServer()
{
   _socket = 0;
}

TransportServer::~TransportServer() noexcept
{
   destruct();
}

void TransportServer::construct(ITransport::ISettings *settings)
{
   destruct();

   Settings *ts = (Settings *) settings;

   _socket = new SocketTcpServer(ts->_timeout, ts->_retrys);

   if (ts->_managed)
   {
     //SentinelServer::initializeSocketFromSentinel(ts->_service, _socket);
      _socket->initialize(ts->_sockFD);
   }
   else
   {
      _socket->initialize(ts->_service, 0, true);
   }
}


void TransportServer::destruct() noexcept
{
   _DELETE(_socket)
}


int32_t TransportServer::signature() const
{
   return 0x0BA5E7C9;
}


IReader *TransportServer::newReader(Mettle::IO::IStream *s) const
{
   return new BigEndianReader(s);
}


IWriter *TransportServer::newWriter(Mettle::IO::IStream *s) const
{
   return new BigEndianWriter(s);
}


RpcHeader *TransportServer::createHeader()
{
   return new TcpHeader();
}


void TransportServer::send(RpcHeader *header,
                           IStream   *data)
{
   MemoryStream  ms;
   TcpHeader    *head      = (TcpHeader *) header;
   unsigned int  lenToSend = 0;
   RDC           rdc;

   if (data)
      data->poop(&ms, 0);

   if (ms.size() > 0)
   {
      rdc.eat(&ms, ms.size(), ms.size());
      rdc.compress();

      head->messageSize    = rdc._length;
      head->compressedSize = rdc._compressedLength;

      if (rdc.state() == RDC::Compressed)
         lenToSend = rdc._compressedLength;
      else
         lenToSend = rdc._length;
   }

   MemoryStream msHead;

   BigEndianWriter  bigw(&msHead);

   head->_serialize(&bigw, head->_name());

   _socket->send(&msHead);

   if (lenToSend > 0)
      _socket->send(rdc._data->_memory, lenToSend);
}

void TransportServer::sendErrorMessage(RpcHeader  *head,
                                       String     *errMsg)
{
   MemoryStream     blob;
   BigEndianWriter  bigw(&blob);

   head->_serialize(&bigw, head->_name());

   _socket->send(&blob);

   blob.clear();

   bigw.writeStart("ServerException");
   bigw.write("ErrorMessage", *errMsg);
   bigw.writeEnd("ServerException");

   _socket->send(&blob);
}

void TransportServer::receiveHeader(RpcHeader *header)
{
   TcpHeader    *head = (TcpHeader *) header;
   MemoryStream  blob;

   if (!_socket->receive(&blob))
      throw xMettle(__FILE__, __LINE__, xMettle::ComsReceiveError, MODULE, "Header: Remote client connection terminated unexpectedly!");

   BigEndianReader  bigr(&blob);

   head->_deserialize(&bigr, head->_name());
}

void TransportServer::receive(RpcHeader *header,
                              IStream   *data)
{
   TcpHeader    *head = (TcpHeader *) header;
   MemoryStream  blob;

   if (head->compressedSize != 0 &&
       head->compressedSize != head->messageSize)
   {
      blob.addMemory(head->compressedSize);

      _socket->receive(blob._memory, head->compressedSize);

      RDC rdc;

      rdc.eat(&blob, head->messageSize, head->compressedSize);

      if (rdc.state() == RDC::Compressed)
         rdc.uncompress();

      blob.eat(rdc._data);
   }
   else
   {
      blob.addMemory(head->messageSize);

      _socket->receive(blob._memory, head->messageSize);
   }

   data->eat(&blob);
}


void TransportServer::receiveErrorMessage(RpcHeader   *head,
                                          String      *errMsg)
{
}

void TransportServer::terminate()
{
   if (_socket)
      _socket->closeAccepted();
}

void TransportServer::houseKeeping()
{
}

bool TransportServer::waitForConnection(const int32_t timeOut)
{
   return _socket->waitForConnection(timeOut);
}

String *TransportServer::clientAddress(String *buf)
{
   *buf  = _socket->clientAddress();

   return buf;
}

SocketTcpServer *TransportServer::getSocket()
{
   return _socket;
}


#undef MODULE

}}}
