/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/tcp/sockettcpserver.h"

#include "mettle/lib/c99standard.h"
#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"

#include "mettle/io/memorystream.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#if defined (COMPILER_MSC)

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>

#elif defined (COMPILER_GNU)

   #if defined(OS_MS_WINDOWS)

      #define WIN32_LEAN_AND_MEAN
      #include <winsock2.h>

   #else

      #include <errno.h>
      #include <netdb.h>
      #include <unistd.h>
      #include <sys/types.h>
      #include <sys/socket.h>
      #include <sys/select.h>
      #include <sys/ioctl.h>
      #include <netinet/in.h>
      #include <arpa/inet.h>

      #if __GNUC__ > 2

         #include <netinet/tcp.h>

      #endif

      #define closesocket ::close

   #endif

#elif defined (COMPILER_XLC)

#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <sys/tiuser.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define closesocket ::close

#endif

using namespace Mettle::Lib;
using namespace Mettle::IO;

namespace Mettle { namespace Braze { namespace TCP {

SocketTcpServer::SocketTcpServer(const int timeout, const int retrys) noexcept
                :SocketTcp(timeout, retrys)
{
   _acceptedSocket = -1;
}


SocketTcpServer::~SocketTcpServer() noexcept
{
   close(true);
}


void SocketTcpServer::initialize(const int socket)
{
   close();

   _socket      = socket;
   _socketOwner = false;
}


void SocketTcpServer::initialize(const char* port,
                                 const int   backlog,
                                 const bool  multiSocket)
{
   struct sockaddr_in serverAddr;

   close();

   try
   {
      memset(&serverAddr, 0, sizeof(serverAddr));

      serverAddr.sin_addr.s_addr = INADDR_ANY;
      serverAddr.sin_family      = AF_INET;
      serverAddr.sin_port        = getPortNo(port);

#if defined(OS_MS_WINDOWS)

      if ((_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
         throwSocketError(__LINE__, "socket(%s/%d)", port, backlog);

      ULONG nonblk = 1;

      ioctlsocket(_socket, FIONBIO, &nonblk);

#else

      if ((_socket = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0)) == -1)
         throwSocketError(__LINE__, "socket(%s/%d)", port, backlog);

#endif


#if defined (TCP_NODELAY)
      {
         int opt = 1;

         if (setsockopt(_socket, IPPROTO_TCP, TCP_NODELAY, (char *) &opt, sizeof(opt)) != 0)
            throwSocketError(__LINE__, "setsocketopt(IPPROTO_TCP, TCP_NODELAY)");
      }
#endif

      if (multiSocket)
      {
#if defined (SO_REUSEADDR)

         int addrOpt = 1;

         if (setsockopt(_socket, SOL_SOCKET, SO_REUSEADDR, (char *) &addrOpt, sizeof(addrOpt)) != 0)
            throwSocketError(__LINE__, "setsocketopt(SOL_SOCKET, SO_REUSEADDR)");
#endif

#if defined (SO_REUSEPORT)

         int portOpt = 1;

         if (setsockopt(_socket, SOL_SOCKET, SO_REUSEPORT, (char *) &portOpt, sizeof(portOpt)) != 0)
            throwSocketError(__LINE__, "setsocketopt(SOL_SOCKET, SO_REUSEPORT)");
#endif
      }

      if ((bind(_socket, (struct sockaddr*) &serverAddr, sizeof(serverAddr))) == -1)
         throwSocketError(__LINE__, "bind()");

      if (listen(_socket, backlog) == -1)
         throwSocketError(__LINE__, "listen()");
   }
   catch (...)
   {
      close(true);
      throw;
   }
}


void SocketTcpServer::close(const bool noThrow)
{
   closeAccepted(noThrow);
   SocketTcp::close(noThrow);
}


void SocketTcpServer::closeAccepted(const bool noThrow)
{
   if (_acceptedSocket == -1)
      return;

   if (closesocket(_acceptedSocket) != 0)
   {
      _acceptedSocket = -1;

      if (!noThrow)
         throwSocketError(__LINE__, "closesocket()");
   }

   _acceptedSocket = -1;
}


bool SocketTcpServer::waitForConnection(const int waitTimeout)
{
   if (!waitForRead(waitTimeout, _socket))
      return false;

   closeAccepted();

   if ((_acceptedSocket = accept(_socket, 0, 0)) < 0)
   {
#if defined (OS_MS_WINDOWS)

   #if defined (WINCE)

      if (WSAGetLastError() == WSAEWOULDBLOCK)

   #else

      if (errno == WSAEWOULDBLOCK)

   #endif

#else

      if (errno == EAGAIN ||
          errno == EWOULDBLOCK)

#endif
      {
         _acceptedSocket = -1;
         return false;
      }

      throwSocketError(__LINE__, "accept()");
   }


#if defined (TCP_NODELAY)
   {
      int opt = 1;

      if (setsockopt(_acceptedSocket, IPPROTO_TCP, TCP_NODELAY, (char *) &opt, sizeof(opt)) != 0)
         throwSocketError(__LINE__, "setsocketopt(IPPROTO_TCP, TCP_NODELAY)");
   }
#endif

   return true;
}


int SocketTcpServer::acceptedSocket() const noexcept
{
   return _acceptedSocket;
}


void SocketTcpServer::send(const Mettle::IO::MemoryStream* data)
{
   SocketTcp::send(data, _acceptedSocket);
}


void SocketTcpServer::send(const void* data, uint32_t len)
{
   SocketTcp::send(data, len, _acceptedSocket);
}


bool SocketTcpServer::receive(Mettle::IO::MemoryStream* data)
{
   return SocketTcp::receive(data, _acceptedSocket);
}


bool SocketTcpServer::receive(void* data, uint32_t len)
{
   return SocketTcp::receive(data, len, _acceptedSocket);
}


String SocketTcpServer::clientAddress()
{
   if (_acceptedSocket == -1)
      return String("No active client connection");

   struct sockaddr_in clientAddr;

   memset(&clientAddr, 0, sizeof(clientAddr));

#if defined (OS_MS_WINDOWS)
   int size = sizeof(clientAddr);
#else
   socklen_t size  = sizeof(clientAddr);
#endif

	if (getpeername(_acceptedSocket, (struct sockaddr*)&clientAddr, &size) == -1)
      return String().format("Count not determine client address");

   return String().format("%s", inet_ntoa(clientAddr.sin_addr));
}


}}}
