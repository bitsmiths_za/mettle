/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "mettle/braze/tcp/transportclient.h"

#include "mettle/braze/tcp/tcpheader.h"
#include "mettle/braze/tcp/sockettcpclient.h"

#include "mettle/braze/rdc.h"

#include "mettle/lib/macros.h"
#include "mettle/lib/platform.h"
#include "mettle/lib/string.h"

#include "mettle/io/bigendianreader.h"
#include "mettle/io/bigendianwriter.h"
#include "mettle/io/memorystream.h"

#include <string.h>
#include <stdlib.h>

#if defined (OS_MS_WINDOWS)

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>

#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#endif

using namespace Mettle::Lib;
using namespace Mettle::IO;
using namespace Mettle::Braze;

namespace Mettle { namespace Braze { namespace TCP {

#define MODULE "TCP::ClientTransport"

TransportClient::Settings::Settings(const char *host,
                                    const char *service,
                                    const int   timeout)
               :ITransport::ISettings()
{
   _host     = (char *) malloc(strlen(host) + 1);
   _service  = (char *) malloc(strlen(service) + 1);
   _timeout  = timeout;

   if (!_host || !_service)
   {
      _FREE(_host)
      _FREE(_service)

      throw xMettle(__FILE__, __LINE__, xMettle::OutOfMemory, MODULE, "Could not allocate memory for host/service strings - '%s'/'%s'", host, service);
   }

   strcpy(_host,    host);
   strcpy(_service, service);
}


TransportClient::Settings::~Settings() noexcept
{
   _FREE(_host)
   _FREE(_service)
}


TransportClient::TransportClient()
{
   _socket = 0;
}


TransportClient::~TransportClient() noexcept
{
   destruct();
}


void TransportClient::construct(ITransport::ISettings *settings)
{
   destruct();

   Settings *ts = (Settings *) settings;

   _socket = new SocketTcpClient(ts->_timeout);

   _socket->initialize(ts->_service, ts->_host);
}


void TransportClient::destruct() noexcept
{
   _DELETE(_socket)
}


int32_t TransportClient::signature() const
{
   return 0x0BA5E7C9;
}


IReader *TransportClient::newReader(Mettle::IO::IStream *s) const
{
   return new BigEndianReader(s);
}


IWriter *TransportClient::newWriter(Mettle::IO::IStream *s) const
{
   return new BigEndianWriter(s);
}


RpcHeader *TransportClient::createHeader()
{
   return new TcpHeader();
}


void TransportClient::send(RpcHeader *header,
                           IStream   *data)
{
   MemoryStream  ms;
   TcpHeader    *head      = (TcpHeader *) header;
   unsigned int  lenToSend = 0;
   RDC           rdc;

   if (data)
      data->poop(&ms, 0);

   if (ms.size() > 0)
   {
      rdc.eat(&ms, ms.size(), ms.size());
      rdc.compress();

      head->messageSize    = rdc._length;
      head->compressedSize = rdc._compressedLength;

      if (rdc.state() == RDC::Compressed)
         lenToSend = rdc._compressedLength;
      else
         lenToSend = rdc._length;
   }

   MemoryStream msHead;

   BigEndianWriter  bigErw(&msHead);

   head->_serialize(&bigErw, head->_name());

   _socket->open();
   _socket->send(&msHead);

   if (lenToSend > 0)
      _socket->send(rdc._data->_memory, lenToSend);
}


void TransportClient::sendErrorMessage(RpcHeader  *head,
                                       String     *errMsg)
{
}

void TransportClient::receiveHeader(RpcHeader *header)
{
   TcpHeader    *head = (TcpHeader *) header;
   MemoryStream  blob;

   if (!_socket->receive(&blob))
      throw xMettle(__FILE__, __LINE__, xMettle::ComsReceiveError, MODULE, "Header: Remote server connection terminated unexpectedly!");

   BigEndianReader  bigErw(&blob);

   head->_deserialize(&bigErw, head->_name());
}

void TransportClient::receive(RpcHeader *header,
                              IStream   *data)
{
   TcpHeader    *head = (TcpHeader *) header;
   MemoryStream  blob;

   if (head->compressedSize != 0 &&
       head->compressedSize != head->messageSize)
   {
      blob.addMemory(head->compressedSize);

      _socket->receive(blob._memory, head->compressedSize);

      RDC rdc;

      rdc.eat(&blob, head->messageSize, head->compressedSize);

      if (rdc.state() == RDC::Compressed)
         rdc.uncompress();

      blob.eat(rdc._data);
   }
   else
   {
      blob.addMemory(head->messageSize);

      _socket->receive(blob._memory, head->messageSize);
   }

   data->eat(&blob);
}

void TransportClient::receiveErrorMessage(RpcHeader   *head,
                                          String      *errMsg)
{
   MemoryStream     blob;
   BigEndianReader  bigErw(&blob);

   if (!_socket->receive(&blob))
      throw xMettle(__FILE__, __LINE__, xMettle::ComsReceiveError, MODULE, "Remote/Server connection terminated unexpectedly!");

   bigErw.readStart("ServerException");
   bigErw.read("ErrorMessage", *errMsg);
   bigErw.readEnd("ServerException");
}

void TransportClient::terminate()
{
}

void TransportClient::houseKeeping()
{
}

bool TransportClient::waitForConnection(const int32_t timeOut)
{
   return true;
}

String *TransportClient::clientAddress(String *buf)
{
   return buf;
}

SocketTcpClient *TransportClient::getSocket()
{
   return _socket;
}

#undef MODULE

}}}
