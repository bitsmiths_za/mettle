UNAME   = $(OS)

ifeq ($(UNAME), Windows_NT)
	MAKE        = make.exe
	SUP-SQL    = psql.exe -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U postgres -v ON_ERROR_STOP=1 -At
	SUP-DB-SQL = psql.exe -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U postgres -d $(TARGET_DB_NAME) -v ON_ERROR_STOP=1 -At
	SQL        = psql.exe -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U $(TARGET_DB_USER) -v ON_ERROR_STOP=1 -At
	DEL_CMD     = del
else
	MAKE        = make
	SUP-SQL    = psql -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U postgres -v ON_ERROR_STOP=1 -At
	SUP-DB-SQL = psql -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U postgres -d $(TARGET_DB_NAME) -v ON_ERROR_STOP=1 -At
	SQL        = psql -h $(TARGET_DB_HOST) -p $(TARGET_DB_PORT) -U $(TARGET_DB_USER) -v ON_ERROR_STOP=1 -At
	DEL_CMD     = rm -f
endif

TARGET_DB_NAME = mettle
TARGET_DB_USER = mettle
TARGET_DB_PASS = dev
TARGET_DB_HOST = 127.0.0.1
TARGET_DB_PORT = 5432

SQL_CMD_ARG   = "-c"
SQL_FILE_ARG  = "-f"
DB_TYPE       = postgresql
SCHEMAS       = mettle

METTLE_PATH  = ../genes/db/sqldef
METTLE_INDEX = $(patsubst %.index,%.run_index, $(sort $(wildcard $(METTLE_PATH)/$(DB_TYPE)/*.index)))
METTLE_TABLE = $(patsubst %.table,%.run_table, $(sort $(wildcard $(METTLE_PATH)/$(DB_TYPE)/*.table)))
METTLE_CONST = $(patsubst %.constraint,%.run_const, $(sort $(wildcard $(METTLE_PATH)/$(DB_TYPE)/*.constraint)))


.PHONY: help
help:
	@echo
	@echo "  Settings [host:$(TARGET_DB_HOST)/$(TARGET_DB_PORT), db:$(TARGET_DB_NAME) user:$(TARGET_DB_USER)]"
	@echo
	@echo "  Usage: localdb.postgres.makefile [target]"
	@echo "  ----------------------------------------"
	@echo "  rebuild         : rebuild the mettle database"
	@echo
	@echo "  -- Utility --"
	@echo
	@echo "  create-user     : create the user if it is missing"
	@echo "  create-db       : create the database if it is missing"
	@echo "  create-schemas  : create required schemas"
	@echo "  drop-user       : drop the user, won't work if has dependencies"
	@echo "  drop-db         : drops the db "
	@echo
	@echo "  help            : this help"
	@echo


.PHONY: drop-user
drop-user:role_count = $(shell $(SUP-SQL) -c "SELECT 1 FROM pg_roles WHERE rolname='$(TARGET_DB_USER)'")
drop-user:
	$(if $(filter-out $(role_count), 1),\
		@echo "  role [$(TARGET_DB_USER)] does not exist"\
	,\
		@echo "  dropping role [$(TARGET_DB_USER)]" ;\
		$(SUP-SQL) -c "DROP OWNED BY \"$(TARGET_DB_USER)\"" ;\
		$(SUP-SQL) -c "DROP USER \"$(TARGET_DB_USER)\"" \
	)


.PHONY: create-user
create-user:role_count = $(shell $(SUP-SQL) -c "SELECT 1 FROM pg_roles WHERE rolname='$(TARGET_DB_USER)'")
create-user:
	$(if $(filter-out $(role_count), 1),\
		@echo "  creating role [$(TARGET_DB_USER)]" ;\
		$(SUP-SQL) -c "CREATE USER \"$(TARGET_DB_USER)\" WITH PASSWORD '$(TARGET_DB_PASS)'" \
	,\
		@echo "  role [$(TARGET_DB_USER)] already exists"\
    )


.PHONY: create-schemas
create-schemas:
	for sname in $(SCHEMAS); do\
		$(SQL) $(SQL_CMD_ARG) "CREATE SCHEMA $$sname;" $(SQL_DEST_ARG);\
	done


.PHONY: drop-db
drop-db:db_count = $(shell $(SUP-SQL) -c "SELECT 1 FROM pg_database WHERE datname='$(TARGET_DB_NAME)'")
drop-db:
	$(if $(filter-out $(db_count), 1),\
		@echo "  database [$(TARGET_DB_NAME)] does not exist" \
	,\
		@echo "  dropping database [$(TARGET_DB_NAME)]" ;\
		$(SUP-SQL) -c "DROP DATABASE \"$(TARGET_DB_NAME)\"" \
    )


.PHONY: create-db
create-db:db_count = $(shell $(SUP-SQL) -c "SELECT 1 FROM pg_database WHERE datname='$(TARGET_DB_NAME)'")
create-db: create-user
	$(if $(filter-out $(db_count), 1),\
		@echo "  creating database [$(TARGET_DB_NAME)]" ;\
		$(SUP-SQL) -c "CREATE DATABASE \"$(TARGET_DB_NAME)\" ENCODING \"UTF-8\" OWNER \"$(TARGET_DB_USER)\"" ;\
		$(SUP-SQL) -c "GRANT ALL PRIVILEGES ON DATABASE \"$(TARGET_DB_NAME)\" TO \"$(TARGET_DB_USER)\"" ;\
		$(SUP-DB-SQL) -c "ALTER SCHEMA public OWNER TO \"${TARGET_DB_USER}\"" ;\
	,\
		@echo "  database [$(TARGET_DB_NAME)] already exists"\
    )


.PHONY: build-mettle-tables
build-mettle-tables: $(METTLE_TABLE) $(METTLE_CONST) $(METTLE_INDEX)
	$(DEL_CMD) *.log


.PHONY: rebuild
rebuild: drop-db create-db build-mettle-tables


%.run_index: %.index
	$(SQL) $(SQL_FILE_ARG) $< $(SQL_DEST_ARG)

%.run_table: %.table
	$(SQL) $(SQL_FILE_ARG) $< $(SQL_DEST_ARG)

%.run_const: %.constraint
	$(SQL) $(SQL_FILE_ARG) $< $(SQL_DEST_ARG)
