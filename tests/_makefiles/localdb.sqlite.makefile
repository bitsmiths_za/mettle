UNAME   = $(OS)

ifeq ($(UNAME), Windows_NT)
	MAKE        = make.exe
	SQL        = sqlite3.exe -bail $(TARGET_DB)
	DEL_CMD     = del
else
	MAKE        = make
	SQL        = sqlite3 -bail $(TARGET_DB)
	DEL_CMD     = rm -f
endif

TARGET_DB = $(LOCAL_PATH)/tests/mettle_sqlite3.db

SQL_CMD_ARG   = "-c"
SQL_FILE_ARG  = "-f"
DB_TYPE       = sqlite

METTLE_PATH  = $(LOCAL_PATH)/tests/genes/db/sqldef
METTLE_INDEX = $(patsubst %.index,%.run_index, $(sort $(wildcard $(METTLE_PATH)/$(DB_TYPE)/*.index)))
METTLE_TABLE = $(patsubst %.table,%.run_table, $(sort $(wildcard $(METTLE_PATH)/$(DB_TYPE)/*.table)))
METTLE_CONST = $(patsubst %.constraint,%.run_const, $(sort $(wildcard $(METTLE_PATH)/$(DB_TYPE)/*.constraint)))


.PHONY: help
help:
	@echo
	@echo "  Settings [host:$(TARGET_DB_HOST)/$(TARGET_DB_PORT), db:$(TARGET_DB_NAME) user:$(TARGET_DB_USER)]"
	@echo
	@echo "  Usage: localdb.sqlite.makefile [target]"
	@echo "  ----------------------------------------"
	@echo "  rebuild         : rebuild the mettle database"
	@echo
	@echo "  -- Utility --"
	@echo
	@echo "  create-db       : create the database if it is missing"
	@echo "  drop-db         : drops the db "
	@echo
	@echo "  help            : this help"
	@echo


.PHONY: drop-db
drop-db:
	$(DEL_CMD) $(TARGET_DB)


.PHONY: create-db
create-db: drop-db
	touch $(TARGET_DB)


.PHONY: build-mettle-tables
build-mettle-tables: $(METTLE_TABLE) $(METTLE_CONST) $(METTLE_INDEX)
	$(DEL_CMD) *.log


.PHONY: rebuild
rebuild: drop-db create-db build-mettle-tables


%.run_index: %.index
	$(SQL) ".read $(<)" ""

%.run_table: %.table
	$(SQL) ".read $(<)" ""

%.run_const: %.constraint
	$(SQL) ".read $(<)" ""
