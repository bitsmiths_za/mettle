# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2022 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os.path
import pytest
import shutil

import mettle.genes

pytestmark = [ pytest.mark.generators ]


def test_check_projects(ut_genes_data):
    """
    Enusre all the test projects exists.
    """
    assert os.path.exists(ut_genes_data['root-path'])
    assert os.path.exists(ut_genes_data['genes-path'])

    for item, proj in ut_genes_data['projects'].items():
        assert os.path.exists(proj)


def test_remove_generated_paths(ut_genes_data):
    """
    Removes all the generated paths for a fresh build.
    """
    for item, path in ut_genes_data['gen-paths'].items():
        if not os.path.exists(path):
            continue

        shutil.rmtree(path)


def test_run_generators(ut_genes_data):
    """
    Runs the generatorss for each project.
    """
    for item, proj in ut_genes_data['projects'].items():
        pparts = os.path.split(proj)

        os.chdir(pparts[0])

        assert 0 == os.system(f'{ut_genes_data["gen-cmd"]} {pparts[1]}')


def test_check_generated_paths(ut_genes_data):
    for item, path in ut_genes_data['gen-paths'].items():
        assert os.path.exists(path)

    sqldef    = ut_genes_data['gen-paths']['sqldef']
    db_types  = [ 'postgresql', 'sqlite' ]
    tbl_files = [ 'AllTypes', 'Child', 'Parent', 'ParentType' ]
    ext_types = [ '.drop', '.index', '.sql', '.constraint', '.table' ]

    for dbt in db_types:
        assert os.path.exists(os.path.join(sqldef, dbt))

        for fle in tbl_files:
            for ext in ext_types:
                assert os.path.exists(os.path.join(sqldef, dbt, f'{fle}{ext}'))


    py_braze  = os.path.join(ut_genes_data['tests-path'], '_test_python3', 'braze')
    exp_files = ( 'addr_type_couplet.py', 'btest_arrays.py', 'btest_auth.py', 'btest_dav.py', 'btest_db_coupl_ref.py',
                  'btest_db_rec.py', 'btest_memory_block.py', 'btest_rec.py', '__init__.py', 'numbers_couplet.py',
                  'status_couplet.py', 'test_braze_async_server_interface.py', 'test_braze_async_server_marshaler.py',
                  'test_braze_client_interface.py', 'test_braze_client_marshaler.py', 'test_braze_client_server_impl.py',
                  'test_braze_server_interface.py', 'test_braze_server_marshaler.py' )

    for ef in exp_files:
        assert os.path.exists(os.path.join(py_braze, ef))


def test_sql_build(ut_genes_data):
    sqldef = ut_genes_data['gen-paths']['sqldef']
    sqlfile = os.path.join(sqldef, '_all.sql')

    assert os.path.exists(sqldef)

    clean_args = [ '-c', sqlfile ]
    assert 0 == mettle.genes.cli_sql_build(clean_args)

    with open(sqlfile, 'rt') as fh:
        assert '' == fh.read()

    onefile = os.path.join(sqldef, 'postgresql', 'AllTypes.sql')
    onefile_args = [ '-f', onefile, sqlfile ]

    assert 0 == mettle.genes.cli_sql_build(onefile_args)

    with open(sqlfile, 'rt') as fh:
        assert fh.read()

    clean_args = [ '-c', sqlfile ]
    assert 0 == mettle.genes.cli_sql_build(clean_args)

    for ext in [ '*.table', '*.constraint', '*.index' ]:
        allfile = os.path.join(sqldef, 'postgresql', ext)

        all_args = [ '-f', allfile, sqlfile ]
        assert 0 == mettle.genes.cli_sql_build(all_args)

    with open(sqlfile, 'rt') as fh:
        assert fh.read()
