import { TappPage } from './app.po';

describe('tapp App', () => {
  let page: TappPage;

  beforeEach(() => {
    page = new TappPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
