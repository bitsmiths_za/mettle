#!/usr/bin/env node

const esbuild = require('esbuild');

esbuild.build({
    entryPoints: ['src/main.ts'],
    bundle: true,
    minify: false,
    treeShaking: false,
    platform: 'browser',
    logLevel: 'info',
    outdir: 'dist/testapp',
    assetNames: 'assets/[name]',
    chunkNames: '[ext]/[name]',
    loader: {
        ".html": "text",
        ".css": "text"
    },
    plugins: [
    ],
}).catch(() => process.exit(1))
