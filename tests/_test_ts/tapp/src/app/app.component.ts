/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

import { Component } from '@angular/core';

import { MettlePromise } from '@bitsmiths/mettle-lib';
import { Ref }           from '@bitsmiths/mettle-lib';
import { xMettle }       from '@bitsmiths/mettle-lib';
import {
  List,
  Int64List,
} from '@bitsmiths/mettle-braze';

import {
  ObjWriter,
  ObjReader,
  JSONUtil,
} from '@bitsmiths/mettle-io';

import { TestBrazeService } from './testbraze.service';

import {
  bTestAuth,
  bTestDav,
  bTestRec,
} from './mettlebraze/testBrazeStructs';

import { tChild }         from './mettledb/tables/tchild';
import { tParent }        from './mettledb/tables/tparent';
import { iParentGetCode } from './mettledb/tables/iparentGetCode';
import { oParentGetCode } from './mettledb/tables/oparentGetCode';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(public _bs: TestBrazeService) {
  }

  SuccessFunc(method: string, data: any) {
    console.log("-- " + method + " -- OK");
    console.log(data);
  }

  ErrorFunc(method: string, err: string) {
    console.log("-- " + method + " -- ERROR");
    console.log(err);
  }

  ut_TestCallVoid() {
    this._bs.getClient().CallVoid()
      .then(()=>{
        this.SuccessFunc('CallVoid', undefined);
      })
      .catch((err: xMettle)=>{
        this.ErrorFunc('CallVoid', err.getMsg());
      });
  }

  ut_TestCallStrings() {
    this._bs.getClient().CallStrings('hello world Stévie', 'foo')
      .then((res: any)=>{
        this.SuccessFunc('CallStrings', res);
      })
      .catch((err: xMettle)=>{
        this.ErrorFunc('CallStrings', err.getMsg());
      });
  }

  ut_TestCallDoubles() {
    this._bs.getClient().CallDoubles(123.456789, 0.0)
      .then((res: any)=>{
        this.SuccessFunc('CallDoubles', res);
      })
      .catch((err: xMettle)=>{
        this.ErrorFunc('CallDoubles', err.getMsg());
      });
  }

  ut_TestCallDates() {
    let indt  = new Date();
    let indtm = new Date();

    this._bs.getClient().CallDates(indt, indtm, undefined, indtm)
      .then((res: any)=>{
        this.SuccessFunc('CallDates', res);
      })
      .catch((err: xMettle)=>{
        this.ErrorFunc('CallDates', err.getMsg());
      });
  }

  ut_TestBrazeLists() {
    let testRecList = new List<bTestRec>(bTestRec);

    testRecList.push(new bTestRec()._initDeft(1,   2,  3, 4,  "another string",     11.34, new Date("2017-05-03"), undefined));
    testRecList.push(new bTestRec()._initDeft(10, 20, 30, 40, "yet another string", 16.89, new Date("2017-05-04"), undefined));

    let testRecList2 = new List<bTestRec>(bTestRec);

    testRecList2.push(new bTestRec()._initDeft(5,   6,  7,  8, "another string 2",     16.39, new Date("2017-05-07"), undefined));
    testRecList2.push(new bTestRec()._initDeft(14, 24, 34, 44, "yet another string 2", 19.41, new Date("2017-05-08"), undefined));

    this._bs.getClient().CallBrazeLists(testRecList, testRecList)
      .then((res: any) => {
        this.SuccessFunc('CallBrazeLists', res);
      })
      .catch((err: xMettle)=>{
        this.SuccessFunc('CallBrazeLists', err);
      });
  }

  ut_TestIntegerLists() {
    let inInt    = new Int64List();
    let inoutInt = new Int64List();

    inInt.push(-42)
    inInt.push(0)
    inInt.push(42)

    inoutInt.push(-8012345)
    inoutInt.push(0)
    inoutInt.push(8012345)

    this._bs.getClient().CallIntLists(inInt, inoutInt)
      .then((res: any) => {
        this.SuccessFunc('CallBrazeLists', res);
      })
      .catch((err: xMettle)=>{
        this.SuccessFunc('CallBrazeLists', err);
      });
  }

  ut_TestNullIntegers() {
    let recnn = new iParentGetCode()._initDeft(3);

    this._bs.getClient().CallDbNullInt(recnn)
      .then((res: any) => {
        this.SuccessFunc('CallDbNullInt', res);
      })
      .catch((err: xMettle)=>{
        this.SuccessFunc('CallDbNullInt', err);
      });

    let recnull = new iParentGetCode()._initDeft(undefined);

    this._bs.getClient().CallDbNullInt(recnull)
      .then((res: any) => {
        this.SuccessFunc('CallDbNullInt', res);
      })
      .catch((err: xMettle)=>{
        this.ErrorFunc('CallDbNullInt', err.getMsg());
      });
  }

  ut_TestDbStructs() {
    let pirec  = new tParent()._initDeft(1, 2, "parent short", "parent fullname", 10.3, 133242, new Date("2017-05-02"), false, '', 'usr');
    let pilist = new List<tParent>(tParent);

    pilist.push(pirec);
    pilist.push(new tParent()._initDeft(2, 3, "Mary", "Sue", 10.3, 133242, new Date("2017-12-02"), true, undefined, 'usr'));

    this._bs.getClient().CallDb(pirec, pilist)
      .then((res: any) => {
        this.SuccessFunc('ut_TestDbStructs', res);
      })
      .catch((err: xMettle)=>{
        this.ErrorFunc('ut_TestDbStructs', err.getMsg());
      });
  }

   ut_TestBrazeAuth() {
    let auth : bTestAuth = new bTestAuth()._initDeft('test.user', 'green');

    this._bs.getClient().CallAuthTest(auth, 'test.user')
      .then((res: any)=>{
        this.SuccessFunc('ut_TestDbStructs <Success>', res);
      })
      .catch((err: xMettle)=>{
        this.ErrorFunc('ut_TestDbStructs <Success>', err.getMsg());
      });

    let auth_fail: bTestAuth    = new bTestAuth()._initDeft('test.user', 'red');

    this._bs.getClient().CallAuthTest(auth_fail, 'test.user')
      .then((res: any)=>{
        this.ErrorFunc('ut_TestDbStructs <Failure>', '<NOT SUPPOSED TO SUCCEED!>');
      })
      .catch((err: xMettle)=>{
        this.SuccessFunc('ut_TestDbStructs <failure>', undefined);
      });
  }

  ut_TestObjSerializers() {
    let testRec = new tParent()._initDeft(1, 2, "parent short", "parent fullname", 10.3, 133242, new Date("2017-05-02"), true, '', 'usr');

    console.log("  -- tParent created: ", testRec);
    console.log("       - stringify: ", JSON.stringify(testRec))

    let x: string = '';

    try {
      x = JSONUtil.convertToJSON(testRec);

      console.log("       - converted: ", x)

      let obj = JSON.parse(x);

      console.log("  - parsed: ", obj);

      let newRec = new tParent();

      JSONUtil.convertToObj(x, newRec);

      console.log("-- ut_TestObjSerializers -- OK");
      console.log(newRec);
    }
    catch (x) {
      console.log("-- ut_TestObjSerializers -- ERROR");
      console.log(x);
      return;
    }
  }
}
