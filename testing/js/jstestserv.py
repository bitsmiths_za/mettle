#!/usr/bin/python3

#
# Copyright (C) 2017 Bitsmiths (Pty) Ltd.  All rights reserved.
#

import sys
import os
import os.path

from http.server import HTTPServer
from http.server import BaseHTTPRequestHandler

import mimetypes
import logging
import json
import traceback

sys.path.append(os.path.join('..', '..', 'code', 'python3'))
sys.path.append(os.path.join('..', 'python3'))

import mettle

from mettlebraze.TestBrazeServerMarshaler import TestBrazeServerMarshaler

from mettle.braze.http.TransportServer         import TransportServer
from mettle.braze.http.TransportServerSettings import TransportServerSettings

from TestBrazeServerImpl import TestBrazeServerImpl

g_trans  = TransportServer()
g_impl   = TestBrazeServerImpl()
g_marsh  = TestBrazeServerMarshaler(g_impl)
g_server = mettle.braze.ServerMultiMarsh([g_marsh], g_trans)

g_trans.construct(TransportServerSettings())


class TestHandler(BaseHTTPRequestHandler):  # since it can go back our of the path the get files


    def __init__(self, *args):
        BaseHTTPRequestHandler.__init__(self, *args)


    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()


    def do_GET(self):
        """Respond to a GET request."""
        logging.getLogger().info("handling get start (%s)" % self.path)
        print("handling get start (%s)" % self.path)

        try:

            if self.path == "/":
                self.path = "/unittestBraze.html"

            if self.path == '/favicon.ico':
                logging.getLogger().debug("404'ing favicon")
                print("404'ing favicon")
                self.send_response(404)
                return

            if self.path == '/code/js/mettle.js':
                logging.getLogger().debug("returning mettle js")
                print("returning mettle js")
                self.send_response(200)
                self.send_header("Content-type", mimetypes.types_map['.js'])
                self.end_headers()

                with open('../../code/js/mettle.js', 'r') as fh:
                    self.wfile.write(fh.read().encode('utf-8'))

            elif self.path == '/mettledb/testdb.js':
                logging.getLogger().debug("returning testdb.js")
                print("returning testdb.js")
                self.send_response(200)
                self.send_header("Content-type", mimetypes.types_map['.js'])
                self.end_headers()

                with open('mettledb/testdb.js', 'r') as fh:
                    self.wfile.write(fh.read().encode('utf-8'))

            elif self.path == '/mettlebraze/testbrazeStructs.js':
                logging.getLogger().debug("returning testbrazeStructs.js")
                print("returning testbrazeStructs.js")
                self.send_response(200)
                self.send_header("Content-type", mimetypes.types_map['.js'])
                self.end_headers()

                with open('mettlebraze/testbrazeStructs.js', 'r') as fh:
                    self.wfile.write(fh.read().encode('utf-8'))

            elif self.path == '/mettlebraze/testbrazeClient.js':
                logging.getLogger().debug("returning testbrazeClient.js")
                print("returning testbrazeClient.js")
                self.send_response(200)
                self.send_header("Content-type", mimetypes.types_map['.js'])
                self.end_headers()

                with open('mettlebraze/testbrazeClient.js', 'r') as fh:
                    self.wfile.write(fh.read().encode('utf-8'))

            elif self.path == "/unittestBraze.html":
                logging.getLogger().debug("returning unittestBraze.html")
                print("returning unittestBraze.html")
                self.send_response(200)
                self.send_header("Content-type", "text/html")
                self.end_headers()

                with open('unittestBraze.html', 'r') as fh:
                    self.wfile.write(fh.read().encode('utf-8'))

            # self.send_response(200)
            # self.send_header("Content-type", "text/html")
            # self.end_headers()

            # self.wfile.write(page.encode("utf8"))

        except Exception as e:
            logging.getLogger().exception(e)

        logging.getLogger().info("handling get done")


    def do_POST(self):
        """!
        Respond to a POST request.
        """
        print("do_POST - Start (%s)" % self.path)
        print('  request:%s' % str(self.request))

        try:
            self.data_string = self.rfile.read(int(self.headers['Content-Length']))
            data             = json.loads(self.data_string.decode('utf-8'))

            print('-DATA IN----------------------')
            print(str(data))
            print('------------------------------')

            if type(data) != list:
                print('ERROR - List type expected from json data, got [%s]' % (str(type(data))))

            g_trans.setStreamList(data)

            rc = g_server.serviceRPC()

            if rc != 0:
                print('... call failed... todo: error handling/throws error 500 etcs here.')

            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()

            retData = json.dumps(g_trans.getStreamList())

            print('-DATA OUT----------------------')
            print(str(retData))
            print('------------------------------')

            self.wfile.write(retData.encode('utf-8'))

        except Exception as e:
            print('Exception caught [error:%s, trace:%s]' % (str(e), traceback.format_exc()))

        print("do_POST - Done")




if __name__ == '__main__':

    httpd = None

    try:
        mimetypes.init()

        httpd = HTTPServer(('', 9999), TestHandler)

        logging.basicConfig(filename='jstestserv.log', level=logging.DEBUG, format='%(asctime)s %(message)s')

        logging.getLogger().info('Started httpserver on port 9999')
        print('Started httpserver on port 9999')

        # Wait forever for incoming http requests
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('^C received, shutting down the server')
        httpd.socket.close()
