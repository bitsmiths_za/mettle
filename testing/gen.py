#!/usr/bin/python3

# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import sys
import os.path

sys.path.append(os.path.join('..', 'code', 'python3'))

import mettle.mfgen


def genCPP(mfgen):
    cppGen  = mfgen.getGenerator('cpp')
    libPath = 'bin/cpp'

    cppGen._osCompiler['win']   = 'mingw'
    cppGen._osCompiler['linux'] = 'gnu'

    cflags  = []
    incl    = ['code/cpp', 'testing/cpp']
    cppLibs = [
        '/'.join([libPath, 'libmettle_braze_tcp']),
        '/'.join([libPath, 'libmettle_util']),
        '/'.join([libPath, 'libmettle_xml']),
        '/'.join([libPath, 'libmettle_json']),
        '/'.join([libPath, 'libmettle_braze_sharedobj']),
        '/'.join([libPath, 'libmettle_braze']),
        '/'.join([libPath, 'libmettle_db']),
        '/'.join([libPath, 'libmettle_io']),
        '/'.join([libPath, 'libmettle_lib']),
    ]
    sharedLibs = [
        'gnu|pthread',
        'gnu|uuid',
        'gnu|dl',
    ]
    extLibs = [
        # 'gnu|uuid.so',  # for the guid generation
        'mingw|' + '/'.join(['externs', 'mingw', 'lib', 'libws2_32.a']),
    ]

    mfpath   = '/'.join(['testing', 'cpp', 'mettlelib'])

    mfgen.gen('cpp', {
        'makefile.dir' : mfpath,
        'dest.out'     : 'exe',
        'dest.name'    : 'test_mettle_lib',
        'dest.descr'   : 'Unittest Mettle Library',
        'srcPath'      : ['/'.join([mfpath, '*'])],
        'incPath'      : incl,
        'cflags'       : cflags,
        'ilibs'        : cppLibs,
        'slibs'        : sharedLibs,
        'elibs'        : extLibs,
    })

    mfpath   = '/'.join(['testing', 'cpp', 'mettleio'])

    mfgen.gen('cpp', {
        'makefile.dir' : mfpath,
        'dest.out'     : 'exe',
        'dest.name'    : 'test_mettle_io',
        'dest.descr'   : 'Unittest Mettle IO',
        'srcPath'      : ['/'.join([mfpath, '*'])],
        'incPath'      : incl,
        'cflags'       : cflags,
        'ilibs'        : cppLibs,
        'slibs'        : sharedLibs,
        'elibs'        : extLibs,
    })

    mfpath = '/'.join(['cpp', 'mettledb', 'test_sqlite_mettle'])

    if not os.path.exists(mfpath.replace('/', os.sep)):
        os.mkdir(mfpath.replace('/', os.sep))

    mfpath = '/'.join(['testing', 'cpp', 'mettledb', 'test_sqlite_mettle'])

    mfgen.gen('cpp', {
        'makefile.dir' : mfpath,
        'dest.out'     : 'exe',
        'dest.name'    : 'test_sqlite_mettle',
        'dest.descr'   : 'Unittest MettleDB SQLite',
        'srcPath'      : [
            '/'.join([mfpath, '../*']),
            'testing/cpp/mettledb/tables/*',
            'testing/cpp/mettledb/dao/sqlite/*',
        ],
        'incPath'      : incl + ['testing/cpp/mettledb'],
        'cflags'       : cflags + ['-DMETTLEDB_SQLITE'],
        'ilibs'        : ['/'.join([libPath, 'libmettle_db_sqlite3'])] + cppLibs,
        'slibs'        : sharedLibs,
        'elibs'        : extLibs,
    })

    mfpath = '/'.join(['cpp', 'mettledb', 'test_postgres_mettle'])

    if not os.path.exists(mfpath.replace('/', os.sep)):
        os.mkdir(mfpath.replace('/', os.sep))

    mfpath = '/'.join(['testing', 'cpp', 'mettledb', 'test_postgres_mettle'])

    mfgen.gen('cpp', {
        'makefile.dir' : mfpath,
        'dest.out'     : 'exe',
        'dest.name'    : 'test_postgres_mettle',
        'dest.descr'   : 'Unittest MettleDB PostgreSQL 9',
        'srcPath'      : [
            '/'.join([mfpath, '../*']),
            'testing/cpp/mettledb/tables/*',
            'testing/cpp/mettledb/dao/postgresql/*',
        ],
        'incPath'      : incl + ['testing/cpp/mettledb'],
        'cflags'       : cflags + ['-DMETTLEDB_POSTGRESQL9'],
        'ilibs'        : ['/'.join([libPath, 'libmettle_db_postgresql9'])] + cppLibs,
        'slibs'        : sharedLibs + ['pq', 'crypt'],
        'elibs'        : extLibs,
    })


# ------------------------------------------------------------------------------
# main
# ------------------------------------------------------------------------------
if __name__ == '__main__':
    mfgen    = mettle.mfgen.generator('..')

    mfgen.initialize(['testing.yml'], True)

    genCPP(mfgen)

    mfgen.showWarnings()
