/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "mettle/lib/datetime.h"
#include "mettle/lib/string.h"

using namespace Mettle::Lib;

#define testSuite testDateTime

LT_BEGIN_SUITE(testSuite)

void set_up()
{
}

void tear_down()
{
}

LT_END_SUITE(testSuite)

LT_BEGIN_TEST(testSuite, test_Parse)

   DateTime dtp;
   DateTime dtc;

   dtc.assign(2016, 8, 12, 17, 5, 30);

   dtp.parse("20160812170530", "%Y%m%d%H%M%S");
   LT_CHECK_EQ(dtp, dtc)

   dtp.parse("2016-08-12 17:05:30", DateTime::stdDateTimeFormat());
   LT_CHECK_EQ(dtp, dtc)


LT_END_TEST(test_Parse)


LT_BEGIN_TEST(testSuite, test_Format)

   DateTime dtc;
   char     jotter[128];

   dtc.assign(2016, 8, 12, 17, 5, 30);

   dtc.format(jotter, "%Y%m%d%H%M%S");
   LT_CHECK_EQ(strcmp(jotter, "20160812170530"), 0)

   dtc.format(jotter, DateTime::stdDateTimeFormat());
   LT_CHECK_EQ(strcmp(jotter, "2016-08-12 17:05:30"), 0)


LT_END_TEST(test_Format)


LT_BEGIN_TEST(testSuite, test_TimeZone)

   int8_t  tzh = 2;
   int8_t  tzm = 0;
   int32_t tzo;
   char   *tza;
   char    jot[32];

   DateTime::setLocalTZ(tzh, tzm);

   tzo = DateTime::getLocalTZ();
   LT_CHECK_EQ(tzo, 7200)

   tza = DateTime::getLocalTZAbr(0);
   LT_CHECK_EQ(strcmp(tza, "SAST"), 0)

   DateTime dt = DateTime::now();

   LT_CHECK_EQ(dt.tzOffset(), tzo)
   LT_CHECK_EQ(dt.tzHour(),   tzh)
   LT_CHECK_EQ(dt.tzMinute(), tzm)
   LT_CHECK_EQ(strcmp(tza, dt.tzAbr()), 0)

   DateTime cd = dt;

   LT_CHECK_EQ(dt.tzOffset(), cd.tzOffset())
   LT_CHECK_EQ(dt.tzHour(),   cd.tzHour())
   LT_CHECK_EQ(dt.tzMinute(), cd.tzMinute())
   LT_CHECK_EQ(strcmp(dt.tzAbr(), cd.tzAbr()), 0)

   cd.tzConverTo(0);

   LT_CHECK_EQ(cd.tzOffset(), 0)
   LT_CHECK_EQ(cd.tzHour(),   0)
   LT_CHECK_EQ(cd.tzMinute(), 0)
   LT_CHECK_EQ(strcmp(cd.tzAbr(), "UTC"), 0)
   LT_CHECK_EQ(dt.time.hour() - cd.time.hour(), 2)

   cd.tzConverTo(4, 0);

   LT_CHECK_EQ(cd.tzOffset(), 4*3600)
   LT_CHECK_EQ(cd.tzHour(),   4)
   LT_CHECK_EQ(cd.tzMinute(), 0)
   LT_CHECK_EQ(strcmp(cd.tzAbr(), "SCT"), 0)
   LT_CHECK_EQ(dt.time.hour() - cd.time.hour(), -2)

   cd.tzConverTo(-9, 0);

//   printf("dt:%s\n", dt.toString(jot, true));
//   printf("cd:%s\n", cd.toString(jot, true));

   LT_CHECK_EQ(cd.tzOffset(), -9*3600)
   LT_CHECK_EQ(cd.tzHour(),   -9)
   LT_CHECK_EQ(cd.tzMinute(),  0)
   LT_CHECK_EQ(strcmp(cd.tzAbr(), "AKST"), 0)
   LT_CHECK_EQ(dt.time.hour() - cd.time.hour(), 11)

   dt.assign(2018, 12, 25, 14, 45, 59);
   dt.toString(jot, true);
   LT_CHECK_EQ(strcmp(jot, "20181225144559+0200"), 0)

   dt.format(jot, "%Z %z");
   LT_CHECK_EQ(strcmp(jot, "SAST +0200"), 0)

   dt.tzSetHour(-9);
   dt.tzSetMinute(30);

   dt.format(jot, "%Z %z");
   LT_CHECK_EQ(strcmp(jot, "MART -0930"), 0)

   dt.parse("20181225144559+0630", "%Y%m%d%H%M%S%z");

   LT_CHECK_EQ(dt.tzOffset(),  (6*3600)+1800)
   LT_CHECK_EQ(dt.tzHour(),    6)
   LT_CHECK_EQ(dt.tzMinute(), 30)
   LT_CHECK_EQ(strcmp(dt.tzAbr(), "CCT"), 0)

   cd.parse("20181225144559-0500", "%Y%m%d%H%M%S%z");

   LT_CHECK_EQ(cd.tzOffset(),  -5*3600)
   LT_CHECK_EQ(cd.tzHour(),    -5)
   LT_CHECK_EQ(cd.tzMinute(),   0)
   LT_CHECK_EQ(strcmp(cd.tzAbr(), "CIST"), 0)

   LT_CHECK_EQ(dt.tzHour() - cd.tzHour(), 11);


LT_END_TEST(test_TimeZone)

#undef testSuite
