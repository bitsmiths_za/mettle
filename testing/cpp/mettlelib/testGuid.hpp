/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "mettle/lib/guid.h"

#include <stdio.h>
#include <string.h>

using namespace Mettle::Lib;

#define testGuid testGuid


LT_BEGIN_SUITE(testGuid)

void set_up()
{
}

void tear_down()
{
}

LT_END_SUITE(testGuid)

LT_BEGIN_TEST(testGuid, test_Guids)

#define ASSIGN_GUID      "694b4355-f945-4bf4-997f-bbfb8c583178"

   Guid ga;
   Guid empty;
   Guid cp;
   Guid g1;
   Guid g2;
   Guid g3;
   char jotter[64];
   char jotter2[64];

   ga.parse(ASSIGN_GUID);
   ga.toString(jotter);

   LT_ASSERT_EQ(strcmp(jotter, ASSIGN_GUID),   0)

   cp = ga;

   LT_ASSERT_EQ(ga == cp,       true)
   LT_ASSERT_EQ(empty.isNull(), true)
   LT_ASSERT_EQ(ga != empty,    true)


   g1 = Guid::newGuid();
   g2 = Guid::newGuid(1);
   g3 = Guid::newGuid(1);

   LT_ASSERT_EQ(g1 != ga,  true)
   LT_ASSERT_EQ(g1 != g2,  true)
   LT_ASSERT_EQ(g2 != g3,  true)
   LT_ASSERT_EQ(g1 != g3,  true)


LT_END_TEST(test_Guids)


#undef testGuid
