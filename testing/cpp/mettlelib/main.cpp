/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "testDataVar.hpp"
#include "testDateTime.hpp"
#include "testString.hpp"
#include "testJson.hpp"
#include "testDataNode.hpp"
#include "testGuid.hpp"


LT_BEGIN_TEST_ENV()

    LT_CREATE_RUNNER(testString, runString);
    LT_RUNNER(runString)
      (test_Assignments)
      (test_Operators)
      (test_Split)
      (test_ToTypes)
      (test_FromTypes)
      ();

   LT_CREATE_RUNNER(testDataVar, runDataVar);
   LT_RUNNER(runDataVar)
      (test_detectTypes)
      ();

   LT_CREATE_RUNNER(testDateTime, runDateTime);
   LT_RUNNER(runDateTime)
      (test_Parse)
      (test_Format)
      (test_TimeZone);

   LT_CREATE_RUNNER(testJson, runJson);
   LT_RUNNER(runJson)
      (test_Reading)
      ();

   LT_CREATE_RUNNER(testDataNode, runDataNode);
   LT_RUNNER(runDataNode)
      (test_toBestTypes)
      ();

   LT_CREATE_RUNNER(testGuid, runGuid);
   LT_RUNNER(runGuid)
      (test_Guids)
      ();

LT_END_TEST_ENV()
