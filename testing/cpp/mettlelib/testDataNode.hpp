/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "mettle/lib/datanode.h"
#include "mettle/lib/datavar.h"
#include "mettle/lib/string.h"

using namespace Mettle::Lib;

#define testSuite testDateNode

LT_BEGIN_SUITE(testSuite)

void set_up()
{
}

void tear_down()
{
}

LT_END_SUITE(testSuite)

LT_BEGIN_TEST(testSuite, test_toBestTypes)

   DataNode   root;
   DataNode  *child;
   DataVar   *var;
   String     jot;

   for (int idx = 1; idx <= 5; idx++)
   {
      jot.format("child_%d", idx);

      child = root.newChild(jot.c_str());

      child->write("name",    "Nicolas");
      child->write("surname", "Milicevic");
      child->write("dob",     "1980-07-11");
      child->write("age",     "36");
      child->write("cash",    "123.456");
      child->write("male",    "true");
   }

   root.toBestTypes();

   for (int idx = 1; idx <= 5; idx++)
   {
      jot.format("child_%d", idx);

      child = root.get(jot.c_str());

      LT_ASSERT_NEQ(child, 0)

      var = child->get("name")->value();
      LT_CHECK_EQ(var->valueType(), DataVar::tl_string)

      var = child->get("surname")->value();
      LT_CHECK_EQ(var->valueType(), DataVar::tl_string)

      var = child->get("dob")->value();
      LT_CHECK_EQ(var->valueType(), DataVar::tl_date)

      var = child->get("age")->value();
      LT_CHECK_EQ(var->valueType(), DataVar::tl_int32)

      var = child->get("cash")->value();
      LT_CHECK_EQ(var->valueType(), DataVar::tl_double)

      var = child->get("male")->value();
      LT_CHECK_EQ(var->valueType(), DataVar::tl_bool)
   }


LT_END_TEST(test_toBestTypes)


#undef testSuite
