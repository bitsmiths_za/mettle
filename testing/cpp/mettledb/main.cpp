/******************************************************************************/
/*                          This file is part of:                             */
/*                                  METTLE                                    */
/*                          https://bitsmiths.co.za                           */
/******************************************************************************/
/* Copyright (C) 2015 - 2021 Bitsmiths (Pty) Ltd.  All rights reserved.       */
/*  * https://bitbucket.org/bitsmiths_za/mettle.git                           */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom the      */
/* Software is furnished to do so, subject to the following conditions:       */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included in */
/* all copies or substantial portions of the Software.                        */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#include "littletest.hpp"

#include "testMettleDB.hpp"

#define SOURCE "testMettleDB"

static void initializeDatabase()
{
   try
   {
      std::cout << "Initialize Database - Start()" << std::endl;
   #if defined(METTLEDB_SQLITE)

      Mettle::Lib::FileManager::removeFile(DB_FILENAME, true);

   // #else
   //    throw Mettle::Lib::xMettle(__FILE__, __LINE__, SOURCE, "Target pre-compiler database not specified.");
   #endif

      std::cout << "Initialize Database - Done()" << std::endl;
   }
   catch (std::exception& e)
   {
      std::cout << "Exception during initializeDatabase()" << std::endl;
      std::cout << e.what() << std::endl;
   }
   catch (...)
   {
      std::cout << "Unknown exception during initializeDatabase()" << std::endl;
   }
}


LT_BEGIN_TEST_ENV()

   initializeDatabase();

   LT_CREATE_RUNNER(testSuite, runMettleDB);
   LT_RUNNER(runMettleDB)
      (test_001_CheckFiles)
      (test_002_Connect)
      (test_003_CreateTables)
      (test_004_TestAllTypes)
      (test_014_InsertRecords)
      (test_015_SelectRecords)
      (test_016_UpdateRecords)
      (test_017_TestReset)
      ();


LT_END_TEST_ENV()
