import sqlalchemy
import sqlalchemy.orm

from contextlib import contextmanager

_db_engine = None
Session = None


def initialize(db_url):
    global _db_engine
    global _db_session
    global Session

    options = {
        'pool_recycle' : 3600,
        'pool_size'    : 10,
        'pool_timeout' : 30,
        'max_overflow' : 30,
        'echo'         : False,
        'execution_options': {
            'autocommit': False,
            'autoflush': False,
        }
    }

    _db_engine = sqlalchemy.create_engine(db_url, **options)
    Session = sqlalchemy.orm.sessionmaker(bind=_db_engine)


@contextmanager
def session_scope():
    """
    Provide a transactional scope around a series of operations.
    """
    session = Session()
    try:
        yield session
        session.commit()
    except Exception:
        session.rollback()
        raise
    finally:
        session.close()
