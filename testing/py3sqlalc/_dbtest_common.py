# **************************************************************************** #
#                           This file is part of:                              #
#                                   METTLE                                     #
#                           https://bitsmiths.co.za                            #
# **************************************************************************** #
#  Copyright (C) 2015 - 2018 Bitsmiths (Pty) Ltd.  All rights reserved.        #
#   * https://bitbucket.org/bitsmiths_za/mettle.git                            #
#                                                                              #
#  Permission is hereby granted, free of charge, to any person obtaining a     #
#  copy of this software and associated documentation files (the "Software"),  #
#  to deal in the Software without restriction, including without limitation   #
#  the rights to use, copy, modify, merge, publish, distribute, sublicense,    #
#  and/or sell copies of the Software, and to permit persons to whom the       #
#  Software is furnished to do so, subject to the following conditions:        #
#                                                                              #
#  The above copyright notice and this permission notice shall be included in  #
#  all copies or substantial portions of the Software.                         #
#                                                                              #
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  #
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    #
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     #
#  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  #
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     #
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
#  DEALINGS IN THE SOFTWARE.                                                   #
# **************************************************************************** #

import os
import os.path


def initTestingFolder(folder, cleanFiles = []):
    if not os.path.exists(folder):
        os.makedirs(folder)

    for x in cleanFiles:
        xfile = os.path.join(folder, x)

        if os.path.exists(xfile):
            os.remove(xfile)


def allTestTables():
    return ['AllTypes', 'Child', 'Parent', 'ParentType']  # , 'Toy']


def checkGeneratedFiles(unitTest, tables, pth):
    for x in tables:
        for y in ['.drop', '.table', '.constraint', '.index']:
            fp = os.path.join(pth, '%s%s' % (x, y))

            if not os.path.exists(fp):
                unitTest.fail('Expected file [%s] not found. Please run the '
                              '[generator/test_db.py] successfully first!' % fp)

    if not os.path.exists(os.path.join('mettledb', 'tables')):
        unitTest.fail('Expected path [mettledb/tables] not found. Please run the '
                      '[generator/test_db.py] successfully first!')

    if not os.path.exists(os.path.join('mettledb', 'dao', 'sqlite')):
        unitTest.fail('Expected path [mettledb/dao/sqlite] not found. Please run the '
                      ' [generator/test_db.py] successfully first!')
